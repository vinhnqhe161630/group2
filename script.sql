USE [WebBoardGame]
GO
/****** Object:  Table [dbo].[Accounts]    Script Date: 08/11/2023 11:23:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Accounts](
	[acid] [int] IDENTITY(1,1) NOT NULL,
	[email] [nvarchar](250) NULL,
	[emailConfirm] [bit] NULL,
	[password] [nvarchar](50) NULL,
	[username] [nvarchar](50) NULL,
	[role] [int] NULL,
	[img] [nvarchar](150) NULL,
	[created_at] [date] NULL,
	[status] [int] NULL,
 CONSTRAINT [PK_Accounts] PRIMARY KEY CLUSTERED 
(
	[acid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Banner]    Script Date: 08/11/2023 11:23:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Banner](
	[banid] [int] IDENTITY(1,1) NOT NULL,
	[img] [nvarchar](250) NULL,
	[status] [int] NULL,
	[create_at] [date] NOT NULL,
 CONSTRAINT [PK_Banner] PRIMARY KEY CLUSTERED 
(
	[banid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Categories]    Script Date: 08/11/2023 11:23:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Categories](
	[caid] [int] IDENTITY(1,1) NOT NULL,
	[caname] [nvarchar](150) NULL,
	[status] [bit] NULL,
 CONSTRAINT [PK_Categories] PRIMARY KEY CLUSTERED 
(
	[caid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CategoriesNew]    Script Date: 08/11/2023 11:23:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CategoriesNew](
	[canewId] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](150) NULL,
	[status] [bit] NULL,
 CONSTRAINT [PK_CategoriesNew] PRIMARY KEY CLUSTERED 
(
	[canewId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CodeSale]    Script Date: 08/11/2023 11:23:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CodeSale](
	[csid] [int] IDENTITY(1,1) NOT NULL,
	[codeSale] [nvarchar](50) NULL,
	[discount] [int] NULL,
	[csStatus] [int] NULL,
	[dateStart] [nchar](10) NULL,
	[dateEnd] [nchar](10) NULL,
	[limitedQuantity] [int] NULL,
	[discountConditions] [nchar](10) NULL,
	[titile] [nchar](100) NULL,
	[quantityUsed] [int] NULL,
 CONSTRAINT [PK_CodeSale] PRIMARY KEY CLUSTERED 
(
	[csid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Comment]    Script Date: 08/11/2023 11:23:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Comment](
	[CommentId] [int] IDENTITY(1,1) NOT NULL,
	[CommentContent] [nvarchar](4000) NULL,
	[Create_at] [nvarchar](50) NULL,
	[Status] [int] NULL,
	[ProductId] [int] NULL,
	[AccountId] [int] NULL,
	[Rate] [int] NULL,
	[CustomerName] [nvarchar](50) NULL,
	[CustomerEmail] [nvarchar](50) NULL,
 CONSTRAINT [PK_Comment] PRIMARY KEY CLUSTERED 
(
	[CommentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Comments]    Script Date: 08/11/2023 11:23:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Comments](
	[comid] [int] IDENTITY(1,1) NOT NULL,
	[content] [nvarchar](500) NOT NULL,
	[create_at] [date] NOT NULL,
	[status] [int] NULL,
	[pid] [int] NOT NULL,
	[acid] [int] NOT NULL,
 CONSTRAINT [PK_Comments] PRIMARY KEY CLUSTERED 
(
	[comid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Contact]    Script Date: 08/11/2023 11:23:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contact](
	[contactid] [int] NOT NULL,
	[email] [nvarchar](50) NULL,
 CONSTRAINT [PK_Contact] PRIMARY KEY CLUSTERED 
(
	[contactid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Feedback]    Script Date: 08/11/2023 11:23:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Feedback](
	[FeedbackID] [int] IDENTITY(1,1) NOT NULL,
	[FeedbackBySellerName] [nvarchar](400) NULL,
	[Create_atFeedback] [nvarchar](50) NULL,
	[FeedbackContent] [nvarchar](4000) NULL,
	[AcountId] [int] NULL,
	[CommentId] [int] NULL,
	[StatusFeedback] [int] NULL,
 CONSTRAINT [PK_Table_1] PRIMARY KEY CLUSTERED 
(
	[FeedbackID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[News]    Script Date: 08/11/2023 11:23:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[News](
	[nid] [int] IDENTITY(1,1) NOT NULL,
	[title] [nvarchar](250) NULL,
	[img] [nvarchar](250) NULL,
	[createat] [date] NULL,
	[acid] [int] NULL,
	[canewId] [int] NULL,
 CONSTRAINT [PK_News] PRIMARY KEY CLUSTERED 
(
	[nid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NewsDetails]    Script Date: 08/11/2023 11:23:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NewsDetails](
	[ndid] [int] IDENTITY(1,1) NOT NULL,
	[description] [nvarchar](max) NULL,
	[img] [nvarchar](150) NULL,
	[nid] [int] NULL,
	[view] [int] NULL,
 CONSTRAINT [PK_NewsDetails] PRIMARY KEY CLUSTERED 
(
	[ndid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[orderDetails]    Script Date: 08/11/2023 11:23:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[orderDetails](
	[odid] [int] IDENTITY(1,1) NOT NULL,
	[pid] [int] NULL,
	[oid] [int] NULL,
	[price] [int] NULL,
	[quantity] [int] NULL,
 CONSTRAINT [PK_orderDetails] PRIMARY KEY CLUSTERED 
(
	[odid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[orders]    Script Date: 08/11/2023 11:23:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[orders](
	[oid] [int] IDENTITY(1,1) NOT NULL,
	[acid] [int] NULL,
	[ordered_at] [nchar](10) NULL,
	[TotalAmount] [int] NULL,
	[created_by] [nchar](10) NULL,
	[address] [nchar](150) NULL,
	[phonenumber] [nchar](10) NULL,
	[status] [int] NULL,
	[note] [nchar](250) NULL,
	[receiver] [nvarchar](50) NULL,
	[discount] [int] NULL,
	[enddate] [nchar](10) NULL,
 CONSTRAINT [PK_orders] PRIMARY KEY CLUSTERED 
(
	[oid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductCategory]    Script Date: 08/11/2023 11:23:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductCategory](
	[capid] [int] IDENTITY(1,1) NOT NULL,
	[caid] [int] NULL,
	[pid] [int] NULL,
 CONSTRAINT [PK_ProductCategory] PRIMARY KEY CLUSTERED 
(
	[capid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductDetail]    Script Date: 08/11/2023 11:23:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductDetail](
	[pdid] [int] IDENTITY(1,1) NOT NULL,
	[numplayer] [int] NOT NULL,
	[requiredAge] [int] NOT NULL,
	[description] [nvarchar](max) NULL,
	[rules] [nvarchar](max) NULL,
	[timeplay] [nvarchar](max) NULL,
	[pid] [int] NOT NULL,
 CONSTRAINT [PK_ProductDetail] PRIMARY KEY CLUSTERED 
(
	[pdid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Products]    Script Date: 08/11/2023 11:23:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Products](
	[pid] [int] IDENTITY(1,1) NOT NULL,
	[pname] [nvarchar](250) NULL,
	[rate] [float] NULL,
	[price] [int] NULL,
	[img] [nvarchar](max) NULL,
	[priceSale] [int] NULL,
	[quantity] [int] NULL,
	[pubid] [int] NULL,
	[saled] [int] NULL,
	[isDiscount] [bit] NULL,
	[isSoldout] [bit] NULL,
	[created_at] [nchar](10) NULL,
	[status] [int] NULL,
 CONSTRAINT [PK_Products] PRIMARY KEY CLUSTERED 
(
	[pid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Publishers]    Script Date: 08/11/2023 11:23:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Publishers](
	[pubid] [int] IDENTITY(1,1) NOT NULL,
	[pubname] [nvarchar](150) NULL,
	[country] [nvarchar](150) NULL,
	[material] [nvarchar](150) NULL,
	[size] [nvarchar](50) NULL,
	[weight] [nvarchar](50) NULL,
	[pid] [int] NULL,
 CONSTRAINT [PK_Publishers] PRIMARY KEY CLUSTERED 
(
	[pubid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Role]    Script Date: 08/11/2023 11:23:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[role] [int] IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](50) NULL,
 CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[role] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Setting]    Script Date: 08/11/2023 11:23:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Setting](
	[setting_id] [int] IDENTITY(1,1) NOT NULL,
	[type] [int] NULL,
	[order] [int] NULL,
	[value] [nvarchar](50) NULL,
	[description] [nvarchar](50) NULL,
	[status] [bit] NULL,
 CONSTRAINT [PK_Setting] PRIMARY KEY CLUSTERED 
(
	[setting_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Setting_Type]    Script Date: 08/11/2023 11:23:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Setting_Type](
	[setting_type_id] [int] NOT NULL,
	[setting_type_name] [nvarchar](50) NULL,
 CONSTRAINT [PK_Setting_Type] PRIMARY KEY CLUSTERED 
(
	[setting_type_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ShipmentDetails]    Script Date: 08/11/2023 11:23:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ShipmentDetails](
	[aaid] [int] IDENTITY(1,1) NOT NULL,
	[address] [nvarchar](500) NULL,
	[phonenumber] [nvarchar](50) NULL,
	[status] [int] NULL,
	[acid] [int] NOT NULL,
	[receiver] [nvarchar](50) NULL,
 CONSTRAINT [PK_ShipmentDetails] PRIMARY KEY CLUSTERED 
(
	[aaid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Accounts] ON 

INSERT [dbo].[Accounts] ([acid], [email], [emailConfirm], [password], [username], [role], [img], [created_at], [status]) VALUES (1, N'adhaianthanh1234@gmail.com', 1, N'0xEF797C8118F02DFB649607DD5D3F8C7623048C9C063D532C', N'Nguyễn Thành Đạt', 1, N'user1.jpg', CAST(N'2023-09-01' AS Date), 1)
INSERT [dbo].[Accounts] ([acid], [email], [emailConfirm], [password], [username], [role], [img], [created_at], [status]) VALUES (2, N'anh1234@gmail.com', 1, N'0xEF797C8118F02DFB649607DD5D3F8C7623048C9C063D532C', N'Nguyễn Thành Đạtt', 2, N'user2.jpg', CAST(N'2023-09-02' AS Date), 0)
INSERT [dbo].[Accounts] ([acid], [email], [emailConfirm], [password], [username], [role], [img], [created_at], [status]) VALUES (3, N'quochoi7865@gmail.com', 1, N'0xEF797C8118F02DFB649607DD5D3F8C7623048C9C063D532C', N'Vịnh Xuân Quyền', 4, N'user3.jpg', CAST(N'2023-09-03' AS Date), 1)
INSERT [dbo].[Accounts] ([acid], [email], [emailConfirm], [password], [username], [role], [img], [created_at], [status]) VALUES (4, N'sale23@gmail.com', 1, N'0xEF797C8118F02DFB649607DD5D3F8C7623048C9C063D532C', N'Trần Hải Đăng', 2, N'user4.jpg', CAST(N'2023-09-04' AS Date), 1)
INSERT [dbo].[Accounts] ([acid], [email], [emailConfirm], [password], [username], [role], [img], [created_at], [status]) VALUES (5, N'vanthanh@gmail.com', 1, N'0xEF797C8118F02DFB649607DD5D3F8C7623048C9C063D532C', N'Nguyễn Thành Tiến', 1, N'user5.jpg', CAST(N'2023-09-05' AS Date), 0)
INSERT [dbo].[Accounts] ([acid], [email], [emailConfirm], [password], [username], [role], [img], [created_at], [status]) VALUES (6, N'doichoi999@gmail.com', 1, N'0xEF797C8118F02DFB649607DD5D3F8C7623048C9C063D532C', N'Trình Ngọc Thành', 2, N'user6.jpg', CAST(N'2023-09-06' AS Date), 1)
INSERT [dbo].[Accounts] ([acid], [email], [emailConfirm], [password], [username], [role], [img], [created_at], [status]) VALUES (7, N'huanhoahong987@gmail.com', 1, N'0xEF797C8118F02DFB649607DD5D3F8C7623048C9C063D532C', N'Nguyễn Quang Công Minh', 4, N'user7.jpg', CAST(N'2023-09-07' AS Date), 1)
INSERT [dbo].[Accounts] ([acid], [email], [emailConfirm], [password], [username], [role], [img], [created_at], [status]) VALUES (8, N'9999@gmail.com', 1, N'0xEF797C8118F02DFB649607DD5D3F8C7623048C9C063D532C', N'Vũ Mạnh Tuấn', 3, N'user8.jpg', CAST(N'2023-09-08' AS Date), 0)
INSERT [dbo].[Accounts] ([acid], [email], [emailConfirm], [password], [username], [role], [img], [created_at], [status]) VALUES (9, N'sa9999@gmail.com', 1, N'0xEF797C8118F02DFB649607DD5D3F8C7623048C9C063D532C', N'Vịnh Tuấn Khải', 1, N'user9.jpg', CAST(N'2023-09-09' AS Date), 1)
INSERT [dbo].[Accounts] ([acid], [email], [emailConfirm], [password], [username], [role], [img], [created_at], [status]) VALUES (10, N'emin123@gmail.com', 1, N'0xEF797C8118F02DFB649607DD5D3F8C7623048C9C063D532C', N'Nguyễn Quang Vinh', 1, N'user10.jpg', CAST(N'2023-09-10' AS Date), 1)
INSERT [dbo].[Accounts] ([acid], [email], [emailConfirm], [password], [username], [role], [img], [created_at], [status]) VALUES (11, N'kimanhh162010@gmail.com', NULL, N'0xEF797C8118F02DFB649607DD5D3F8C7623048C9C063D532C', N'Vinh Nam Định', 4, NULL, CAST(N'2023-09-24' AS Date), 1)
INSERT [dbo].[Accounts] ([acid], [email], [emailConfirm], [password], [username], [role], [img], [created_at], [status]) VALUES (12, N'user112@gmail.com', NULL, N'0xEF797C8118F02DFB649607DD5D3F8C7623048C9C063D532C', N'Minh Hà Nộ', 4, NULL, CAST(N'2023-09-24' AS Date), 1)
INSERT [dbo].[Accounts] ([acid], [email], [emailConfirm], [password], [username], [role], [img], [created_at], [status]) VALUES (13, N'kimanhh16@gmail.com', NULL, N'0xEF797C8118F02DFB649607DD5D3F8C7623048C9C063D532C', N'Thành Sơn La', 4, NULL, CAST(N'2023-09-25' AS Date), 1)
INSERT [dbo].[Accounts] ([acid], [email], [emailConfirm], [password], [username], [role], [img], [created_at], [status]) VALUES (1011, N'example@email.com', 1, N'0xEF797C8118F02DFB649607DD5D3F8C7623048C9C063D532C', N'Mai Quốc Khánh', 3, N'profile.jpg', CAST(N'2023-10-12' AS Date), 1)
INSERT [dbo].[Accounts] ([acid], [email], [emailConfirm], [password], [username], [role], [img], [created_at], [status]) VALUES (1012, N'Vinhminhdat@gmail.com', 1, N'0xEF797C8118F02DFB649607DD5D3F8C7623048C9C063D532C', N'VŨ MẠNH ĐẠT', 4, NULL, CAST(N'2023-10-16' AS Date), 1)
INSERT [dbo].[Accounts] ([acid], [email], [emailConfirm], [password], [username], [role], [img], [created_at], [status]) VALUES (1013, N'admin', NULL, N'0xBA7816BF8F01CFEA414140DE5DAE2223B00361A396177A9C', N'Trần Hải Đăn', 1, NULL, CAST(N'2023-10-16' AS Date), 1)
INSERT [dbo].[Accounts] ([acid], [email], [emailConfirm], [password], [username], [role], [img], [created_at], [status]) VALUES (1014, N'vumanhdat123@fpt.edu.vn', NULL, N'0xEF797C8118F02DFB649607DD5D3F8C7623048C9C063D532C', N'Vương Tuấn Khải', 4, NULL, CAST(N'2023-10-18' AS Date), 1)
INSERT [dbo].[Accounts] ([acid], [email], [emailConfirm], [password], [username], [role], [img], [created_at], [status]) VALUES (1015, N'Vanthanh1234@Gmail.Com', NULL, N'0xEF797C8118F02DFB649607DD5D3F8C7623048C9C063D532C', N'username', 2, NULL, CAST(N'2023-10-18' AS Date), 1)
INSERT [dbo].[Accounts] ([acid], [email], [emailConfirm], [password], [username], [role], [img], [created_at], [status]) VALUES (1016, N'thihanh12@gmail.com', NULL, N'0xEF797C8118F02DFB649607DD5D3F8C7623048C9C063D532C', N'Tuấn đạt', 2, NULL, CAST(N'2023-10-18' AS Date), 1)
INSERT [dbo].[Accounts] ([acid], [email], [emailConfirm], [password], [username], [role], [img], [created_at], [status]) VALUES (1017, N'thanhnguyen@gmail.com', NULL, N'0xEF797C8118F02DFB649607DD5D3F8C7623048C9C063D532C', N'Đạt vũ mạnh', 4, NULL, CAST(N'2023-10-18' AS Date), 1)
INSERT [dbo].[Accounts] ([acid], [email], [emailConfirm], [password], [username], [role], [img], [created_at], [status]) VALUES (1018, N'Tahnhtanh89@gmail.com', NULL, N'0xEF797C8118F02DFB649607DD5D3F8C7623048C9C063D532C', N'thnh678', 3, NULL, CAST(N'2023-10-18' AS Date), 1)
INSERT [dbo].[Accounts] ([acid], [email], [emailConfirm], [password], [username], [role], [img], [created_at], [status]) VALUES (1019, N'vumanhdat@gmail.com', NULL, N'0xEF797C8118F02DFB649607DD5D3F8C7623048C9C063D532C', N'Đạt 9999', 3, NULL, CAST(N'2023-10-19' AS Date), 1)
INSERT [dbo].[Accounts] ([acid], [email], [emailConfirm], [password], [username], [role], [img], [created_at], [status]) VALUES (1020, N'marketing', NULL, N'0xEF797C8118F02DFB649607DD5D3F8C7623048C9C063D532C', N'username2', 3, NULL, CAST(N'2023-10-19' AS Date), 1)
INSERT [dbo].[Accounts] ([acid], [email], [emailConfirm], [password], [username], [role], [img], [created_at], [status]) VALUES (2019, N'sale@gmail.Com', NULL, N'0xEF797C8118F02DFB649607DD5D3F8C7623048C9C063D532C', N'username1', 2, NULL, CAST(N'2023-10-26' AS Date), 1)
INSERT [dbo].[Accounts] ([acid], [email], [emailConfirm], [password], [username], [role], [img], [created_at], [status]) VALUES (3019, N'sale1@gmail.com', NULL, N'0xA665A45920422F9D417E4867EFDC4FB8A04A1F3FFF1FA07E', N'Vinh cute', 2, NULL, CAST(N'2023-11-02' AS Date), NULL)
INSERT [dbo].[Accounts] ([acid], [email], [emailConfirm], [password], [username], [role], [img], [created_at], [status]) VALUES (3020, N'sale2@gmail.Com', NULL, N'0xA665A45920422F9D417E4867EFDC4FB8A04A1F3FFF1FA07E', NULL, 4, NULL, CAST(N'2023-11-02' AS Date), NULL)
INSERT [dbo].[Accounts] ([acid], [email], [emailConfirm], [password], [username], [role], [img], [created_at], [status]) VALUES (3021, N'minhkhautrang@gmail.com', NULL, N'0xEF797C8118F02DFB649607DD5D3F8C7623048C9C063D532C', N'Minh Khẩu Trang', 2, NULL, CAST(N'2023-11-02' AS Date), 1)
INSERT [dbo].[Accounts] ([acid], [email], [emailConfirm], [password], [username], [role], [img], [created_at], [status]) VALUES (3022, N'sale1111@gmail.com', NULL, N'0xA665A45920422F9D417E4867EFDC4FB8A04A1F3FFF1FA07E', NULL, 4, NULL, CAST(N'2023-11-02' AS Date), NULL)
SET IDENTITY_INSERT [dbo].[Accounts] OFF
GO
SET IDENTITY_INSERT [dbo].[Banner] ON 

INSERT [dbo].[Banner] ([banid], [img], [status], [create_at]) VALUES (1, N'1.jpg', 1, CAST(N'2023-10-16' AS Date))
INSERT [dbo].[Banner] ([banid], [img], [status], [create_at]) VALUES (2, N'pic1.jpg', 1, CAST(N'2023-10-16' AS Date))
INSERT [dbo].[Banner] ([banid], [img], [status], [create_at]) VALUES (3, N'pic2.png', 1, CAST(N'2023-10-16' AS Date))
INSERT [dbo].[Banner] ([banid], [img], [status], [create_at]) VALUES (4, N'1.jpg', 1, CAST(N'2023-10-16' AS Date))
INSERT [dbo].[Banner] ([banid], [img], [status], [create_at]) VALUES (5, N'1.jpg', 1, CAST(N'2023-10-16' AS Date))
INSERT [dbo].[Banner] ([banid], [img], [status], [create_at]) VALUES (6, N'1.jpg', 1, CAST(N'2023-10-16' AS Date))
INSERT [dbo].[Banner] ([banid], [img], [status], [create_at]) VALUES (7, N'1.jpg', 1, CAST(N'2023-10-16' AS Date))
INSERT [dbo].[Banner] ([banid], [img], [status], [create_at]) VALUES (8, N'1.jpg', 1, CAST(N'2023-10-16' AS Date))
INSERT [dbo].[Banner] ([banid], [img], [status], [create_at]) VALUES (9, N'1.jpg', 1, CAST(N'2023-10-16' AS Date))
INSERT [dbo].[Banner] ([banid], [img], [status], [create_at]) VALUES (10, N'1.jpg', 1, CAST(N'2023-10-16' AS Date))
SET IDENTITY_INSERT [dbo].[Banner] OFF
GO
SET IDENTITY_INSERT [dbo].[Categories] ON 

INSERT [dbo].[Categories] ([caid], [caname], [status]) VALUES (1, N'Board Game trẻ em (dưới 18+)', 0)
INSERT [dbo].[Categories] ([caid], [caname], [status]) VALUES (2, N'Board Game chiến thuật', 1)
INSERT [dbo].[Categories] ([caid], [caname], [status]) VALUES (3, N'Board Game giải trí/nhóm', 1)
INSERT [dbo].[Categories] ([caid], [caname], [status]) VALUES (4, N'Board Game gia đình', 1)
INSERT [dbo].[Categories] ([caid], [caname], [status]) VALUES (5, N'Board Game US', 1)
INSERT [dbo].[Categories] ([caid], [caname], [status]) VALUES (6, N'Board Game bán chạy', 0)
INSERT [dbo].[Categories] ([caid], [caname], [status]) VALUES (7, N'Phụ Kiện Board Game', 1)
INSERT [dbo].[Categories] ([caid], [caname], [status]) VALUES (1001, N'Board Game trẻ em (trên 18+)', 1)
SET IDENTITY_INSERT [dbo].[Categories] OFF
GO
SET IDENTITY_INSERT [dbo].[CategoriesNew] ON 

INSERT [dbo].[CategoriesNew] ([canewId], [name], [status]) VALUES (1, N'Tieu Thuyet', 1)
INSERT [dbo].[CategoriesNew] ([canewId], [name], [status]) VALUES (2, N'Kham pha', 1)
INSERT [dbo].[CategoriesNew] ([canewId], [name], [status]) VALUES (3, N'BGVN Cafe', 1)
INSERT [dbo].[CategoriesNew] ([canewId], [name], [status]) VALUES (4, N'Sale', 1)
INSERT [dbo].[CategoriesNew] ([canewId], [name], [status]) VALUES (5, N'Danh gia', 1)
INSERT [dbo].[CategoriesNew] ([canewId], [name], [status]) VALUES (6, N'Offline', 1)
INSERT [dbo].[CategoriesNew] ([canewId], [name], [status]) VALUES (7, N'Review', NULL)
SET IDENTITY_INSERT [dbo].[CategoriesNew] OFF
GO
SET IDENTITY_INSERT [dbo].[CodeSale] ON 

INSERT [dbo].[CodeSale] ([csid], [codeSale], [discount], [csStatus], [dateStart], [dateEnd], [limitedQuantity], [discountConditions], [titile], [quantityUsed]) VALUES (1, N'VL123', 20000, 1, N'2023-11-02', N'2023-11-01', 2000, N'100000    ', N'Mã Giảm Giá 20K                                                                                     ', 0)
INSERT [dbo].[CodeSale] ([csid], [codeSale], [discount], [csStatus], [dateStart], [dateEnd], [limitedQuantity], [discountConditions], [titile], [quantityUsed]) VALUES (2, N'XYZ20A02', 20000, 1, N'2023-10-10', N'2023-11-10', 2000, N'150000    ', N'Mã giảm giá 20K                                                                                     ', 320)
INSERT [dbo].[CodeSale] ([csid], [codeSale], [discount], [csStatus], [dateStart], [dateEnd], [limitedQuantity], [discountConditions], [titile], [quantityUsed]) VALUES (3, N'LMN15B03', 15000, 1, N'2023-10-10', N'2023-11-10', 1500, N'12000     ', N'Mã giảm giá 15K                                                                                     ', 1000)
INSERT [dbo].[CodeSale] ([csid], [codeSale], [discount], [csStatus], [dateStart], [dateEnd], [limitedQuantity], [discountConditions], [titile], [quantityUsed]) VALUES (4, N'PQR25C04', 25000, 1, N'2023-10-10', N'2023-11-10', 2500, N'200000    ', N'Mã giảm giá 25K                                                                                     ', 100)
INSERT [dbo].[CodeSale] ([csid], [codeSale], [discount], [csStatus], [dateStart], [dateEnd], [limitedQuantity], [discountConditions], [titile], [quantityUsed]) VALUES (5, N'JKL30D05', 30000, 1, N'2023-10-10', N'2023-11-10', 3000, N'250000    ', N'Mã giảm giá 30K                                                                                     ', 231)
INSERT [dbo].[CodeSale] ([csid], [codeSale], [discount], [csStatus], [dateStart], [dateEnd], [limitedQuantity], [discountConditions], [titile], [quantityUsed]) VALUES (6, N'MNO40E06', 40000, 1, N'2023-10-10', N'2023-11-10', 4000, N'500000    ', N'Mã giảm giá 40K                                                                                     ', 321)
INSERT [dbo].[CodeSale] ([csid], [codeSale], [discount], [csStatus], [dateStart], [dateEnd], [limitedQuantity], [discountConditions], [titile], [quantityUsed]) VALUES (2009, N'VLXX10Z2', 20000, 1, N'2023-11-02', N'2023-11-01', 2000, N'100000    ', N'Mã Giảm Giá 20K                                                                                     ', 0)
SET IDENTITY_INSERT [dbo].[CodeSale] OFF
GO
SET IDENTITY_INSERT [dbo].[Comment] ON 

INSERT [dbo].[Comment] ([CommentId], [CommentContent], [Create_at], [Status], [ProductId], [AccountId], [Rate], [CustomerName], [CustomerEmail]) VALUES (4, N'Tuyet', N'16:12 14/10/2023', 1, 1, 1, 3, N'Minh', N'minh@gmail.com')
INSERT [dbo].[Comment] ([CommentId], [CommentContent], [Create_at], [Status], [ProductId], [AccountId], [Rate], [CustomerName], [CustomerEmail]) VALUES (5, N'Tuyet', N'00:15 15/10/2023', 1, 1, 1, 2, N'Minh', N'minh@gmail.com')
INSERT [dbo].[Comment] ([CommentId], [CommentContent], [Create_at], [Status], [ProductId], [AccountId], [Rate], [CustomerName], [CustomerEmail]) VALUES (15, N'', N'05:27 15/10/2023', 1, 1, 0, 1, N'Vương Công Minh', N'hihihi652652@gmail.com')
INSERT [dbo].[Comment] ([CommentId], [CommentContent], [Create_at], [Status], [ProductId], [AccountId], [Rate], [CustomerName], [CustomerEmail]) VALUES (16, N'cơ sở thanh hóa còn ip11 64gb màu trắng không ạ', N'05:33 15/10/2023', 1, 1, 0, 1, N'Vương Công Minh', N'hihihi652652@gmail.com')
INSERT [dbo].[Comment] ([CommentId], [CommentContent], [Create_at], [Status], [ProductId], [AccountId], [Rate], [CustomerName], [CustomerEmail]) VALUES (17, N'f', N'13:19 15/10/2023', 1, 1, 0, 1, N'Vương Công Minh', N'hihihi652652@gmail.com')
INSERT [dbo].[Comment] ([CommentId], [CommentContent], [Create_at], [Status], [ProductId], [AccountId], [Rate], [CustomerName], [CustomerEmail]) VALUES (18, N'cho em hỏi hình thức trả góp sản phẩm này bên mình như thế nào ạ?', N'13:33 15/10/2023', 1, 1, 0, 5, N'Dũng', N'dung@gmail.com')
INSERT [dbo].[Comment] ([CommentId], [CommentContent], [Create_at], [Status], [ProductId], [AccountId], [Rate], [CustomerName], [CustomerEmail]) VALUES (19, N'fuck', N'13:52 15/10/2023', 1, 1, 0, 1, N'Vương Công Minh', N'hihihi652652@gmail.com')
INSERT [dbo].[Comment] ([CommentId], [CommentContent], [Create_at], [Status], [ProductId], [AccountId], [Rate], [CustomerName], [CustomerEmail]) VALUES (20, N'Tuyệt vời', N'13:58 15/10/2023', 1, 1, 0, 5, N'Vương Công Minh', N'hihihi652652@gmail.com')
INSERT [dbo].[Comment] ([CommentId], [CommentContent], [Create_at], [Status], [ProductId], [AccountId], [Rate], [CustomerName], [CustomerEmail]) VALUES (27, N'Sản phẩm tốt hơn mong đợi', N'12:03 18/10/2023', 1, 1, 0, 5, N'Vương Công Minh', N'hihihi652652@gmail.com')
INSERT [dbo].[Comment] ([CommentId], [CommentContent], [Create_at], [Status], [ProductId], [AccountId], [Rate], [CustomerName], [CustomerEmail]) VALUES (28, N'Tôi cần tư vấn', N'12:08 18/10/2023', 0, 2, 0, 5, N'Vương Công Minh', N'hihihi652652@gmail.com')
INSERT [dbo].[Comment] ([CommentId], [CommentContent], [Create_at], [Status], [ProductId], [AccountId], [Rate], [CustomerName], [CustomerEmail]) VALUES (30, N'Good', N'21:20 18/10/2023', 1, 2, 11, 5, N'Vương Công Minh', N'hihihi652652@gmail.com')
INSERT [dbo].[Comment] ([CommentId], [CommentContent], [Create_at], [Status], [ProductId], [AccountId], [Rate], [CustomerName], [CustomerEmail]) VALUES (31, N'Boardgame xịn lắm', N'21:21 18/10/2023', 1, 4, 11, 5, N'Nguyễn Quang Vinh', N'vinh@gmail.com')
INSERT [dbo].[Comment] ([CommentId], [CommentContent], [Create_at], [Status], [ProductId], [AccountId], [Rate], [CustomerName], [CustomerEmail]) VALUES (32, N'Tuyệt', N'21:23 18/10/2023', 1, 3, 20, 5, N'Mai Hoa', N'hoa@gmail.com')
INSERT [dbo].[Comment] ([CommentId], [CommentContent], [Create_at], [Status], [ProductId], [AccountId], [Rate], [CustomerName], [CustomerEmail]) VALUES (37, N'Tuyệt', N'01:28 19/10/2023', 1, 1, 30, 5, N'Hào', N'hao@gmail.com')
INSERT [dbo].[Comment] ([CommentId], [CommentContent], [Create_at], [Status], [ProductId], [AccountId], [Rate], [CustomerName], [CustomerEmail]) VALUES (38, N'Tuyệt', N'01:29 19/10/2023', 1, 2, 30, 5, N'Hào', N'hao@gmail.com')
INSERT [dbo].[Comment] ([CommentId], [CommentContent], [Create_at], [Status], [ProductId], [AccountId], [Rate], [CustomerName], [CustomerEmail]) VALUES (39, N'Sản phẩm tốt', N'02:02 19/10/2023', 1, 2, 32, 5, N'Đạt', N'dat@gmail.com')
INSERT [dbo].[Comment] ([CommentId], [CommentContent], [Create_at], [Status], [ProductId], [AccountId], [Rate], [CustomerName], [CustomerEmail]) VALUES (40, N'Tuyệt', N'02:03 19/10/2023', 1, 3, 33, 5, N'Chính Minh', N'cminh@gmail.com')
INSERT [dbo].[Comment] ([CommentId], [CommentContent], [Create_at], [Status], [ProductId], [AccountId], [Rate], [CustomerName], [CustomerEmail]) VALUES (41, N'Tuyệt', N'02:07 19/10/2023', 1, 4, 33, 5, N'Chính Minh', N'cminh@gmail.com')
INSERT [dbo].[Comment] ([CommentId], [CommentContent], [Create_at], [Status], [ProductId], [AccountId], [Rate], [CustomerName], [CustomerEmail]) VALUES (42, N'Tuyệt ', N'02:08 19/10/2023', 1, 7, 35, 5, N'Chính Minh', N'congminh@gmail.com')
INSERT [dbo].[Comment] ([CommentId], [CommentContent], [Create_at], [Status], [ProductId], [AccountId], [Rate], [CustomerName], [CustomerEmail]) VALUES (43, N'Tuyệt', N'02:10 19/10/2023', 1, 7, 11, 5, N'Vương Công Minh', N'hihihi652652@gmail.com')
INSERT [dbo].[Comment] ([CommentId], [CommentContent], [Create_at], [Status], [ProductId], [AccountId], [Rate], [CustomerName], [CustomerEmail]) VALUES (48, N'Hay', N'02:59 19/10/2023', 1, 1, 52, 5, N'Hồng Liên', N'lien@gmail.com')
INSERT [dbo].[Comment] ([CommentId], [CommentContent], [Create_at], [Status], [ProductId], [AccountId], [Rate], [CustomerName], [CustomerEmail]) VALUES (53, N'Cảm ơn bạn', N'10:41 23/10/2023', 1, 1, 55, 5, N'Tâm', N'tam@gmail.com')
INSERT [dbo].[Comment] ([CommentId], [CommentContent], [Create_at], [Status], [ProductId], [AccountId], [Rate], [CustomerName], [CustomerEmail]) VALUES (54, N'', N'11:06 23/10/2023', 0, 10, 56, 5, N'Vương Mai ', N'maiv@gmail.com')
INSERT [dbo].[Comment] ([CommentId], [CommentContent], [Create_at], [Status], [ProductId], [AccountId], [Rate], [CustomerName], [CustomerEmail]) VALUES (55, N'Tuyệt vời', N'11:10 23/10/2023', 1, 10, 11, 5, N'Minh', N'hihihi652652@gmail.com')
INSERT [dbo].[Comment] ([CommentId], [CommentContent], [Create_at], [Status], [ProductId], [AccountId], [Rate], [CustomerName], [CustomerEmail]) VALUES (56, N'10 điểm không có nhưng', N'11:24 23/10/2023', 1, 10, 0, 5, N'mai', N'huong@gmail.com')
INSERT [dbo].[Comment] ([CommentId], [CommentContent], [Create_at], [Status], [ProductId], [AccountId], [Rate], [CustomerName], [CustomerEmail]) VALUES (57, N'Tuyệt cà là vời', N'11:25 23/10/2023', 1, 10, 57, 4, N'Tuânn', N'tuan@gmail.com')
INSERT [dbo].[Comment] ([CommentId], [CommentContent], [Create_at], [Status], [ProductId], [AccountId], [Rate], [CustomerName], [CustomerEmail]) VALUES (58, N'', N'12:09 23/10/2023', 1, 1, 58, 5, N'       ', N'!acb@hotmail.com')
INSERT [dbo].[Comment] ([CommentId], [CommentContent], [Create_at], [Status], [ProductId], [AccountId], [Rate], [CustomerName], [CustomerEmail]) VALUES (59, N'*** ', N'12:18 23/10/2023', 1, 7, 59, 5, N'Minh Anh', N'anh@gmail.com')
SET IDENTITY_INSERT [dbo].[Comment] OFF
GO
SET IDENTITY_INSERT [dbo].[Feedback] ON 

INSERT [dbo].[Feedback] ([FeedbackID], [FeedbackBySellerName], [Create_atFeedback], [FeedbackContent], [AcountId], [CommentId], [StatusFeedback]) VALUES (1, N'Boardgame CSKH', N'15:12 05/01/2023', N'ClickBuy xin chào anh/chị Nam.
Dạ vâng máy này bên em hàng mới chính hãng full box nguyên seal ạ
Sản phẩm iPhone 11 64GB Chính hãng VN/A màu đen có giá 10,790,000 ₫ạ
Máy này bên em hàng Máy mới nguyên seal Fullbox chính hãng VN/A, chưa Active. Bảo hành 12 tháng tại trung tâm bảo hành ủy quyền chính hãng.
Sản phẩm đang có khuyến mãi:
Tặng gói bảo hành chính hãng 2 năm trị giá 5.000.000đ ( 1 năm bảo hành tại hãng, 1 năm bảo hành tại clickbuy). Xem chi tiết
Tặng gói bảo hành rơi vỡ, vào nước 12 tháng trị giá đến 10.000.000đ
TẾT – PHÁT LỘC VÀNG ( từ 03/01 đến 20/01/2023), cơ hội trúng 23 chỉ vàng cùng nhiều bao lì xì lên đến 1 triệu đồng. (100% trúng thưởng – xem chi tiết)
Tặng nón bảo hiểm trị giá 250.000đ cho đơn hàng từ 300.000đ (đến khi hết quà)
Giảm giá đến 200.000đ cho học sinh – sinh viên ( xem chi tiết)
Giảm thêm 1.000.000đ khi thu cũ – Lên đời ( xem chi tiết)
Giảm giá 5% khi mua phụ kiện
Dạ em gửi thông tin sản phẩm anh/chị tham khảo thêm ạ.
Mời anh/chị liên hệ trước với cửa hàng để được Nhân viên kiểm tra giữ máy và hỗ trợ mua hàng ạ
Xin thông tin tới Anh/Chị.', 1, 1, 1)
INSERT [dbo].[Feedback] ([FeedbackID], [FeedbackBySellerName], [Create_atFeedback], [FeedbackContent], [AcountId], [CommentId], [StatusFeedback]) VALUES (2, N'Boardgame CSKH', N'16:24 14/10/2023', N'Minh', 1, 1, 1)
INSERT [dbo].[Feedback] ([FeedbackID], [FeedbackBySellerName], [Create_atFeedback], [FeedbackContent], [AcountId], [CommentId], [StatusFeedback]) VALUES (3, N'Vương ', N'13:22 15/10/2023', N'f', 0, 17, 1)
INSERT [dbo].[Feedback] ([FeedbackID], [FeedbackBySellerName], [Create_atFeedback], [FeedbackContent], [AcountId], [CommentId], [StatusFeedback]) VALUES (4, N'BoardGame CSKH', N'13:34 15/10/2023', N'Clickbuy xin chào anh/chị!
Dạ sản phẩm này shop có hỗ trợ trả góp qua công ty tài chính và qua thẻ tín dụng credit ạ
Anh/chị vui lòng để lại số điện thoại hoặc liên hệ giúp shop vào hotline HCM 1900.63.39.09 (Bấm phím 1)-Hotline HN 0966.06.2468 sẽ có nhân viên hỗ trợ tư vấn thông tin góp cho anh/chị ạ
Cảm ơn anh/chị đã quan tâm đến sản phẩm tại hệ thống bán lẻ Smartphone ClickBuy. Rất hân hạnh được phục vụ anh/chị ạ!', 0, 18, 1)
INSERT [dbo].[Feedback] ([FeedbackID], [FeedbackBySellerName], [Create_atFeedback], [FeedbackContent], [AcountId], [CommentId], [StatusFeedback]) VALUES (5, N'Vương ', N'08:20 16/10/2023', N'Cảm ơn bạn đã quan tâm', 0, 20, 1)
INSERT [dbo].[Feedback] ([FeedbackID], [FeedbackBySellerName], [Create_atFeedback], [FeedbackContent], [AcountId], [CommentId], [StatusFeedback]) VALUES (7, N'Vương ', N'11:46 16/10/2023', N'Cảm ơn bạn', 0, 21, 1)
INSERT [dbo].[Feedback] ([FeedbackID], [FeedbackBySellerName], [Create_atFeedback], [FeedbackContent], [AcountId], [CommentId], [StatusFeedback]) VALUES (8, N'Vương ', N'03:43 19/10/2023', N'Cảm ơn bạn ', 0, 49, 0)
INSERT [dbo].[Feedback] ([FeedbackID], [FeedbackBySellerName], [Create_atFeedback], [FeedbackContent], [AcountId], [CommentId], [StatusFeedback]) VALUES (9, N'Minh', N'03:49 19/10/2023', N'Shop sẽ liên hệ với bạn sau.', 11, 49, 1)
INSERT [dbo].[Feedback] ([FeedbackID], [FeedbackBySellerName], [Create_atFeedback], [FeedbackContent], [AcountId], [CommentId], [StatusFeedback]) VALUES (10, N'BoardGame CSKH', N'01:20 23/10/2023', N'Cảm ơn bạn', 0, 50, 1)
INSERT [dbo].[Feedback] ([FeedbackID], [FeedbackBySellerName], [Create_atFeedback], [FeedbackContent], [AcountId], [CommentId], [StatusFeedback]) VALUES (12, N'Minh', N'01:26 23/10/2023', N'Shop đã nhận được thông báo ', 11, 50, 1)
INSERT [dbo].[Feedback] ([FeedbackID], [FeedbackBySellerName], [Create_atFeedback], [FeedbackContent], [AcountId], [CommentId], [StatusFeedback]) VALUES (13, N'Minh', N'01:26 23/10/2023', N'Shop đã nhận được thông báo ', 11, 50, 1)
INSERT [dbo].[Feedback] ([FeedbackID], [FeedbackBySellerName], [Create_atFeedback], [FeedbackContent], [AcountId], [CommentId], [StatusFeedback]) VALUES (14, N'Minh', N'01:26 23/10/2023', N'Shop đã nhận được thông báo ', 11, 50, 1)
INSERT [dbo].[Feedback] ([FeedbackID], [FeedbackBySellerName], [Create_atFeedback], [FeedbackContent], [AcountId], [CommentId], [StatusFeedback]) VALUES (15, N'Minh', N'01:28 23/10/2023', N'Cảm ơn bạn', 11, 50, 1)
INSERT [dbo].[Feedback] ([FeedbackID], [FeedbackBySellerName], [Create_atFeedback], [FeedbackContent], [AcountId], [CommentId], [StatusFeedback]) VALUES (16, N'Minh', N'02:02 23/10/2023', N'Cảm ơn bạn đã phản hồi
', 11, 47, 1)
INSERT [dbo].[Feedback] ([FeedbackID], [FeedbackBySellerName], [Create_atFeedback], [FeedbackContent], [AcountId], [CommentId], [StatusFeedback]) VALUES (17, N'BoardGame CSKH', N'02:11 23/10/2023', N'Sản phẩm tốt', 1, 47, 1)
INSERT [dbo].[Feedback] ([FeedbackID], [FeedbackBySellerName], [Create_atFeedback], [FeedbackContent], [AcountId], [CommentId], [StatusFeedback]) VALUES (18, N'Hải', N'07:52 23/10/2023', N'Quan tâm', 53, 51, 1)
INSERT [dbo].[Feedback] ([FeedbackID], [FeedbackBySellerName], [Create_atFeedback], [FeedbackContent], [AcountId], [CommentId], [StatusFeedback]) VALUES (19, N'BoardGame CSKH', N'09:47 23/10/2023', N'Chơi được từ 2-8 người ', 0, 52, 0)
INSERT [dbo].[Feedback] ([FeedbackID], [FeedbackBySellerName], [Create_atFeedback], [FeedbackContent], [AcountId], [CommentId], [StatusFeedback]) VALUES (20, N'BoardGame CSKH', N'09:58 23/10/2023', N'Cảm ơn bạn', 0, 52, 0)
INSERT [dbo].[Feedback] ([FeedbackID], [FeedbackBySellerName], [Create_atFeedback], [FeedbackContent], [AcountId], [CommentId], [StatusFeedback]) VALUES (22, N'Minh', N'11:12 23/10/2023', N'Cảm ơn quý khác đã quan tâm sản phẩm', 1, 53, 1)
INSERT [dbo].[Feedback] ([FeedbackID], [FeedbackBySellerName], [Create_atFeedback], [FeedbackContent], [AcountId], [CommentId], [StatusFeedback]) VALUES (23, N'BoardGame CSKH', N'11:24 23/10/2023', N'có nhưng', 1, 56, 1)
INSERT [dbo].[Feedback] ([FeedbackID], [FeedbackBySellerName], [Create_atFeedback], [FeedbackContent], [AcountId], [CommentId], [StatusFeedback]) VALUES (24, N'BoardGame CSKH', N'11:37 23/10/2023', N'Cảm ơn bạn', 0, 57, 0)
INSERT [dbo].[Feedback] ([FeedbackID], [FeedbackBySellerName], [Create_atFeedback], [FeedbackContent], [AcountId], [CommentId], [StatusFeedback]) VALUES (26, N'BoardGame CSKH', N'11:37 23/10/2023', N'Cảm ơn bạn', 0, 57, 0)
INSERT [dbo].[Feedback] ([FeedbackID], [FeedbackBySellerName], [Create_atFeedback], [FeedbackContent], [AcountId], [CommentId], [StatusFeedback]) VALUES (27, N'BoardGame CSKH', N'11:44 23/10/2023', N'Sớm sẽ sớm liên hệ với bạn', 0, 57, 0)
SET IDENTITY_INSERT [dbo].[Feedback] OFF
GO
SET IDENTITY_INSERT [dbo].[News] ON 

INSERT [dbo].[News] ([nid], [title], [img], [createat], [acid], [canewId]) VALUES (1, N'Khai trương Board Game VN Vạn Hạnh Mall', N'img\pic1.jpg', CAST(N'2023-10-13' AS Date), 1, 2)
INSERT [dbo].[News] ([nid], [title], [img], [createat], [acid], [canewId]) VALUES (2, N'5 Boardgame Cực Kỳ Dễ Mà Bạn Có Thể Tự Làm Tại Nhà', N'img\pic2.jpg', CAST(N'2023-09-24' AS Date), 1, 2)
INSERT [dbo].[News] ([nid], [title], [img], [createat], [acid], [canewId]) VALUES (3, N'Khai trương Board Game VN Vạn Hạnh Mall', N'img\pic1.jpg', CAST(N'2023-09-24' AS Date), 1, 3)
INSERT [dbo].[News] ([nid], [title], [img], [createat], [acid], [canewId]) VALUES (4, N'4 Boardgame Dưới 500.000đ Nhưng Artwork Siêu Đẹp Không Thể Bỏ Qua', N'img\pic4.jpg', CAST(N'2023-09-24' AS Date), 1, 4)
INSERT [dbo].[News] ([nid], [title], [img], [createat], [acid], [canewId]) VALUES (5, N'Khai tr??ng Board Game VN V?n H?nh Mall', N'img\pic5.jpg', CAST(N'2023-09-24' AS Date), 1, 5)
INSERT [dbo].[News] ([nid], [title], [img], [createat], [acid], [canewId]) VALUES (6, N'Azul: Summer Pavilion - Liệu có vượt qua được người tiền nhiệm?', N'img\pic6.jpg', CAST(N'2023-10-13' AS Date), 1, 4)
INSERT [dbo].[News] ([nid], [title], [img], [createat], [acid], [canewId]) VALUES (7, N'【THÁM TỬ LỪNG DANH CONAN - NÀNG DÂU HALLOWEEN: THÍNH】SỰ TRỞ LẠI BÙNG NỔ CỦA BOARD GAME CHUYỂN THỂ TỪ TRUYỆN TRANH', N'img\pic7.jpg', CAST(N'2023-09-25' AS Date), 1, 4)
INSERT [dbo].[News] ([nid], [title], [img], [createat], [acid], [canewId]) VALUES (8, N'Hướng dẫn chơi board game Lớp Học Mật Ngữ Khu rừng Kim Cương chi tiết và mẹo chiến thắng mới nhất 2021', N'img\pic1.jpg', CAST(N'2023-09-25' AS Date), 1, 4)
INSERT [dbo].[News] ([nid], [title], [img], [createat], [acid], [canewId]) VALUES (9, N'Giải mã sức hút không nguội suốt 5 năm của Lớp Học Mật Ngữ', N'img\pic9.jpg', CAST(N'2023-09-25' AS Date), 1, 4)
INSERT [dbo].[News] ([nid], [title], [img], [createat], [acid], [canewId]) VALUES (10, N'Bỏ túi 12 board game chơi cùng hội bạn nhưng vẫn an toàn phòng dịch Tết 2021', N'img\pic3.jpg', CAST(N'2023-09-25' AS Date), 1, 4)
INSERT [dbo].[News] ([nid], [title], [img], [createat], [acid], [canewId]) VALUES (11, N'15 board game cho gia đình bố mẹ nên biết để Tết này vừa vui mà vẫn an toàn mùa dịch cho cả nhà', N'img\pic1.jpg', CAST(N'2023-09-25' AS Date), 1, 4)
INSERT [dbo].[News] ([nid], [title], [img], [createat], [acid], [canewId]) VALUES (12, N'Boardgame Thám Tử Lừng Danh Conan: Hồi Kết Chính Thức Mở Bán Tại TIKI', N'img\pic7.jpg', CAST(N'2023-09-25' AS Date), 1, 4)
INSERT [dbo].[News] ([nid], [title], [img], [createat], [acid], [canewId]) VALUES (1012, N'adbc', N'0di1689512915_2.png', CAST(N'2023-10-19' AS Date), 1018, 3)
SET IDENTITY_INSERT [dbo].[News] OFF
GO
SET IDENTITY_INSERT [dbo].[NewsDetails] ON 

INSERT [dbo].[NewsDetails] ([ndid], [description], [img], [nid], [view]) VALUES (1, N'Ngày 19/01/2019 Board Game VN Vạn Hạnh Mall chính thức khai trương với những hoạt động hấp dẫn và sự góp mặt của streamer VIRUSS. Sự kiện diễn ra rất thành công với đông đảo các bạn trẻ đến tham dự và rinh về nhà những phần quà giá trị. 
  Nằm tại tầng 4 của Vạn Hạnh Mall số 11 đường Sư Vạn Hạnh phường 12 Quận 10 HCM - Board Game VN nổi bật với đủ loại board game đình đám nhất hiện nay cùng với những chiếc bàn board game có thể chơi thử bất cứ lúc nào. Với những nhân viên thân thiện và nhiệt tình sẵn sàng chỉ dạy và giải đáp mọi thắc mắc của khách hàng.
 
 Với những hoạt động độc đáo được diễn ra trong buổi khai trương như:

- Check - in liền tay nhận ngay bộ UNO mini #hoàn_toàn_miễn_phí 

- Thử thách "Dare to win" cực vui nhộn: Thắng hay thua đều có.

- Các board game do người Việt sáng tạo đang nổi đình nổi đám như: Lớp Học Mật Ngữ, Dozen War, ... cũng được giới thiệu tại sự kiện khai trương. Đặc biệt là board game sinh tồn đầu tiên tại Việt Nam - Bắn Gà Là Tạch với sự tham gia của VIRUSS.

Giới thiệu và chơi thử bộ board game sinh tồn bắn súng đầu tiên tại Việt Nam: BẮN GÀ LÀ TẠCH. Cùng VIRUSS chạy bo ngay tại Vạn Hạnh Mall :v 
Buổi chơi thử vui bá cháy cũng không kém phần kịch tính khi mà những người chơi cố gắng để sinh tồn.

Ngoài ra tại buổi khai trương còn các các hoạt động nổi bật khác:
Giao lưu Tam Quốc Sát All Stars: Quy tụ các anh hùng lão luyện trong cộng đồng Tam Quốc Sát với sự góp mặt của Chung Tử Lưu - Top 8 Battle of The Kings. Và 1 trận chiến duy nhất, giải đấu hội tụ All Stars. Giải đấu tiền đề cho tournament Quốc chiến sẽ bùng nổ sau Tết.

Giờ đây với những fan của board game tại thành phố Hồ Chí Minh đã có thêm 1 địa điểm mới hoàn toàn thích hợp và lý tưởng để chơi board game một cách thuận tiện nhất, với dịch vụ tốt nhất với những board game được cập nhật đều đặn, ... tại Tầng 4 TTTM Vạn Hạnh, số 11 đường Sư Vạn Hạnh phường 12 Quận 10 HCM.

Hãy note ngay địa chỉ này vào và hẹn bạn bè qua chơi với chúng mình nhé.', N'nd1.png', 1, 123)
INSERT [dbo].[NewsDetails] ([ndid], [description], [img], [nid], [view]) VALUES (2, N'Đôi lúc trong những cuộc vui với đám bạn, chơi boardgame sẽ là gia vị không thể thiếu cho cuộc vui trở nên thú vị hơn. Tuy nhiên, không phải lúc nào bạn cũng sẽ có sẵn một bộ boardgame trong người. Dưới đây là 5 bộ boardgame mà bạn có thể tự chế trong thời gian ngắn.

1. Ma sói: 
Ma sói là một trong những boardgame có số lượng người chơi đông đảo và cực thích hợp cho những buổi party hay team building với số lượng từ 8 người chơi trở lên. Để có một bộ ma sói handmade vô cùng đơn giản, bạn chỉ cần 1 tờ giấy, 1 cái kéo và 1 cái bút. Sau đó, bạn cắt giấy thành các hình vuông, ghi tên chức năng và nhân vật. Tiện thể, bạn có thể tự chế một vài nhân vật theo sở thích nhưng miễn là đảm bảo được sự cân bằng của trò chơi. Thế là xong.

2. Cờ caro:
Cờ caro là một trong những trò chơi kinh điển của biết bao thế hệ học trò. Phải nói rằng để chơi cờ caro, bạn chỉ cần nguyên liệu vô cùng đơn giản. Một cái bút và một tờ giấy là bạn có thể bắt đầu chơi rồi đấy.

3. Lô tô:
Bing go là trò chơi nổi tiếng trên thế giới nhưng ở Việt Nam, nó được gọi bằng một cái tên vô cùng thân thương là lô tô. Lô tô thường để chơi trong dịp Tết hoặc trong các gánh hát lô tô tại miền Nam. Để làm lô tô, bạn chỉ cần chuẩn bị: giấy trắng ( tùy số lượng người chơi mà bạn chuẩn bị giấy), thước kẻ, bút và giấy. Tùy theo bạn muốn chơi đến số bao nhiêu mà trên tờ giấy sẽ được chia cột theo thứ tự hàng dọc đầu tiên là số có một chữ số và các hàng dọc tiếp theo sẽ được đánh theo số thứ tự tăng dần.

4. 5 second rule:
Nếu bạn từng xem qua “Ellen Show” thì bạn chắc chắn sẽ biết trò chơi 5 second rule. Trong 5 second rule, người chơi sẽ trả lời các câu hỏi dạng liệt kê trong 5s. Để làm 5 second rule, bạn chỉ cần chuẩn bị sẵn các câu hỏi mà bạn cảm thấy phù hợp. Một chiếc điện thoại để bấm giờ và hình phạt dành cho người thua cuộc. Bạn đã sẵn sàng chơi 5 sceconde rule chưa?

5. Photo Memory:
Photo Memory là trò chơi mà bạn có thể tự làm tại nhà khi có trẻ em hoặc cần những trò chơi đơn giản. Với Photo Memory, bạn chỉ cần chuẩn bị các cặp bức hình giống nhau và điều thú vị là bạn có thể tự chọn hình theo ý thích của bản thân. Nào giờ bắt đầu chơi thôi nào.
', N'nd2.png', 2, 1243)
INSERT [dbo].[NewsDetails] ([ndid], [description], [img], [nid], [view]) VALUES (3, N'Artwork của một bộ boardgame luôn là đề tài tranh luận sôi nổi và điểm thu hút fan bên cạnh cơ chế chơi và tên tuổi của nhà phát hành. Nếu trước đây, artwork chỉ được đầu tư mạnh tay vào các bộ boardgame đắt tiền thì bây giờ xu hướng này đã thay đổi. Bạn hoàn toàn có thể sở hữu những bộ boardgame vừa túi tiền mà artwork không thể chê được. Cùng Board Game VN điểm qua 4 boardgame giá dưới 500.000đ nhưng sở hữu artwork siêu đẹp nhé! 

1.Boardgame Lotus:
Đứng đầu danh sách này phải kể đến Lotus. Boardgame dành giải thưởng Golden Geek Best Board Game Artwork & Presentation Nominee vào năm 2016.  Với Lotus, mỗi lần chơi là một lần bạn thực sự tạo ra những tác phẩm nghệ thuật độc đáo dành cho mình. Tạm thời quên đi những căng thẳng của bộn bề cuộc sống hằng ngày và hòa mình vào thế giới của tĩnh tâm, bạn sẽ lạc bước vào vườn sen. Tại đây, bạn cần phải chăm sóc những bông hoa sen đến khi chúng đạt được cảnh giới cao nhất và ban phát cho bạn những quyền năng mà chúng đang sở hữu. Dĩ nhiên, bạn cũng sẽ cần sự giúp đỡ của những sinh vật sống xung quanh hồ để hoàn thành thử thách. Hãy coi chừng những người chơi khác vì họ có thể làm bất cứ điều gì để chiếm đoạt những bông hoa sen thần bí này. 

2.The Legend of Cherry Tree that Blossoms Every Ten Years: 

Một tựa game dài về tên gọi và dài về câu chuyện về văn hóa ấn chứa trong đó. Truyền thuyết kể rằng, cứ sau 1 thập kỷ hoa anh đào sẽ nở ra những bông hoa đẹp nhất. Lúc đấy, cả cây hoa anh đào như được che phủ bởi một vẻ đẹp tuyệt mỹ của những ngày đầu xuân với hàng ngàn bông hoa rực rỡ. Thế nhưng, hoa nở rồi cũng sẽ tàn. Những cánh hóa sẽ rơi xuống đất và trước khi chúng héo tàn, bạn hãy nhanh chóng thu nhặt nó. Bởi vì người nào có được nhiều hoa nhất, bạn sẽ nhận được điều ước hoa anh đào: biến ước mơ của mình thành sự thật. Nhưng nếu bạn quá tham lam, cây hoa anh đào sẽ cảm thấy bạn không xứng đáng và sẽ không trao cho bạn món quà này. 

3. Meeple Circus: The Wild & Aerial Show
Phiên bản thu nhỏ của Meeple Circus! Hãy bắt đầu gánh xiếc của mình riêng mình với Meeple Circus. Trong trò chơi này, bạn sẽ có 2 người bạn hoang dã là gấu, sư tử cùng với 2 người cộng sự thân thiết là Otto và Luna. Cùng với đội này, bạn hãy tạo ra những màn nhào loạn ngoạn mục để chinh phục khán giả và tăng danh tiếng cho rạp xiếc của mình.

4. Sushi Go!:
Nếu là tín đồ của những món ăn sushi Nhật Bản thì bạn đừng bỏ qua boardgame này nhé! Hãy thử tưởng tượng, bạn đang bước vào một nhà hàng sushi và nhiệm vụ của bạn là ăn được nhiều loại sushi nhất có thể, cuốn thật nhiều sashimi, nhúng nigiri yêu thích của bạn vào wasabi để nhân ba giá trị của nó! Và một khi bạn đã ăn hết, hãy kết thúc bữa ăn của mình với tất cả bánh pudding có trong tay! Hãy cẩn thận với bạn bè của bạn, những loại sushi mà họ đưa cho bạn có thể là vũ khí để đánh bại bạn trong cuộc chiến sushi này đấy! 
', N'nd3.png', 3, 34)
INSERT [dbo].[NewsDetails] ([ndid], [description], [img], [nid], [view]) VALUES (4, N'Ra mắt lần đầu vào năm 2002, Bang đã giành được nhiều giải thưởng board game, đề cử uy tín thế giới như “Trò chơi thẻ bài truyền thông hay nhất”, “Trò chơi có thiết kế đẹp nhất”, giải Golden Geek cho party game hay nhất,... 19 năm đã trôi qua, Bang! vẫn giữ được sức hút của mình khi đạt 4.8/5 điểm review cho độ vui và cơ chế chơi trên trang Amazon. Vậy board game này có gì đặc biệt mà khiến cả cộng đồng người chơi say mê đến thế, hãy cùng Board Game VN tìm hiểu nhé!

1. Đánh giá tổng quát
Những điều bạn có thể thích khi chơi Bang:

- Đa dạng về chiến thuật.
- Yêu cầu khả năng tương tác và thuyết phục.
- Yếu tố ẩn vai.

 Điểm bạn có thể không thích ở Bang: 

- Dễ bị loại nếu đi sau cùng.
- Mất thời gian cho việc chờ lượt chơi.

2. Những điểm độc đáo có trong Bang phiên bản Việt hóa chính hãng
So với phiên bản phát hành trên thế giới, phiên bản do Board Game VN phát hành sở hữu những điểm đặc biệt mà chỉ duy nhất bản này mới có được bao gồm:
- 14 lá bài chơi theo chủ đề Việt Nam.
- 7 bảng chơi.
- 30 token viên đạn.
Những lá bài đặc biệt này lấy cảm hứng từ những nét văn hóa truyền thống đặc trưng của Việt Nam và đây là nỗ lực của Board Game VN khi mong muốn quảng bá Việt Nam trên bản đồ board game thế giới.

3. Cốt truyện của Bang

Bối cảnh của Bang đưa người chơi du hành ngược thời gian quay về vùng đất Texas những năm của thế kỷ 19. Khi đó, những cuộc đọ súng diễn ra hằng ngày giữa những người bảo vệ công lý và tội phạm hung ác. Nguyên nhân của cuộc chiến là vì sự xung đột lợi ích giữa hai thế lực. Một bên muốn trở thành bá chủ thao túng cả vùng đất rộng lớn và một bên để bảo vệ sự yên bình cho người dân lương thiện.

4. Thành phần của board game Bang
Bộ board game Bang! phiên bản Việt hóa bao gồm 103 lá bài chia thành:

- 7 lá vai trò (1 cảnh sát trưởng, 2 cảnh sát phó, 3 tội phạm và 1 kẻ phản bội).
- 16 lá nhân vật.
- 80 lá hành động và trang bị.
- 14 lá đặc biệt chủ đề Việt Nam.
- 7 lá hướng dẫn cơ bản.
- 7 bảng chơi.
 -1 cuốn luật chơi.

 Cách chơi cơ bản của Bang
Người chơi sẽ được chia thành 3 phe: Cảnh sát là những người thực thi công lý chính nghĩa, tội phạm - những kẻ luôn muốn tiêu diệt lực lượng cảnh sát để nắm quyền kiểm soát thị trấn và cuối cùng là kẻ phản bội 2 mặt đa mang. 
Dưới đây là danh tính trong game tương ứng với nhiệm vụ chiến thắng:
Cảnh sát trưởng: Tiêu diệt tội phạm và kẻ phản bội
Cảnh sát: Bảo vệ cảnh sát trưởng và tiêu diệt tội phạm
Tội phạm: Loại cảnh sát trưởng
Kẻ phản bội: Trở thành người sống sót cuối cùng.

Trò chơi sẽ diễn ra theo các bước sau đây:
Bước 1: Mỗi người chơi được chia 1 lá nhân vật, tất cả đều ẩn danh trừ nhân vật Cảnh sát trưởng. Cảnh sát trưởng lật bài và được cộng thêm 1 máu.

Bước 2: Mỗi người được phát 2 lá nhân vật. Người chơi phải chọn 1 trong 2 lá nhân vật , và lật lên cho mọi người biết, lá còn lại úp xuống để đánh dấu số máu của mình.

Bước 3: Xào các lá bài chơi và phát cho mỗi người số bài bằng số máu của mình.

Bước 4: Cảnh sát trưởng sẽ là người đi đầu tiên. Người chơi lần lượt đi theo chiều kim đồng hồ. 

Mỗi lượt của một người chơi sẽ có 3 hành động bao gồm:

1 - Rút 2 lá từ chồng bài.

2 - Đánh các quân bài trên tay.

3 - Giữ số bài bằng số máu mình đang có.

 5. Ai sẽ là người không thể bỏ qua Bang!

Đây là bộ board game dành cho người chơi trên 8 tuổi. Số lượng người chơi từ 4-7 người và thời gian cho một ván từ 20-40 phút. Cho nên, Bang! cực kỳ phù hợp với những bạn làm việc tại các văn phòng hoặc học sinh/sinh viên có quỹ thời gian eo hẹp nhưng vẫn muốn kết nối và giải trí với nhau. Đặc biệt nếu bạn là một người thích những board game cần sự đấu trí hoặc chứng tỏ bản lĩnh lãnh đạo của mình thì đừng bỏ qua Bang! nhé.

6. Mức độ chơi lại của Bang!
Bang! có khá nhiều nhân vật để ẩn danh, chức năng và mục tiêu mỗi lá bài đa dạng, khiến cho mỗi ván đấu đều là một cuộc chiến hoàn toàn mới! Điều này khiến game trở nên gây nghiện ngay từ lần chơi đầu tiên. Thật dễ hiểu khi Bang đạt 4/5 điểm cho hạng mục này.

7. Tổng kết
Sau khi đọc xong bài viết này, bạn có tưởng tượng ra được hình bóng quen thuộc của những người bạn mình khi chơi Bang không? Kẻ phản bội hẳn là vai diễn dành cho thằng bạn thích chơi hard core 1 mình chống cả thế giới và đòi hỏi kỹ năng chiến đấu ẩn vai cực cao! Lại còn cả những đứa bạn chỉ thích ngồi không, chỉ đạo và nhìn cả đám “bắn” loạn xì ngầu và xu nịnh mình, có phải cảnh sát trưởng là dành cho họ? Một cuộc chơi Bang đơn giản chỉ là ngồi cười nói với nhau sau khoảng thời gian dài không gặp mặt. 
Nếu đúng là như thế, thì đừng ngại thử sức với Bang! - Cuộc chiến Viễn Tây. Chắc chắn bạn sẽ phải thốt lên những tiếng wow khi nhìn thấy bộ bài trên tay và bị cuốn ngay vào game.
', N'nd4.png', 4, 245)
INSERT [dbo].[NewsDetails] ([ndid], [description], [img], [nid], [view]) VALUES (5, N'Tóm tắt
Trong Hellapagos - trò chơi giống như bộ phim truyền hình "Lost - Mất tích" mà chúng ta hay xem trên TV, bạn và những người chơi khác phải làm mọi cách để sinh tồn và tìm cách rời khỏi hòn đảo hoang trước khi cơn bão ập đến! Tuy nhiên, trong trò chơi này, ngoài chạy đua với thời gian, bạn còn phải đấu tranh cùng những người khác.
Hellapagos là một trò chơi Semi - Coop phù hợp với nhóm từ 3 đến 12 người chơi trong khoảng 30 phút.

Cốt truyện
Sau khi bị đắm tàu, một nhóm người bị mắc kẹt bị mắc kẹt trên một hòn đảo hoang vắng. Trong khi khung cảnh đẹp như tranh vẽ, nước và thực phẩm lại vô cùng khan hiếm và một cơn bão lớn thì đang di chuyển dần về phía hòn đảo, mọi người cần phải hợp tác với nhau, gạt bỏ mọi tư thù hiềm khích để ra khỏi đảo bằng cách xây dựng một chiếc bè lớn trước khi cơn bão ập đến.
Mỗi vòng, người chơi sẽ được thực hiện 1 trong 4 hành động sau:
- Lấy gỗ để xây bè
- Câu cá kiếm thức ăn
- Thu thập nước
- Quay lại chỗ thuyền bị đắm để tìm kiếm các vật phẩm hữu ích trong đống đổ nát. 

Trong khi mỗi cái đều đơn giản, chúng đầy thách thức. Ví dụ như khi vào rừng kiếm gỗ, nếu bạn không cẩn thận, có thể bị rắn cắn, và bạn bị ốm không thể làm gì trong ngày tiếp theo. Hoặc khi câu cá bạn chỉ có thể kiếm được mỗi cá cho mình bạn. Kết quả được xác định bằng cách lấy 1 quả bóng ngẫu nhiên ra khỏi túi vải. Thu thập nước được xác định bởi thời tiết ngày hôm đó: trời có thể mưa lớn và bạn có nhiều nước, hoặc là trời không hề mưa và bạn không kiếm được chút nước nào. Cuối cùng, tìm kiếm xác tàu đắm chứa đầy những vật phẩm hữu ích như đèn pin, thức ăn, nước uống, v...v....
Vào cuối mỗi vòng, cả làng sẽ tiêu thụ thức ăn và nước cho mỗi người đang sống. Nếu không đủ đồ ăn\nước uống cho tất cả mọi người, sẽ có một cuộc bỏ phiếu để chọn người chơi nào không được ăn/uống tối hôm đó, họ sẽ bị bỏ đói và chết. Tuy nhiên, người này cũng có thể tự cứu mình bằng cách lấy ra đồ ăn, nước uống mà họ giấu chỉ dành cho riêng họ. Hoặc sử dụng để giúp đỡ những người chơi khác. Một số trường hợp cực đoan hơn thì rút súng ra và “đoàng” 1 phát. Thế là đủ đồ ăn cho tất cả mọi người rồi, đỡ phải tranh nhau nữa nhỉ !!!??
Các ngày sẽ tiếp tục như vậy cho đến khi cơn bão xuất hiện. Mọi người cần có đủ số thức ăn và nước uống cho ngày hôm đó, và thêm 1 ngày dự trữ để ở trên biển nữa, ngoài ra là số bè phải đủ cho số người chơi còn sống. Nếu như không đủ 1 trong các yếu tố trên. Mọi người trên đảo sẽ phải biểu quyết để bỏ lại mọi người, cho đến khi nào đủ tài nguyên cho những người còn lại sống sót thì thôi. Những người chơi còn sống và đi ra khỏi đảo sẽ giành chiến thắng trong trò chơi. Tuy nhiên, cũng sẽ có trường hợp là chả có ai sống sót ra khỏi đảo được cả..

Trải nghiệm trò chơi:
Khi mới trải nghiệm, Hellapagos có vẻ giống như một trò chơi đồng đội, nơi bạn và những người sống sót khác có thể dễ dàng kết hợp với nhau để “đưa nhau đi trốn”... ra khỏi đảo. Mặc dù hòn đảo không có KFC, và điện thoại của bạn thì bị hỏng nên không gọi Foody để ship đồ ăn được, nhưng trên đảo vẫn có có thể đủ thức ăn và nước cho các bạn. Tuy nhiên, một vài chuyến câu cá tồi tệ, không có nước từ trên trời rơi xuống, người chơi bị rắn cắn trong khi tìm kiếm gỗ và trò chơi nhanh chóng chuyển từ trò chơi sinh tồn nhóm sang thể loại “Player Elimination - Loại trừ người chơi khác”.
Các cơ chế cơ bản của trò chơi rất đơn giản và đây là một điều tốt. Tất cả các hành động bạn thực hiện dưới hai mươi giây và cho phép các vòng tiến triển nhanh chóng. Việc sử dụng túi và bóng để xác định thành công khi câu cá cá và lấy gỗ là sáng tạo. Về cơ bản, nó giống như sử dụng xúc xắc để xác định một kết quả, nhưng theo cách thú vị hơn nhiều. Chiếc túi làm tăng độ kịch tính, căng thẳng khi bạn lấy quả bóng và điều đó không giống như bốc bài hoặc lắc xúc xắc.
Việc trục vớt đống đổ nát là một điều cần thiết, nhưng có thể nói đây là một hành động khá ích kỷ. Cái giá của nó là không thể giúp nhóm tìm kiếm đồ quan trọng như đồ ăn/thức uống. Thu thập các đồ vật trên thuyền làm gia tăng nhiều gia vị cho trò chơi. Bạn có thể tìm được những thực phẩm bổ sung đồ ăn hoặc khẩu phần nước mà bạn cần, hay những vật phẩm đặc biệt giúp bạn trở nên hiệu quả và có giá trị hơn, và thậm chí là những vật phẩm vô giá trị như là quần lót của ông nào đó bị sóng đánh tụt ra. Hầu hết các lá bài có một số giá trị và sẽ được sử dụng trong trò chơi. Tất cả các lá bài được giữ kín trên tay người chơi, và điều đó có thể dẫn đến những yếu tố bất ngờ ở vòng biểu quyết.
Mặc dù trò chơi có nói rằng: “Tất cả đều có thể ra khỏi đảo”, nhiều khả năng bạn sẽ không ra khỏi đây được đâu. Điều này làm cho việc bỏ phiếu là một phần quan trọng của trò chơi. Hãy lưu ý rằng: Nếu một người chơi được bầu chọn để bị bỏ đói, họ vẫn có thể tự cứu mình bằng cách đánh ra 1 cái bánh mỳ tìm được ở tàu. Điều này gây ra bỏ phiếu đôi khi có một số kết quả thú vị. Trong một vòng bỏ phiếu, người viết đã đánh ra một lá bài để cho phép phiếu bầu được tính là 2, và sau đó người chơi mà mình biểu quyết đã rút súng ra và đã kết liễu mình ngay lập tức. Đó là một khoảnh khắc tuyệt vời và cả nhóm đều muốn sống lại khoảnh khắc nhìn thằng khác ăn đạn.

Điểm trừ:
Trò chơi có thể trở nên tối tăm một cách nhanh chóng cho bạn. Có khả năng phải bỏ lại 3 đến 4 người chơi trong một vòng vì trời không có mưa mấy ngày liền, không có nước mấy ngày liền và câu cá thì không đủ. Bạn không chỉ phải cố gắng giữ cho mọi người có đủ nước và thức ăn, bạn cũng cần tìm thời gian để xây dựng chiếc bè; đó là điều khó thực hiện khi mà ăn còn không đủ, sức đâu mà làm cơ chứ.

Suy nghĩ cuối cùng:
Hellapagos là một trải nghiệm độc đáo. Tất cả các hành động đều đơn giản và nhanh chóng, cho phép một trò chơi đi với tốc độ nhanh như chớp ngay cả với số lượng người chơi cao hơn. Và như một số phận được sắp đặt, người chơi chắc chắn sẽ quay lưng lại với nhau khi thức ăn / nước hết. 
Đôi lúc, lối chơi dẫn đến trải nghiệm không đồng đều vì tính ngẫu nhiên của một số hành động: như trời sẽ khô hạn trong nhiều ngày, hay cá không câu được con nào. Điều này có thể khiến bạn cực kỳ thiếu thức ăn / nước làm mất rất nhiều người chơi trong một vòng.

Đánh giá:
+ Dễ chơi / Dễ hướng dẫn 
+ Tiết tấu nhanh, không phải chờ đợi lâu
+ Chơi được đến 12 người
+ Component độc đáo và xịn sò
+ Vui hết nóc
- Game có thể đi từ thăng hoa xuống bế tắc rất là nhanh
- Khó để có thể cứu được tất cả
- Yếu tố loại trừ người chơi dẫn đến việc có người sẽ buồn 
', N'nd5.png', 5, 56)
INSERT [dbo].[NewsDetails] ([ndid], [description], [img], [nid], [view]) VALUES (6, N'Ngay khi nhìn vào hình ảnh hộp game, có lẽ mọi người đều nhận ra tên tựa game quen thuộc: Azul. Azul là chủ nhân của giải thưởng game hay nhất năm 2018, Spiel Des Jahres, và chắc hẳn phần lớn những người chơi board game đều đã thử qua trò chơi tuyệt vời này.
Azul Summer Pavilion là game thứ 3 trong series Azul, sau Azul và Azul Stained Glass of Sintra. Một tốc độ ra game khá đều đặn đến từ Next Move Games. Và vẫn giữ bối cảnh cũ của cả series, câu chuyện của game vẫn là bạn hóa thân thành một kiến trúc sư được giao phó xây dựng những cảnh quan tráng lệ cho cung điện, và lần này là dưới yêu cầu của vua Manuel tại Bồ Đào Nha vào thế kỉ 16, một khung cảnh nhằm vinh danh những người thuộc hoàng tộc.
Game thuộc thể loại xếp mảnh, thu thập bộ, dành cho 2-4 người chơi với thời gian chơi khoảng 30 phút, tương đồng với những game đi trước.
Nếu như Azul Stained Glass of Sintra không thể tái lập kỳ tích của Azul, vậy liệu chúng ta có thể mong chờ những gì tại đứa con thứ 3 này của hãng? Hãy cùng nhìn sâu hơn

Đánh giá
Với việc các tile có khu vực dự trữ (ở 4 góc của bàn người chơi),  chúng ta sẽ có thêm nhiều toan tính trong chiến thuật chơi. Và giờ đây mỗi miếng gạch cũng có nhiều hơn lựa chọn để đặt thay vì chỉ ở hàng ngang hay cột dọc như trước đây, giúp cho việc lắp sẽ được đa dạng hơn.
Và việc có wildtile có thể đặt ở bất kỳ đâu sẽ khiến cho vai trò của token đi đầu sẽ quan trọng hơn, và người chơi sẽ cần cân nhắc nên dừng ở thời điểm nào để lấy token đi trước (việc đi trước không quá quan trọng trong 2 bản trước).
Thành phần của game vẫn giữ được chất liệu tốt như những người đi trước, cùng với thay đổi tile hình kim cương, lắp thành các khối hình sao tạo hình mới mẻ hấp dẫn với người chơi.
Tổng kết

Sẽ thật khó để Azul Summer Pavilion vượt qua được cái bóng quá lớn của Azul, và đến thời điểm này, game cũng mới chỉ nhận được 1 đề cử cho giải game gia đình hay nhất, nhưng không vì thế mà phủ nhận những điểm mới mẻ mà Summer Pavilion mang lại, cũng như những sáng tạo của Kiesling dành cho tựa game này.
Nếu bạn là fan của series Azul hoặc chưa có cơ hội chơi qua các tựa Azul, thì Summer Pavilion sẽ là một tựa game rất đáng sở hữu. Hay nếu bạn đang tìm kiếm một game đối kháng nhẹ nhàng, thì Azul Summer Pavilion cũng là một sự lựa chọn phù hợp.
Hãy thử ngay game để có những trải nghiệm đánh giá cho riêng mình nhé!
', N'nd6.png', 6, 123)
INSERT [dbo].[NewsDetails] ([ndid], [description], [img], [nid], [view]) VALUES (7, N'*Conan - Nàng dâu Halloween: Thính là bộ trò chơi thứ 2 được chuyển thể từ bộ truyện tranh nổi tiếng “Thám tử lừng danh Conan”. Lấy bối cảnh một bữa tiệc nhỏ trước ngày đám cưới của cặp đôi Sato - Takagi, bộ trò chơi mang màu sắc hài hước và tươi sáng hơn so với người tiền nhiệm của mình - board game “Thám tử lừng danh Conan: Hồi Kết”.
*Bộ boardgame hứa hẹn sẽ mang đến cho người chơi những giây phút “cười ra nước mắt”, cũng như là một công cụ hữu ích, giúp hội bạn tha hồ “bóc phốt” và “khui” những bí mật hài hước của nhau.

CÙNG KHÁM PHÁ TRÒ CHƠI CONAN - NÀNG DÂU HALLOWEEN: THÍNH!!
Trước ngày đám cưới mùa Halloween, cô dâu Sato Miwako và chú rể Takagi Wataru đã tổ chức một bữa tiệc ấm cúng nho nhỏ cùng những người bạn thân thiết. Cả Conan và Đội Thám Tử Nhí cũng có mặt. Để bầu không khí thêm phần vui vẻ, tất cả đã cùng nhau trải nghiệm trò chơi "Thính". Tại đây, rất nhiều câu chuyện thú vị đã được tiết lộ. Cùng tìm hiểu ngay nào!

THÀNH PHẦN CỦA BỘ TRÒ CHƠI
- 1 hộp đựng nhỏ gọn, vừa tay, tiện lợi cho bạn mang mang theo bên mình.
- 56 thẻ bài với Artwork bao đẹp đến từ Conan Movie 25. Đặc biệt, các bạn chắc chắn không thể bỏ qua những thẻ hành động với các chức năng rất chi là “mận vải”.
- 1 luật chơi hướng dẫn chi tiết, giúp bạn dễ dàng phá đảo bộ game chì trong tích tắc.

HƯỚNG DẪN PHÁ ĐẢO SIÊU TỐC
>> MỤC TIÊU CHIẾN THẮNG: Trở thành người đầu tiên đánh hết bài trên tay

Chuẩn bị nhập cuộc:
- Tráo bài và chia cho mỗi người chơi 5 lá, phần dư để thành 1 chồng giữa khu vực chơi (dùng để bốc bài). 
- Lật lá bài trên cùng của chồng bài bốc làm lá khởi đầu. 
- Chọn 1 người đi đầu và chơi theo chiều kim đồng hồ.

Bắt đầu trò chơi:
Trong lượt của mình, người chơi sẽ đánh một lá bài cùng màu hoặc cùng số với lá bài gần nhất được đánh ra. Ví dụ: Người chơi A đánh lá 3 đỏ, B chơi tiếp theo sẽ phải đánh lá 5 đỏ (cùng màu), sau đó đến lượt C đánh lá 5 xanh (cùng số với lá bài của B). 
Nếu bạn không có bài phù hợp để đánh, bạn bắt buộc phải bốc 1 lá bài từ chồng bài bốc.
Nếu lá bài vừa bốc phù hợp, bạn có thể đánh ngay lập tức. Nếu không, bạn mất lượt và đến lượt người chơi tiếp theo. 
Khi còn 1 lá bài trên tay, bạn phải báo hiệu với những người chơi khác bằng cách hô "Sự thật chỉ có 1". Nếu bạn quên hô mà bị người khác bắt được, bạn phải bốc phạt 2 lá.
Kết thúc trò chơi Khi một người chơi đánh hết bài trên tay, trò chơi kết thúc ngay lập tức và người đó giành chiến thắng. Mọi người cùng đếm bài trên tay, ai còn nhiều bài nhất sẽ phải thực hiện 1 mệnh lệnh bất kỳ.

NHỮNG LÁ BÀI CHỨC NĂNG “NGẦU ĐÉT” CỦA “THÁM TỬ LỪNG DANH CONAN - NÀNG DÂU HALLOWEEN: THÍNH”
- Chém gió: Đưa ra một chủ đề, bắt đầu từ người chơi tiếp theo lần lượt liệt kê 1 từ khóa liên quan đến chủ đề đó. Nếu ai không trả lời được trong vòng 3 giây hoặc trùng với đáp án trước đó thì bị phạt.
- Hoá trang: Người đánh lá bài này có thể chọn 1 màu và vòng chơi sẽ tiếp tục với màu đó. Ví dụ người chơi A đánh lá Hoá trang và chọn màu đỏ, thì người tiếp theo phải đánh 1 lá màu đỏ tùy ý.
- Hóng hớt: Người đánh lá bài này đặt 1 câu hỏi cho người kế tiếp và người đó phải trả lời thật lòng.
- Quay xe!: Đảo chiều lượt chơi hiện tại. Ví dụ đang chơi theo chiều kim đồng hồ thì sau khi ra lá bài này, lượt chơi của người kế tiếp sẽ là người ngồi bên cạnh người đó theo ngược chiều kim đồng hồ.
- Thách: Người đánh lá bài này được yêu cầu người chơi khác làm 1 hành động bất kỳ.
- Thả thính: Người đánh lá bài này được chỉ định người chơi tiếp theo phải làm 1 cử chỉ đáng yêu với 1 người bất kỳ trong bàn chơi.
- Lên!: Người chơi kế tiếp phải bốc 2 lá bài. Nếu họ đánh chồng lá Lên! khác thì không phải bốc mà người tiếp theo phải bốc 4 lá. Cứ như vậy cho đến khi một người không đánh được nữa phải bốc số lá đã cộng dồn trước đó.
- SOS: Bỏ qua lượt của bạn. Nếu trước đó là lá chức năng thì tác dụng sẽ được chuyển sang người tiếp theo. Ví dụ người chơi A đánh lá Lên! đỏ, B chơi tiếp đánh lá SOS đỏ thì người chơi tiếp theo là C sẽ phải bốc 2 lá bài.
', N'nd7.png', 7, 43)
INSERT [dbo].[NewsDetails] ([ndid], [description], [img], [nid], [view]) VALUES (8, N'Mặc dù mới ra mắt từ tháng 12, board game Lớp Học Mật Ngữ Khu rừng Kim Cương nhanh chóng đạt Top 1 trò chơi bán chạy nhất trên Shopee chỉ sau 7 ngày ra mắt. Cùng Board Game VN tìm hiểu về trò chơi gây sốt trong thời gian vừa qua nhé! 

I. Giới thiệu board game Lớp Học Mật Ngữ Khu rừng Kim Cương
Board game Lớp học Mật Ngữ Khu rừng Kim Cương là trò chơi thứ 3 trong series trò chơi gia đình bán chạy nhất* (Theo báo cáo của FAHASA). Với thông điệp “Cùng ươm hạt giống, gặt hái thành công”, thế giới các bạn nhỏ tuổi tween được khắc họa sinh động qua những thành phần thiết kế siêu đẹp, dễ thương và đặc sắc được sáng tạo từ chính nhóm tác giả B.R.O. Trò chơi dành cho nhiều độ tuổi, từ các bạn nhỏ đến bố mẹ đều có cách chơi thú vị cho riêng mình. 
Không chỉ mang lại tiếng cười vui vẻ cho mọi thành viên, trò chơi còn mang đến cho người chơi câu chuyện ý nghĩa. Trong cuộc sống, chúng ta có thể kiên trì theo đuổi một mục tiêu lớn, hay chọn hoàn thành những nhiệm vụ nhỏ để “tích tiểu thành đại” và đạt được thành tựu mong muốn. Cách nào cũng sẽ đến đích nhưng quan trọng là cảm giác hạnh phúc khi được tự mình đưa ra quyết định.

II. Thành phần chi tiết của trò chơi Khu rừng Kim Cương 
Một bộ trò chơi Khu rừng Kim Cương hoàn chỉnh bao gồm: 1 bản đồ, 1 Giỏ Thành Quả, 1 Gấu Mèo, 1 túi thần, 80 kim cương, 40 thẻ nhiệm vụ hằng ngày, 12 nhiệm vụ trọn đời, 4 bộ cây (mỗi bộ 7 cây), 4 kho kim cương, 2 xí ngầu (đen, đỏ) và 1 sách hướng dẫn chơi.

III. Hướng dẫn chơi Khu rừng Kim Cương chuẩn không cần chỉnh

3.1. Câu chuyện của Khu rừng Kim Cương
Sinh nhật Xử Nữ năm nay, Sư Tử rủ cả lớp đi chơi Khu Rừng Kim Cương - trò chơi được yêu thích nhất ở công viên Ngân Hà. Chưa kịp vui vẻ, hạnh phúc vì “sống trong bể ngọc kim cương không bằng sống giữa tình thương” của bạn bè, Xử Nữ phát hiện ra trò chơi không đơn giản, ấm áp như mình nghĩ. Vốn là người khó tính, tỉ mỉ, khắt khe và không chấp nhận thua cuộc, Xử Nữ biến bữa tiệc sinh nhật trở thành 1 cuộc đấu trí kịch tính. Cùng chúng mình chinh phục Khu rừng Kim Cương với bạn bè nhé!

3.2. Mục tiêu chiến thắng của trò chơi
Người chơi cùng nhau thu thập hạt giống kim cương và tích lũy điểm để trồng cây lên các ô đất trên bản đồ. Người trồng hết 7 cây của mình nhanh nhất sẽ giành chiến thắng.

3.3. Các chế độ chơi của board game Lớp Học Mật Ngữ Khu rừng Kim Cương
Trò chơi có 2 chế độ cho người chơi tha hồ thử sức: chiến thuật đối kháng dành cho 2 người chơi và giải trí nhưng kịch tính không kém với chế độ cho 3-4 người.

3.4. Set up trước khi bắt đầu chơi
Bước 1: Mở bản đồ, phân loại 4 màu kim cương đặt bên cạnh, gọi là Quỹ Chung.
Bước 2: Chia cho mỗi người 1 bộ 7 cây kim cương cùng màu.
Bước 3: Tráo đều 2 chồng bài nhiệm vụ, chia cho mỗi người chơi 3 thẻ nhiệm vụ hằng ngày và 1 thẻ nhiệm vụ trọn đời. Nhớ giữ bí mật nhé!
Bước 4: Gieo xí ngầu, ai có điểm cao nhất thì đi trước.
Bước 5: Lần lượt từng người chơi được trồng 1 cây miễn phí lên khu vực đất trồng cây theo thứ tự người đi sau sẽ được trồng cây trước. Sau đó lấy về 1 kim cương cùng màu ô đất mình đã trồng.

3.5. Hành động nhận nhiệm vụ
1. Nhận nhiệm vụ mới
Đầu lượt, kiểm tra và bốc cho đủ 3 thẻ nhiệm vụ hằng ngày trên tay. Nếu trên tay đã đủ 3 thẻ thì không được nhận thêm.

2. Hoàn thành nhiệm vụ
Bất cứ lúc nào, nếu điều kiện ghi trên thẻ nhiệm vụ được thoả mãn, người chơi đánh thẻ ra để tích điểm và thông báo cho mọi người biết. Bạn được hoàn thành cùng lúc nhiều nhiệm vụ giống nhau.
*Lưu ý: Mỗi người chỉ được hoàn thành 1 nhiệm vụ trọn đời trong cả ván chơi. 

3.6. Hành động thu thập kim cương
Để thu thập kim cương, tất cả người chơi sẽ cùng điều khiển chung 2 nhân vật là Gấu Mèo và Giỏ Thành Quả. Đến lượt, gieo cùng lúc 2 xí ngầu đen, đỏ để di chuyển Gấu Mèo và Giỏ trên bản đồ bằng số nút trên xí ngầu, theo thứ tự Gấu Mèo đi trước, Giỏ đi sau.
Gấu Mèo:
- Di chuyển theo nút xí ngầu màu đen.
- Gấu Mèo đi đến ô đất có cây của ai thì người đó bị mất 1 kim cương tùy chọn vào Quỹ chung.
- Khi người chơi không có kim cương để trả thì sẽ bị “Nợ Kim Cương”. 

Giỏ thành quả:
- Di chuyển theo nút xí ngầu màu đỏ.
- Giỏ đi đến ô đất có cây của ai thì người đó được thu hoạch kim cương tương ứng với số cây và màu của ô đất.
- Giỏ đi đến ô đất trống thì người điều khiển được thu hoạch 1 kim cương theo màu ô đất.
- Khi Giỏ đi hết 1 vòng quay về GO thì mọi người được thu hoạch kim cương từ tất cả cây đã trồng.

3.7. Hành động giao dịch
1. Đổi nhiệm vụ
Một lần trong lượt, người chơi có thể dùng 1 kim cương bất kỳ để đổi 1 nhiệm vụ mới bao gồm cả nhiệm vụ trọn đời. Đặt nhiệm vụ đã đổi xuống dưới chồng bài chưa sử dụng.

2. Trao đổi kim cương
Tiến hành mua và bán kim cương thông qua sạp mua bán ở rìa bản đồ. Chỉ được rao bán và mua kim cương trong lượt của mình.
Rao bán kim cương:
- Treo kim cương muốn bán ở vị trí tương ứng với hình kim cương muốn mua. (tối đa 4 viên)
- Khi bị mất kim cương do Gấu Mèo hoặc sự kiện, được dùng kim cương đang bán để trả.
Mua kim cương:
- Đặt kim cương thoả mãn yêu cầu trao đổi trên sạp của người bán và lấy kim cương của họ về.
- Được mua tất cả kim cương mà người khác đang bán miễn là thoả mãn điều kiện yêu cầu.

3.8. Hành động trồng cây
Trồng cây là hành động cuối cùng trong lượt chơi của bạn. Có thể trồng số cây tuỳ ý, tuy nhiên mỗi lượt chỉ được chọn 1 trong 3 kiểu trồng cây sau:
Cách 1: Trồng cây bằng kim cương
Cách 2: Trồng cây bằng điểm
Cách 3: Chuyển cây

* Lưu ý:
- Mỗi ô đất được trồng tối đa 4 cây.
- Không được trồng cây cùng 1 ô đất với người khác.
- Sau khi trồng cây bằng điểm mà bị dư thì điểm dư sẽ không được giữ lại. (Ví dụ dùng hai thẻ 2 điểm và 4 điểm để trồng cây thì sẽ mất luôn điểm dư là 1)
- Thẻ điểm sau khi sử dụng để trồng cây thì đặt úp sang 1 bên, có thể tái sử dụng khi chồng thẻ nhiệm vụ hết.
- Trước khi trồng cây bị khoá cần phải chuộc cây ra khỏi ô khoá.

3.9. Kích hoạt các Ô Sự Kiện trong trò chơi
Chỉ được kích hoạt khi Giỏ đi đến và tác động đến người điều khiển Giỏ hoặc tất cả mọi người.
- Mỏ kim cương: Bạn được nhận 1 kim cương theo màu các ô này.
- Túi thần: Đặt 4 kim cương khác màu vào trong túi, bạn rút ngẫu nhiên ra 1 viên, xem màu và nhận kim cương từ Quỹ chung.
- Giao dịch: Bạn được đổi 2 kim cương bất kỳ thành 1 kim cương mong muốn.
- Cổng không gian: Bạn được chọn thu hoạch 1 kim cương từ 1 cây mình đã trồng.
- Nảy mầm: Bạn được dùng 3 kim cương bất kỳ để đặt thêm 1 cây lên ô đất mình đã trồng. (Sự kiện này không ảnh hưởng đến hành động trồng cây)
- Đại dịch: Sâu mập tấn công vùng đất trồng cây. Tất cả người chơi mất 1 kim cương cho mỗi ô đất mà mình đang có. Nếu không có thì bị tính “Nợ Kim Cương”.
- Góp quỹ: Tất cả người chơi đóng góp 1 kim cương vào Quỹ chung. Nếu không có thì bị tính “Nợ Kim Cương”.

4.0. Giải thích sự kiện "Nợ Kim Cương"
Trong quá trình chơi, khi không có kim cương để chi trả cho Gấu Mèo hoặc các sự kiện mất mát thì sẽ bị ghi Nợ Kim Cương. Hành động Nợ Kim Cương sẽ được thực hiện như sau: 
- Đặt 1 cây chưa trồng vào khu vực ô khoá của mình trên bản đồ tại vị trí tương ứng với số kim cương đang nợ.
- Khi nợ tăng thêm, di chuyển cây ghi nợ qua ô nợ tương ứng với tổng số kim cương đang nợ.
- Mỗi người chỉ được đặt 1 cây ghi nợ. Nếu số nợ vượt quá 7 kim cương, người chơi sẽ bị xử thua và loại khỏi trò chơi.
Thực hiện hành động trả nợ:  Bạn chỉ được trả nợ trong khi bạn trồng cây đã khoá để ghi nợ. Khi trồng cây này, người chơi phải trả hết số kim cương đang nợ để chuộc cây rồi mới được trồng.

IV. Hướng dẫn set - up chế độ chơi dành cho 2 người nâng cao
Nếu ván chơi chỉ có 2 người, trước khi người chơi trồng cây miễn phí, hãy sử dụng 1 bộ cây không chơi đặt lên bản đồ theo vị trí như hình dưới:
- Những cây này gọi là “cây tự nhiên", người chơi không thể thu hoạch hay làm mất kim cương từ những cây này.
- Người chơi vẫn có thể sử dụng hành động “chuyển cây” với những cây này để làm nhiệm vụ hoặc sở hữu các ô đất cần thiết.

V. Mẹo chiến thắng Khu rừng Kim Cương
Dưới đây là một số mẹo mà chỉ có cao thủ mới biết, giúp bạn nâng tỷ lệ chiến thắng khi chơi Khu rừng Kim Cương!
- Trồng cây ở các vị trí "ngã ba đường" sẽ nâng khả năng nhận ngọc của bạn lên.
- Cây đầu tiên nên trồng ở vị trí giúp bạn hoàn thành nhiệm vụ trọn đời hoặc thẻ nhiệm vụ hằng ngày. 
- Bạn chỉ có thể chọn 1 trong 3 cách để trồng cây nhưng không giới hạn số lượng trồng cây trong một lần. Vì thế hãy tính toán kỹ số lượng điểm có trên thẻ nhiệm vụ để trồng nhiều cây càng tốt nhé!  
', N'nd8.png', 8, 2)
INSERT [dbo].[NewsDetails] ([ndid], [description], [img], [nid], [view]) VALUES (9, N'Đối với lứa tuổi mới lớn, truyện tranh góp phần mang đến cho trẻ một thế giới giải trí sống động. Truyện tranh “made in Việt Nam” là giấc mơ chưa bao giờ tắt của nhiều thế hệ sáng tạo, và sau hơn 3 thập kỷ phát triển, ngày càng có nhiều thành tựu được ghi nhận trong lĩnh vực này. Một trong những tựa truyện Việt được độc giả nhí mến mộ và yêu thích là Lớp Học Mật Ngữ.

Bộ truyện "con thích, bố mẹ an tâm"
“Cho con đọc truyện, mình quan tâm đến từng lời nói, tình huống của nhân vật”. Đó là chia sẻ của chị Hà An - một phụ huynh - về việc chọn truyện phù hợp cho trẻ đọc. Chị nhận định nếu truyện tranh thế hệ trước mang tính nhân văn nhưng cách truyền tải còn thiếu thu hút, thì hiện tại, những cuốn sách, truyện đã có cách nắm bắt tâm lý trẻ em rất tốt. Không chỉ cuốn hút về mặt nội dung, những sản phẩm mới còn có cách thể hiện sáng tạo, qua hệ thống nhân vật phong phú.
Không riêng chị An, ngày càng nhiều phụ huynh Việt quan tâm đến những thông tin con trẻ được tiếp nhận qua những trang truyện mua về.
Năm 2016, trong bối cảnh trẻ em và phụ huynh Việt Nam bắt đầu có những nhu cầu và đòi hỏi cao hơn về cốt truyện, hình ảnh, tựa truyện tranh Lớp Học Mật Ngữ dành cho các bạn nhỏ 8-12 tuổi ra đời. Ngay lập tức, cuốn truyện đạt top 1 bán chạy tại các hệ thống nhà sách lớn trên toàn quốc.
Sau hơn 5 năm, tựa truyện đã “bỏ túi” thành tích xứng đáng với những nỗ lực bền bỉ của đội ngũ tác giả, với gần 30 tập đến tay hàng triệu độc giả nhí.
Lý do cho việc liên tục nằm trong top 10 tựa sách bán chạy phải kể đến khả năng xây dựng hình ảnh, cá tính, hiện đại, thời trang của từng nhân vật rất độc đáo, rõ nét, dựa theo đặc điểm các cung hoàng đạo và phù hợp với thị hiếu của độ tuổi tween (8-12 tuổi). Với nội dung xoay quanh đời sống học đường, tình bạn giữa các thành viên trong lớp học, chương truyện mang nhiều tiếng cười, gần gũi với các bé thiếu niên, nhi đồng.
Lớp Học Mật Ngữ mở ra thế giới của 12 cung Hoàng đạo tại Trường THCS chất lượng vũ trụ Ngân Hà. Ở đây, mỗi bạn nhỏ có thể gặp chính mình trong đó.
"Mỗi người sinh ra đều là một cá thể độc đáo với những đặc điểm khác nhau, điều này tạo nên sự đa dạng cho cộng đồng. Chấp nhận sự đa dạng và trưởng thành cùng nó, bạn sẽ có tuổi thơ đẹp nhất, sống đúng với bản thân mình”- đại diện nhóm tác giả chia sẻ về thông điệp của tựa truyện.
Bên cạnh việc khai thác những khía cạnh đặc sắc về tính cách của 12 cung hoàng đạo, Lớp Học Mật Ngữ còn gợi nhắc tuổi thơ của người đọc trong từng nhân vật. Mỗi câu chuyện đem đến ký ức, kỷ niệm không thể nào quên của tuổi “nhất quỷ nhì ma, thứ ba học trò” mà ai cũng từng trải qua trong đời.
Đan xen những tràng cười sảng khoái, phân đoạn tình cảm gà bông đặc trưng hay việc “thầm” thích một bạn nào đó trong lớp cũng tạo nên tính thực tế, thú vị cho bộ truyện. “Hồi hộp, vui, gay cấn…” là những cảm xúc của fan nhí gửi gắm tới Lớp Học Mật Ngữ.
Không dừng lại ở truyện tranh, Lớp Học Mật Ngữ còn mang đến những câu chuyện dí dỏm, giúp thổi bay áp lực học đường với series "Bộ sách kỹ năng cho các bạn thanh thiếu nhi".
Cách tiếp cận mới mẻ giúp những vấn đề phức tạp trở nên dễ hiểu hơn, thông qua hình ảnh thể hiện của nhóm bạn Cung Hoàng Đạo. Qua đó, các bạn có thể nhận diện và "nhảy phóc" qua những vấn đề gặp phải, để mỗi ngày tới trường là một ngày vui.
Được phát hành lần đầu vào tháng 1 năm nay, series đã có 2 tập - "Thổi bay áp lực học đường" và "Băng qua cơn sóng 4.0" ra mắt.
Hệ sinh thái phong phú của Lớp Học Mật Ngữ
Một thương hiệu truyện tranh phát triển bền vững theo thời gian sẽ không chỉ xuất bản sách, mà còn có những sản phẩm khai thác hình ảnh nhân vật. Đây là xu hướng tất yếu, khi hình ảnh siêu anh hùng hay chú mèo máy có thể được bắt gặp dễ dàng trên đồ dùng hàng ngày, thay vì chỉ trên màn ảnh hay truyện tranh như trước đây.
Lớp Học Mật Ngữ cũng không phải ngoại lệ. Những hình ảnh tràn đầy năng lượng của truyện còn được ứng dụng trên sản phẩm đồ chơi và phụ kiện học tập, giúp các độc giả nhí mang theo nhân vật yêu thích tới trường hàng ngày. Bộ sưu tập phụ kiện học tập gồm ba lô chống gù, hộp bút vải, thẻ tên, huy hiệu, hộp bút… đã lần lượt ra mắt fan bộ truyện theo thời gian.
Trong thế giới truyện tranh đầy sắc màu, Lớp Học Mật Ngữ lan tỏa những câu chuyện về tuổi thơ đến gần hơn với độc giả nhí và phụ huynh. Thế giới 12 cung hoàng đạo đa dạng nhân vật, tính cách với thông điệp "tôn trọng và chấp nhận sự khác biệt" sẽ còn là nguồn cảm hứng và lay động trái tim hàng triệu bạn nhỏ Việt Nam trong thời gian tới.
Bạn đọc có thể chờ đón series phim hoạt hình từ Lớp Học Mật Ngữ được công chiếu từ tháng 11 và trò chơi dành cho gia đình - Khu rừng Kim Cương - ra mắt vào tháng 12. Những ưu đãi hấp dẫn khi đặt mua trò chơi Khu Rừng Kim Cương trong tháng 11.
', N'nd9.png', 9, 65)
INSERT [dbo].[NewsDetails] ([ndid], [description], [img], [nid], [view]) VALUES (10, N'Tết đến Xuân về là thời điểm tụ tập đông đủ anh em bạn bè sau một năm dài bận rộn. Hẳn các bạn đang cân nhắc chơi boardg game nào vừa giải trí lại dễ hiểu cho tất cả mọi người? Hãy để Board Game VN giới thiệu đến các bạn list game cực vui cho hội bạn, đảm bảo một mùa Tết không bao giờ chán nhé.

1. Bài Uno (Mattel) - game bài nổi tiếng nhất mọi thời đại
Uno là một game bài nổi tiếng thế giới và đã trở thành kinh điển trong thể loại này.
Số người chơi của một ván Uno có thể từ 2-10 người, rất phù hợp để chơi với mọi hội bạn bè. Luật chơi Uno đơn giản, hầu như ai ai cũng đã từng chơi qua một lần. Với đủ 3 yếu tố dễ chơi, chơi nhanh và cực vui, Uno hứa hẹn sẽ mang lại những buổi tụ họp mùa Tết rộn ràng với những tràng cười ngả nghiêng, những phút giây gắn kết bạn bè sẽ còn nhớ mãi. Bạn đã có Uno để chơi Tết chưa?

2.Ma Sói Ultimate Werewolf - Deluxe Edition - trò chơi ẩn vai kinh điển:
Ma Sói là tựa board game kinh điển trong thể loại ẩn vai.
Hẳn ai cũng đã từng chơi qua tựa board game cực hot Ma Sói. Trong phiên bản Ma sói Ultimate Werewolf, số lượng người chơi có thể lên đến 75 người! Mặc dù hội bạn đông đến đâu, Ma Sói Ultimate đều có thể xử lý hết. Trò chơi ẩn vai cực vui với nhiều yếu tố bất ngờ này chắc chắn sẽ hâm nóng không khí buổi đi chơi tụ họp anh em bạn bè. Hãy nhanh tay đặt ngay 1 bộ Ma sói để Tết này có game cùng chơi cùng vui nhé.

3. Thám tử lừng danh Conan - Hồi Kết: board game hot nhất đầu năm 2021
Conan: Hồi Kết là board game ẩn vai đầu tiên tại Việt Nam chuyển thể từ series truyện tranh trinh thám nổi tiếng thế giới Thám tử lừng danh Conan.
Trong game, bạn sẽ hóa thân thành Conan, Gin và các nhân vật khác, nhập vai vào cuộc đối đầu cân não giữa phe Thám tử và Tổ chức áo đen, cùng tìm ra hồi kết cho Conan. Trò chơi chứa đầy những yếu tố bất ngờ, những cú lật mặt gây “ngã ngửa” đủ vui cho mọi buổi tụ tập bạn bè. Số lượng người chơi linh hoạt từ 6-30 người. Chức năng nhân vật được chuyển thể từ ngay trong cốt truyện. Luật game cực đơn giản mở màn cho những cuộc vui không hồi kết.

4. Thông Thỏ - trò chơi thách thức nhân phẩm từ Thỏ Bảy Màu: 
Thông Thỏ là bộ trò chơi cực vui được hợp tác sản xuất giữa Board Game Việt Nam và Thỏ Bảy Màu.
Thông Thỏ - một party game mới lạ với artwork ngộ nghĩnh dễ thương, thú vị trong từng nước đi, gây “ú òa” bất ngờ với nội dung của từng thẻ bài hành động. Những pha lật kèo không chỉ vì chiến thuật xuất sắc mà đôi khi còn là vì hên xui. Cùng mở bộ Thông Thỏ với hội bạn thân để thách thức nhân phẩm từng người, thách thức thêm cả độ xui xẻo xem sau 1 năm, đứa nào “nặng nghiệp” nhất nhé.

5. Lầy - Party game lầy nhất hệ mặt trời:
Lầy, như cái tên của mình, là một board game party có luật chơi khá giống với Uno nhưng "lầy lội" hơn hẳn.
Lầy - party game nổi bật nhất năm với độ vui, lầy và bựa đủ sức làm cho bất kỳ người chơi nào cũng bật ra những tiếng cười bất ngờ. Luật chơi đơn giản dễ hiểu, cùng combo những lá bài chức năng đặc biệt với nội dung siêu cấp lầy lội. Trò chơi phù hợp với mọi hội bạn từ 2-10 người. 
Lầy xứng đáng là board game không thể bỏ qua mùa Tết để gắn kết mọi người, hứa hẹn những cú lật mặt cười ra nước mắt, những phút giây sảng khoái cùng bạn bè.

6. Lội - Bản mở rộng #1 từ party game Lầy:
Lội là phiên bản mở rộng đầu tiên của board game Lầy.
Lầy - Lội, combo hẳn đã quá quen thuộc với nhiều hội bạn. Lội giữ vững các yếu tố cơ bản của Lầy như luật chơi đơn giản dễ hiểu, lá bài thiết kế minh họa bắt mắt, nhưng độ LẦY LỘI ở một đẳng cấp hoàn toàn khác. Với tổ hợp 23 lá chức năng mới toanh, giúp bạn rũ bỏ bớt cơ số bài trên tay. Đã sở hữu LẦY rồi thì đừng quên sắm thêm LỘI để đảm bảo combo board game chơi Tết vui không bao giờ chán.

7. Bài Thính - Vừa vui vừa thả Thính
Thính là bộ card game phiên bản đậm chất "thả thính" từ bộ game bài kinh điển Uno.
Luật chơi của bài Thính tương tự như Uno thông thường nhưng thêm vào các lá bài đặc biệt có chức năng như ra lệnh, đặt một chủ để, xoay vòng, uống một ly nước, hoặc thậm chí ép phải kiss người khác! Bài Thính là một game phù hợp để chơi trong các nhóm lớn, đặc biệt là những buổi chè chén, party. Nếu bạn thích Uno, bạn không thể bỏ qua Bài Thính được! Đừng quên sắm ngay bài Thính, vừa chơi vui Tết, vừa tập thả thính để kiếm người yêu nhé.

8. Splendor - Board game chiến thuật đỉnh cao nhất:
Splendor là một board game chiến thuật đấu trí được đề cử cho hạng mục game hay nhất năm Spiel Des Jahres,
Splendor là tựa game chiến thuật cân não nhưng không kém phần giải trí, dành cho các hội bạn từ 2-4 người. Kể từ khi ra mắt năm 2014 Splendor đã nhận được vô số giải thưởng và những lời tán dương từ các nhà phê bình cho đến cộng đồng những người yêu thích board game. Và cũng chẳng hề bất ngờ khi trò “Nhặt ngọc” đình đám này đã nhanh chóng trở thành 1 trong những board game được yêu thích nhất tại Việt Nam. Chắc hẳn Splendor sẽ luôn là sự lựa chọn hàng đầu cho những người chơi đam mê game đấu trí sử dụng IQ vô cực nhưng cũng muốn có thời gian vui vẻ cùng bạn bè.

9. Gizmos - Cỗ máy tối thượng
Gizmos là một board game đấu trí chiến thuật nhận được giải thưởng Mensa Select cao quý.
Gizmos - tựa game đấu trí quen thuộc với cách chơi tương tự như Splendor nhưng độ khó cao hơn hẳn. Với luật chơi đơn giản, thành phần màu sắc đẹp mắt, và tính chơi lại rất cao, không khó hiểu khi Gizmos đã nhận được giải thưởng Mensa Select ngay sau khi ra mắt - là giải thưởng chứng nhận cho những sản phẩm có lợi cho sự phát triển của tư duy và tính toán của não bộ. Cùng sắm ngay một bộ Gizmos về chơi cùng hội bạn để có những ngày Tết không chỉ cười xả láng mà còn rèn luyện trí óc nhé.

10. Bom Lắc - Party game không thể thiếu cho hội bạn vui Tết
Bom Lắc - một board game cực vui và quen thuộc với các bạn trẻ.
Bom Lắc là một party game theo đúng các tiêu chuẩn: Đơn giản, dễ học, chơi nhanh, đông người, tương tác cao và hài hước. Đây là một game phù hợp với xu hướng party đông người ở Việt Nam hiện nay. Hơn hết, Bom Lắc một game thuần Việt hoàn toàn, được thiết kế bởi tác giả Nguyên Trường và độc quyền bởi BoardgameVN. Trò chơi vừa hài vừa bựa, siêu bất ngờ và siêu vui. Học luật chơi trong 5 phút và game có thể được chơi đến 50 người. Sắm ngay một bộ Bom Lắc để quẩy Tết cùng đám bạn nào.

11. Avalon - phiên bản căng não hơn của Ma Sói
Avalon là một tựa board game ẩn vai đấu trí đứng thứ 6 trong danh sách các party game hay nhất mọi thời đại.
Các bạn đang quá chán Ma Sói, muốn tìm một tựa game ẩn vai cân não nhưng luật chơi đơn giản? Avalon chính là câu trả lời cho bạn. Trong game, người chơi ẩn vai vào cuộc chiến giữa 2 phe Thiện và Ác, thu thập thông tin, bỏ phiếu và đưa trò chơi đến thời khắc quyết định. Điểm đặc biệt nhất của Avalon là bạn không cần một quản trò cho cuộc vui này. Nếu như là một fan của dòng game suy luận nói chung, hay là fan của game ma sói nói riêng, thì nhất định Avalon sẽ là một tựa game mà bạn nhất định không nên bỏ lỡ. Hãy nhanh tay sở hữu Avalon để có bộ board game chống chán cho mùa Tết 2021 này nhé.

12. Tam Quốc Sát - Vương Triều Chiến - Boardgame hay nhất 2015
Tam Quốc Sát là một board game với lối chơi đa dạng, biến hóa và có giá trị chơi lại cực cao.
Tam Quốc Sát là một tựa card game đình đám phổ biến suốt 5 năm qua, game cực kỳ quen thuộc cho các nhóm bạn mỗi khi offline. Cách chơi của Tam Quốc Sát khá giống với Bang và có thể coi đây là bản “Super nâng cấp” của Bang. Chỉ cần thật sự hiểu, game sẽ vô cùng gây nghiện cho người chơi do giá trị chơi lại của nó quá cao. Tam Quốc Sát được update liên tục khiến cho game sẽ không bao giờ có thể lỗi thời được. Nếu bạn chuẩn bị có một mùa Tết đầy những buổi offline cùng hội bạn, đừng quên mang theo Tam Quốc Sát bên mình nhé.
', N'nd10.png', 10, 234)
INSERT [dbo].[NewsDetails] ([ndid], [description], [img], [nid], [view]) VALUES (11, N'15 board game cho gia đình bố mẹ nên biết để Tết này vừa vui mà vẫn an toàn mùa dịch cho cả nhàĐăng ngày 25/01/2021

Không chỉ là thời điểm các thành viên dành thời gian quây quần, Tết cũng là lúc nhiều gia đình gặp khó khăn trong việc chọn: “Chơi gì Tết này?”. Cả nhà đừng lo vì đã có boardgame mang niềm vui và gắn kết các thành viên rồi. Cùng Board Game VN tìm hiểu top 15 boardgame dành cho gia đình không thể bỏ lỡ trong dịp Tết nhé!

1. Đường đua tài chính - Boardgame giúp bé quản lý lì xì hiệu quả mùa Tết này
Đường đua tài chính là bộ trò chơi giáo dục tư duy quản lý tài chính cho trẻ em
Đường đua tài chính là board game cực kỳ phù hợp trong những ngày Tết cho gia đình có trẻ từ 6-14 tuổi. Trò chơi được phát triển bởi bộ ba tiến sĩ - giảng viên ĐH Quốc gia Hà Nội với mục tiêu giúp người chơi có thêm kiến thức cơ bản về tài chính như đầu tư, chi tiêu và thu nhập…cũng như quản lý tiền thông minh. Đặc biệt trong dịp Tết này, khi bố mẹ gặp khó khăn trong việc giúp trẻ quản lý tiền lì xì thì đây là cách thức đơn giản mà đem lại hiệu quả cao.

2. Kính hiển vi giấy Foldscope deluxe - Món quà vui khoa học khám phá đầu xuân
Foldscope deluxe là sản phẩm kính hiển vi nhỏ gọn bằng giấy tiện dụng cho các em học sinh lắp ráp và sử dụng.
Kính hiển vi giấy Foldscope deluxe là sáng chế do 2 nhà khoa học thuộc đại học Stanford là Jim Cybulski và Manu Prakash. Được bình chọn là đồ chơi khoa học yêu thích nhất năm 2019, phiên bản Foldscope deluxe cho phép những nhà thám hiểm khoa học nhí có thể quan sát các sinh vật, thực vật ở cuộc sống xung quanh dưới bằng mắt thường với độ phóng đại lên đến 3000 lần thông qua thiết bị điện thoại. Board Game VN được ủy quyền bán và nhập khẩu chính hãng sản phẩm kính hiển vi Foldscope. Hết năm 2019, Foldscope Việt Nam đã mang hơn 20.000 chiếc kính hiển vi đến cho các trẻ em, học sinh trên toàn quốc.

3. Cờ cổ tích - Bé thêm yêu văn hóa dân gian
Cờ cổ tích là boardgame nằm trong chuỗi dự án “Với Bitis, bé thêm yêu Văn Hóa Dân Gian”. 
Với cờ cổ tích, cả gia đình sẽ cùng hóa thân thành các nhân vật quen thuộc trong cổ tích Việt Nam như Mai An Tiêm, Lang Liêu, bé Lì Xì và Táo Quân. Mọi người sẽ sử dụng quyền năng cổ tích của mình, hóa giải các thử thách kỳ ảo trên đường để về đích đầu tiên và giúp Ngọc Hoàng khai xuân đón Tết đúng hẹn. Đây là bộ trò chơi rất ý nghĩa trong dịp Tết cổ truyền này, đặc biệt có ý nghĩa giáo dục về truyền thống dân tộc cho các bé.

4. Monopoly Classic - Lựa chọn hàng đầu cho gia đình từ 3-5 người 
Cờ Tỷ Phú Monopoly Classic là trò chơi board game quen thuộc gắn liền với nhiều thế hệ gia đình Việt Nam.
Monopoly classic hay còn gọi là Cờ Tỷ Phú luôn là trò chơi được yêu thích nhất trong dịp Tết đến Xuân về bởi độ dễ chơi cũng như tính bất ngờ đến từ các viên xúc xắc. Trong Monopoly, các thành viên sẽ trở thành các đại gia giàu có sở hữu khối tài sản khổng lồ. Nhiệm vụ của mọi người là số tiền được phát làm xây dựng nhà ở và khách sạn hợp lý để trở thành người giàu nhất trong trò chơi.

5. Monopoly ultimate banking - Phiên bản hiện đại của cờ tỷ phú
Cờ Tỷ Phú ngân hàng điện tử Monopoly ultimate banking là phiên bản hiện đại đầu tiên của trò chơi kinh điển Cờ tỷ phú.
Monopoly ultimate banking vẫn giữ nguyên cách chơi truyền thống của cờ tỷ phú nhưng bổ sung thêm những tính năng cực hấp dẫn khiến mọi người phải vỡ òa vì thích thú. Tiền giấy được thay toàn bộ bằng thẻ ATM siêu xịn sò. Các lá bài cơ hội, nhà đất được mã hóa để quản lý bằng chip điện tử. Ngoài ra, phiên bản này còn bổ sung thêm luật đấu giá giúp người chơi mua những miếng đất vàng với giá rẻ bèo. Hãy cùng Cờ tỷ phú ngân hàng điện tử dạy các con những bài học đầu tiên về sử dụng thẻ ngân hàng nhé.

6. Katamino - Trò chơi giải đố cho đầu xuân thêm năng lượng 
Katamino pocket là bộ trò chơi xếp hình giải đố với các mức độ tăng dần từ dễ đến khó cho trẻ em.
Suốt nhiều năm liền, Katamino luôn được bình chọn là trò chơi giải đố được yêu thích trên toàn thế giới. Luật chơi của Katamino rất đơn giản nhưng mang lại nhiều thách thức cho các thành viên gia đình. Trong thời gian 60s, mọi người phải xếp thành các khối hình vuông hoặc hình chữ nhật với một số thẻ gỗ nhất định. Katamino sẽ khiến mọi người chìm đắm vào trong những khối vuông kỳ ảo và vui sướng khi hoàn thành thử thách. Một bộ Katamino có hơn 500 thử thách để cả nhà chơi không chán trong Tết này đó!

7. Xếp toán - Vui học toán Tết này
Xếp Toán Cộng Trừ là board game rèn luyện khả năng tính toán và phản xạ cho trẻ em.
Xếp toán là một trò chơi dành cho các bạn nhỏ từ 6 tuổi với mục tiêu giúp trẻ nhỏ làm quen với các phép cộng trừ đơn giản. Người chơi sẽ chiến thắng khi thực hiện phép tính đúng dựa từ các lá bài trên tay. Xếp toán là trò chơi đơn giản với luật chơi thú vị, giúp nâng cao khả năng tính toán nhanh và giúp cả gia đình kết nối tương tác các thành viên gia đình trong dịp Tết này.

8. Cuộc đua sao chổi - Món quà Tết dễ thương cho các bé
Cuộc đua sao chổi là bộ trò chơi được phát triển từ serie truyện tranh yêu thích Lớp học Mật ngữ.
Cuộc đua sao chổi là bộ trò chơi và cũng là món quà tuyệt vời nhất cho những người là fan của bộ truyện tranh Lớp học Mật Ngữ. Với artwork cực bắt mắt của các cung Hoàng đạo, Cuộc đua sao chổi sẽ là món quà lì xì tuyệt vời cho các bé trong dịp đầu năm. Được chấp bút bởi nhóm tác giả vàng B.R.O và luật chơi vui nhộn, thật dễ hiểu khi boardgame này luôn nằm trong best seller tại hệ thống bán lẻ FAHASA và TIKI. Mùa Tết này, cả nhà mình cùng quây quần bên Cuộc đua sao chổi để có thêm nhiều tiếng cười nhé!

9. Siêu thú ngân hà - Board Game bán chạy nhất dịp Tết 2020
Siêu thú ngân hà là phần tiếp theo của bộ board game được các bạn nhỏ yêu thích - Cuộc đua sao chổi.
Siêu thú ngân hà luôn đứng top món quà mà các bạn nhỏ yêu thích nhất trong những ngày lễ bởi độ xịn sò và hấp dẫn của trò chơi. Cả nhà sẽ hóa thân thành các chiến binh theo cung Hoàng đạo, thu phục các siêu thú cổ đại và giải cứu hành tinh Cầu Vồng. Điểm đặc biệt của Siêu thú ngân hà là hệ thống quái thú siêu ngầu cùng 4 tượng siêu thú tinh xảo khiến các bạn nhỏ “vỡ òa” vì thích thú ngay từ cái nhìn đầu tiên. Trò chơi này chắc chắn sẽ là món quà các bé muốn nhận được nhất trong mùa Tết 2021 này.

10. Brain Connect - Board Game rèn luyện tư duy não bộ
Brain Connect là một tựa board game giải đố với mục đích rèn luyện tư duy.
Trong Brain Connect, người chơi cần suy nghĩ và hành động nhanh nhất có thể, kết nối các mảnh ghép phù hợp trong trò chơi theo đúng thứ tự để chiến thắng. Thời gian chơi chỉ 20 phút cho mỗi lượt, game phù hợp với mọi độ tuổi của các thành viên trong gia đình. Brain Connect chắc chắn là một board game không chỉ siêu vui mà còn phù hợp cho cả nhà cùng “luyện não”.

11. Cupcake Academy - Học viện bánh ngọt vui nhộn
Cupcake Academy là bộ board game dễ thương giúp rèn luyện trí thông minh cho các bé.
Cupcake Academy là đòi hỏi các bạn nhỏ phải sử dụng và rèn luyện khả năng tư duy và khả năng làm việc nhóm. Với luật chơi đơn giản, cha mẹ có thể nhanh chóng nhập cuộc cùng vui và rèn luyện các kỹ năng cùng con trẻ. Cupcake Academy - học viện bánh ngọt xứng đáng là một board game không thể bỏ qua cho gia đình mùa Tết 2021 này bởi độ vui, hình ảnh game bắt mắt và ý nghĩa giáo dục của nó.

12. Difference - Board Game rèn luyện nhanh tay tinh mắt
Difference là một trò chơi tìm điểm khác biệt với hình họa bắt mắt thu hút, dành cho mọi lứa tuổi.
Difference hay còn gọi là trò chơi tìm điểm khác biệt. Tuy nhiên, Difference nổi bật hơn hẳn với artwork cực bắt mắt thu hút, luật chơi đơn giản nhưng dễ gây nghiện. Số lượng người chơi lý tưởng là từ 2-6 người, vừa vặn để cả gia đình cùng tham gia. Hãy dành thời gian mùa Tết này để luyện nhanh tay tinh mắt cùng các gia đình bằng Difference nhé.

13. Difference Junior - Phiên bản tìm điểm khác biệt cho các bé
Difference Junior là trò chơi tìm điểm khác biệt tương tự như Difference nhưng là phiên bản dành cho các bé từ 4 đến 6 tuổi.
Vẫn là trò chơi tìm điểm khác biệt nhưng Difference Junior là phiên bản có hình ảnh đơn giản hơn, phù hợp hơn cho những người chơi trên 4 tuổi. Trò chơi dành cho các gia đình đang muốn tìm một board game đơn giản để luyện những bước nhanh tay tinh mắt đầu tiên cho bé nhà mình. Cả Difference và Difference Junior đều là những sản phẩm không thể bỏ qua cho gia đình ngày Tết 2021.

14. Pylos Mini - Board game tưởng chừng đơn giản nhưng vô cùng cân não
Pylos Mini là phiên bản mini của game Pylos - trò chơi đặt bi.
Pylos Mini là một trò chơi trong đó người chiến thắng là người sẽ đặt được viên bi lên đỉnh kim tự tháp trước. Luật chơi dễ hiểu đơn giản đến mức bất ngờ, chỉ mất 1 phút cho người mới học được cách chơi. Game dễ chơi, giá trị chơi lại cao, Pylos Mini đảm bảo một mùa Tết vui vẻ ngập tràn tiếng cười cho gia đình.

15. Quixo Mini - Cờ Caro phiên bản nâng cấp
Tương tự với Pylos Mini, Quixo Mini là phiên bản mini của game Quixo - trò chơi khá giống với cờ Caro truyền thống.
Cả nhà có đang mong chờ một phiên bản nâng cấp của cờ Caro truyền thống? Vẫn là nguyên tắc cơ bản dễ hiểu của bộ môn cờ kinh điển mà ai cũng biết chơi nhưng Quixo Mini thú vị hơn hẳn. Người chơi sẽ nhanh chóng nhập cuộc vui thử thách, rèn luyện tư duy trí tuệ trong mỗi quân cờ. Game phù hợp cho mọi lứa tuổi. Cả nhà hãy dành thời gian cùng nhau giải trí trong ngày Tết cùng Quixo Mini nhé.
', N'nd11.png', 11, 764)
INSERT [dbo].[NewsDetails] ([ndid], [description], [img], [nid], [view]) VALUES (12, N'Thám tử lừng danh Conan: Hồi Kết - nhập vai suy luận viết nên “hồi kết” cho cuộc đối đầu gay cấn giữa Gin và Conan. Đây là bộ boardgame đầu tiên tại Việt Nam được chuyển thể từ bộ truyện tranh Thám tử lừng danh Conan. Trong trò chơi, người chơi sẽ được hóa thân thành các nhân vật quen thuộc như Conan, Gin, Mori, Ran thậm chí là Siêu đạo chính Kid hoặc Hannin (Hung thủ). 

Mặc dù là một boardgame ẩn vai nhưng Thám tử lừng danh Conan: Hồi kết vẫn chiều lòng fan hâm mộ bộ truyện tranh này bởi artwork của thẻ bài và đặc biệt là bộ hồ sơ nhân vật với 25 nhân vật chủ chốt cùng với câu nói tạo nên thương hiệu của các nhân vật. 
Dưới đây là một số ảnh chụp của sản phẩm mà bạn có thể chiêm ngưỡng: 
', N'nd12.png', 12, 32)
INSERT [dbo].[NewsDetails] ([ndid], [description], [img], [nid], [view]) VALUES (18, N'ạdjajd', N'1.jpg', 1012, 12)
SET IDENTITY_INSERT [dbo].[NewsDetails] OFF
GO
SET IDENTITY_INSERT [dbo].[orderDetails] ON 

INSERT [dbo].[orderDetails] ([odid], [pid], [oid], [price], [quantity]) VALUES (2040, 2, 1026, 459000, 1)
INSERT [dbo].[orderDetails] ([odid], [pid], [oid], [price], [quantity]) VALUES (2041, 8, 1027, 200000, 1)
INSERT [dbo].[orderDetails] ([odid], [pid], [oid], [price], [quantity]) VALUES (2042, 8, 1028, 200000, 5)
INSERT [dbo].[orderDetails] ([odid], [pid], [oid], [price], [quantity]) VALUES (2043, 10, 1029, 143200, 1)
INSERT [dbo].[orderDetails] ([odid], [pid], [oid], [price], [quantity]) VALUES (2044, 10, 1030, 143200, 1)
INSERT [dbo].[orderDetails] ([odid], [pid], [oid], [price], [quantity]) VALUES (2045, 9, 1031, 111200, 1)
INSERT [dbo].[orderDetails] ([odid], [pid], [oid], [price], [quantity]) VALUES (2046, 10, 1031, 143200, 1)
INSERT [dbo].[orderDetails] ([odid], [pid], [oid], [price], [quantity]) VALUES (2047, 9, 1035, 111200, 1)
INSERT [dbo].[orderDetails] ([odid], [pid], [oid], [price], [quantity]) VALUES (2048, 10, 1036, 143200, 1)
INSERT [dbo].[orderDetails] ([odid], [pid], [oid], [price], [quantity]) VALUES (2049, 10, 1037, 143200, 1)
INSERT [dbo].[orderDetails] ([odid], [pid], [oid], [price], [quantity]) VALUES (2050, 8, 1038, 200000, 1)
INSERT [dbo].[orderDetails] ([odid], [pid], [oid], [price], [quantity]) VALUES (2051, 8, 1039, 200000, 1)
INSERT [dbo].[orderDetails] ([odid], [pid], [oid], [price], [quantity]) VALUES (2052, 8, 1040, 200000, 1)
INSERT [dbo].[orderDetails] ([odid], [pid], [oid], [price], [quantity]) VALUES (2053, 7, 1041, 127200, 1)
INSERT [dbo].[orderDetails] ([odid], [pid], [oid], [price], [quantity]) VALUES (2054, 8, 1040, 200000, 1)
INSERT [dbo].[orderDetails] ([odid], [pid], [oid], [price], [quantity]) VALUES (2055, 8, 1040, 200000, 1)
INSERT [dbo].[orderDetails] ([odid], [pid], [oid], [price], [quantity]) VALUES (2056, 9, 1043, 111200, 1)
INSERT [dbo].[orderDetails] ([odid], [pid], [oid], [price], [quantity]) VALUES (2057, 10, 1044, 143200, 1)
INSERT [dbo].[orderDetails] ([odid], [pid], [oid], [price], [quantity]) VALUES (2058, 10, 1044, 143200, 1)
INSERT [dbo].[orderDetails] ([odid], [pid], [oid], [price], [quantity]) VALUES (2059, 7, 1046, 127200, 1)
INSERT [dbo].[orderDetails] ([odid], [pid], [oid], [price], [quantity]) VALUES (2060, 8, 1046, 200000, 1)
INSERT [dbo].[orderDetails] ([odid], [pid], [oid], [price], [quantity]) VALUES (2061, 9, 1046, 111200, 2)
INSERT [dbo].[orderDetails] ([odid], [pid], [oid], [price], [quantity]) VALUES (2062, 2, 1047, 459000, 1)
INSERT [dbo].[orderDetails] ([odid], [pid], [oid], [price], [quantity]) VALUES (2063, 8, 1047, 200000, 1)
INSERT [dbo].[orderDetails] ([odid], [pid], [oid], [price], [quantity]) VALUES (2064, 5, 1048, 240000, 1)
INSERT [dbo].[orderDetails] ([odid], [pid], [oid], [price], [quantity]) VALUES (2065, 6, 1049, 228000, 1)
INSERT [dbo].[orderDetails] ([odid], [pid], [oid], [price], [quantity]) VALUES (2066, 2, 1050, 459000, 1)
INSERT [dbo].[orderDetails] ([odid], [pid], [oid], [price], [quantity]) VALUES (2067, 2, 1051, 459000, 1)
INSERT [dbo].[orderDetails] ([odid], [pid], [oid], [price], [quantity]) VALUES (2068, 5, 1051, 240000, 1)
INSERT [dbo].[orderDetails] ([odid], [pid], [oid], [price], [quantity]) VALUES (2069, 2, 1052, 459000, 1)
INSERT [dbo].[orderDetails] ([odid], [pid], [oid], [price], [quantity]) VALUES (2070, 6, 1053, 228000, 1)
INSERT [dbo].[orderDetails] ([odid], [pid], [oid], [price], [quantity]) VALUES (2071, 3, 1054, 127200, 1)
INSERT [dbo].[orderDetails] ([odid], [pid], [oid], [price], [quantity]) VALUES (2072, 4, 1055, 228000, 1)
INSERT [dbo].[orderDetails] ([odid], [pid], [oid], [price], [quantity]) VALUES (3066, 1, 2050, 300000, 4)
INSERT [dbo].[orderDetails] ([odid], [pid], [oid], [price], [quantity]) VALUES (3067, 1041, 2050, 110000, 2)
INSERT [dbo].[orderDetails] ([odid], [pid], [oid], [price], [quantity]) VALUES (3068, 1040, 2050, 11, 3)
INSERT [dbo].[orderDetails] ([odid], [pid], [oid], [price], [quantity]) VALUES (3069, 12, 2051, 798400, 1)
INSERT [dbo].[orderDetails] ([odid], [pid], [oid], [price], [quantity]) VALUES (3070, 1, 2052, 300000, 1)
INSERT [dbo].[orderDetails] ([odid], [pid], [oid], [price], [quantity]) VALUES (3071, 1, 2053, 300000, 1)
INSERT [dbo].[orderDetails] ([odid], [pid], [oid], [price], [quantity]) VALUES (3072, 1, 2054, 300000, 1)
INSERT [dbo].[orderDetails] ([odid], [pid], [oid], [price], [quantity]) VALUES (3073, 1, 2055, 300000, 1)
INSERT [dbo].[orderDetails] ([odid], [pid], [oid], [price], [quantity]) VALUES (3074, 1, 2056, 300000, 1)
INSERT [dbo].[orderDetails] ([odid], [pid], [oid], [price], [quantity]) VALUES (3075, 4, 2057, 228000, 1)
INSERT [dbo].[orderDetails] ([odid], [pid], [oid], [price], [quantity]) VALUES (4066, 1, 2058, 300000, 1)
INSERT [dbo].[orderDetails] ([odid], [pid], [oid], [price], [quantity]) VALUES (4067, 3, 2058, 127200, 1)
INSERT [dbo].[orderDetails] ([odid], [pid], [oid], [price], [quantity]) VALUES (4068, 4, 2058, 228000, 1)
INSERT [dbo].[orderDetails] ([odid], [pid], [oid], [price], [quantity]) VALUES (4069, 6, 2058, 228000, 1)
INSERT [dbo].[orderDetails] ([odid], [pid], [oid], [price], [quantity]) VALUES (4070, 1040, 2058, 110000, 1)
INSERT [dbo].[orderDetails] ([odid], [pid], [oid], [price], [quantity]) VALUES (4071, 1, 2059, 300000, 1)
SET IDENTITY_INSERT [dbo].[orderDetails] OFF
GO
SET IDENTITY_INSERT [dbo].[orders] ON 

INSERT [dbo].[orders] ([oid], [acid], [ordered_at], [TotalAmount], [created_by], [address], [phonenumber], [status], [note], [receiver], [discount], [enddate]) VALUES (1026, 10, N'2023-9-1  ', 459000, NULL, N'Địa chỉ: 21 Đường Nguyễn Đình Chính, Quận Phú Nhuận, TP. Hồ Chí Minh                                                                                  ', N'0976543210', 2, N'                                                                                                                                                                                                                                                          ', N'Mr A', 1, N'2023-10-25')
INSERT [dbo].[orders] ([oid], [acid], [ordered_at], [TotalAmount], [created_by], [address], [phonenumber], [status], [note], [receiver], [discount], [enddate]) VALUES (1027, 10, N'2023-9-1  ', 200000, NULL, N'Địa chỉ: 21 Đường Nguyễn Đình Chính, Quận Phú Nhuận, TP. Hồ Chí Minh                                                                                  ', N'0976543210', 1, N'                                                                                                                                                                                                                                                          ', N'Mr A', 1, NULL)
INSERT [dbo].[orders] ([oid], [acid], [ordered_at], [TotalAmount], [created_by], [address], [phonenumber], [status], [note], [receiver], [discount], [enddate]) VALUES (1028, 10, N'2023-9-1  ', 1000000, NULL, N'Địa chỉ: 21 Đường Nguyễn Đình Chính, Quận Phú Nhuận, TP. Hồ Chí Minh                                                                                  ', N'0976543210', 1, N'                                                                                                                                                                                                                                                          ', N'Mr C', 1, NULL)
INSERT [dbo].[orders] ([oid], [acid], [ordered_at], [TotalAmount], [created_by], [address], [phonenumber], [status], [note], [receiver], [discount], [enddate]) VALUES (1029, 9, N'2023-9-1  ', 143200, NULL, N'Tỉnh Hà Giang, Huyện Quản Bạ, Xã Bát Đại Sơn, 123123                                                                                                  ', N'1231231231', 1, N'                                                                                                                                                                                                                                                          ', N'Nulaa', 1, NULL)
INSERT [dbo].[orders] ([oid], [acid], [ordered_at], [TotalAmount], [created_by], [address], [phonenumber], [status], [note], [receiver], [discount], [enddate]) VALUES (1030, 1011, N'2023-8-1  ', 143200, NULL, N'Tỉnh Hà Giang, Huyện Đồng Văn, Xã Lũng Thầu, 123123                                                                                                   ', N'1231231231', 1, N'                                                                                                                                                                                                                                                          ', N'Mr A', 1, NULL)
INSERT [dbo].[orders] ([oid], [acid], [ordered_at], [TotalAmount], [created_by], [address], [phonenumber], [status], [note], [receiver], [discount], [enddate]) VALUES (1031, 12, N'2023-8-1  ', 254400, NULL, N'6 Đường Nguyễn Công Trứ, TP. Hạ Long, Quảng Ninh                                                                                                      ', N'1231231231', 1, N'                                                                                                                                                                                                                                                          ', N'Mr C', 1, NULL)
INSERT [dbo].[orders] ([oid], [acid], [ordered_at], [TotalAmount], [created_by], [address], [phonenumber], [status], [note], [receiver], [discount], [enddate]) VALUES (1032, 12, N'2023-8-1  ', 798400, NULL, N'6 Đường Nguyễn Công Trứ, TP. Hạ Long, Quảng Ninh                                                                                                      ', N'1231231231', 1, N'                                                                                                                                                                                                                                                          ', N'Trần Hải Đăng', 1, NULL)
INSERT [dbo].[orders] ([oid], [acid], [ordered_at], [TotalAmount], [created_by], [address], [phonenumber], [status], [note], [receiver], [discount], [enddate]) VALUES (1033, 4, N'2023-8-1  ', 143200, NULL, N'Tỉnh Hà Giang, Huyện Mèo Vạc, Xã Pải Lủng, 123123                                                                                                     ', N'1231231231', 1, N'                                                                                                                                                                                                                                                          ', N'Hòa Bì', 1, NULL)
INSERT [dbo].[orders] ([oid], [acid], [ordered_at], [TotalAmount], [created_by], [address], [phonenumber], [status], [note], [receiver], [discount], [enddate]) VALUES (1034, 6, N'2023-7-1  ', 143200, NULL, N'Tỉnh Hà Giang, Huyện Đồng Văn, Thị trấn Phó Bảng, 123123                                                                                              ', N'1231231231', 0, N'                                                                                                                                                                                                                                                          ', N'Hòa Bì', 1, NULL)
INSERT [dbo].[orders] ([oid], [acid], [ordered_at], [TotalAmount], [created_by], [address], [phonenumber], [status], [note], [receiver], [discount], [enddate]) VALUES (1035, 12, N'2023-7-1  ', 111200, NULL, N'6 Đường Nguyễn Công Trứ, TP. Hạ Long, Quảng Ninh                                                                                                      ', N'1231231231', 1, N'                                                                                                                                                                                                                                                          ', N'Trần Hải Đăng', 1, NULL)
INSERT [dbo].[orders] ([oid], [acid], [ordered_at], [TotalAmount], [created_by], [address], [phonenumber], [status], [note], [receiver], [discount], [enddate]) VALUES (1036, 12, N'2023-7-1  ', 143200, NULL, N'6 Đường Nguyễn Công Trứ, TP. Hạ Long, Quảng Ninh                                                                                                      ', N'1231231231', 0, N'                                                                                                                                                                                                                                                          ', N'Trần Hải Đăng', 1, NULL)
INSERT [dbo].[orders] ([oid], [acid], [ordered_at], [TotalAmount], [created_by], [address], [phonenumber], [status], [note], [receiver], [discount], [enddate]) VALUES (1037, 12, N'2023-7-1  ', 143200, NULL, N'6 Đường Nguyễn Công Trứ, TP. Hạ Long, Quảng Ninh                                                                                                      ', N'1231231231', 2, N'                                                                                                                                                                                                                                                          ', N'Trần Hải Đăng', 1, N'2023-10-25')
INSERT [dbo].[orders] ([oid], [acid], [ordered_at], [TotalAmount], [created_by], [address], [phonenumber], [status], [note], [receiver], [discount], [enddate]) VALUES (1038, 12, N'2023-7-1  ', 200000, NULL, N'6 Đường Nguyễn Công Trứ, TP. Hạ Long, Quảng Ninh                                                                                                      ', N'1231231231', 0, N'                                                                                                                                                                                                                                                          ', N'Trần Hải Đăng', 1, NULL)
INSERT [dbo].[orders] ([oid], [acid], [ordered_at], [TotalAmount], [created_by], [address], [phonenumber], [status], [note], [receiver], [discount], [enddate]) VALUES (1039, 12, N'2023-7-1  ', 200000, NULL, N'6 Đường Nguyễn Công Trứ, TP. Hạ Long, Quảng Ninh                                                                                                      ', N'1231231231', 0, N'                                                                                                                                                                                                                                                          ', N'Trần Hải Đăng', 1, NULL)
INSERT [dbo].[orders] ([oid], [acid], [ordered_at], [TotalAmount], [created_by], [address], [phonenumber], [status], [note], [receiver], [discount], [enddate]) VALUES (1040, 12, N'2023-7-1  ', 200000, NULL, N'6 Đường Nguyễn Công Trứ, TP. Hạ Long, Quảng Ninh                                                                                                      ', N'1231231231', 3, N'                                                                                                                                                                                                                                                          ', N'Trần Hải Đăng', 1, N'2023-10-25')
INSERT [dbo].[orders] ([oid], [acid], [ordered_at], [TotalAmount], [created_by], [address], [phonenumber], [status], [note], [receiver], [discount], [enddate]) VALUES (1041, 12, N'2023-6-1  ', 127200, NULL, N'6 Đường Nguyễn Công Trứ, TP. Hạ Long, Quảng Ninh                                                                                                      ', N'1231231231', 0, N'                                                                                                                                                                                                                                                          ', N'Trần Hải Đăng', 1, NULL)
INSERT [dbo].[orders] ([oid], [acid], [ordered_at], [TotalAmount], [created_by], [address], [phonenumber], [status], [note], [receiver], [discount], [enddate]) VALUES (1042, 12, N'2023-5-1  ', 200000, NULL, N'6 Đường Nguyễn Công Trứ, TP. Hạ Long, Quảng Ninh                                                                                                      ', N'1231231231', 0, N'                                                                                                                                                                                                                                                          ', N'Trần Hải Đăng', 10000, NULL)
INSERT [dbo].[orders] ([oid], [acid], [ordered_at], [TotalAmount], [created_by], [address], [phonenumber], [status], [note], [receiver], [discount], [enddate]) VALUES (1043, 12, N'2023-5-1  ', 111200, NULL, N'6 Đường Nguyễn Công Trứ, TP. Hạ Long, Quảng Ninh                                                                                                      ', N'1231231231', 0, N'                                                                                                                                                                                                                                                          ', N'Trần Hải Đăng', 0, NULL)
INSERT [dbo].[orders] ([oid], [acid], [ordered_at], [TotalAmount], [created_by], [address], [phonenumber], [status], [note], [receiver], [discount], [enddate]) VALUES (1044, 12, N'2023-5-1  ', 143200, NULL, N'6 Đường Nguyễn Công Trứ, TP. Hạ Long, Quảng Ninh                                                                                                      ', N'1231231231', 1, N'                                                                                                                                                                                                                                                          ', N'Trần Hải Đăng', 0, NULL)
INSERT [dbo].[orders] ([oid], [acid], [ordered_at], [TotalAmount], [created_by], [address], [phonenumber], [status], [note], [receiver], [discount], [enddate]) VALUES (1045, 12, N'2023-5-1  ', 143200, NULL, N'6 Đường Nguyễn Công Trứ, TP. Hạ Long, Quảng Ninh                                                                                                      ', N'1231231231', 1, N'                                                                                                                                                                                                                                                          ', N'Trần Hải Đăng', 10000, NULL)
INSERT [dbo].[orders] ([oid], [acid], [ordered_at], [TotalAmount], [created_by], [address], [phonenumber], [status], [note], [receiver], [discount], [enddate]) VALUES (1046, 12, N'2023-5-1  ', 549600, NULL, N'Tỉnh Cao Bằng, Thành phố Cao Bằng, Phường Sông Hiến, xóm 9                                                                                            ', N'0334343519', 3, N'                                                                                                                                                                                                                                                          ', N'Tuân Cụtte', 40000, N'2023-10-30')
INSERT [dbo].[orders] ([oid], [acid], [ordered_at], [TotalAmount], [created_by], [address], [phonenumber], [status], [note], [receiver], [discount], [enddate]) VALUES (1047, 12, N'2023-4-1  ', 659000, NULL, N'Tỉnh Vĩnh Phúc, Thành phố Phúc Yên, Phường Nam Viêm, xóm 9                                                                                            ', N'0334343519', 3, N'                                                                                                                                                                                                                                                          ', N'Lương Hự', 0, N'2023-10-25')
INSERT [dbo].[orders] ([oid], [acid], [ordered_at], [TotalAmount], [created_by], [address], [phonenumber], [status], [note], [receiver], [discount], [enddate]) VALUES (1048, 12, N'2023-4-1  ', 240000, NULL, N'Tỉnh Vĩnh Phúc, Thành phố Phúc Yên, Phường Nam Viêm, xóm 9                                                                                            ', N'0334343519', 2, N'                                                                                                                                                                                                                                                          ', N'Lương Hự', 20000, N'2023-10-25')
INSERT [dbo].[orders] ([oid], [acid], [ordered_at], [TotalAmount], [created_by], [address], [phonenumber], [status], [note], [receiver], [discount], [enddate]) VALUES (1049, 12, N'2023-4-1  ', 228000, NULL, N'Tỉnh Phú Thọ, Huyện Thanh Sơn, Xã Tân Lập, Xom 2                                                                                                      ', N'0334343520', 3, N'                                                                                                                                                                                                                                                          ', N'Hỏa Cự', 20000, N'2023-10-25')
INSERT [dbo].[orders] ([oid], [acid], [ordered_at], [TotalAmount], [created_by], [address], [phonenumber], [status], [note], [receiver], [discount], [enddate]) VALUES (1050, 12, N'2023-4-1  ', 459000, NULL, N'Thành phố Hải Phòng, Huyện Vĩnh Bảo, Xã Hưng Nhân, 123                                                                                                ', N'0334343519', 1, N'                                                                                                                                                                                                                                                          ', N'Đạt hoa đa', 0, NULL)
INSERT [dbo].[orders] ([oid], [acid], [ordered_at], [TotalAmount], [created_by], [address], [phonenumber], [status], [note], [receiver], [discount], [enddate]) VALUES (1051, 12, N'2023-4-1  ', 699000, NULL, N'Tỉnh Cao Bằng, Thành phố Cao Bằng, Phường Sông Hiến, xóm 9                                                                                            ', N'0334343519', 1, N'                                                                                                                                                                                                                                                          ', N'Tuân Cụtte', 10000, NULL)
INSERT [dbo].[orders] ([oid], [acid], [ordered_at], [TotalAmount], [created_by], [address], [phonenumber], [status], [note], [receiver], [discount], [enddate]) VALUES (1052, 4, N'2023-4-1  ', 459000, NULL, N'Tỉnh Cao Bằng, Thành phố Cao Bằng, Phường Sông Hiến, xóm 9                                                                                            ', N'0334343519', 3, N'                                                                                                                                                                                                                                                          ', N'Tuân Cụtte', 0, N'2023-10-29')
INSERT [dbo].[orders] ([oid], [acid], [ordered_at], [TotalAmount], [created_by], [address], [phonenumber], [status], [note], [receiver], [discount], [enddate]) VALUES (1053, 4, N'2023-4-1  ', 228000, NULL, N'Tỉnh Cao Bằng, Thành phố Cao Bằng, Phường Sông Hiến, xóm 9                                                                                            ', N'0334343519', 2, N'                                                                                                                                                                                                                                                          ', N'Tuân Cụtte', 10000, N'2023-11-01')
INSERT [dbo].[orders] ([oid], [acid], [ordered_at], [TotalAmount], [created_by], [address], [phonenumber], [status], [note], [receiver], [discount], [enddate]) VALUES (1054, 4, N'2023-4-1  ', 127200, NULL, N'Tỉnh Cao Bằng, Thành phố Cao Bằng, Phường Sông Hiến, xóm 9                                                                                            ', N'0334343519', 2, N'                                                                                                                                                                                                                                                          ', N'Tuân Cụtte', 0, N'2023-10-28')
INSERT [dbo].[orders] ([oid], [acid], [ordered_at], [TotalAmount], [created_by], [address], [phonenumber], [status], [note], [receiver], [discount], [enddate]) VALUES (1055, 12, N'2023-4-1  ', 228000, NULL, N'Tỉnh Phú Thọ, Huyện Thanh Sơn, Xã Tân Lập, Xom 2                                                                                                      ', N'0334343520', 3, N'                                                                                                                                                                                                                                                          ', N'Hỏa Cự', 0, N'2023-10-25')
INSERT [dbo].[orders] ([oid], [acid], [ordered_at], [TotalAmount], [created_by], [address], [phonenumber], [status], [note], [receiver], [discount], [enddate]) VALUES (2050, 4, N'2023-10-15', 1420033, NULL, N'Tỉnh Bắc Ninh, Huyện Yên Phong, Xã Đông Phong, Xom 1                                                                                                  ', N'0334343519', 2, N'                                                                                                                                                                                                                                                          ', N'Tuân Cụtte', 0, N'2023-11-01')
INSERT [dbo].[orders] ([oid], [acid], [ordered_at], [TotalAmount], [created_by], [address], [phonenumber], [status], [note], [receiver], [discount], [enddate]) VALUES (2051, 4, N'2023-10-15', 798400, NULL, N'Tỉnh Bắc Ninh, Huyện Yên Phong, Xã Đông Phong, Xom 1                                                                                                  ', N'0334343519', 2, N'                                                                                                                                                                                                                                                          ', N'Tuân Cụtte', 0, N'2023-10-29')
INSERT [dbo].[orders] ([oid], [acid], [ordered_at], [TotalAmount], [created_by], [address], [phonenumber], [status], [note], [receiver], [discount], [enddate]) VALUES (2052, 8, N'2023-10-15', 300000, NULL, N'Tỉnh Lai Châu, Huyện Sìn Hồ, Xã Chăn Nưa, =========                                                                                                   ', N'0987654321', 2, N'                                                                                                                                                                                                                                                          ', N'========', 0, N'2023-10-28')
INSERT [dbo].[orders] ([oid], [acid], [ordered_at], [TotalAmount], [created_by], [address], [phonenumber], [status], [note], [receiver], [discount], [enddate]) VALUES (2053, 8, N'2023-10-15', 300000, NULL, N'Tỉnh Lai Châu, Huyện Sìn Hồ, Xã Chăn Nưa, =========                                                                                                   ', N'0987654321', 2, N'                                                                                                                                                                                                                                                          ', N'========', 0, N'2023-10-25')
INSERT [dbo].[orders] ([oid], [acid], [ordered_at], [TotalAmount], [created_by], [address], [phonenumber], [status], [note], [receiver], [discount], [enddate]) VALUES (2054, 4, N'2023-10-15', 300000, NULL, N'Tỉnh Bắc Ninh, Huyện Yên Phong, Xã Đông Phong, Xom 1                                                                                                  ', N'0334343519', 2, N'                                                                                                                                                                                                                                                          ', N'Tuân Cụtte', 0, N'2023-10-29')
INSERT [dbo].[orders] ([oid], [acid], [ordered_at], [TotalAmount], [created_by], [address], [phonenumber], [status], [note], [receiver], [discount], [enddate]) VALUES (2055, 4, N'2023-10-15', 300000, NULL, N'Tỉnh Bắc Ninh, Huyện Yên Phong, Xã Đông Phong, Xom 1                                                                                                  ', N'0334343519', 2, N'                                                                                                                                                                                                                                                          ', N'Tuân Cụtte', 0, N'2023-10-25')
INSERT [dbo].[orders] ([oid], [acid], [ordered_at], [TotalAmount], [created_by], [address], [phonenumber], [status], [note], [receiver], [discount], [enddate]) VALUES (2056, 4, N'2023-10-15', 300000, NULL, N'Tỉnh Bắc Ninh, Huyện Yên Phong, Xã Đông Phong, Xom 1                                                                                                  ', N'0334343519', 3, N'                                                                                                                                                                                                                                                          ', N'Tuân Cụtte', 0, N'2023-10-25')
INSERT [dbo].[orders] ([oid], [acid], [ordered_at], [TotalAmount], [created_by], [address], [phonenumber], [status], [note], [receiver], [discount], [enddate]) VALUES (2057, 8, N'2023-10-15', 228000, NULL, N'Tỉnh Hoà Bình, Huyện Lương Sơn, Xã Lâm Sơn, Xom 1                                                                                                     ', N'0334343619', 3, N'                                                                                                                                                                                                                                                          ', N'Van An', 0, N'2023-10-25')
INSERT [dbo].[orders] ([oid], [acid], [ordered_at], [TotalAmount], [created_by], [address], [phonenumber], [status], [note], [receiver], [discount], [enddate]) VALUES (2058, 11, N'2023-11-01', 993200, NULL, N'Tỉnh Hà Giang, Huyện Yên Minh, Xã Phú Lũng, Xom 1                                                                                                     ', N'0334343619', 0, N'                                                                                                                                                                                                                                                          ', N'Hòa Bì', 0, NULL)
INSERT [dbo].[orders] ([oid], [acid], [ordered_at], [TotalAmount], [created_by], [address], [phonenumber], [status], [note], [receiver], [discount], [enddate]) VALUES (2059, 12, N'2023-11-02', 300000, NULL, N'Tỉnh Hải Dương, Huyện Tứ Kỳ, Xã Phượng Kỳ, Xon 1                                                                                                      ', N'0334343619', 2, N'                                                                                                                                                                                                                                                          ', N'Hoa bi', 0, N'2023-11-02')
SET IDENTITY_INSERT [dbo].[orders] OFF
GO
SET IDENTITY_INSERT [dbo].[ProductCategory] ON 

INSERT [dbo].[ProductCategory] ([capid], [caid], [pid]) VALUES (1, 1, 2)
INSERT [dbo].[ProductCategory] ([capid], [caid], [pid]) VALUES (2, 2, 2)
INSERT [dbo].[ProductCategory] ([capid], [caid], [pid]) VALUES (3, 5, 2)
INSERT [dbo].[ProductCategory] ([capid], [caid], [pid]) VALUES (4, 1, 3)
INSERT [dbo].[ProductCategory] ([capid], [caid], [pid]) VALUES (5, 4, 3)
INSERT [dbo].[ProductCategory] ([capid], [caid], [pid]) VALUES (6, 6, 3)
INSERT [dbo].[ProductCategory] ([capid], [caid], [pid]) VALUES (7, 1, 4)
INSERT [dbo].[ProductCategory] ([capid], [caid], [pid]) VALUES (8, 2, 4)
INSERT [dbo].[ProductCategory] ([capid], [caid], [pid]) VALUES (9, 5, 4)
INSERT [dbo].[ProductCategory] ([capid], [caid], [pid]) VALUES (10, 1, 5)
INSERT [dbo].[ProductCategory] ([capid], [caid], [pid]) VALUES (11, 3, 5)
INSERT [dbo].[ProductCategory] ([capid], [caid], [pid]) VALUES (12, 6, 5)
INSERT [dbo].[ProductCategory] ([capid], [caid], [pid]) VALUES (13, 1, 6)
INSERT [dbo].[ProductCategory] ([capid], [caid], [pid]) VALUES (14, 2, 6)
INSERT [dbo].[ProductCategory] ([capid], [caid], [pid]) VALUES (15, 5, 6)
INSERT [dbo].[ProductCategory] ([capid], [caid], [pid]) VALUES (16, 1, 7)
INSERT [dbo].[ProductCategory] ([capid], [caid], [pid]) VALUES (17, 3, 7)
INSERT [dbo].[ProductCategory] ([capid], [caid], [pid]) VALUES (18, 3, 7)
INSERT [dbo].[ProductCategory] ([capid], [caid], [pid]) VALUES (19, 1, 1)
INSERT [dbo].[ProductCategory] ([capid], [caid], [pid]) VALUES (20, 3, 4)
INSERT [dbo].[ProductCategory] ([capid], [caid], [pid]) VALUES (21, 7, 6)
INSERT [dbo].[ProductCategory] ([capid], [caid], [pid]) VALUES (22, 7, 13)
INSERT [dbo].[ProductCategory] ([capid], [caid], [pid]) VALUES (1001, 4, 39)
INSERT [dbo].[ProductCategory] ([capid], [caid], [pid]) VALUES (1002, 5, 1040)
INSERT [dbo].[ProductCategory] ([capid], [caid], [pid]) VALUES (2001, 2, 1041)
INSERT [dbo].[ProductCategory] ([capid], [caid], [pid]) VALUES (2007, 1, 1041)
INSERT [dbo].[ProductCategory] ([capid], [caid], [pid]) VALUES (2009, 2, 1041)
INSERT [dbo].[ProductCategory] ([capid], [caid], [pid]) VALUES (3001, 1, 1041)
INSERT [dbo].[ProductCategory] ([capid], [caid], [pid]) VALUES (3002, 1, 3040)
SET IDENTITY_INSERT [dbo].[ProductCategory] OFF
GO
SET IDENTITY_INSERT [dbo].[ProductDetail] ON 

INSERT [dbo].[ProductDetail] ([pdid], [numplayer], [requiredAge], [description], [rules], [timeplay], [pid]) VALUES (1, 2, 12, N'Những chiếc bánh ngọt đã sẵn sàng và chỉ còn công đoạn cuối cùng: trang trí. Là một học viên của Học Viện Bánh Ngọt, nhiệm vụ của bạn là hoàn thành việc trang trí chúng trong khoảng thời gian giới hạn. Dĩ nhiên, bạn cần phải phối hợp với các thành viên trong nhóm để đạt kết quả tốt nhất! Đơn giản thật đấy nhưng bạn sẽ phát hiện ra rằng đây là không phải là trang trí bánh ngọt đơn thuần mà là cả một nghệ thuật!', N'Luật chơi sản phẩm 1', N'60', 1)
INSERT [dbo].[ProductDetail] ([pdid], [numplayer], [requiredAge], [description], [rules], [timeplay], [pid]) VALUES (2, 3, 9, N'Xếp gỗ tạo hình hấp dẫn - Có thể tạo đến hơn 50 hình khác nhau', N'Lu?t choi s?n ph?m 2', N'90', 2)
INSERT [dbo].[ProductDetail] ([pdid], [numplayer], [requiredAge], [description], [rules], [timeplay], [pid]) VALUES (3, 2, 7, N'Trong A Feast for Odin, bạn sẽ là những nhà thám hiểm vĩ đại, đồng thời cũng là người lãnh đạo bộ tộc của mình và duy trì sự ổn định trong cộng đồng. Điều này bao gồm cả việc trang bị vũ khí, mở rộng lãnh thổ song song với sản xuất nông nghiệp và giao thương để phát triển.', N'Luật chơi sản phẩm 3', N'45', 4)
INSERT [dbo].[ProductDetail] ([pdid], [numplayer], [requiredAge], [description], [rules], [timeplay], [pid]) VALUES (4, 6, 8, N'Lớp Học Mật Ngữ là một trò chơi cực kỳ dễ thương và đặc sắc được sáng tạo từ chính nhóm tác giả B.R.O, bộ board game được chuyển thể từ truyện tranh cùng tên, Best Seller 2016-2018 tại Fahasa và là một trong 10 tựa sách được yêu thích nhất 2018. Được phát hành bởi BoardgameVN và Review bởi Time Sun See Studio, Toy Station, Comicola, Thơ Nguyễn', N'Luật chơi sản phẩm 3', N'100', 5)
INSERT [dbo].[ProductDetail] ([pdid], [numplayer], [requiredAge], [description], [rules], [timeplay], [pid]) VALUES (5, 4, 12, N'Trong thế giới ngầm bạn luôn phải đón nhận những rủi ro vào mọi lúc, hãy chuẩn bị để gài bom đối phương trước khi chính bạn bị phát nổ. Chỉ người sống sót cuối cùng giành chiến thắng.', N'Luật chơi sản phẩm 3', N'60', 6)
INSERT [dbo].[ProductDetail] ([pdid], [numplayer], [requiredAge], [description], [rules], [timeplay], [pid]) VALUES (6, 4, 12, N'Trong thế giới ngầm bạn luôn phải đón nhận những rủi ro vào mọi lúc, hãy chuẩn bị để gài bom đối phương trước khi chính bạn bị phát nổ. Chỉ người sống sót cuối cùng giành chiến thắng.', N'Luật chơi sản phẩm 3', N'60', 7)
INSERT [dbo].[ProductDetail] ([pdid], [numplayer], [requiredAge], [description], [rules], [timeplay], [pid]) VALUES (7, 2, 12, N'Những người “NÊN” chơi Splendor ;Nếu bạn là Phụ huynh thì Splendor sẽ là board game hấp dẫn không thể thiếu để chơi cùng con để giải trí, gắn kết các thành viên trong gia đình và rèn luyện kỹ năng tư duy logic, khả năng ghi nhớ cho con từ 10 tuổi.', N'Luật chơi sản phẩm 3', N'60', 8)
INSERT [dbo].[ProductDetail] ([pdid], [numplayer], [requiredAge], [description], [rules], [timeplay], [pid]) VALUES (8, 5, 12, N'Hội nghị khoa học lớn nhất năm đang đến gần! Tất cả những nhà khoa học lỗi lạc nhất đều đã tụ hội ở nơi đây, sẵn sàng trình diễn và tạo ra những cỗ máy mê hoặc lòng người. Tất cả vì mục tiêu trở thành nhà khoa học vĩ đại nhất, và tạo ra những cỗ máy tối thượng!.', N'Luật chơi sản phẩm 3', N'60', 9)
INSERT [dbo].[ProductDetail] ([pdid], [numplayer], [requiredAge], [description], [rules], [timeplay], [pid]) VALUES (9, 4, 11, N'SLEEVE BỌC BÀI GIÚP BẢO VỆ THẺ BÀI CỦA BẠN NHƯ MỚI! Nhiều người mua board game thẻ bài về nhưng lại không bọc Sleeves để giữ gìn, kết quả gặp phải các vấn đề như bài bị dính nước, trầy xước (lộ mặt sau), rách rưới khiến cho toàn bộ bài hư hỏng không thể chơi được nữa chỉ sau thời gian ngắn. ', N'Luật chơi sản phẩm 3', N'60', 10)
INSERT [dbo].[ProductDetail] ([pdid], [numplayer], [requiredAge], [description], [rules], [timeplay], [pid]) VALUES (10, 4, 11, N'SLEEVE BỌC BÀI GIÚP BẢO VỆ THẺ BÀI CỦA BẠN NHƯ MỚI! Nhiều người mua board game thẻ bài về nhưng lại không bọc Sleeves để giữ gìn, kết quả gặp phải các vấn đề như bài bị dính nước, trầy xước (lộ mặt sau), rách rưới khiến cho toàn bộ bài hư hỏng không thể chơi được nữa chỉ sau thời gian ngắn. ', N'Luật chơi sản phẩm 3', N'60', 11)
INSERT [dbo].[ProductDetail] ([pdid], [numplayer], [requiredAge], [description], [rules], [timeplay], [pid]) VALUES (11, 2, 12, N'Một cuộc chiến không khoan nhượng để lấp đầy chiếc dạ dày đang kêu gào thảm thiết của bạn. Trò chơi gồm bốn loại món ăn khác nhau, Mexico, Đông Á, Địa Trung Hải và Nam Á - mỗi loại có một thẻ bánh pudding giúp bạn ghi thêm điểm. Đây là một trò chơi không thể không trải nghiệm nếu bạn đam mê ẩm thực', N'Luật chơi sản phẩm 3', N'60', 12)
INSERT [dbo].[ProductDetail] ([pdid], [numplayer], [requiredAge], [description], [rules], [timeplay], [pid]) VALUES (12, 1, 15, N'Lầy phiên bản Lội - Phiên bản mở rộng đầu tiên từ party game LẦY.Tổ hợp 23 lá chức năng mới toanh, giúp bạn rũ bỏ bớt cơ số bài trên tay.Bái bai việc lụt lội trong biển bài, thời đã đến, thả trôi hết bài trên tay trong sung sướng thôi nào!!', N'Luật chơi sản phẩm 3', N'23', 13)
INSERT [dbo].[ProductDetail] ([pdid], [numplayer], [requiredAge], [description], [rules], [timeplay], [pid]) VALUES (13, 10, 18, N'Phiên bản đặc biệt của game bài nổi tiếng Uno, đó chính là Bài Thính! Luật chơi tương tự như Uno thông thường nhưng thêm vào các lá bài đặc biệt có chức năng như ra lệnh, đặt một chủ để, xoay vòng, uống một ly nước, hoặc thậm chí ép phải .... kiss người khác!', N'Luật chơi sản phẩm 3', N'120', 13)
INSERT [dbo].[ProductDetail] ([pdid], [numplayer], [requiredAge], [description], [rules], [timeplay], [pid]) VALUES (15, 6, 18, N'Game hay', N'Game hay', N'30', 39)
INSERT [dbo].[ProductDetail] ([pdid], [numplayer], [requiredAge], [description], [rules], [timeplay], [pid]) VALUES (1016, 2, 11, N'Game thú vị', N'Game thú vị', N'30', 1040)
INSERT [dbo].[ProductDetail] ([pdid], [numplayer], [requiredAge], [description], [rules], [timeplay], [pid]) VALUES (1017, 2, 18, N'Game chiến thuật đa dạng', N'Phong phú ', N'30', 1041)
INSERT [dbo].[ProductDetail] ([pdid], [numplayer], [requiredAge], [description], [rules], [timeplay], [pid]) VALUES (3016, 1, 1, N'1', N'1', N'1', 3040)
INSERT [dbo].[ProductDetail] ([pdid], [numplayer], [requiredAge], [description], [rules], [timeplay], [pid]) VALUES (3017, 2, 18, N'aaa', N'aaaa', N'30', 3041)
SET IDENTITY_INSERT [dbo].[ProductDetail] OFF
GO
SET IDENTITY_INSERT [dbo].[Products] ON 

INSERT [dbo].[Products] ([pid], [pname], [rate], [price], [img], [priceSale], [quantity], [pubid], [saled], [isDiscount], [isSoldout], [created_at], [status]) VALUES (1, N'Lớp Học Mật Ngữ - Siêu Thú Ngân Hà', 4.5, 300000, N'7cq1640081325_1.jpg', NULL, 989, NULL, 991, NULL, NULL, NULL, 1)
INSERT [dbo].[Products] ([pid], [pname], [rate], [price], [img], [priceSale], [quantity], [pubid], [saled], [isDiscount], [isSoldout], [created_at], [status]) VALUES (2, N'Lớp Học Mật Ngữ - Cuộc đua sao chổi', 4.5, 459000, N'productImg/p2.png', NULL, 996, NULL, 20, NULL, NULL, NULL, 1)
INSERT [dbo].[Products] ([pid], [pname], [rate], [price], [img], [priceSale], [quantity], [pubid], [saled], [isDiscount], [isSoldout], [created_at], [status]) VALUES (3, N'Meow - Game vui dành cho cả nhà', 4.5, 159000, N'productImg/p3.png', 127200, 998, NULL, 1000, 1, NULL, NULL, 1)
INSERT [dbo].[Products] ([pid], [pname], [rate], [price], [img], [priceSale], [quantity], [pubid], [saled], [isDiscount], [isSoldout], [created_at], [status]) VALUES (4, N'Tinh Mắt Bắt Xe', 4.5, 285000, N'productImg/p4.png', 228000, 997, NULL, 999, 1, NULL, NULL, 1)
INSERT [dbo].[Products] ([pid], [pname], [rate], [price], [img], [priceSale], [quantity], [pubid], [saled], [isDiscount], [isSoldout], [created_at], [status]) VALUES (5, N'Mooncake Master', 4.5, 300000, N'productImg/p5.png', 240000, 998, NULL, 20, 1, NULL, NULL, 1)
INSERT [dbo].[Products] ([pid], [pname], [rate], [price], [img], [priceSale], [quantity], [pubid], [saled], [isDiscount], [isSoldout], [created_at], [status]) VALUES (6, N'Câu Cá Sắc Màu', 4.5, 285000, N'productImg/p1.png', 228000, 997, NULL, 999, 1, NULL, NULL, 1)
INSERT [dbo].[Products] ([pid], [pname], [rate], [price], [img], [priceSale], [quantity], [pubid], [saled], [isDiscount], [isSoldout], [created_at], [status]) VALUES (7, N'Red7', 4.5, 159000, N'productImg/p7.png', 127200, 999, NULL, 20, 1, NULL, NULL, 1)
INSERT [dbo].[Products] ([pid], [pname], [rate], [price], [img], [priceSale], [quantity], [pubid], [saled], [isDiscount], [isSoldout], [created_at], [status]) VALUES (8, N'Star Wars: Edge Of Darkness Expansion', 4.5, 250000, N'productImg/p8.png', 200000, 995, NULL, 20, 1, NULL, NULL, 1)
INSERT [dbo].[Products] ([pid], [pname], [rate], [price], [img], [priceSale], [quantity], [pubid], [saled], [isDiscount], [isSoldout], [created_at], [status]) VALUES (9, N'THƯƠNG VỤ RAU CỦ', 4.5, 139000, N'productImg/p9.png', 111200, 997, NULL, 20, 1, NULL, NULL, 1)
INSERT [dbo].[Products] ([pid], [pname], [rate], [price], [img], [priceSale], [quantity], [pubid], [saled], [isDiscount], [isSoldout], [created_at], [status]) VALUES (10, N'Battle Line - Chiến Thần Vũ Trụ', 4.5, 179000, N'2nd1620193444_1.png', 143200, 100, NULL, 10, 1, NULL, NULL, 1)
INSERT [dbo].[Products] ([pid], [pname], [rate], [price], [img], [priceSale], [quantity], [pubid], [saled], [isDiscount], [isSoldout], [created_at], [status]) VALUES (11, N'COUP - Cuộc chiến vương quyền', 4.5, 275000, N'productImg/p11.png', 220000, 1000, NULL, 20, 1, NULL, NULL, 1)
INSERT [dbo].[Products] ([pid], [pname], [rate], [price], [img], [priceSale], [quantity], [pubid], [saled], [isDiscount], [isSoldout], [created_at], [status]) VALUES (12, N'Trò chơi Cờ Ô Chữ - Phá vỡ giới hạn vốn từ của bạn', 4.5, 998000, N'productImg/p12.png', 798400, 999, NULL, 20, 1, NULL, NULL, 1)
INSERT [dbo].[Products] ([pid], [pname], [rate], [price], [img], [priceSale], [quantity], [pubid], [saled], [isDiscount], [isSoldout], [created_at], [status]) VALUES (13, N'Bài Lầy - Party game Lầy nhất hệ mặt trời', 4.5, 129000, N'productImg/p13.png', 103200, 1000, NULL, 20, 1, NULL, NULL, 1)
INSERT [dbo].[Products] ([pid], [pname], [rate], [price], [img], [priceSale], [quantity], [pubid], [saled], [isDiscount], [isSoldout], [created_at], [status]) VALUES (14, N'Yêu Nhầm F.A', 4.5, 305000, N'productImg/p14.png', 244000, 1000, NULL, 20, 1, NULL, NULL, 1)
INSERT [dbo].[Products] ([pid], [pname], [rate], [price], [img], [priceSale], [quantity], [pubid], [saled], [isDiscount], [isSoldout], [created_at], [status]) VALUES (15, N'Bom Lắc 1 - Trò chơi định mệnh', 4.5, 115000, N'productImg/p15.png', NULL, 1000, NULL, 20, NULL, NULL, NULL, 1)
INSERT [dbo].[Products] ([pid], [pname], [rate], [price], [img], [priceSale], [quantity], [pubid], [saled], [isDiscount], [isSoldout], [created_at], [status]) VALUES (16, N'Touch It! - Chạm và đoán xem ai nào', 4.5, 179000, N'productImg/p16.png', NULL, 1000, NULL, 20, NULL, NULL, NULL, 1)
INSERT [dbo].[Products] ([pid], [pname], [rate], [price], [img], [priceSale], [quantity], [pubid], [saled], [isDiscount], [isSoldout], [created_at], [status]) VALUES (17, N'Kho Báu Vua Hải Tặc', 4.5, 300000, N'productImg/p17.png', NULL, 1000, NULL, 20, NULL, NULL, NULL, 1)
INSERT [dbo].[Products] ([pid], [pname], [rate], [price], [img], [priceSale], [quantity], [pubid], [saled], [isDiscount], [isSoldout], [created_at], [status]) VALUES (18, N'WOLFOO- NHẢY BAO BỐ', 4.5, 499000, N'productImg/p18.png', NULL, 1000, NULL, 20, NULL, NULL, NULL, 1)
INSERT [dbo].[Products] ([pid], [pname], [rate], [price], [img], [priceSale], [quantity], [pubid], [saled], [isDiscount], [isSoldout], [created_at], [status]) VALUES (19, N'Xếp hình/Puzzle Mato - Chủ đề: Vui vẻ không quạo', 4.5, 185000, N'productImg/p19.png', NULL, 1000, NULL, 20, NULL, NULL, NULL, 1)
INSERT [dbo].[Products] ([pid], [pname], [rate], [price], [img], [priceSale], [quantity], [pubid], [saled], [isDiscount], [isSoldout], [created_at], [status]) VALUES (20, N'Xếp hình/Puzzle Mato - Chủ đề: Khu chợ nhộn nhịp', 4.5, 185000, N'productImg/p20.png', NULL, 1000, NULL, 20, NULL, NULL, NULL, 1)
INSERT [dbo].[Products] ([pid], [pname], [rate], [price], [img], [priceSale], [quantity], [pubid], [saled], [isDiscount], [isSoldout], [created_at], [status]) VALUES (21, N'Xếp hình Puzzle Mato - chủ đề Tiệm sửa xe bá đạo', 4.5, 185000, N'productImg/p21.png', NULL, 1000, NULL, 20, NULL, NULL, NULL, 1)
INSERT [dbo].[Products] ([pid], [pname], [rate], [price], [img], [priceSale], [quantity], [pubid], [saled], [isDiscount], [isSoldout], [created_at], [status]) VALUES (22, N'Xếp hình/Puzzle Mato - Chủ đề: Ở nhà vẫn vui', 4.5, 185000, N'productImg/p22.png', NULL, 1000, NULL, 20, NULL, NULL, NULL, 1)
INSERT [dbo].[Products] ([pid], [pname], [rate], [price], [img], [priceSale], [quantity], [pubid], [saled], [isDiscount], [isSoldout], [created_at], [status]) VALUES (23, N'Difference (US)', 4.5, 135000, N'productImg/p23.png', NULL, 1000, NULL, 20, NULL, NULL, NULL, 1)
INSERT [dbo].[Products] ([pid], [pname], [rate], [price], [img], [priceSale], [quantity], [pubid], [saled], [isDiscount], [isSoldout], [created_at], [status]) VALUES (24, N'Lotus (US)', 4.5, 860000, N'productImg/p24.png', NULL, 1000, NULL, 20, NULL, NULL, NULL, 1)
INSERT [dbo].[Products] ([pid], [pname], [rate], [price], [img], [priceSale], [quantity], [pubid], [saled], [isDiscount], [isSoldout], [created_at], [status]) VALUES (25, N'New York Slice (US)', 4.5, 1100000, N'productImg/p25.png', NULL, 1000, NULL, 20, NULL, NULL, NULL, 1)
INSERT [dbo].[Products] ([pid], [pname], [rate], [price], [img], [priceSale], [quantity], [pubid], [saled], [isDiscount], [isSoldout], [created_at], [status]) VALUES (26, N'Gobblet Gobblers (Plastic) (US)', 4.5, 550000, N'productImg/p26.png', NULL, 1000, NULL, 20, NULL, NULL, NULL, 1)
INSERT [dbo].[Products] ([pid], [pname], [rate], [price], [img], [priceSale], [quantity], [pubid], [saled], [isDiscount], [isSoldout], [created_at], [status]) VALUES (27, N'Slide Quest (US)', 4.5, 800000, N'productImg/p27.png', NULL, 1000, NULL, 20, NULL, NULL, NULL, 1)
INSERT [dbo].[Products] ([pid], [pname], [rate], [price], [img], [priceSale], [quantity], [pubid], [saled], [isDiscount], [isSoldout], [created_at], [status]) VALUES (28, N'ĐIỆP VIÊN SỐ DÁCH', 4.5, 350000, N'productImg/p28.png', NULL, 1000, NULL, 20, NULL, NULL, NULL, 1)
INSERT [dbo].[Products] ([pid], [pname], [rate], [price], [img], [priceSale], [quantity], [pubid], [saled], [isDiscount], [isSoldout], [created_at], [status]) VALUES (29, N'Splendor - Cuộc chiến đá quý', 4.5, 1100000, N'productImg/p29.png', NULL, 1000, NULL, 20, NULL, NULL, NULL, 1)
INSERT [dbo].[Products] ([pid], [pname], [rate], [price], [img], [priceSale], [quantity], [pubid], [saled], [isDiscount], [isSoldout], [created_at], [status]) VALUES (30, N'Avalon - Sứ Mệnh Hiệp Sĩ', 4.5, 350000, N'productImg/p30.png', NULL, 1000, NULL, 20, NULL, NULL, NULL, 1)
INSERT [dbo].[Products] ([pid], [pname], [rate], [price], [img], [priceSale], [quantity], [pubid], [saled], [isDiscount], [isSoldout], [created_at], [status]) VALUES (31, N'Lội - Bản mở rộng #1 từ party game Lầy', 4.5, 44000, N'productImg/p31.png', NULL, 1000, NULL, 20, NULL, NULL, NULL, 1)
INSERT [dbo].[Products] ([pid], [pname], [rate], [price], [img], [priceSale], [quantity], [pubid], [saled], [isDiscount], [isSoldout], [created_at], [status]) VALUES (32, N'Sleeves bọc bài 5.7 x 8.7 cm', 4.5, 20000, N'productImg/p32.png', NULL, 1000, NULL, 20, NULL, NULL, NULL, 1)
INSERT [dbo].[Products] ([pid], [pname], [rate], [price], [img], [priceSale], [quantity], [pubid], [saled], [isDiscount], [isSoldout], [created_at], [status]) VALUES (33, N'Sleeves bọc bài Siêu Thú Ngân Hà', 4.5, 55000, N'productImg/p33.png', NULL, 1000, NULL, 20, NULL, NULL, NULL, 1)
INSERT [dbo].[Products] ([pid], [pname], [rate], [price], [img], [priceSale], [quantity], [pubid], [saled], [isDiscount], [isSoldout], [created_at], [status]) VALUES (34, N'Sleeves bọc bài 6.5 x 9.0 cm', 4.5, 20000, N'productImg/p34.png', NULL, 1000, NULL, 20, NULL, NULL, NULL, 1)
INSERT [dbo].[Products] ([pid], [pname], [rate], [price], [img], [priceSale], [quantity], [pubid], [saled], [isDiscount], [isSoldout], [created_at], [status]) VALUES (35, N'Chuông Để Bàn', 4.5, 45000, N'productImg/p35.png', NULL, 1000, NULL, 20, NULL, NULL, NULL, 1)
INSERT [dbo].[Products] ([pid], [pname], [rate], [price], [img], [priceSale], [quantity], [pubid], [saled], [isDiscount], [isSoldout], [created_at], [status]) VALUES (36, N'Sleeves bọc bài 6.6x9.3 cm', 4.5, 25000, N'productImg/p36.png', NULL, 1000, NULL, 20, NULL, NULL, NULL, 1)
INSERT [dbo].[Products] ([pid], [pname], [rate], [price], [img], [priceSale], [quantity], [pubid], [saled], [isDiscount], [isSoldout], [created_at], [status]) VALUES (37, N'Sleeves bọc bài Ma sói Characters (8.2 x 8.2cm)', 4.5, 20000, N'productImg/p37.png', NULL, 1000, NULL, 20, NULL, NULL, NULL, 1)
INSERT [dbo].[Products] ([pid], [pname], [rate], [price], [img], [priceSale], [quantity], [pubid], [saled], [isDiscount], [isSoldout], [created_at], [status]) VALUES (39, N'Board game', NULL, 10000, N'4b11620195011.png', NULL, 1000, NULL, NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[Products] ([pid], [pname], [rate], [price], [img], [priceSale], [quantity], [pubid], [saled], [isDiscount], [isSoldout], [created_at], [status]) VALUES (1040, N'Board game thú vị', NULL, 110000, N'5_9.jpg', NULL, 99996, NULL, 99998, NULL, NULL, N'2023-10-20', 1)
INSERT [dbo].[Products] ([pid], [pname], [rate], [price], [img], [priceSale], [quantity], [pubid], [saled], [isDiscount], [isSoldout], [created_at], [status]) VALUES (1041, N'Board Game quân sự ', NULL, 110000, N'z1e1672976153_1.png', NULL, 0, NULL, NULL, NULL, NULL, N'2024-10-20', 0)
INSERT [dbo].[Products] ([pid], [pname], [rate], [price], [img], [priceSale], [quantity], [pubid], [saled], [isDiscount], [isSoldout], [created_at], [status]) VALUES (3040, N'Test success', NULL, 1111, N'1_9.jpg', NULL, 21, NULL, NULL, NULL, NULL, N'Nov  2 202', 1)
INSERT [dbo].[Products] ([pid], [pname], [rate], [price], [img], [priceSale], [quantity], [pubid], [saled], [isDiscount], [isSoldout], [created_at], [status]) VALUES (3041, N'aaaaa', NULL, 2000, N'0di1689512915_2.png', NULL, 1000, NULL, NULL, NULL, NULL, N'Nov  2 202', 1)
SET IDENTITY_INSERT [dbo].[Products] OFF
GO
SET IDENTITY_INSERT [dbo].[Publishers] ON 

INSERT [dbo].[Publishers] ([pubid], [pubname], [country], [material], [size], [weight], [pid]) VALUES (1, N'Nguyễn Trường', N'Việt Nam', N'Giấy', N'10x10x2 cm', N'1000g', 1)
INSERT [dbo].[Publishers] ([pubid], [pubname], [country], [material], [size], [weight], [pid]) VALUES (2, N'Nguyễn Lan', N'Việt Nam', N'Gỗ', N'10x10x3 cm', N'1200g', 1)
INSERT [dbo].[Publishers] ([pubid], [pubname], [country], [material], [size], [weight], [pid]) VALUES (3, N'Nguyễn Tuân', N'Việt Nam', N'Nhôm', N'10x10x1 cm', N'1100g', 2)
INSERT [dbo].[Publishers] ([pubid], [pubname], [country], [material], [size], [weight], [pid]) VALUES (4, N'Nguyễn Hải', N'Việt Nam', N'Đồng', N'6x10x4 cm', N'1040g', 2)
INSERT [dbo].[Publishers] ([pubid], [pubname], [country], [material], [size], [weight], [pid]) VALUES (5, N'Lê Kỳ', N'Việt Nam', N'Giấy', N'10x1x4 cm', N'100g', 4)
INSERT [dbo].[Publishers] ([pubid], [pubname], [country], [material], [size], [weight], [pid]) VALUES (6, N'Quang Vinh', N'Việt Nam', N'Nhựa', N'5x5x2 cm', N'1020g', 4)
INSERT [dbo].[Publishers] ([pubid], [pubname], [country], [material], [size], [weight], [pid]) VALUES (7, N'Công Minh', N'Việt Nam', N'Nhựa', N'3x4x5 cm', N'2200g', 5)
INSERT [dbo].[Publishers] ([pubid], [pubname], [country], [material], [size], [weight], [pid]) VALUES (8, N'Tiến Thành', N'Việt Nam', N'Kim cương', N'5x6x4 cm', N'4100g', 3)
INSERT [dbo].[Publishers] ([pubid], [pubname], [country], [material], [size], [weight], [pid]) VALUES (9, N'Mạnh Đạt', N'Việt Nam', N'Giấy', N'3x2x1 cm', N'1500g', 6)
INSERT [dbo].[Publishers] ([pubid], [pubname], [country], [material], [size], [weight], [pid]) VALUES (10, N'Vinh cute', N'Viet Nam', N'Giaays', N'Big', N'1000g', 1040)
INSERT [dbo].[Publishers] ([pubid], [pubname], [country], [material], [size], [weight], [pid]) VALUES (11, N'Vinh cute', N'Viet Nam', N'Giaays', N'Big', N'1000g', 1041)
INSERT [dbo].[Publishers] ([pubid], [pubname], [country], [material], [size], [weight], [pid]) VALUES (1010, N'Vinh cute', N'Viet Nam', N'Giay', N'Big', N'1000g', 2040)
INSERT [dbo].[Publishers] ([pubid], [pubname], [country], [material], [size], [weight], [pid]) VALUES (1011, N'Vinh cute', N'Viet Nam', N'Giay', N'Big', N'1000g', 2041)
INSERT [dbo].[Publishers] ([pubid], [pubname], [country], [material], [size], [weight], [pid]) VALUES (1012, N'Vinh cute', N'Viet Nam', N'Giay', N'Big', N'1000g', 2042)
INSERT [dbo].[Publishers] ([pubid], [pubname], [country], [material], [size], [weight], [pid]) VALUES (1013, N'Vinh cute', N'Viet Nam', N'Giay', N'Big', N'1000g', 2043)
INSERT [dbo].[Publishers] ([pubid], [pubname], [country], [material], [size], [weight], [pid]) VALUES (1014, N'Vinh cute', N'Viet Nam', N'Giay', N'Big', N'1000g', 2044)
INSERT [dbo].[Publishers] ([pubid], [pubname], [country], [material], [size], [weight], [pid]) VALUES (1015, N'Vinh cute', N'Viet Nam', N'Giay', N'Big', N'1000g', 2045)
INSERT [dbo].[Publishers] ([pubid], [pubname], [country], [material], [size], [weight], [pid]) VALUES (1016, N'Vinh cute', N'Viet Nam', N'Giay', N'Big', N'1000g', 2046)
INSERT [dbo].[Publishers] ([pubid], [pubname], [country], [material], [size], [weight], [pid]) VALUES (1017, N'Vinh cute', N'Viet Nam', N'Giay', N'Big', N'1000g', 2047)
INSERT [dbo].[Publishers] ([pubid], [pubname], [country], [material], [size], [weight], [pid]) VALUES (1018, N'Vinh cute', N'Viet Nam', N'Giay', N'Big', N'1000g', 2048)
INSERT [dbo].[Publishers] ([pubid], [pubname], [country], [material], [size], [weight], [pid]) VALUES (2010, N'1', N'1', N'Giay', N'1', N'1000g', 3040)
INSERT [dbo].[Publishers] ([pubid], [pubname], [country], [material], [size], [weight], [pid]) VALUES (2011, N'Vinh cute', N'Viet Nam', N'Giay', N'Big', N'1000g', 3041)
SET IDENTITY_INSERT [dbo].[Publishers] OFF
GO
SET IDENTITY_INSERT [dbo].[Role] ON 

INSERT [dbo].[Role] ([role], [Description]) VALUES (1, N'Quản trị viên')
INSERT [dbo].[Role] ([role], [Description]) VALUES (2, N'Saller')
INSERT [dbo].[Role] ([role], [Description]) VALUES (3, N'Maketing')
INSERT [dbo].[Role] ([role], [Description]) VALUES (4, N'Cusromer')
INSERT [dbo].[Role] ([role], [Description]) VALUES (5, N'Guest')
SET IDENTITY_INSERT [dbo].[Role] OFF
GO
SET IDENTITY_INSERT [dbo].[Setting] ON 

INSERT [dbo].[Setting] ([setting_id], [type], [order], [value], [description], [status]) VALUES (1, 1, 1, N'Board Game trẻ em (dưới 18+)', N'Board Game trẻ em (dưới 18+)', 0)
INSERT [dbo].[Setting] ([setting_id], [type], [order], [value], [description], [status]) VALUES (2, 1, 2, N'Board Game chiến thuật', N'Board Game chiến thuật', 1)
INSERT [dbo].[Setting] ([setting_id], [type], [order], [value], [description], [status]) VALUES (3, 1, 3, N'Board Game giải trí/nhóm', N'Board Game giải trí/nhóm', 1)
INSERT [dbo].[Setting] ([setting_id], [type], [order], [value], [description], [status]) VALUES (4, 1, 4, N'Board Game gia đình', N'Board Game gia đình', 1)
INSERT [dbo].[Setting] ([setting_id], [type], [order], [value], [description], [status]) VALUES (5, 1, 5, N'Board Game US', N'Board Game US', 1)
INSERT [dbo].[Setting] ([setting_id], [type], [order], [value], [description], [status]) VALUES (6, 1, 6, N'Board Game bán chạy', N'Board Game bán chạy', 0)
INSERT [dbo].[Setting] ([setting_id], [type], [order], [value], [description], [status]) VALUES (7, 1, 7, N'Phụ Kiện Board Game', N'Phụ Kiện Board Game', 1)
INSERT [dbo].[Setting] ([setting_id], [type], [order], [value], [description], [status]) VALUES (8, 2, 1, N'BoardGame có gì mới', N'BoardGame có gì mới', 1)
INSERT [dbo].[Setting] ([setting_id], [type], [order], [value], [description], [status]) VALUES (9, 3, 1, N'Đang gửi', N'Đang gửi', 1)
INSERT [dbo].[Setting] ([setting_id], [type], [order], [value], [description], [status]) VALUES (10, 3, 2, N'Thành công', N'Thành công', 1)
INSERT [dbo].[Setting] ([setting_id], [type], [order], [value], [description], [status]) VALUES (11, 3, 3, N'Đã hủy', N'Đã hủy', 1)
INSERT [dbo].[Setting] ([setting_id], [type], [order], [value], [description], [status]) VALUES (12, 4, 1, N'customer', N'customer', 1)
INSERT [dbo].[Setting] ([setting_id], [type], [order], [value], [description], [status]) VALUES (13, 4, 2, N'marketing', N'marketing', 1)
INSERT [dbo].[Setting] ([setting_id], [type], [order], [value], [description], [status]) VALUES (14, 4, 3, N'sale', N'sale', 1)
INSERT [dbo].[Setting] ([setting_id], [type], [order], [value], [description], [status]) VALUES (15, 4, 4, N'sale manager', N'sale manager', 1)
INSERT [dbo].[Setting] ([setting_id], [type], [order], [value], [description], [status]) VALUES (16, 4, 5, N'admin', N'admin', 1)
INSERT [dbo].[Setting] ([setting_id], [type], [order], [value], [description], [status]) VALUES (17, 4, 6, N'Chủ Shop', N'Chủ Shop', 1)
INSERT [dbo].[Setting] ([setting_id], [type], [order], [value], [description], [status]) VALUES (1002, 1, 1001, N'Board Game trẻ em (trên 18+)', N'Board Game trẻ em (trên 18+)', 1)
SET IDENTITY_INSERT [dbo].[Setting] OFF
GO
INSERT [dbo].[Setting_Type] ([setting_type_id], [setting_type_name]) VALUES (1, N'Danh mục sản phẩm')
INSERT [dbo].[Setting_Type] ([setting_type_id], [setting_type_name]) VALUES (2, N'Danh mục tin tức')
INSERT [dbo].[Setting_Type] ([setting_type_id], [setting_type_name]) VALUES (3, N'Trạng thái đơn hàng')
INSERT [dbo].[Setting_Type] ([setting_type_id], [setting_type_name]) VALUES (4, N'Vai trò')
GO
SET IDENTITY_INSERT [dbo].[ShipmentDetails] ON 

INSERT [dbo].[ShipmentDetails] ([aaid], [address], [phonenumber], [status], [acid], [receiver]) VALUES (1, N'3 Đường Nguyễn Du, Quận 1, TP. Hồ Chí Minh', N'0901234567', 0, 1, N'Vũ Mạnh Đạt')
INSERT [dbo].[ShipmentDetails] ([aaid], [address], [phonenumber], [status], [acid], [receiver]) VALUES (2, N'46 Đường Lê Lai, Quận 3, TP. Hồ Chí Minh', N'0987654321', 1, 2, N'Nguyễn Quang An')
INSERT [dbo].[ShipmentDetails] ([aaid], [address], [phonenumber], [status], [acid], [receiver]) VALUES (3, N'79 Đường Lý Tự Trọng, Quận 5, TP. Hồ Chí Minh', N'0912345678', 0, 3, N'Nguyễn A')
INSERT [dbo].[ShipmentDetails] ([aaid], [address], [phonenumber], [status], [acid], [receiver]) VALUES (4, N'21 Đường Nguyễn Đình Chính, Quận Phú Nhuận, TP. Hồ Chí Minh', N'0976543210', 0, 1, N'Nguyễn B')
INSERT [dbo].[ShipmentDetails] ([aaid], [address], [phonenumber], [status], [acid], [receiver]) VALUES (5, N'85 Đường Cách Mạng Tháng Tám, Quận 10, TP. Hồ Chí Minh', N'0888888532', 0, 2, N'Nguyễn C')
INSERT [dbo].[ShipmentDetails] ([aaid], [address], [phonenumber], [status], [acid], [receiver]) VALUES (6, N'8 Đường Hùng Vương, TP. Đà Nẵng', N'0945678901', 0, 3, N'Nguyễn D')
INSERT [dbo].[ShipmentDetails] ([aaid], [address], [phonenumber], [status], [acid], [receiver]) VALUES (7, N'7 Đường Trần Phú, TP. Hải Phòng', N'0965432109', 0, 1, N'Nguyễn E')
INSERT [dbo].[ShipmentDetails] ([aaid], [address], [phonenumber], [status], [acid], [receiver]) VALUES (8, N'99 Đường Ngô Quyền, Quận Hồng Bàng, TP. Hải Phòng', N'0912345658', 0, 2, N'Nguyễn G')
INSERT [dbo].[ShipmentDetails] ([aaid], [address], [phonenumber], [status], [acid], [receiver]) VALUES (9, N'6 Đường Nguyễn Công Trứ, TP. Hạ Long, Quảng Ninh', N'0976543215', 1, 3, N'Hà Duy')
INSERT [dbo].[ShipmentDetails] ([aaid], [address], [phonenumber], [status], [acid], [receiver]) VALUES (10, N'44 Đường Trần Hưng Đạo, TP. Hà Nội', N'09765432163', 0, 1, N'Thái Hoàng')
INSERT [dbo].[ShipmentDetails] ([aaid], [address], [phonenumber], [status], [acid], [receiver]) VALUES (3006, N'Tỉnh Bắc Kạn, Huyện Pác Nặm, Xã Bộc Bố, nhà 1', N'1231231231', 0, 10, N'Hòa La')
INSERT [dbo].[ShipmentDetails] ([aaid], [address], [phonenumber], [status], [acid], [receiver]) VALUES (3008, N'Tỉnh Hà Giang, Huyện Quản Bạ, Xã Bát Đại Sơn, 123123', N'1231231231', 1, 9, N'Tam Ba')
INSERT [dbo].[ShipmentDetails] ([aaid], [address], [phonenumber], [status], [acid], [receiver]) VALUES (3009, N'Tỉnh Vĩnh Phúc, Thành phố Phúc Yên, Phường Nam Viêm, xóm 9', N'0334343519', 0, 12, N'Lương Hự')
INSERT [dbo].[ShipmentDetails] ([aaid], [address], [phonenumber], [status], [acid], [receiver]) VALUES (3010, N'Tỉnh Hà Giang, Huyện Đồng Văn, Xã Lũng Thầu, 123123', N'1231231231', 1, 1011, N'Hóa Là')
INSERT [dbo].[ShipmentDetails] ([aaid], [address], [phonenumber], [status], [acid], [receiver]) VALUES (3013, N'Tỉnh Cao Bằng, Thành phố Cao Bằng, Phường Sông Hiến, xóm 9', N'0334343519', 0, 4, N'Tuân Cụtte')
INSERT [dbo].[ShipmentDetails] ([aaid], [address], [phonenumber], [status], [acid], [receiver]) VALUES (3014, N'Tỉnh Bắc Ninh, Huyện Yên Phong, Xã Đông Phong, Xom 1', N'0334343519', 0, 4, N'Tuân Cụtte')
INSERT [dbo].[ShipmentDetails] ([aaid], [address], [phonenumber], [status], [acid], [receiver]) VALUES (3015, N'Tỉnh Hà Giang, Huyện Mèo Vạc, Xã Pải Lủng, 123123', N'1231231231', 0, 4, N'Hòa Bì')
INSERT [dbo].[ShipmentDetails] ([aaid], [address], [phonenumber], [status], [acid], [receiver]) VALUES (3017, N'Thành phố Hải Phòng, Huyện Vĩnh Bảo, Xã Hưng Nhân, 123', N'0334343519', 0, 4, N'Đạt hoa đa')
INSERT [dbo].[ShipmentDetails] ([aaid], [address], [phonenumber], [status], [acid], [receiver]) VALUES (3018, N'Tỉnh Phú Thọ, Huyện Thanh Sơn, Xã Tân Lập, Xom 2', N'0334343520', 0, 12, N'Hỏa Cự')
INSERT [dbo].[ShipmentDetails] ([aaid], [address], [phonenumber], [status], [acid], [receiver]) VALUES (3019, N'Tỉnh Cao Bằng, Huyện Bảo Lâm, Xã Lý Bôn, a1', N'0334343519', 0, 12, N'Vinh')
INSERT [dbo].[ShipmentDetails] ([aaid], [address], [phonenumber], [status], [acid], [receiver]) VALUES (4021, N'Tỉnh Bắc Kạn, Huyện Pác Nặm, Xã Nhạn Môn, abc', N'1231231231', 1, 4, N'An Vu')
INSERT [dbo].[ShipmentDetails] ([aaid], [address], [phonenumber], [status], [acid], [receiver]) VALUES (5019, N'Tỉnh Hà Giang, Huyện Yên Minh, Xã Phú Lũng, Xom 1', N'0334343619', 1, 11, N'Hòa Bì')
INSERT [dbo].[ShipmentDetails] ([aaid], [address], [phonenumber], [status], [acid], [receiver]) VALUES (5020, N'Tỉnh Hải Dương, Huyện Tứ Kỳ, Xã Phượng Kỳ, Xon 1', N'0334343619', 1, 12, N'Hoa bi')
INSERT [dbo].[ShipmentDetails] ([aaid], [address], [phonenumber], [status], [acid], [receiver]) VALUES (5021, NULL, N'0123012312', NULL, 3019, NULL)
INSERT [dbo].[ShipmentDetails] ([aaid], [address], [phonenumber], [status], [acid], [receiver]) VALUES (5022, NULL, N'0123012312', NULL, 3020, NULL)
INSERT [dbo].[ShipmentDetails] ([aaid], [address], [phonenumber], [status], [acid], [receiver]) VALUES (5023, NULL, N'0334343619', NULL, 3022, NULL)
SET IDENTITY_INSERT [dbo].[ShipmentDetails] OFF
GO
ALTER TABLE [dbo].[Comments]  WITH CHECK ADD FOREIGN KEY([acid])
REFERENCES [dbo].[Accounts] ([acid])
GO
ALTER TABLE [dbo].[Comments]  WITH CHECK ADD FOREIGN KEY([pid])
REFERENCES [dbo].[Products] ([pid])
GO
ALTER TABLE [dbo].[News]  WITH CHECK ADD FOREIGN KEY([acid])
REFERENCES [dbo].[Accounts] ([acid])
GO
ALTER TABLE [dbo].[News]  WITH CHECK ADD FOREIGN KEY([canewId])
REFERENCES [dbo].[CategoriesNew] ([canewId])
GO
ALTER TABLE [dbo].[NewsDetails]  WITH CHECK ADD FOREIGN KEY([nid])
REFERENCES [dbo].[News] ([nid])
GO
ALTER TABLE [dbo].[orderDetails]  WITH CHECK ADD FOREIGN KEY([oid])
REFERENCES [dbo].[orders] ([oid])
GO
ALTER TABLE [dbo].[orderDetails]  WITH CHECK ADD FOREIGN KEY([pid])
REFERENCES [dbo].[Products] ([pid])
GO
ALTER TABLE [dbo].[orders]  WITH CHECK ADD FOREIGN KEY([acid])
REFERENCES [dbo].[Accounts] ([acid])
GO
ALTER TABLE [dbo].[ProductCategory]  WITH CHECK ADD  CONSTRAINT [FK_ProductCategory_Categories] FOREIGN KEY([caid])
REFERENCES [dbo].[Categories] ([caid])
GO
ALTER TABLE [dbo].[ProductCategory] CHECK CONSTRAINT [FK_ProductCategory_Categories]
GO
ALTER TABLE [dbo].[ProductCategory]  WITH CHECK ADD  CONSTRAINT [FK_ProductCategory_Products] FOREIGN KEY([pid])
REFERENCES [dbo].[Products] ([pid])
GO
ALTER TABLE [dbo].[ProductCategory] CHECK CONSTRAINT [FK_ProductCategory_Products]
GO
ALTER TABLE [dbo].[ProductDetail]  WITH CHECK ADD FOREIGN KEY([pid])
REFERENCES [dbo].[Products] ([pid])
GO
ALTER TABLE [dbo].[Products]  WITH CHECK ADD FOREIGN KEY([pubid])
REFERENCES [dbo].[Publishers] ([pubid])
GO
ALTER TABLE [dbo].[ShipmentDetails]  WITH CHECK ADD FOREIGN KEY([acid])
REFERENCES [dbo].[Accounts] ([acid])
GO
