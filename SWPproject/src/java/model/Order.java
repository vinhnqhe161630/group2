/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author HP
 */
@Builder
@Getter
@Setter
@ToString
public class Order {
    private int oid;
    private int acid;
    private String ordered_at;
    private int totalAmount;
    private String created_by;
     private String address;
      private String phonenumber;
      private int status;
      private String note;
      private String receiver;
      private int discount;
      private String enddate;
      
   
}
