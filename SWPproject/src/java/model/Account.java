/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author HP
 */
@Builder
@Getter
@Setter
@ToString
public class Account {



    private int acid;
    private String email;
    private int emailConfirm;
    private String password;
    private String username;
    private int role;
    private String img;
    private String created_at;
    private int status;

   

}
