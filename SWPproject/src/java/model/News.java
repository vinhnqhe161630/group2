/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author tuanm
 */
@Builder
@Getter
@Setter
@ToString
public class News {
    private int nid;
    private String title;
    private String img;
    private String createat;
    private int acid;
    private int canewId;
    
    
}
