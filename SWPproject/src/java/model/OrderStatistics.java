/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author tuanm
 */
public class OrderStatistics {

   private int year;
   private int month;
   private double totalAmount;

    public OrderStatistics() {
    }

    public OrderStatistics(int year, int month, double totalAmount) {
        this.year = year;
        this.month = month;
        this.totalAmount = totalAmount;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }
        @Override
    public String toString() {
        return "OrderStatistics [Year=" + year + ", Month=" + month + ", TotalAmount=" + totalAmount + "]";
    }
   
    
}
