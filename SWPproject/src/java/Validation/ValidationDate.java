/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Validation;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;


/**
 *
 * @author HP
 */
public class ValidationDate {
public  String convertDateFormat(String dateString) {
     if (dateString == null) {
            return null;
        }
         dateString = dateString.trim();

        if (dateString.isEmpty()) {
            return null; 
        }
        
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat outputFormat = new SimpleDateFormat("MM-dd-yyyy");
        try {
            Date date = inputFormat.parse(dateString);
            return outputFormat.format(date);
        } catch (Exception e) {
            e.printStackTrace();
            return "Invalid date format";
        }
    }






    public String getCurrentTime() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        String currentTime = formatter.format(date);
        return currentTime;

    }

    public static void main(String[] args) {
      
        ValidationDate ed = new ValidationDate();
        String currentTime = ed.getCurrentTime();
        System.out.println(currentTime);
        String a = "";
        a = ed.convertDateFormat("01-02-2023");
        System.out.println(a);
       
    }
}
