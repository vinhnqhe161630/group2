/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author HP
 */
public class ValidationString {
     public  String trimAndCheckBlank(String input) {
        if (input == null) {
            return null;
        }
        String trimmedInput = input.trim();

        if (trimmedInput.isEmpty()) {
            return null; 
        }
        return trimmedInput;
    }
      public boolean validatePhoneNumber(String phoneNumber) {
        // Remove any non-digit characters from the phone number
        if(phoneNumber==null){
            return false;
        }
        String cleanedNumber = phoneNumber.replaceAll("[^0-9]", "");

        return cleanedNumber.length() == 10;
    }
      public String checkValueIsNull(String input){
          if(input==null){
             return null; 
          }
          if(input.equals("null")){
          return null;
          }else{
              return input;
          }
          
      }
     

    
    public boolean isValidName(String username) {
        if(username==null){
             return false;
        }
    String sanitizedUsername = username.trim();
    int minLength = 3;
    int maxLength = 50;
    String specialCharacters = "[~!@#$%^&*()_+{}\":;'<>?,.\\[\\]]";
  

    if (sanitizedUsername.length() < minLength || sanitizedUsername.length() > maxLength) {
        return false;
    }

    if (sanitizedUsername.matches(".*" + specialCharacters + ".*")) {
        return false;
    }

    

    return true;
}
      public static String validateAndCensorContent(String content) {
        // Kiểm tra và thay thế các từ tục tĩu bằng dấu ***
        String censoredContent = censorInappropriateWords(content);
        // Thêm các quy tắc kiểm tra tùy chỉnh khác ở đây nếu cần
        return censoredContent;
    }

    private static String censorInappropriateWords(String content) {
        String[] inappropriateWords = {"tục_tĩu_word1", "tục_tĩu_word2", "tục_tĩu_word3","fuck", "damage","jiggered","die","damn","hell","chết", "đĩ"};

        for (String word : inappropriateWords) {
            String regex = "\\b" + Pattern.quote(word) + "\\b";
            Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(content);
            content = matcher.replaceAll("***");
        }

        return content;
    }
    public static void main(String[] args) {
        ValidationString t = new ValidationString();
        System.out.println(t.checkValueIsNull(null));
    }

   


}
