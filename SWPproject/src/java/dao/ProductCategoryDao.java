/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import context.DBContext;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.ProductCategory;
import model.ProductDetail;

/**
 *
 * @author tuanm
 */
public class ProductCategoryDao extends DBContext {

    public List<ProductCategory> getProductCategoryByCaid(String Caid) {
        ArrayList<ProductCategory> data = new ArrayList<>();
        try {
            String sql = "SELECT \n"
                    + "    C.[caid],\n"
                    + "    C.[caname],\n"
                    + "    PC.[capid],\n"
                    + "    P.[pid],\n"
                    + "    P.[pname],\n"
                    + "    P.[rate],\n"
                    + "    P.[price],\n"
                    + "    P.[img],\n"
                    + "    P.[priceSale],\n"
                    + "    P.[quantity],\n"
                    + "    P.[pubid],\n"
                    + "    P.[created_at],\n"
                    + "    P.[saled],\n"
                    + "    P.[isDiscount],\n"
                    + "    P.[isSoldout],\n"
                    + "    P.[csid]\n"
                    + "FROM \n"
                    + "    [WebBoardGame4].[dbo].[Categories] AS C\n"
                    + "INNER JOIN \n"
                    + "    [WebBoardGame4].[dbo].[ProductCategory] AS PC\n"
                    + "ON \n"
                    + "    C.[caid] = PC.[caid]\n"
                    + "INNER JOIN \n"
                    + "    [WebBoardGame4].[dbo].[Products] AS P\n"
                    + "ON \n"
                    + "    PC.[pid] = P.[pid]\n"
                    + "	where c.caid = " + Caid;

            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                ProductCategory pc = ProductCategory.builder()
                        .caid(rs.getInt("caid"))
                        .capid(rs.getInt("capid"))
                        .pid(rs.getInt("pid"))
                        .build();
                data.add(pc);
            }
        } catch (Exception e) {
            System.out.println("get Product Category By Caid " + e.getMessage());
        }
        return data;
    }

      public void updateProductCategory(int caid, int pid) {

        try {
            String sql = "     UPDATE [WebBoardGame4].[dbo].[ProductCategory]\n"
                    + "SET \n"
                    + "  caid = ?\n"
                    + "\n"
                    + "WHERE pid = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, caid);
            st.setInt(2, pid);

            st.execute();

        } catch (Exception e) {
        }

    }
    public void insertProductCategory(int caid) {

        int pid = 0;
        try {
            String sql = " Select MAX(pid) AS Maxpid from Products";

            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                pid = rs.getInt("Maxpid");

            }

        } catch (Exception e) {
            System.out.println("getpid " + e.getMessage());
        }

        try {
            String sql = "   INSERT INTO ProductCategory (caid,pid)\n"
                    + "VALUES (?,?);";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, caid);
            st.setInt(2, pid);
            st.execute();

        } catch (Exception e) {
        }
    }
     public void deleteProductCategory(int pid) {

        try {
            String sql = "delete from ProductCategory where pid = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, pid);
            st.executeUpdate();

        } catch (Exception e) {
        }
    }

    public static void main(String[] args) {
        ProductCategoryDao pc = new ProductCategoryDao();
        System.out.println(pc.getProductCategoryByCaid("5"));
    }
}
