/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import context.DBContext;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import model.Comment;

/**
 *
 * @author hihih
 */
public class CommentDao extends DBContext {

    public List<Comment> getAllComments() {
        List<Comment> comments = new ArrayList<>();
        String sql = "SELECT * FROM Comment";
        try ( PreparedStatement statement = connection.prepareStatement(sql);  ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                Comment comment = Comment.builder()
                        .CommentId(resultSet.getInt("CommentId"))
                        .CommentContent(Validation.ValidationString.validateAndCensorContent(resultSet.getString("CommentContent")))
                        .Create_at(resultSet.getString("Create_at"))
                        .Status(resultSet.getInt("Status"))
                        .ProductId(resultSet.getInt("ProductId"))
                        .AccountId(resultSet.getInt("AccountId"))
                        .Rate(resultSet.getInt("Rate"))
                        .CustomerName(resultSet.getString("CustomerName"))
                        .CustomerEmail(resultSet.getString("CustomerEmail"))
                        .build();
                comments.add(comment);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return comments;
    }

    public List<Comment> getAllComments(String Pid) {
        List<Comment> comments = new ArrayList<>();
        String sql = "SELECT * FROM Comment where  Status=1 and ProductId=" + Pid;
        try ( PreparedStatement statement = connection.prepareStatement(sql);  ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                Comment comment = Comment.builder()
                        .CommentId(resultSet.getInt("CommentId"))
                        .CommentContent(Validation.ValidationString.validateAndCensorContent(resultSet.getString("CommentContent")))
                        .Create_at(resultSet.getString("Create_at"))
                        .Status(resultSet.getInt("Status"))
                        .ProductId(resultSet.getInt("ProductId"))
                        .AccountId(resultSet.getInt("AccountId"))
                        .Rate(resultSet.getInt("Rate"))
                        .CustomerName(resultSet.getString("CustomerName"))
                        .CustomerEmail(resultSet.getString("CustomerEmail"))
                        .build();
                comments.add(comment);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return comments;
    }

    public List<Comment> getAllCommentsSortedByFormattedCreateAt(String Pid) {
        List<Comment> comments = getAllComments(Pid);

        // Sắp xếp danh sách bình luận sử dụng Comparator
        Collections.sort(comments, new Comparator<Comment>() {
            @Override
            public int compare(Comment comment1, Comment comment2) {
                // Định dạng ngày từ string "HH:mm dd/MM/yyyy" và so sánh
                SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm dd/MM/yyyy");
                try {
                    Date date1 = dateFormat.parse(comment1.getCreate_at());
                    Date date2 = dateFormat.parse(comment2.getCreate_at());
                    return date2.compareTo(date1); // Sắp xếp giảm dần (mới nhất trước)
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                return 0; // Trả về 0 nếu có lỗi
            }
        });

        return comments;
    }

    public List<Comment> getAllCommentsSortedByFormattedCreateAt() {
        List<Comment> comments = getAllComments();

        // Sắp xếp danh sách bình luận sử dụng Comparator
        Collections.sort(comments, new Comparator<Comment>() {
            @Override
            public int compare(Comment comment1, Comment comment2) {
                // Định dạng ngày từ string "HH:mm dd/MM/yyyy" và so sánh
                SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm dd/MM/yyyy");
                try {
                    Date date1 = dateFormat.parse(comment1.getCreate_at());
                    Date date2 = dateFormat.parse(comment2.getCreate_at());
                    return date2.compareTo(date1); // Sắp xếp giảm dần (mới nhất trước)
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                return 0; // Trả về 0 nếu có lỗi
            }
        });

        return comments;
    }

    // Thêm một bình luận mới vào cơ sở dữ liệu
    public void addComment(Comment comment) {
        String sql = "INSERT INTO Comment (CommentContent, Create_at, Status, ProductId, AccountId, Rate, CustomerName, CustomerEmail) "
                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        try ( PreparedStatement statement = connection.prepareStatement(sql)) {
            Date currentDate = new Date();
            // Định dạng ngày/tháng/năm (dd/MM/yyyy)
            SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm dd/MM/yyyy");
            // Chuyển đổi thời gian hiện tại sang chuỗi theo định dạng
            String formattedDate = dateFormat.format(currentDate);
            statement.setString(1, Validation.ValidationString.validateAndCensorContent(comment.getCommentContent()));
            statement.setString(2, formattedDate);
            statement.setInt(3, comment.getStatus());
            statement.setInt(4, comment.getProductId());
            statement.setInt(5, comment.getAccountId());
            statement.setInt(6, comment.getRate());
            statement.setString(7, comment.getCustomerName());
            statement.setString(8, comment.getCustomerEmail());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public double calculateAverageRate(String Pid) {
        String sql = "SELECT ROUND(AVG(CAST(Rate AS DECIMAL(10, 2))), 2)  as averageRate FROM Comment where Productid= " + Pid;
        try ( PreparedStatement statement = connection.prepareStatement(sql);  ResultSet resultSet = statement.executeQuery()) {
            if (resultSet.next()) {
                return resultSet.getDouble("averageRate");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0.0;
    }

    public double calculateAverageRate() {
        String sql = "SELECT ROUND(AVG(CAST(Rate AS DECIMAL(10, 2))), 2)  as averageRate FROM Comment ";
        try ( PreparedStatement statement = connection.prepareStatement(sql);  ResultSet resultSet = statement.executeQuery()) {
            if (resultSet.next()) {
                return resultSet.getDouble("averageRate");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0.0;
    }

    public Map<Integer, Map<String, Double>> getRateStatistics() {
        Map<Integer, Map<String, Double>> rateStatistics = new HashMap<>();
        String sql = "SELECT Rate, CAST(COUNT(Rate) AS DECIMAL) as RateCount, (CAST(COUNT(Rate) AS DECIMAL) / (SELECT COUNT(*) FROM Comment)) * 100 as RatePercentage FROM Comment GROUP BY Rate";
        try ( PreparedStatement statement = connection.prepareStatement(sql);  ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                int rate = resultSet.getInt("Rate");
                double rateCount = resultSet.getDouble("RateCount");
                double ratePercentage = resultSet.getDouble("RatePercentage");

                Map<String, Double> rateInfo = new HashMap<>();
                rateInfo.put("RateCount", rateCount);
                rateInfo.put("RatePercentage", ratePercentage);

                rateStatistics.put(rate, rateInfo);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rateStatistics;
    }

    public Map<Integer, Map<String, Double>> getRateStatistics(String Pid) {
        Map<Integer, Map<String, Double>> rateStatistics = new HashMap<>();
        String sql = "SELECT Rate, CAST(COUNT(Rate) AS DECIMAL) as RateCount, (CAST(COUNT(Rate) AS DECIMAL) / (SELECT COUNT(*) FROM Comment)) * 100 as RatePercentage "
                + "FROM Comment where Productid=" + Pid
                + "GROUP BY Rate";
        try ( PreparedStatement statement = connection.prepareStatement(sql);  ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                int rate = resultSet.getInt("Rate");
                double rateCount = resultSet.getDouble("RateCount");
                double ratePercentage = resultSet.getDouble("RatePercentage");

                Map<String, Double> rateInfo = new HashMap<>();
                rateInfo.put("RateCount", rateCount);
                rateInfo.put("RatePercentage", ratePercentage);

                rateStatistics.put(rate, rateInfo);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rateStatistics;
    }

    public List<Comment> getCommentsByPage(int page, int commentsPerPage) {
        List<Comment> comments = new ArrayList<>();
        int startIndex = (page - 1) * commentsPerPage;

        String query = "SELECT * FROM Comment LIMIT ? OFFSET ?";
        try ( PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setInt(1, commentsPerPage);
            preparedStatement.setInt(2, startIndex);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Comment comment = Comment.builder()
                        .CommentId(resultSet.getInt("CommentId"))
                        .CommentContent(Validation.ValidationString.validateAndCensorContent(resultSet.getString("CommentContent")))
                        .Create_at(resultSet.getString("Create_at"))
                        .Status(resultSet.getInt("Status"))
                        .ProductId(resultSet.getInt("ProductId"))
                        .AccountId(resultSet.getInt("AccountId"))
                        .Rate(resultSet.getInt("Rate"))
                        .CustomerName(resultSet.getString("CustomerName"))
                        .CustomerEmail(resultSet.getString("CustomerEmail"))
                        .build();
                comments.add(comment);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return comments;
    }

    public boolean updateComment(Comment comment) {
        String updateQuery = "UPDATE Comment "
                + "SET CommentContent = ?, Create_at = ?, Status = ?, Rate = ? "
                + "WHERE CommentId = ?";

        try (
                 PreparedStatement preparedStatement = connection.prepareStatement(updateQuery)) {
            Date currentDate = new Date();
            // Định dạng ngày/tháng/năm (dd/MM/yyyy)
            SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm dd/MM/yyyy");
            // Chuyển đổi thời gian hiện tại sang chuỗi theo định dạng
            String formattedDate = dateFormat.format(currentDate);
            preparedStatement.setString(1, comment.getCommentContent());
            preparedStatement.setString(2, formattedDate);
            preparedStatement.setInt(3, comment.getStatus());
            preparedStatement.setInt(4, comment.getRate());
            preparedStatement.setInt(5, comment.getCommentId());

            int rowsUpdated = preparedStatement.executeUpdate();

            return rowsUpdated > 0; // Return true if at least one row was updated
        } catch (SQLException e) {
            e.printStackTrace();
            return false; // Return false on error
        }
    }

    public boolean deleteComment(int commentId) {
        String deleteQuery = "DELETE FROM Comment WHERE CommentId = ?";

        try (
                 PreparedStatement preparedStatement = connection.prepareStatement(deleteQuery)) {
            preparedStatement.setInt(1, commentId);

            int rowsDeleted = preparedStatement.executeUpdate();

            return rowsDeleted > 0; // Return true if at least one row was deleted
        } catch (SQLException e) {
            e.printStackTrace();
            return false; // Return false on error
        }
    }

    public List<Comment> search(String keyword) {
        List<Comment> comments = getAllComments();
        List<Comment> searchResults = new ArrayList<>();
        keyword = keyword.toLowerCase(); // Chuyển keyword về chữ thường để tìm kiếm không phân biệt hoa thường

        for (Comment comment : comments) {
            if (comment.getCommentContent().toLowerCase().contains(keyword)) {
                searchResults.add(comment);
            }
        }

        return searchResults;
    }
public List<Comment> search1(String keyword) {
        List<Comment> results = new ArrayList<>();
        String sql = "SELECT * FROM Comment WHERE CommentContent LIKE ? OR Create_at LIKE ? OR Status LIKE ? OR ProductId LIKE ? OR AccountId LIKE ? OR Rate LIKE ? OR CustomerName LIKE ? OR CustomerEmail LIKE ?";
        try (
             PreparedStatement statement = connection.prepareStatement(sql)) {
            String likeKeyword = "%" + keyword + "%";
            for (int i = 1; i <= 8; i++) {
                statement.setString(i, likeKeyword);
            }
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    Comment comment = Comment.builder()
                        .CommentId(resultSet.getInt("CommentId"))
                        .CommentContent(Validation.ValidationString.validateAndCensorContent(resultSet.getString("CommentContent")))
                        .Create_at(resultSet.getString("Create_at"))
                        .Status(resultSet.getInt("Status"))
                        .ProductId(resultSet.getInt("ProductId"))
                        .AccountId(resultSet.getInt("AccountId"))
                        .Rate(resultSet.getInt("Rate"))
                        .CustomerName(resultSet.getString("CustomerName"))
                        .CustomerEmail(resultSet.getString("CustomerEmail"))
                        .build();
                    results.add(comment);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return results;
    }    

    public boolean editComment(int commentId, String newContent, int newStatus) {
        String updateQuery = "UPDATE Comment SET CommentContent=?, Status=? WHERE CommentId=?";
        try (
                 PreparedStatement stmt = connection.prepareStatement(updateQuery)) {
            stmt.setString(1, newContent);
            stmt.setInt(2, newStatus);
            stmt.setInt(3, commentId);

            int rowsAffected = stmt.executeUpdate();
            return rowsAffected > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
    public Comment getCommentById(int commentId) {
        Comment comment = null;
        String query = "SELECT * FROM Comment WHERE CommentId = ?";
        
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, commentId);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                comment = Comment.builder()
                .CommentId(resultSet.getInt("CommentId"))
                .CommentContent(resultSet.getString("CommentContent"))
                .Create_at(resultSet.getString("Create_at"))
                .Status(resultSet.getInt("Status"))
                .ProductId(resultSet.getInt("ProductId"))
                .AccountId(resultSet.getInt("AccountId"))
                .Rate(resultSet.getInt("Rate"))
                .CustomerName(resultSet.getString("CustomerName"))
                .CustomerEmail(resultSet.getString("CustomerEmail"))
                .build();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return comment;
    }
     public void deleteComment1(int commentId) {
        String sql = "UPDATE Comment SET Status = 0 WHERE CommentId = ?";

        try (
             PreparedStatement stmt = connection.prepareStatement(sql)) {
            stmt.setInt(1, commentId);

            int rowsUpdated = stmt.executeUpdate();

            if (rowsUpdated > 0) {
                System.out.println("Comment có CommentId = " + commentId + " đã bị xóa.");
            } else {
                System.out.println("Không tìm thấy comment với CommentId = " + commentId);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
     
     public int getTotalAllComment() {
        int totalCommentCount = 0;

        try {
            String sql = "SELECT COUNT(*) AS TotalCount FROM Comment";
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet resultSet = st.executeQuery();

            if (resultSet.next()) {
                totalCommentCount = resultSet.getInt("TotalCount");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return totalCommentCount;
    }

    public Map<Integer, Integer> getRatebyRateType() {
    Map<Integer, Integer> rateCounts = new HashMap<>();
    String sql = "SELECT Rate, COUNT(*) AS TotalCount\n" +
                 "FROM Comment\n" +
                 "GROUP BY Rate";
    try (PreparedStatement statement = connection.prepareStatement(sql);
         ResultSet resultSet = statement.executeQuery()) {
        while (resultSet.next()) {
            int rate = resultSet.getInt("Rate");
            int totalCount = resultSet.getInt("TotalCount");
            rateCounts.put(rate, totalCount);
        }
    } catch (SQLException e) {
        e.printStackTrace();
    }
    return rateCounts;
}

    public static void main(String[] args) {
        CommentDao c = new CommentDao();
        Date currentDate = new Date();

        // Định dạng ngày/tháng/năm (dd/MM/yyyy)
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm dd/MM/yyyy");
        // Chuyển đổi thời gian hiện tại sang chuỗi theo định dạng
        String formattedDate = dateFormat.format(currentDate);

        System.out.println(formattedDate);
        Comment ct = Comment.builder()
                .CommentId(6)
                .CommentContent("Tuyệt")
                .CustomerEmail("ha@gmail.com")
                .Rate(3)
                .build();
      
        System.out.println(c.search1("1"));
    }

}
