/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import Validation.ValidationString;
import context.DBContext;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import model.Account;

import model.ShipmentDetails;

/**
 *
 * @author HP
 */
public class AccountDao extends DBContext {

    public Account checkAucc(String email, String password) {
        String sql = "select * from [dbo].[Accounts] where email = '"+email+"' and password = CONVERT(nvarchar(50), HashBytes('SHA2_256', CONVERT(varbinary, '"+password+"')), 1)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            if (rs.next()) {

                Account ac = Account.builder()
                        .acid(rs.getInt("acid"))
                        .email(rs.getString("email"))
                        .emailConfirm(rs.getInt("emailConfirm"))
                        .password(rs.getString("password"))
                        .username(rs.getString("username"))
                        .role(rs.getInt("role"))
                        .img(rs.getString("img"))
                        .created_at(rs.getString("created_at"))
                        .status(rs.getInt("status"))
                        .build();
                return ac;
            }
        } catch (SQLException e) {
        }
        return null;
    }

    public Account checkAcc(String email) {
        String sql = "select*from Accounts \n"
                + "where  email = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, email);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {

                Account ac = Account.builder()
                        .acid(rs.getInt("acid"))
                        .email(rs.getString("email"))
                        .emailConfirm(rs.getInt("emailConfirm"))
                        .password(rs.getString("password"))
                        .username(rs.getString("username"))
                        .role(rs.getInt("role"))
                        .img(rs.getString("img"))
                        .created_at(rs.getString("created_at"))
                        .status(rs.getInt("status"))
                        .build();
                return ac;
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public ShipmentDetails getMarkSipment(String email) {
        String sql = "select *\n"
                + "	from Accounts a\n"
                + "	inner join ShipmentDetails p on a.acid = p.acid\n"
                + "\n"
                + "	where email = ? and p.status = 1; ";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, email);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                ShipmentDetails s = ShipmentDetails.builder()
                        .aaid(rs.getInt("aaid"))
                        .address(rs.getString("address"))
                        .phonenumber(rs.getString("phonenumber"))
                        .acid(rs.getInt("acid"))
                        .status(rs.getInt("status"))
                        .receiver(rs.getString("receiver"))
                        .build();
                return s;

            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public boolean changeName(String newusername, int acid) {
        String sql = "UPDATE [dbo].[Accounts]\n"
                + "   SET\n"
                + "      [username] = ?\n"
                + " WHERE acid = ?";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, newusername);

            st.setInt(2, acid);

            st.executeUpdate();
            return true;

        } catch (SQLException e) {
            System.out.println(e);
            return false;
        }

    }

    public boolean updateAddressPhone(String address, String phonenumber, int aaid) {
        String sql = "UPDATE [dbo].[ShipmentDetails]\n"
                + "   SET\n"
                + "      [address] = ? \n"
                + "      ,[phonenumber] = ?\n"
                + " WHERE aaid = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            st.setString(1, address);
            st.setString(2, phonenumber);
            st.setInt(3, aaid);
            st.executeUpdate();
            return true;

        } catch (SQLException e) {
            System.out.println(e);
            return false;
        }

    }

    public boolean RemoveInfoShip(String aaid) {

        String sql = "DELETE FROM [dbo].[ShipmentDetails]\n"
                + "WHERE [aaid] = " + aaid;

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.executeUpdate();
            return true;

        } catch (SQLException e) {
            System.out.println(e);
            return false;
        }

    }

    public boolean changeInforShip(int aaid, int acid) {

        String sql = "  UPDATE [dbo].[ShipmentDetails]\n"
                + "                   SET\n"
                + "                  \n"
                + "                   [status] = 0\n"
                + "              WHERE [status] = 1 and acid = " + acid;
        sql += " UPDATE [dbo].[ShipmentDetails]\n"
                + "               SET   \n"
                + "                    [status] = 1\n"
                + "                 WHERE aaid = ? and acid = " + acid;

        try {
            PreparedStatement st = connection.prepareStatement(sql);

            st.setInt(1, aaid);
            st.executeUpdate();
            return true;

        } catch (SQLException e) {
            System.out.println(e);
            return false;
        }

    }

    public List<ShipmentDetails> getListShipmentOfAcc(String email) {
        List<ShipmentDetails> list = new ArrayList<>();
        String sql = "select *\n"
                + "	from Accounts a\n"
                + "	inner join ShipmentDetails p on a.acid = p.acid\n"
                + "\n"
                + "	where email = ? "
                + " Order by p.[status] DESC";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, email);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                ShipmentDetails s = ShipmentDetails.builder()
                        .aaid(rs.getInt("aaid"))
                        .address(rs.getString("address"))
                        .phonenumber(rs.getString("phonenumber"))
                        .acid(rs.getInt("acid"))
                        .status(rs.getInt("status"))
                        .receiver(rs.getString("receiver"))
                        .build();

                list.add(s);
            }

        } catch (SQLException e) {
            System.out.println(e);
        }

        return list;

    }

    public boolean createShipment(String address, String phonnumber, int acid, String receiver) {
        String sql = "UPDATE [dbo].[ShipmentDetails]\n"
                + "                   SET\n"
                + "                  \n"
                + "                   [status] = 0\n"
                + "              WHERE [status] = 1 and acid = " + acid;
        sql = sql + "INSERT INTO [dbo].[ShipmentDetails]\n"
                + "           ([address]\n"
                + "           ,[phonenumber]\n"
                + "           ,[status]\n"
                + "           ,[acid]\n"
                + "     ,[receiver])\n"
                + "     VALUES\n"
                + "           (?,?,1,?,?)";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, address);
            st.setString(2, phonnumber);
            st.setInt(3, acid);
            st.setString(4, receiver);

            st.executeUpdate();
            return true;

        } catch (SQLException e) {
            System.out.println(e);
            return false;
        }

    }

    public boolean checkEmailExist(String email) {
        try {
            String str = "select * from Accounts\n"
                    + "where email=?";
            PreparedStatement pstm = connection.prepareStatement(str);
            pstm.setString(1, email);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                return true;
            }
        } catch (Exception e) {
        }
        return false;
    }

    public boolean checkPhoneExist(String phone) {
        try {
            String str = "select * from ShipmentDetails\n"
                    + "where phone=?";
            PreparedStatement pstm = connection.prepareStatement(str);
            pstm.setString(1, phone);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                return true;
            }
        } catch (Exception e) {
        }
        return false;
    }

    public void register(String email, String phone, String pass) {
        try {
            String str = "insert into Accounts([email], [password], [created_at] ,[role]) "
                    + "values (?, CONVERT(nvarchar(50), HashBytes('SHA2_256', CONVERT(varbinary, '"+pass+"')), 1), ?, 4);\n"
                    + "insert into ShipmentDetails([phonenumber], [acid]) values (?, \n"
                    + "(select MAX(acid) from Accounts));\n";
            PreparedStatement pstm = connection.prepareStatement(str);
            pstm.setString(1, email);
         
            pstm.setString(3, phone);
            Date date = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            pstm.setString(2, sdf.format(date));

            pstm.executeUpdate();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void changepass(String email, String password) {
        String sql = " UPDATE [dbo].[Accounts]\n"
                + "                   SET [password] = CONVERT(nvarchar(50), HashBytes('SHA2_256', CONVERT(varbinary, '"+password+"')), 1)\n"
                + "                WHERE [email]= ?";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            
            st.setString(1, email);

            st.executeUpdate();

        } catch (SQLException e) {

        }

    }

    public ShipmentDetails isAcchaveShipment(int acid, String address) {
        List<ShipmentDetails> list = new ArrayList<>();
        String sql = "select *\n"
                + "	from Accounts a\n"
                + "	inner join ShipmentDetails p on a.acid = p.acid\n"
                + "\n"
                + "	where a.acid = ? and p.address = ? ";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, acid);
            st.setString(2, address);

            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                ShipmentDetails s = ShipmentDetails.builder()
                        .aaid(rs.getInt("aaid"))
                        .address(rs.getString("address"))
                        .phonenumber(rs.getString("phonenumber"))
                        .acid(rs.getInt("acid"))
                        .status(rs.getInt("status"))
                        .receiver(rs.getString("receiver"))
                        .build();

                return s;
            }

        } catch (SQLException e) {
            System.out.println(e);
        }

        return null;

    }

    public List<Account> getAllUsers() {
        String sql = "SELECT * FROM [dbo].[Accounts]";
        List<Account> userList = new ArrayList<>();

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                Account ac = Account.builder()
                        .acid(rs.getInt("acid"))
                        .email(rs.getString("email"))
                        .emailConfirm(rs.getInt("emailConfirm"))
                        .password(rs.getString("password"))
                        .username(rs.getString("username"))
                        .role(rs.getInt("role"))
                        .img(rs.getString("img"))
                        .created_at(rs.getString("created_at"))
                        .status(rs.getInt("status"))
                        .build();
                userList.add(ac);
            }

        } catch (SQLException e) {
            System.out.println(e);
        }

        return userList;
    }

    public void insertAccount(String email,
            String password,
            String username, int role, String created_at) {

        String sql = "INSERT INTO [dbo].[Accounts]\n"
                + "           ([email]\n"
                + "           ,[password]\n"
                + "           ,[username]\n"
                + "           ,[role]\n"
                + "           ,[created_at]\n"
                + "           ,[status])"
                + "values(?,CONVERT(nvarchar(50), HashBytes('SHA2_256', CONVERT(varbinary, '"+password+"')), 1),?,?,?,1)";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, email);
           
            st.setString(2, username);
            st.setInt(3, role);
            st.setString(4, created_at);

            st.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e);
        }

    }

    public Account getAccountById(String acid) {
        String sql = "Select * from Accounts where acid = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, acid);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Account a = Account.builder()
                        .acid(rs.getInt("acid"))
                        .email(rs.getString("email"))
                        .emailConfirm(rs.getInt("emailConfirm"))
                        .password(rs.getString("password"))
                        .username(rs.getString("username"))
                        .role(rs.getInt("role"))
                        .created_at(rs.getString("created_at"))
                        .status(rs.getInt("status"))
                        .build();
                return a; // Trả về đối tượng Account đã tạo
            }
        } catch (SQLException e) {
            e.printStackTrace(); // In thông báo lỗi hoặc ghi log
        }

        return null;

    }

    public Account getAccountFromAid(int acid) {

        String sql = "select * from Accounts where acid=?";
        List<Account> userList = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, acid);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Account ac = Account.builder()
                        .acid(rs.getInt("acid"))
                        .email(rs.getString("email"))
                        .emailConfirm(rs.getInt("emailConfirm"))
                        .password(rs.getString("password"))
                        .username(rs.getString("username"))
                        .role(rs.getInt("role"))
                        .created_at(rs.getString("created_at"))
                        .status(rs.getInt("status"))
                        .build();
                userList.add(ac);
            }

        } catch (SQLException e) {
        }
        return null;
    }

    public int updateAccountStatus(Account a, int status) {
        String sql = "update Accounts set status=? where acid=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, status);
            st.setInt(2, a.getAcid());
            return st.executeUpdate();

        } catch (SQLException e) {
        }
        return 0;

    }

    public int updateAccountRole(Account a, int i) {
        String sql = "update Accounts set role=? where acid=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, i);
            st.setInt(2, a.getAcid());
            return st.executeUpdate();

        } catch (SQLException e) {
        }
        return 0;

    }

    public List<Account> getAllUsers(String role) {
        String sql = "SELECT [acid]\n"
                + "      ,[email]\n"
                + "      ,[emailConfirm]\n"
                + "      ,[password]\n"
                + "      ,[username]\n"
                + "      ,[role]\n"
                + "      ,[created_at]\n"
                + "      ,[status]\n"
                + "  FROM [dbo].[Accounts]";
        if (role != null) {
            sql = sql + " where role = " + role;
        }

        List<Account> userList = new ArrayList<>();

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                Account ac = Account.builder()
                        .acid(rs.getInt("acid"))
                        .email(rs.getString("email"))
                        .emailConfirm(rs.getInt("emailConfirm"))
                        .password(rs.getString("password"))
                        .username(rs.getString("username"))
                        .role(rs.getInt("role"))
                        .created_at(rs.getString("created_at"))
                        .status(rs.getInt("status"))
                        .build();
                userList.add(ac);
            }

        } catch (SQLException e) {

        }

        return userList;
    }

    public List<Account> getAllAccountbyName(String s) {
        String sql = "select * from Accounts where [username] like '%" + s + "%'";
        List<Account> userList = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Account ac = Account.builder()
                        .acid(rs.getInt("acid"))
                        .email(rs.getString("email"))
                        .emailConfirm(rs.getInt("emailConfirm"))
                        .password(rs.getString("password"))
                        .username(rs.getString("username"))
                        .role(rs.getInt("role"))
                        .created_at(rs.getString("created_at"))
                        .status(rs.getInt("status"))
                        .build();
                userList.add(ac);
            }

        } catch (SQLException e) {
        }
        return userList;
    }

    public Account UsersDetail(String id) {
        String sql = "SELECT [acid]\n"
                + "      ,[email]\n"
                + "      ,[emailConfirm]\n"
                + "      ,[password]\n"
                + "      ,[username]\n"
                + "      ,[role]\n"
                + "      ,[created_at]\n"
                + "      ,[status]\n"
                + "  FROM [dbo].[Accounts] where acid = ?";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, id);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                Account ac = Account.builder()
                        .acid(rs.getInt("acid"))
                        .email(rs.getString("email"))
                        .emailConfirm(rs.getInt("emailConfirm"))
                        .password(rs.getString("password"))
                        .username(rs.getString("username"))
                        .role(rs.getInt("role"))
                        .created_at(rs.getString("created_at"))
                        .status(rs.getInt("status"))
                        .build();
                return ac;
            }

        } catch (SQLException e) {

        }

        return null;
    }

    public void updateAccount(String acid, String email, String password, String username, int role, String status) {
        String sql = "UPDATE [dbo].[Accounts]\n"
                + "   SET [email] = ?\n"
                + "      ,[password] = CONVERT(nvarchar(50), HashBytes('SHA2_256', CONVERT(varbinary, '"+password+"')), 1)\n"
                + "      ,[username] = ?\n"
                + "      ,[role] = ?\n"
                + "      ,[status] = ?\n"
                + " WHERE acid= "+acid;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, email);
          
            st.setString(2, username);
            st.setInt(3, role);
            st.setString(4, status);
         
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public Account getAccountByoid(String oid) {
        String sql = "	select * from Accounts o\n"
                + "	inner join orders od on od.acid = o.acid\n"
                + "	where oid =" + oid;

        try {
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                Account ac = Account.builder()
                        .acid(rs.getInt("acid"))
                        .email(rs.getString("email"))
                        .emailConfirm(rs.getInt("emailConfirm"))
                        .password(rs.getString("password"))
                        .username(rs.getString("username"))
                        .role(rs.getInt("role"))
                        .created_at(rs.getString("created_at"))
                        .status(rs.getInt("status"))
                        .build();
                return ac;
            }

        } catch (SQLException e) {

        }

        return null;
    }
    public void insertAccount(Account a) {
        String sql = "INSERT INTO [dbo].[Accounts]\n"
                + "           ([email]\n"
                + "           ,[password]\n"
                + "           ,[username]\n"
                + "           ,[role]\n"
                + "           ,[created_at]\n"
                + "           ,[status])"
                + "values(?,CONVERT(nvarchar(50), HashBytes('SHA2_256', CONVERT(varbinary, '"+a.getPassword()+"')), 1),?,?,?,1)";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, a.getEmail());
          
            st.setString(2, a.getUsername());
            st.setInt(3, a.getRole());
            st.setString(4, a.getCreated_at());

            st.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e);
        }
    }
    
    public List<Account> getNewestCustomer() {
        List<Account> list = new ArrayList<>();

        try {
            String sql = "SELECT TOP 5 *\n"
                    + "FROM [dbo].[Accounts]\n"
                    + "WHERE role = 4\n"
                    + "ORDER BY created_at DESC";
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                Account ac = Account.builder()
                        .acid(rs.getInt("acid"))
                        .email(rs.getString("email"))
                        .emailConfirm(rs.getInt("emailConfirm"))
                        .password(rs.getString("password"))
                        .username(rs.getString("username"))
                        .role(rs.getInt("role"))
                        .img(rs.getString("img"))
                        .created_at(rs.getString("created_at"))
                        .status(rs.getInt("status"))
                        .build();
                list.add(ac);
            }
            
        } catch (Exception e) {
            System.out.println("get newest customer" + e.getMessage());
        }
        return list;
    }

    public static void main(String[] args) {
        AccountDao d = new AccountDao();
       Account a = d.checkAcc("new123@gmail.com");
       d.insertAccount(a);
       
       
     
    }
}
