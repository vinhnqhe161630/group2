/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import context.DBContext;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.Category;
import model.Product;
import model.ProductCategory;

/**
 *
 * @author tuanm
 */
public class CategotyDao extends DBContext {

    public List<Category> getListCategory() {
        List<Category> list = new ArrayList<>();
        try {
            String sql = "  select*from Categories where status=1";

            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Category c = Category.builder()
                        .caid(rs.getInt("caid"))
                        .caname(rs.getString("caname"))
                        .build();
                list.add(c);
            }
        } catch (Exception e) {
            System.out.println("get Product Category By Caid " + e.getMessage());
        }
        return list;
    }

    public void insertCategory(String caname) {

        try {
            String sql = "    INSERT INTO [WebBoardGame4].[dbo].[Categories] \n"
                    + "(caname)\n"
                    + "VALUES\n"
                    + "(?)";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, caname);
           
            st.execute();

        } catch (Exception e) {
        }
    }
    public List<Product> getProductByCaid(String Caid) {
        ArrayList<Product> data = new ArrayList<>();
        try {
            String sql = "   SELECT TOP (1000)\n"
                    + "	p.pname,\n"
                    + "	p.pid,\n"
                    + "	p.rate,\n"
                    + "	p.price,\n"
                    + "	p.img,\n"
                    + "	p.priceSale,\n"
                    + "	p.quantity,\n"
                    + "	p.pubid,\n"
                    + "	p.saled,\n"
                    + "	p.isDiscount,\n"
                    + "	p.isSoldout,\n"
                    + "	p.status\n"
                    + "FROM [WebBoardGame4].[dbo].[Categories] c\n"
                    + "INNER JOIN [WebBoardGame4].[dbo].[ProductCategory] pc ON c.caid = pc.caid\n"
                    + "INNER JOIN [WebBoardGame4].[dbo].[Products] p ON pc.pid = p.pid where pc.caid = " + Caid;
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Product pc = Product.builder()
                        .pid(rs.getInt("pid"))
                        .pname(rs.getString("pname"))
                        .rate(rs.getFloat("rate"))
                        .price(rs.getInt("price"))
                        .img(rs.getString("img"))
                        .priceSale(rs.getInt("priceSale"))
                        .quantity(rs.getInt("quantity"))
                        .pubid(rs.getInt("pubid"))
                        .saled(rs.getInt("saled"))
                        .isDiscount(rs.getBoolean("isDiscount"))
                        .isSoldout(rs.getBoolean("isSoldout"))
                        .status(rs.getInt("status"))
                        .build();
                data.add(pc);
            }
        } catch (Exception e) {
            System.out.println("get Product Category By Caid " + e.getMessage());
        }
        return data;
    }

    public static void main(String[] args) {
        CategotyDao cd = new CategotyDao();
//        List<Category> list = cd.getListCategory();
//        System.out.println(list);
        String caname = "Game cho người nghiện";
        cd.insertCategory(caname);
    }

}
