/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import context.DBContext;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import model.Order;
import model.OrderStatistics;
import model.Product;

/**
 *
 * @author tuanm
 */
public class StatisticDao extends DBContext {

    public List<OrderStatistics> getOrderStatisticsByMonth() {
        String sql = "SELECT "
                + "YEAR(ordered_at) AS [Year], "
                + "MONTH(ordered_at) AS [Month], "
                + "SUM(TotalAmount) AS TotalAmountByMonth "
                + "FROM [WebBoardGame].[dbo].[orders] "
                + "GROUP BY YEAR(ordered_at), MONTH(ordered_at) "
                + "ORDER BY [Year], [Month];";
        List<OrderStatistics> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int year = rs.getInt("Year");
                int month = rs.getInt("Month");
                double totalAmount = rs.getDouble("TotalAmountByMonth");

                // Tạo đối tượng OrderStatistics và thêm vào danh sách
                OrderStatistics orderStatistics = new OrderStatistics(year, month, totalAmount);
                list.add(orderStatistics);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

        public LinkedHashMap<String, Double> sos() {
        String sql = "SELECT\n"
                + "    CASE\n"
                + "        WHEN [status] = 1 THEN 'InTransitOrders'\n"
                + "        WHEN [status] = 2 THEN 'DeliveredOrders'\n"
                + "        WHEN [status] = 3 THEN 'CancelledOrders'\n"
                + "        ELSE 'UnknownStatus'\n"
                + "    END AS status,\n"
                + "    COUNT(*) AS count\n"
                + "FROM [WebBoardGame].[dbo].[orders]\n"
                + "GROUP BY [status];";
        LinkedHashMap<String, Double> list = new LinkedHashMap<>();
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.put(rs.getString(1), rs.getDouble(2));

            }
        } catch (SQLException e) {

        }
        return list;
    }

    public int getTotalOrders() {
        String sql = "  SELECT COUNT(*) AS TotalOrders\n"
                + "FROM [WebBoardGame].[dbo].[orders];";
        int totalAccounts = 0;
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                totalAccounts = rs.getInt("TotalOrders");
            }
            rs.close();
            ps.close();
        } catch (SQLException e) {
        }
        return totalAccounts;
    }

    public int getTotalEarning() {
        String sql = "SELECT SUM(TotalAmount) AS TotalPaidAmount\n"
                + "FROM [WebBoardGame].[dbo].[orders]\n"
                + "WHERE status = 1;;";
        int totalAccounts = 0;
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                totalAccounts = rs.getInt("TotalPaidAmount");
            }
            rs.close();
            ps.close();
        } catch (SQLException e) {
        }
        return totalAccounts;
    }

    public List<Map<String, Object>> getTopSoldProducts() {
        String sql = "SELECT p.pname AS ProductName, SUM(od.quantity) AS TotalQuantitySold\n"
                + "FROM Products p\n"
                + "INNER JOIN orderDetails od ON p.pid = od.pid\n"
                + "INNER JOIN orders o ON od.oid = o.oid\n"
                + "WHERE o.status = 1\n"
                + "GROUP BY p.pname\n"
                + "ORDER BY TotalQuantitySold DESC;";
        List<Map<String, Object>> list = new ArrayList<>();
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                String productName = rs.getString("ProductName");
                int totalQuantitySold = rs.getInt("TotalQuantitySold");

                // Tạo một map để lưu trữ thông tin sản phẩm
                Map<String, Object> productInfo = new HashMap<>();
                productInfo.put("ProductName", productName);
                productInfo.put("TotalQuantitySold", totalQuantitySold);

                list.add(productInfo);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<Product> ListAllProduct() {
        String sql = "SELECT p.pname AS ProductName, SUM(od.quantity) AS TotalQuantitySold\n"
                + "FROM Products p\n"
                + "INNER JOIN orderDetails od ON p.pid = od.pid\n"
                + "INNER JOIN orders o ON od.oid = o.oid\n"
                + "WHERE o.status = 1\n"
                + "GROUP BY p.pname\n"
                + "ORDER BY TotalQuantitySold DESC;";
        List<Product> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {

                Product p = Product.builder()
                        .pname(rs.getString("ProductName"))
                        .quantity(rs.getInt("TotalQuantitySold"))
                        .build();
                list.add(p);

            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }
    public int getTotalSoldProduct() {
        String sql = "  SELECT SUM(od.quantity) AS TotalSoldProducts\n"
                + "FROM [WebBoardGame].[dbo].[orderDetails] od\n"
                + "INNER JOIN [WebBoardGame].[dbo].[orders] o ON od.oid = o.oid\n"
                + "WHERE o.status = 1";
        int totalSoldProduct = 0;
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                totalSoldProduct = rs.getInt("TotalSoldProducts");
            }
            rs.close();
            ps.close();
        } catch (SQLException e) {
        }
        return totalSoldProduct;
    }

    public int getTotalCustomer() {
        String sql = "  SELECT COUNT(*) AS TotalCustomers\n"
                + "FROM [WebBoardGame].[dbo].[Accounts]\n"
                + "WHERE role = 4";
        int totalSoldProduct = 0;
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                totalSoldProduct = rs.getInt("TotalCustomers");
            }
            rs.close();
            ps.close();
        } catch (SQLException e) {
        }
        return totalSoldProduct;
    }

    public static void main(String[] args) {
        StatisticDao s = new StatisticDao();
//        List<OrderStatistics> list1 = s.getOrderStatisticsByMonth();
//        System.out.println(list1);
//        List<StatusOrderStatistic> list = s.countStatusOrder();
//        System.out.println(list);
//       List<Map<String, Object>> list = s.getTopSoldProducts();
//        int total = s.getTotalEarning();
        List<Product> list = s.ListAllProduct();
        System.out.println(list);
    }
}
