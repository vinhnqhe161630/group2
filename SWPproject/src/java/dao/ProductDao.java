/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import context.DBContext;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Account;

import model.Category;
import model.Product;

/**
 *
 * @author HP
 */
public class ProductDao extends DBContext {

    public List<Category> getListCate() {
        String sql = "select * from [dbo].[Categories] where status=1 ";
        List<Category> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {

                Category c = Category.builder()
                        .caid(rs.getInt(1))
                        .caname(rs.getString(2))
                        .build();
                list.add(c);

            }
        } catch (SQLException e) {
        }
        return list;
    }

    public List<Product> getListNewProduct(String sort, String price, String page_raw) {
        String sql = "SELECT *\n"
                + "FROM [dbo].[Products]\n where [status] = 1";

        if (price != null) {
            if (price.equals("1")) {
                sql += " and price BETWEEN 0 AND 100000";
            } else if (price.equals("2")) {
                sql += " and price BETWEEN 100000 AND 300000";
            } else if (price.equals("3")) {
                sql += " and price BETWEEN 300000 AND 500000";
            } else if (price.equals("4")) {
                sql += " and price > 500000 ";
            }
        }
        if (sort != null) {
            if (!sort.equals("new")) {
                sql += " ORDER BY price " + sort + " ,created_at DESC";
            }
        } else {
            sql += " ORDER BY created_at DESC ";
        }

        try {
            int page = 1;
            if (page_raw != null) {
                page = Integer.parseInt(page_raw);
            }

            sql += " OFFSET " + (page - 1) * 9 + " ROWS\n"
                    + "FETCH FIRST 9 ROWS Only";

        } catch (Exception e) {

        }
        List<Product> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {

                Product p = Product.builder()
                        .pid(rs.getInt(1))
                        .pname(rs.getString(2))
                        .rate(rs.getFloat(3))
                        .price(rs.getInt(4))
                        .img(rs.getString(5))
                        .priceSale(rs.getInt(6))
                        .quantity(rs.getInt(7))
                        .pubid(rs.getInt(8))
                        .saled(rs.getInt(9))
                        .isDiscount(rs.getBoolean(10))
                        .isSoldout(rs.getBoolean(11))
                        .created_at(rs.getString(12))
                        .status(rs.getInt(13))
                        .build();
                list.add(p);

            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Product> getListProductByCate(String caid, String sort, String price, String page_raw) {
        String sql = "select * from Products p\n"
                + "inner join ProductCategory pc on p.pid = pc.pid\n"
                + "inner join Categories c on c.caid = PC.caid\n"
                + "WHERE c.caid = " + caid + " and p.[status] = 1";
        if (price != null) {
            if (price.equals("1")) {
                sql += " AND p.price BETWEEN 0 AND 100000";
            } else if (price.equals("2")) {
                sql += " AND p.price BETWEEN 100000 AND 300000";
            } else if (price.equals("3")) {
                sql += " AND p.price BETWEEN 300000 AND 500000";
            } else if (price.equals("4")) {
                sql += " AND p.price > 500000";
            }
        }

        if (sort != null) {
            if (!sort.equals("new")) {
                sql += " ORDER BY p.price " + sort;
            } else {
                sql += " ORDER BY p.created_at DESC";
            }
        }
        try {
            int page = Integer.parseInt(page_raw);
            if (page == 0) {
                page = 1;
            }

            sql += " OFFSET " + (page - 1) * 9 + " ROWS\n"
                    + "FETCH FIRST 12 ROWS Only";

        } catch (Exception e) {

        }

        List<Product> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {

                Product p = Product.builder()
                        .pid(rs.getInt(1))
                        .pname(rs.getString(2))
                        .rate(rs.getFloat(3))
                        .price(rs.getInt(4))
                        .img(rs.getString(5))
                        .priceSale(rs.getInt(6))
                        .quantity(rs.getInt(7))
                        .pubid(rs.getInt(8))
                        .saled(rs.getInt(9))
                        .isDiscount(rs.getBoolean(10))
                        .isSoldout(rs.getBoolean(11))
                        .created_at(rs.getString(12))
                        .status(rs.getInt(13))
                        .build();
                list.add(p);

            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public int getTotalProduct(String caid, String sort, String price) {
        String sql = "select * from Products p\n"
                + "inner join ProductCategory pc on p.pid = pc.pid\n"
                + "inner join Categories c on c.caid = PC.caid\n"
                + "WHERE c.caid = " + caid + " and p.status = 1";
        if (price != null) {
            if (price.equals("1")) {
                sql += " AND p.price BETWEEN 0 AND 100000";
            } else if (price.equals("2")) {
                sql += " AND p.price BETWEEN 100000 AND 300000";
            } else if (price.equals("3")) {
                sql += " AND p.price BETWEEN 300000 AND 500000";
            } else if (price.equals("4")) {
                sql += " AND p.price > 500000";
            }
        }

        if (sort != null) {
            if (!sort.equals("new")) {
                sql += " ORDER BY p.price " + sort;
            } else {
                sql += " ORDER BY p.created_at DESC";
            }
        }

        int a = 0;
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                a = a + 1;

            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return a;
    }

    public int getNumberPage(int total, int numentries) {

        try {

            int countPage = 0;
            countPage = total / numentries;
            if (total % numentries != 0) {
                countPage++;
            }
            return countPage;

        } catch (Exception e) {
        }

        return 0;
    }

    public List<Product> getListProShockprice(String sort, String price, String page_raw) {
        String sql = "select * from [dbo].[Products]\n"
                + "where isDiscount = 1 and [status]=1\n";

        if (price != null) {
            if (price.equals("1")) {
                sql += " AND price BETWEEN 0 AND 100000";
            } else if (price.equals("2")) {
                sql += " AND price BETWEEN 100000 AND 300000";
            } else if (price.equals("3")) {
                sql += " AND price BETWEEN 300000 AND 500000";
            } else if (price.equals("4")) {
                sql += " AND price > 500000";
            }
        }

        if (sort != null) {
            if (!sort.equals("new")) {
                sql += " ORDER BY price " + sort;
            }
        } else {
            sql += " ORDER BY created_at DESC";
        }
        try {
            int page = 1;
            if (page_raw != null) {
                page = Integer.parseInt(page_raw);
            }

            sql += " , price-priceSale DESC "
                    + "OFFSET " + (page - 1) * 9 + " ROWS\n"
                    + "FETCH FIRST 9 ROWS Only";

        } catch (Exception e) {

        }

        List<Product> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {

                Product p = Product.builder()
                        .pid(rs.getInt(1))
                        .pname(rs.getString(2))
                        .rate(rs.getFloat(3))
                        .price(rs.getInt(4))
                        .img(rs.getString(5))
                        .priceSale(rs.getInt(6))
                        .quantity(rs.getInt(7))
                        .pubid(rs.getInt(8))
                        .saled(rs.getInt(9))
                        .isDiscount(rs.getBoolean(10))
                        .isSoldout(rs.getBoolean(11))
                        .created_at(rs.getString(12))
                        .status(rs.getInt(13))
                        .build();
                list.add(p);

            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Product> getListHotPro(String sort, String price, String page_raw) {
        String sql = "SELECT \n"
                + "    p.*,\n"
                + "    SUM(od.quantity) AS TotalQuantity\n"
                + "FROM\n"
                + "    Products p\n"
                + "LEFT JOIN\n"
                + "    orderDetails od ON p.pid = od.pid\n"
                + "LEFT JOIN\n"
                + "    orders o ON od.oid = o.oid\n"
                + "WHERE\n"
                + "   p.[status]=1\n";

        if (price != null) {
            if (price.equals("1")) {
                sql += " AND p.price BETWEEN 0 AND 100000";
            } else if (price.equals("2")) {
                sql += " AND p.price BETWEEN 100000 AND 300000";
            } else if (price.equals("3")) {
                sql += " AND p.price BETWEEN 300000 AND 500000";
            } else if (price.equals("4")) {
                sql += " AND p.price > 500000";
            }
        }

        sql += "GROUP BY\n"
                + "    p.pid, p.pname, p.rate, p.price, p.[img], p.priceSale, p.quantity,"
                + " p.pubid, p.[created_at], p.saled, p.isDiscount, p.isSoldout,p.status \n";

        if (sort != null) {
            if (!sort.equals("new")) {
                sql += " ORDER BY p.price " + sort;
            }
        } else {
            sql += " ORDER BY p.created_at DESC";
        }
        try {
            int page = 1;
            if (page_raw != null) {
                page = Integer.parseInt(page_raw);
            }

            sql += "  , TotalQuantity DESC "
                    + "OFFSET " + (page - 1) * 9 + " ROWS\n"
                    + "FETCH FIRST 9 ROWS Only";

        } catch (Exception e) {

        }
        List<Product> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {

                Product p = Product.builder()
                        .pid(rs.getInt(1))
                        .pname(rs.getString(2))
                        .rate(rs.getFloat(3))
                        .price(rs.getInt(4))
                        .img(rs.getString(5))
                        .priceSale(rs.getInt(6))
                        .quantity(rs.getInt(7))
                        .pubid(rs.getInt(8))
                        .saled(rs.getInt(9))
                        .isDiscount(rs.getBoolean(10))
                        .isSoldout(rs.getBoolean(11))
                        .created_at(rs.getString(12))
                        .status(rs.getInt(13))
                        .build();
                list.add(p);

            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Product> getListRecomment(int acid, String sort, String price, String page_raw) {
        String sql = "WITH CustomerPurchasedProducts AS (\n"
                + "    SELECT DISTINCT od.pid\n"
                + "    FROM orderDetails od\n"
                + "    INNER JOIN orders o ON od.oid = o.oid\n"
                + "    WHERE o.acid = ?"
                + "), CustomerPurchasedCategories AS (\n"
                + "    SELECT DISTINCT pc.caid\n"
                + "    FROM CustomerPurchasedProducts cpp\n"
                + "    INNER JOIN ProductCategory pc ON cpp.pid = pc.pid\n"
                + ")\n"
                + "\n"
                + "SELECT p.*\n"
                + "FROM Products p\n"
                + "INNER JOIN ProductCategory pc ON p.pid = pc.pid\n"
                + "WHERE pc.caid IN (SELECT caid FROM CustomerPurchasedCategories)\n"
                + "AND p.pid NOT IN (SELECT pid FROM CustomerPurchasedProducts) and p.[status] = 1"
                + "";

        if (price != null) {
            if (price.equals("1")) {
                sql += " AND price BETWEEN 0 AND 100000";
            } else if (price.equals("2")) {
                sql += " AND price BETWEEN 100000 AND 300000";
            } else if (price.equals("3")) {
                sql += " AND price BETWEEN 300000 AND 500000";
            } else if (price.equals("4")) {
                sql += " AND price > 500000";
            }
        }

        if (sort != null) {
            if (!sort.equals("new")) {
                sql += " ORDER BY price " + sort;
            }
        } else {
            sql += " ORDER BY created_at DESC";
        }
        try {
            int page = 1;
            if (page_raw != null) {
                page = Integer.parseInt(page_raw);
            }

            sql += "   "
                    + "OFFSET " + (page - 1) * 9 + " ROWS\n"
                    + "FETCH FIRST 9 ROWS Only";

        } catch (Exception e) {

        }

        if (acid == 0) {
            acid = 1;
        }
        List<Product> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, acid);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {

                Product p = Product.builder()
                        .pid(rs.getInt(1))
                        .pname(rs.getString(2))
                        .rate(rs.getFloat(3))
                        .price(rs.getInt(4))
                        .img(rs.getString(5))
                        .priceSale(rs.getInt(6))
                        .quantity(rs.getInt(7))
                        .pubid(rs.getInt(8))
                        .saled(rs.getInt(9))
                        .isDiscount(rs.getBoolean(10))
                        .isSoldout(rs.getBoolean(11))
                        .created_at(rs.getString(12))
                        .status(rs.getInt(13))
                        .build();
                list.add(p);

            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Product> getListpBySaled(String sort, String price, String page_raw) {
        String sql = "SELECT *\n"
                + "FROM [dbo].[Products]\n where [status] = 1 ";

        if (price != null) {
            if (price.equals("1")) {
                sql += " and price BETWEEN 0 AND 100000";
            } else if (price.equals("2")) {
                sql += " and price BETWEEN 100000 AND 300000";
            } else if (price.equals("3")) {
                sql += " and price BETWEEN 300000 AND 500000";
            } else if (price.equals("4")) {
                sql += " and price > 500000 ";
            }
        }
        if (sort != null) {
            if (!sort.equals("new")) {
                sql += " ORDER BY price " + sort + " ,created_at DESC";
            }
        } else {
            sql += " ORDER BY saled DESC ";
        }

        try {
            int page = 1;
            if (page_raw != null) {
                page = Integer.parseInt(page_raw);
            }

            sql += " OFFSET " + (page - 1) * 9 + " ROWS\n"
                    + "FETCH FIRST 9 ROWS Only";

        } catch (Exception e) {

        }
        List<Product> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {

                Product p = Product.builder()
                        .pid(rs.getInt(1))
                        .pname(rs.getString(2))
                        .rate(rs.getFloat(3))
                        .price(rs.getInt(4))
                        .img(rs.getString(5))
                        .priceSale(rs.getInt(6))
                        .quantity(rs.getInt(7))
                        .pubid(rs.getInt(8))
                        .saled(rs.getInt(9))
                        .isDiscount(rs.getBoolean(10))
                        .isSoldout(rs.getBoolean(11))
                        .created_at(rs.getString(12))
                        .status(rs.getInt(13))
                        .build();
                list.add(p);

            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public ArrayList<Product> getListProduct() {
        ArrayList<Product> pList = new ArrayList<>();

        try {
            String sql = "select * from Products where [status] = 1";
            PreparedStatement pstm = connection.prepareStatement(sql);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                Product pro = Product.builder()
                        .pid(rs.getInt("pid"))
                        .pname(rs.getString("pname"))
                        .rate(rs.getFloat("rate"))
                        .price(rs.getInt("price"))
                        .img(rs.getString("img"))
                        .priceSale(rs.getInt("priceSale"))
                        .quantity(rs.getInt("quantity"))
                        .pubid(rs.getInt("pubid"))
                        .saled(rs.getInt("saled"))
                        .isDiscount(rs.getBoolean("isDiscount"))
                        .isSoldout(rs.getBoolean("isSoldout"))
                        .status(rs.getInt("status"))
                        .build();
                pList.add(pro);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return pList;
    }

    public Product getProductbyID(String pid) {
        String sql = "SELECT * FROM [dbo].[Products] where pid = " + pid + " and [status] = 1";
        try {
            PreparedStatement pstm = connection.prepareStatement(sql);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                Product pro = Product.builder()
                        .pid(rs.getInt("pid"))
                        .pname(rs.getString("pname"))
                        .rate(rs.getFloat("rate"))
                        .price(rs.getInt("price"))
                        .img(rs.getString("img"))
                        .priceSale(rs.getInt("priceSale"))
                        .quantity(rs.getInt("quantity"))
                        .pubid(rs.getInt("pubid"))
                        .saled(rs.getInt("saled"))
                        .isDiscount(rs.getBoolean("isDiscount"))
                        .isSoldout(rs.getBoolean("isSoldout"))
                        .status(rs.getInt("status"))
                        .build();
                return pro;
            }

        } catch (Exception e) {

        }
        return null;
    }

    public void UpdateQuantityProductWhenOrder(int pid, int quantity, int saled) {
        String sql = " Update Products \n"
                + "set quantity = " + quantity
                + ", saled = " + saled
                + " where pid = " + pid;

        try {
            PreparedStatement st = connection.prepareStatement(sql);

            st.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e);

        }
    }

    public int getQuantityProductByPid(int pid) {

        try {
            String sql = "select * from [dbo].Products "
                    + "               where pid = " + pid;
            PreparedStatement pstm = connection.prepareStatement(sql);
            ResultSet rs = pstm.executeQuery();
            if (rs.next()) {

                return rs.getInt("quantity");

            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return 0;
    }

    public Product getProductByID(String Pid) {

        try {
            String sql = "select * from Products where pid = " + Pid;
            PreparedStatement pstm = connection.prepareStatement(sql);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                Product pro = Product.builder()
                        .pid(rs.getInt("pid"))
                        .pname(rs.getString("pname"))
                        .rate(rs.getFloat("rate"))
                        .price(rs.getInt("price"))
                        .img(rs.getString("img"))
                        .priceSale(rs.getInt("priceSale"))
                        .quantity(rs.getInt("quantity"))
                        .pubid(rs.getInt("pubid"))
                        .saled(rs.getInt("saled"))
                        .created_at(rs.getString("created_at"))
                        .isDiscount(rs.getBoolean("isDiscount"))
                        .isSoldout(rs.getBoolean("isSoldout"))
                        .status(rs.getInt("status"))
                        .build();
                return pro;
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    public List<Category> getListCategoryByPid(String Pid) {
        ArrayList<Category> data = new ArrayList<>();
        try {
            String sql = " select p.pid, pc.caid, c.caname from Products p INNER JOIN ProductCategory pc ON p.[pid] = pc.[pid] INNER JOIN Categories c ON pc.[caid] = c.[caid] where pc.pid = " + Pid;

            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Category ca = Category.builder()
                        .caid(rs.getInt("caid"))
                        .caname(rs.getString("caname"))
                        .build();
                data.add(ca);
            }
        } catch (Exception e) {
            System.out.println("get List Category By Pid " + e.getMessage());
        }
        return data;
    }

    //dem so luong san pham trong database
    public int getTotalProduct() {
        String sql = "select count(*) from Products ";
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException e) {
        }
        return 0;
    }

    public List<Product> pagingProduct(int index) {
        List<Product> list = new ArrayList<>();
        String sql = "SELECT *\n"
                + "FROM Products\n"
                + "WHERE status = 1 OR status = 0\n"
                + "ORDER BY pid DESC\n"
                + "OFFSET ? ROWS FETCH NEXT 10 ROWS ONLY ";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, (index - 1) * 10);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Product p = Product.builder()
                        .pid(rs.getInt(1))
                        .pname(rs.getString(2))
                        .rate(rs.getFloat(3))
                        .price(rs.getInt(4))
                        .img(rs.getString(5))
                        .priceSale(rs.getInt(6))
                        .quantity(rs.getInt(7))
                        .pubid(rs.getInt(8))
                        .saled(rs.getInt(9))
                        .isDiscount(rs.getBoolean(10))
                        .isSoldout(rs.getBoolean(11))
                        .created_at(rs.getString(12))
                        .status(rs.getInt(13))
                        .build();
                list.add(p);
            }
        } catch (Exception e) {
        }
        return list;
    }

    public List<Product> searchProductByName(String pname) {
        List<Product> list = new ArrayList<>();
        String sql = "select * from Products where pname like ? ";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, "%" + pname + "%");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Product p = Product.builder()
                        .pid(rs.getInt(1))
                        .pname(rs.getString(2))
                        .rate(rs.getFloat(3))
                        .price(rs.getInt(4))
                        .img(rs.getString(5))
                        .priceSale(rs.getInt(6))
                        .quantity(rs.getInt(7))
                        .pubid(rs.getInt(8))
                        .saled(rs.getInt(9))
                        .isDiscount(rs.getBoolean(10))
                        .isSoldout(rs.getBoolean(11))
                        .created_at(rs.getString(12))
                        .status(rs.getInt(13))
                        .build();
                list.add(p);
            }

        } catch (Exception e) {
        }
        return list;
    }

    public void deleteProduct(int pid) {

        try {
            String sql = "delete from Products where pid = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, pid);
            st.executeUpdate();

        } catch (Exception e) {
        }
    }

      public void updateProduct(String pname, int price, String img, int quantity, String status, int pid) {

        try {
            String sql = "    UPDATE [WebBoardGame4].[dbo].[Products]\n"
                    + "SET\n"
                    + "  pname = ?,\n"
                    + "  price = ?, \n"
                    + "  img = ?,\n"
                    + "  quantity = ?,\n"
                    + "  status = ?\n"
                    + "WHERE pid = ? ";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, pname);
            st.setInt(2, price);
            st.setString(3, img);
            st.setInt(4, quantity);
            st.setString(5, status);
            st.setInt(6, pid);

            st.execute();

        } catch (Exception e) {
        }
    }

    public void insertProduct(String pname, int price, String img, int quantity, int status) {

        try {
            String sql = "    INSERT INTO [WebBoardGame].[dbo].[Products] \n"
                    + "( pname, price, img, quantity,created_at,status )\n"
                    + "VALUES\n"
                    + "(?, ?, ?, ?, GetDate(), ?) ";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, pname);
            st.setInt(2, price);
            st.setString(3, img);
            st.setInt(4, quantity);
            st.setInt(5, status);

            st.execute();

        } catch (Exception e) {
        }
    }
        public boolean checkUserExist(String pname) {
        try {
            String strSelect = " SELECT * FROM Products WHERE pname = ?";
            PreparedStatement st = connection.prepareStatement(strSelect);
            st.setString(1, pname);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return true;
            }
        } catch (Exception e) {
            System.out.println("checkUserExist" + e.getMessage());
        }
        return false;
    }

    public int getTotalSoldProduct() {
        int toltalQuantity = 0;
        try {
            String sql = "SELECT SUM(saled) AS total FROM Products";
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();

            if (rs.next()) {
                toltalQuantity = rs.getInt("total");
            }
        } catch (Exception e) {
            System.out.println("total View " + e.getMessage());
        }
        return toltalQuantity;
    }

    public List<Product> getTop10Sold() {
        List<Product> list = new ArrayList<>();
        try {
            String sql = "SELECT TOP (10) saled, pid, pname, price, rate FROM Products ORDER BY saled DESC ;";
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                Product p = Product.builder()
                        .pid(rs.getInt("pid"))
                        .pname(rs.getString("pname"))
                        .saled(rs.getInt("saled"))
                        .price(rs.getInt("price"))
                        .rate(rs.getFloat("rate"))
                        .build();
                list.add(p);
            }
        } catch (Exception e) {
            System.out.println("top 10 " + e.getMessage());
        }
        return list;
    }
 public List<Integer> getProductCountByCaid() {
        List<Integer> productCounts = new ArrayList<>();
                    String sql = "SELECT caid, COUNT(*) AS product_count FROM Productcategory GROUP BY caid";

        // Kết nối đến cơ sở dữ liệu (bạn cần cung cấp thông tin kết nối thích hợp)
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                int productCount = resultSet.getInt("product_count");
                productCounts.add(productCount);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return productCounts;
    }
    public List<Product> getTop10SlowestSold() {
        List<Product> list = new ArrayList<>();
        try {
            String sql = "SELECT TOP (10) saled, pid, pname, price, rate FROM Products ORDER BY saled ASC ;";
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                Product p = Product.builder()
                        .pid(rs.getInt("pid"))
                        .pname(rs.getString("pname"))
                        .saled(rs.getInt("saled"))
                        .price(rs.getInt("price"))
                        .rate(rs.getFloat("rate"))
                        .build();
                list.add(p);
            }
        } catch (Exception e) {
            System.out.println("top 10 " + e.getMessage());
        }
        return list;
    }



    public static void main(String[] args) {
        ProductDao pd = new ProductDao();
        System.out.println();

        pd.UpdateQuantityProductWhenOrder(10, 100, 10);
    }
}
