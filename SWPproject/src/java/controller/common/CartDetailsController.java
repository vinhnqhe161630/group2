/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.common;

import Validation.Cart;
import dao.AccountDao;
import dao.NewsDao;
import dao.OrderDao;
import dao.ProductDao;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.LinkedHashMap;
import java.util.List;
import model.Account;
import model.CategoriesNew;
import model.Category;
import model.Order;
import model.Product;

/**
 *
 * @author HP
 */
@WebServlet(name = "CartDetails", urlPatterns = {"/cartDetails"})
public class CartDetailsController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CartDetails</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CartDetails at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        OrderDao od = new OrderDao();
        ProductDao pd = new ProductDao();
        NewsDao nd = new NewsDao();
        HttpSession session = request.getSession();  
        Account ac = (Account) session.getAttribute("account");

        //List category header
        List<Category> caList = pd.getListCate();
        List<CategoriesNew> canewList = nd.getListCategoriesNew();
        request.setAttribute("caList", caList);
        request.setAttribute("canewList", canewList);

        Cookie[] arr = request.getCookies();
        String txt = "";
        if (arr != null) {
            for (Cookie o : arr) {
                if (o.getName().equals("cart")) {
                    txt += o.getValue();

                }
            }
        }
        Cart t = new Cart();
        LinkedHashMap<Product, Integer> pList = t.getCart(txt);
        request.setAttribute("pList", pList);
        
        //hot sale    
         List<Product> photsale  = pd.getListpBySaled(null,null,null);
   
     request.setAttribute("photsale", photsale);
        request.getRequestDispatcher("CartDetails.jsp").forward(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String pid = request.getParameter("pid");
        String quantity_raw = request.getParameter("quantity");
        String type = request.getParameter("type");

        Cart t = new Cart();
        ProductDao pd = new ProductDao();
      String script1 = "<script type='text/javascript'>window.location.href='cartDetails';</script>";

    
        Product p = pd.getProductbyID(pid);
        //Lấy cookie
        Cookie[] arr = request.getCookies();
        String txt = "";
        if (arr != null) {
            for (Cookie o : arr) {
                if (o.getName().equals("cart")) {
                    txt += o.getValue();
                    //Xóa cookie
                    o.setMaxAge(0);
                    response.addCookie(o);
                }
            }
        }
        if (type != null) {
            txt = t.removeProductinCart(txt, pid);
           
            
        } else {

            //Update quantity
            try {

                int quantity = Integer.parseInt(quantity_raw);

                if (quantity > 0 && quantity<=p.getQuantity()) {
                    txt = t.updateCartQuantity(txt, pid, quantity);
                }else if(quantity>=p.getQuantity()){
                 String  alertMessage = "Vượt quá số lượng của sản phẩm hiện có";
     
        script1 = "<script type='text/javascript'>alert('" + alertMessage + "');window.location.href='cartDetails';</script>";
     
     
                }  
                else {
                    txt = t.removeProductinCart(txt, pid);
                     
                }
            } catch (Exception e) {
            }
        }
//Tao cookie mới
        if (!txt.equals("")) {
            Cookie c = new Cookie("cart", txt);
            c.setMaxAge(2 * 24 * 60 * 60);

            response.addCookie(c);
        }
       
   
    response.setCharacterEncoding("UTF-8");
    response.setContentType("text/html;charset=UTF-8");
    response.getWriter().println(script1);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
