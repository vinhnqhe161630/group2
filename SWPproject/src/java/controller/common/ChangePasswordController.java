/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.common;

import Validation.ValidationString;
import dao.AccountDao;
import dao.NewsDao;
import dao.OrderDao;
import dao.ProductDao;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import model.Account;
import model.CategoriesNew;
import model.Category;
import model.ShipmentDetails;

/**
 *
 * @author HP
 */
@WebServlet(name = "ChangePasswordController", urlPatterns = {"/changepass"})
public class ChangePasswordController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ChangePasswordController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ChangePasswordController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        AccountDao d = new AccountDao();
        AccountDao ad = new AccountDao();
        ProductDao pd = new ProductDao();
        NewsDao nd = new NewsDao();
        OrderDao od = new OrderDao();
        Account ac = (Account) session.getAttribute("account");
        String email = ac.getEmail();

        String cupass = request.getParameter("cupass");
        String newpass = request.getParameter("newpass");
        String copass = request.getParameter("copass");

        String err = "";

        String displayForm = "block";

        if (newpass == null || copass == null || cupass == null) {
            err = "Not null!.Please re-input";
        } else if (newpass.length() < 3) {
            err = "Newpassword too short";
        } else if (!copass.equals(newpass)) {
            err = "Newpassword does't eaqual Confirmpassword";
        } else if (d.checkAucc(email, cupass) == null) {
            err = "Current Password is incorrect";
        } else {
            session.removeAttribute("accounts");

            // Thay đổi mật khẩu
            d.changepass(email, newpass);
            // Lưu tài khoản bằng session
            session.setAttribute("account", d.checkAucc(email, newpass));
            
        }
        
        List<ShipmentDetails> listsh = ad.getListShipmentOfAcc(ac.getEmail());

        //List category header
        List<Category> caList = pd.getListCate();
        List<CategoriesNew> canewList = nd.getListCategoriesNew();
        request.setAttribute("caList", caList);
        request.setAttribute("canewList", canewList);

        request.setAttribute("account", ac);
        request.setAttribute("listsh", listsh);
        
        request.setAttribute("err", err);
         request.setAttribute("displayForm", displayForm);
         
         request.setAttribute("cupass", cupass);
         request.setAttribute("newpass", newpass);
         request.setAttribute("copass", copass);
        if(err.length()==0){
        String script = "<script type='text/javascript'>alert('Đổi mật khẩu thành công');window.location.href='profile';</script>";
  response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        response.getWriter().println(script);
            }else{
              request.getRequestDispatcher("ProfileUser.jsp").forward(request, response);
        }
       
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
