/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller.common;

import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.News;
import model.NewsDetails;
import dao.NewsDao;
import dao.ProductDao;
import java.util.ArrayList;
import java.util.List;
import model.CategoriesNew;
import model.Category;

/**
 *
 * @author Thanh
 */
@WebServlet(name="NewsDetailController", urlPatterns={"/newsdetails"})
public class NewsDetailController extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet NewsDetailController</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet NewsDetailController at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        String nid = request.getParameter("nid");
        NewsDao ndao = new NewsDao();
        ProductDao pd = new ProductDao();
        
        List<News> nlist = ndao.getListNew();
        List<NewsDetails> ndlist = ndao.getListNewsDetails();
        List<CategoriesNew> canewList  = ndao.getListCategoriesNew();
         List<Category> caList = pd.getListCate();
        
         
        
        News news = ndao.getNewsbyID(nid);
        NewsDetails nd = ndao.getNewsDetailsById(nid);
                
          
//          image address handler
         if(nd!=null){
            String images = nd.getImg();
       
            images = ".\\images\\UploadImgs\\" + images; 
                      
            nd.setImg( images);   
        }
                     
             
        
        request.setAttribute("NewsDetails", nd);
        request.setAttribute("canewList", canewList);
        request.setAttribute("caList", caList);
        request.setAttribute("news", news);
        request.setAttribute("nlist", nlist);
        request.setAttribute("ndlist", ndlist);
        request.getRequestDispatcher("DetailNew.jsp").forward(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
