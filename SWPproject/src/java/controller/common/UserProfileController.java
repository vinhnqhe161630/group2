/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.common;

import Validation.ValidationString;
import dao.AccountDao;
import dao.NewsDao;
import dao.OrderDao;
import dao.ProductDao;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import model.Account;
import model.CategoriesNew;
import model.Category;
import model.Order;
import model.OrderDetails;
import model.Product;
import model.ShipmentDetails;

/**
 *
 * @author HP
 */
@WebServlet(name = "UserProfile", urlPatterns = {"/profile"})
public class UserProfileController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet UserProfile</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet UserProfile at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        AccountDao ad = new AccountDao();
        ProductDao pd = new ProductDao();
        NewsDao nd = new NewsDao();
        OrderDao od = new OrderDao();
        Account ac = (Account) session.getAttribute("account");

        //Thông tin tài khoản
        session.setAttribute("account", ac);
        List<ShipmentDetails> listsh = ad.getListShipmentOfAcc(ac.getEmail());

        //List category header
        List<Category> caList = pd.getListCate();
        List<CategoriesNew> canewList = nd.getListCategoriesNew();
        request.setAttribute("caList", caList);
        request.setAttribute("canewList", canewList);

        request.setAttribute("account", ac);
        request.setAttribute("listsh", listsh);

        request.getRequestDispatcher("ProfileUser.jsp").forward(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String username = request.getParameter("username");

       
        //Khai bao classes
        HttpSession session = request.getSession();
        AccountDao ad = new AccountDao();
        ValidationString vas = new ValidationString();
        String alertMessage = "";
String script = "profile";
        Account ac = (Account) session.getAttribute("account");

        ShipmentDetails sh = ad.getMarkSipment(ac.getEmail());

        // Validate du lieu string
        username = vas.trimAndCheckBlank(username);
       

//        Update profile
        try {
                //Change username
                if (!vas.isValidName(username)) {
                      alertMessage = "Tên người dùng có ít nhất 3 kí tự và không có kí tự đặc biệt. Vui lòng nhập lại tên !";
                 
         script = "<script type='text/javascript'>alert('" + alertMessage + "');window.location.href='profile';</script>";
                } else  if (username == null) {
                      alertMessage = "Vui lòng nhập tên đầy đủ không được để trống.";
               
         script = "<script type='text/javascript'>alert('" + alertMessage + "');window.location.href='profile';</script>";
                }  else {
                    //Xoa sesion cũ
                    session.removeAttribute("account");

                    ad.changeName(username, ac.getAcid());
                    //Tao session moi
                    Account newAcc = ad.checkAcc(ac.getEmail());
                    session.setAttribute("account", newAcc);
 script = "<script type='text/javascript'>window.location.href='profile';</script>";
                }
             
               
        } catch (Exception e) {

        }
        
       
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        response.getWriter().println(script);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
