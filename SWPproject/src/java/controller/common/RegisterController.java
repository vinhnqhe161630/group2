/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller.common;

import Validation.ValidationString;
import dao.AccountDao;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.Account;

/**
 *
 * @author HP
 */
@WebServlet(name="RegisterController", urlPatterns={"/register"})
public class RegisterController extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet RegisterController</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet RegisterController at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        String email = request.getParameter("email");
        String phone = request.getParameter("phone");
        String pass = request.getParameter("pass");
        String repass = request.getParameter("repass");                
        request.setAttribute("email", email);
        request.setAttribute("phone", phone);
        request.setAttribute("pass", pass);
        request.setAttribute("repass", repass);
        
        ValidationString vas = new ValidationString();
        
        email = vas.trimAndCheckBlank(email);
        phone = vas.trimAndCheckBlank(phone);
        pass = vas.trimAndCheckBlank(pass);
        repass = vas.trimAndCheckBlank(repass);
        String alertMessage = "Register successfully.";
        AccountDao adao = new AccountDao();
        String redirectUrl ="";
        if (adao.checkEmailExist(email)) {
          alertMessage= "Email is exist!";
    
           
            redirectUrl ="home";
        }
        else if (adao.checkPhoneExist(phone)) {
            alertMessage= "Phone is exist!";
           
         
            redirectUrl ="home";
        }
        else if (pass.equals(repass) == false) {
           alertMessage= "Password not equal repassword";
                      
           
            redirectUrl ="home";
        }
        else {
           
            adao.register(email, phone, pass);        
               HttpSession session = request.getSession();
               Account a = adao.checkAcc(email);
                session.setAttribute("account", a);
             redirectUrl ="profile";
        }
        
            String script = "<script type='text/javascript'>alert('" + alertMessage + "');window.location.href='" + redirectUrl + "';</script>";
            response.getWriter().println(script);
        
        
    
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
