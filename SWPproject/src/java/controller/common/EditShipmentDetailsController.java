/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.common;

import Validation.ValidationString;
import dao.AccountDao;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.Account;
import model.ShipmentDetails;

/**
 *
 * @author HP
 */
@WebServlet(name = "EditShipmentDetailsController", urlPatterns = {"/editShipment"})
public class EditShipmentDetailsController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet EditShipmentDetailsController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet EditShipmentDetailsController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String changeMarkShip_raw = request.getParameter("changeMarkShip");
        String newPhonenumber = request.getParameter("newPhonenumber");
        String newAddress = request.getParameter("newAddress");
        String newReceiver = request.getParameter("newReceiver");
        String city = request.getParameter("city");
        String district = request.getParameter("district");
        String ward = request.getParameter("ward");
        String type = request.getParameter("type");
String codeSale = request.getParameter("codeSale");

        HttpSession session = request.getSession();
        AccountDao ad = new AccountDao();
        ValidationString vas = new ValidationString();
        
        String alertMessage = "";
         String  redirectUrl = "useCodeSale?codeSale=" + codeSale + 
                "&address=" + newAddress + "&phonenumber=" + newPhonenumber + "&receiver=" + newReceiver+
         "&city=" + city + "&district=" + district + "&ward=" + ward+"&displayForm=block";
         
        try{
        Account ac = (Account) session.getAttribute("account");
        ShipmentDetails sh = ad.getMarkSipment(ac.getEmail());

        newPhonenumber = vas.trimAndCheckBlank(newPhonenumber);
        newAddress = vas.trimAndCheckBlank(newAddress);
        newReceiver = vas.trimAndCheckBlank(newReceiver);
  
        // Change Mark shipmentDetails
        if (type.equals("changeInfo")) {
            if (changeMarkShip_raw != null) {

                int changeShipid = Integer.parseInt(changeMarkShip_raw);
                ad.changeInforShip(changeShipid, ac.getAcid());
                alertMessage = "Đổi thành công.";

            } else {
                alertMessage = "Vui lòng chọn địa chỉ muốn đổi.";
            }
             redirectUrl = "useCodeSale?codeSale=" + codeSale ;
        }
      
        //Add new shipmentDetails
        if (type.equals("addNew")) {
            
          if (!city.isEmpty() && !district.isEmpty() && !ward.isEmpty()) {
                 String address = city + ", " + district + ", " + ward + ", " + newAddress;
          
         
            if (!vas.isValidName(newReceiver)) {
                    alertMessage = "Tên người nhận không hợp lệ ! Vui lòng nhập lại.";
             
                } else if (ad.isAcchaveShipment(ac.getAcid(), address) != null) {
                    alertMessage = "Địa chỉ đã tồn tại ! Vui lòng nhập lại.";
                } else if (!vas.validatePhoneNumber(newPhonenumber)) {
                    alertMessage = "Số điện thoại phải có 10 số! Vui lòng nhập lại.";
               
                } else {
                    ad.createShipment(address, newPhonenumber, ac.getAcid(),newReceiver);
                    alertMessage = "Thêm thành công";
                     redirectUrl = "useCodeSale?codeSale=" + codeSale ;
                }

            
            }else{
                
              if(city.isEmpty()){
               alertMessage = " Vui lòng nhập đầy đủ thông tin tỉnh thành!";
               }else if(district.isEmpty()){
                    alertMessage = " Vui lòng nhập đầy đủ thông tin thành phố!";
               }else if(ward.isEmpty()){
                    alertMessage = " Vui lòng nhập đầy đủ thông tin xã phường!";
               }
          }
        }
        }catch(Exception e){
            
        }
      

        
        String script = "<script type='text/javascript'>alert('" + alertMessage + "');window.location.href='" + redirectUrl + "';</script>";
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        response.getWriter().println(script);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
