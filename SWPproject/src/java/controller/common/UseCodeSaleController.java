/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller.common;

import Validation.Cart;
import Validation.ValidationDate;
import Validation.ValidationString;
import dao.AccountDao;
import dao.CodeSaleDao;
import dao.NewsDao;
import dao.OrderDao;
import dao.ProductDao;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.LinkedHashMap;
import java.util.List;
import model.Account;
import model.CategoriesNew;
import model.Category;
import model.CodeSale;
import model.Product;
import model.ShipmentDetails;

/**
 *
 * @author HP
 */
@WebServlet(name="UseCodeSaleController", urlPatterns={"/useCodeSale"})
public class UseCodeSaleController extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet UseCodeSaleController</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet UseCodeSaleController at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
         Cart t = new Cart();
        ProductDao pd = new ProductDao();
        NewsDao nd = new NewsDao();
        OrderDao od = new OrderDao();
        CodeSaleDao cd = new CodeSaleDao();
         ValidationString vas = new ValidationString();
         
        HttpSession session = request.getSession();
        AccountDao ad = new AccountDao();
        String codeSale = request.getParameter("codeSale");
        
         String receiver = request.getParameter("receiver");
        String address = request.getParameter("address");
        String phonenumber = request.getParameter("phonenumber");

        String city = request.getParameter("city");
        String district = request.getParameter("district");
        String ward = request.getParameter("ward");
        
        String displayForm = request.getParameter("displayForm");
       

        codeSale = codeSale.trim();
       receiver = vas.checkValueIsNull(receiver);
         address = vas.checkValueIsNull(address);
           phonenumber = vas.checkValueIsNull(phonenumber);
        //lấy code Sale
      
            ValidationDate valDate = new ValidationDate();
            String currentDate = valDate.getCurrentTime();
            CodeSale code = cd.getCodeSalebyCode(currentDate,codeSale);
            
            if(code!=null){
            request.setAttribute("codeSale", code); 
            }else{
                 request.setAttribute("err", "Mã giảm giá không hợp lệ! Vui lòng tìm hiểu rõ thông tin ."); 
            } 
            
              Account ac = (Account) session.getAttribute("account");

        //Thông tin tài khoản
        session.setAttribute("account", ac);
        List<ShipmentDetails> listsh = ad.getListShipmentOfAcc(ac.getEmail());
          //List category header
        List<Category> caList = pd.getListCate();
        List<CategoriesNew> canewList = nd.getListCategoriesNew();
        request.setAttribute("caList", caList);
        request.setAttribute("canewList", canewList);
        
          //Lay sản phẩm sẽ mua  
        String txt = "";
        LinkedHashMap<Product, Integer> pList = new LinkedHashMap<>();
       

            Cookie[] arr = request.getCookies();
            // Lấy các sản phẩm trong cart
            if (arr != null) {
                for (Cookie o : arr) {
                    if (o.getName().equals("checkout")) {
                        txt += o.getValue();

                    }
                   
                }
            }
              pList = t.getCart(txt);
                request.setAttribute("pList", pList);
      
        request.setAttribute("listsh", listsh);
        
        request.setAttribute("receiver", receiver);
        request.setAttribute("address", address);
        request.setAttribute("phonenumber", phonenumber);

        request.setAttribute("city", city);
        request.setAttribute("district", district);
        request.setAttribute("ward", ward);
        request.setAttribute("displayForm", displayForm);
           
        request.getRequestDispatcher("Checkout.jsp").forward(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
