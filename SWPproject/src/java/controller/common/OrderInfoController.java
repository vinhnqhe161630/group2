/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller.common;

import Validation.ValidationDate;
import dao.AccountDao;
import dao.CodeSaleDao;
import dao.NewsDao;
import dao.OrderDao;
import dao.ProductDao;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.LinkedHashMap;
import java.util.List;
import model.Account;
import model.CategoriesNew;
import model.Category;
import model.CodeSale;
import model.Order;
import model.OrderDetails;
import model.Product;
import model.ShipmentDetails;

/**
 *
 * @author HP
 */
@WebServlet(name="OrderInfoController", urlPatterns={"/orderInfo"})
public class OrderInfoController extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet OrderInfoController</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet OrderInfoController at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {String oid = request.getParameter("oid");
      OrderDao od = new OrderDao();
     ValidationDate valdate = new ValidationDate();
     String currenDate = valdate.getCurrentTime();
      // Huy don hang
      od.cancelOrder(oid,currenDate);
      
      response.sendRedirect("orderHistory");
          } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        String oid = request.getParameter("oid");
        HttpSession session = request.getSession();
        AccountDao ad = new AccountDao();
        ProductDao pd = new ProductDao();
        NewsDao nd = new NewsDao();
        OrderDao od = new OrderDao();
        CodeSaleDao cd = new CodeSaleDao();
        Account ac = (Account) session.getAttribute("account");

        //Thông tin tài khoản
        session.setAttribute("account", ac);
       
         //List category header
        List<Category> caList = pd.getListCate();
        List<CategoriesNew> canewList = nd.getListCategoriesNew();
        request.setAttribute("caList", caList);
        request.setAttribute("canewList", canewList);
        
        //Order 

        Order order = od.getOrderById(oid);
    
       
        LinkedHashMap<OrderDetails,Product> list = new LinkedHashMap<>();
        List<OrderDetails> orderDetailsList = od.getOrderDetails(oid);
        for (OrderDetails ord : orderDetailsList) {
            Product p = od.getProductByIdOrderDetails(ord.getOdid());
            list.put(ord, p);
            
        }
     
        request.setAttribute("order", order);
     
        request.setAttribute("list", list);
      
       

       
        request.getRequestDispatcher("OrderInfo.jsp").forward(request, response);

    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
