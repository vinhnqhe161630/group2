/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller.maketing;

import Validation.ValidationDate;
import dao.BannerDao;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import java.io.File;
import model.Banner;

/**
 *
 * @author Thanh
 */

@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 2, //50MB
        maxFileSize = 1024 * 1024 * 50, // 50MB
        maxRequestSize = 1024 * 1024 * 50) // 50MB
@WebServlet(name="AddNewBanner", urlPatterns={"/addnewbanner"})
public class AddNewBanner extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AddNewBanner</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AddNewBanner at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        BannerDao ndao = new BannerDao();
        request.getRequestDispatcher("AddNewBanner.jsp").forward(request, response);
    } 

    private String extractFileName(Part part) {
        String contentDisp = part.getHeader("content-disposition");
        String[] items = contentDisp.split(";");
        for (String s : items) {
            if (s.trim().startsWith("filename")) {
                return s.substring(s.indexOf("=") + 2, s.length() - 1);
            }
        }
        return "";
    }

    private File getFolderUpload() {        
        String basePath = getServletContext().getRealPath(File.separator) + "\\..\\..\\web\\images\\UploadImgs\\";
//        System.out.println(basePath);
        File folderUpload = new File(basePath);
        if (!folderUpload.exists()) {
            folderUpload.mkdirs();
        }
        return folderUpload;
    }
    
    private File getFolderUpload2() {        
        String basePath = getServletContext().getRealPath(File.separator) + "\\images\\UploadImgs\\";
//        System.out.println(basePath);
        File folderUpload = new File(basePath);
        if (!folderUpload.exists()) {
            folderUpload.mkdirs();
        }
        return folderUpload;
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
         String files = "";
        for (Part part : request.getParts()) {
            if (part.getName().equals("file")) {
                String fileName = extractFileName(part);
                fileName = new File(fileName).getName();                                            
                files += (files.length()>0?";":"") + fileName;
                System.out.println(this.getFolderUpload().getAbsolutePath() + File.separator + fileName);
                System.out.println(this.getFolderUpload2().getAbsolutePath() + File.separator + fileName);
                part.write(this.getFolderUpload().getAbsolutePath() + File.separator + fileName);                
                part.write(this.getFolderUpload2().getAbsolutePath() + File.separator + fileName);                
            }
        }
        
         String alertMessage = "";
        String redirectUrl = "";
        
        ValidationDate vd = new ValidationDate();
        String status = request.getParameter("status");
        BannerDao dao = new BannerDao();
        
        if (status == null && status.isEmpty() && files.isEmpty() && files == null) {
            alertMessage = "Add Banner Fail";
            redirectUrl = "addnewbanner";
        } else {
             Banner ban = Banner.builder()
                     .create_at(vd.getCurrentTime())
                     .img(files)
                     .status(Integer.parseInt(status))
                     .build();
             dao.addNewBanner(ban);
             alertMessage = "Add Banner Successfully";
            redirectUrl = "bannerlist";
        }
        String script = "<script type='text/javascript'>alert('" + alertMessage + "');window.location.href='" + redirectUrl + "';</script>";
            response.getWriter().println(script);
        
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
