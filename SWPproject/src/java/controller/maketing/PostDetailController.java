/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.maketing;

import dao.NewsDao;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import model.Account;
import model.CategoriesNew;
import model.News;
import model.NewsDetails;

/**
 *
 * @author Thanh
 */
@WebServlet(name = "PostDetailController", urlPatterns = {"/postdetail"})
public class PostDetailController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet PostDetailController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet PostDetailController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String nid = request.getParameter("nid");

        NewsDao ndao = new NewsDao();
        News n = ndao.getNewsbyID(nid);
        NewsDetails nd = ndao.getNewsDetailsById(nid);

        //get categories name
        HashMap<News, String> cate = new HashMap<>();
        CategoriesNew canew = ndao.getCateNewbyId(n.getCanewId() + "");
        cate.put(n, canew.getName());

        //get user name
        HashMap<News, String> user = new HashMap<>();
        Account username = ndao.getUserNameById(n.getAcid() + "");
        user.put(n, username.getUsername());

//      News image address handler
        String images = n.getImg();

        images = ".\\images\\UploadImgs\\" + images;

        n.setImg(images);

//      News Details image address handler
        String ndimages = nd.getImg();

        ndimages = ".\\images\\UploadImgs\\" + ndimages;

        nd.setImg(ndimages);

        request.setAttribute("news", n);
        request.setAttribute("newdetail", nd);
        request.setAttribute("canew", cate);
        request.setAttribute("username", user);
        request.getRequestDispatcher("PostDetails.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
