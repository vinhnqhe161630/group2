/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.maketing;

import dao.NewsDao;
import model.News;
import model.NewsDetails;
import model.CategoriesNew;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import model.Account;

/**
 *
 * @author Thanh
 */
@WebServlet(name = "PostListController", urlPatterns = {"/postlist"})
public class PostListController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet PostListController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet PostListController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String search = request.getParameter("search");
        String sortAtribute = request.getParameter("sort");

        NewsDao ndao = new NewsDao();
        List<News> listpost = ndao.listPost(search,sortAtribute);
        

        //get categories name
        LinkedHashMap<News, String> list = new LinkedHashMap<>();
        for (News news1 : listpost) {
            CategoriesNew canew = ndao.getCateNewbyId(news1.getCanewId() + "");
            list.put(news1, canew.getName());
        }

        //get user name
        LinkedHashMap<News, String> list2 = new LinkedHashMap<>();
        for (News news2 : listpost) {
            Account username = ndao.getUserNameById(news2.getAcid() + "");
            list2.put(news2, username.getUsername());
        }

//        // image address handler
//        for (News n : listpost) {
//            String images = n.getImg();
//        
//            images = ".\\images\\" + images; 
//                      
//            n.setImg( images);            
//        }       
        // Pagination
        int page, numPerPage = 10, size = listpost.size();
        String xpage = request.getParameter("page");
        if (xpage == null) {
            page = 1;
        } else {
            page = Integer.parseInt(xpage);
        }
        int begin = numPerPage * (page - 1);
        int end = Math.min(numPerPage * page, size);
        
        int totalNewsCount = ndao.getTotalNewsCount();
        
        
        request.setAttribute("total", totalNewsCount);
        request.setAttribute("canew", list);
        request.setAttribute("username", list2);
        request.setAttribute("postlist", ndao.getListByPage(listpost, begin, end));
        request.setAttribute("size", size%numPerPage==0?size/numPerPage:(size/numPerPage)+1);
        request.setAttribute("page", page);
        request.getRequestDispatcher("PostList.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
