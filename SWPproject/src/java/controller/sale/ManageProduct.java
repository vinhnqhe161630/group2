/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.sale;

import dao.CategotyDao;
import dao.ProductDao;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import model.Category;
import model.Product;

/**
 *
 * @author tuanm
 */
@WebServlet(name = "ManageProduct", urlPatterns = {"/manageproduct"})
public class ManageProduct extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ManageProduct</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ManageProduct at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ProductDao pd = new ProductDao();
        CategotyDao cd = new CategotyDao();

        String indexPage = request.getParameter("index");
        String Caid = request.getParameter("Caid");
        String selectedCategory = "1";
        String sort = request.getParameter("sort");
        String direction = request.getParameter("direction");
        if (Caid != null) {
            selectedCategory = Caid; // Cập nhật biến selectedCategory với giá trị danh mục đã chọn.
        }

        if (indexPage == null) {
            indexPage = "1";
        }
        int index = Integer.parseInt(indexPage);

        List<Product> listp = pd.getListProduct();
        List<Product> pageProduct = pd.pagingProduct(index);
        List<Category> listc = cd.getListCategory();
        List<Product> listpp = cd.getProductByCaid(Caid);

        int count = pd.getTotalProduct();
        int endPage = count / 10;
        if (count / 10 != 0) {
            endPage++;
        }
          if ("pname".equals(sort)) {
            Comparator<Product> comparator = Comparator.comparing(Product::getPname);
            if ("desc".equals(direction)) {
                comparator = comparator.reversed();
            }
             Collections.sort(pageProduct, comparator);
        }
           if ("price".equals(sort)) {
            Comparator<Product> comparator = Comparator.comparing(Product::getPrice);
            if ("desc".equals(direction)) {
                comparator = comparator.reversed();
            }
             Collections.sort(pageProduct, comparator);
        }
           if ("quantity".equals(sort)) {
            Comparator<Product> comparator = Comparator.comparing(Product::getQuantity);
            if ("desc".equals(direction)) {
                comparator = comparator.reversed();
            }
             Collections.sort(pageProduct, comparator);
        }
           if ("status".equals(sort)) {
            Comparator<Product> comparator = Comparator.comparing(Product::getStatus);
            if ("desc".equals(direction)) {
                comparator = comparator.reversed();
            }
             Collections.sort(pageProduct, comparator);
        }

        request.setAttribute("indexPage", indexPage);

        request.setAttribute("pageProduct", pageProduct);
        request.setAttribute("endPage", endPage);
        request.setAttribute("listp", listp);
        request.setAttribute("listc", listc);
        request.setAttribute("count", count);
        request.setAttribute("listpp", listpp);
        request.setAttribute("Caid", Caid);

        request.getRequestDispatcher("SaleProduct.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ProductDao pd = new ProductDao();
        CategotyDao cd = new CategotyDao();

        String pname = request.getParameter("pname");

        List<Product> searchProduct = pd.searchProductByName(pname);

        if (searchProduct == null || searchProduct.isEmpty()) {
            request.setAttribute("notfound", "Không tìm thấy sản phẩm cần tìm !");
        }
        request.setAttribute("pageProduct", searchProduct);
        request.getRequestDispatcher("SaleProduct.jsp").forward(request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
