/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.sale;

import model.Category;
import dao.CategotyDao;
import dao.ProductCategoryDao;
import dao.ProductDao;
import dao.ProductDetailDao;
import dao.PublisherDao;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import model.ProductCategory;
import model.Publisher;

/**
 *
 * @author tuanm
 */
@WebServlet(name = "AddProduct", urlPatterns = {"/addproduct"})
public class AddProduct extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AddProduct</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AddProduct at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String quantity_raw = request.getParameter("quantity");
        String price_raw = request.getParameter("productPrice");
        String requireAge_raw = request.getParameter("requiredage");
        String numPlayer_raw = request.getParameter("numofplayer");
        String caid_raw = request.getParameter("category");

//        ProductCategoryDao pcd = new ProductCategoryDao();
//        if (request.getParameter("category") != null) {
//            int caid = Integer.parseInt(request.getParameter("category"));
//            pcd.insertProductCategory(caid);
//        } 
        CategotyDao ca = new CategotyDao();

        List<Category> listca = ca.getListCategory();

        request.setAttribute("listca", listca);

//        String caname = request.getParameter("caname");
//        if (caname != null) {
//            ca.insertCategory(caname);
//        }
        if (caid_raw != null && price_raw != null && quantity_raw != null && numPlayer_raw != null && requireAge_raw != null) {
            ProductDao pd = new ProductDao();

            String img = request.getParameter("picture");
            String pname = request.getParameter("pname");

            String timePlay = request.getParameter("timeplay");
            String description = request.getParameter("description");
            String rules = request.getParameter("rules");
            String isDiscount = request.getParameter("productDiscountedPrice");

            String pubname = request.getParameter("author");
            String country = request.getParameter("country");
            String material = request.getParameter("material");
            String size = request.getParameter("size");
            String weight = request.getParameter("weight");

            int price = Integer.parseInt(price_raw);
            int quantity = Integer.parseInt(quantity_raw);
            int requiredAge = Integer.parseInt(requireAge_raw);
            int numPayer = Integer.parseInt(numPlayer_raw);
            int caid = Integer.parseInt(caid_raw);

            int status = 1;

            if (pname.trim().isEmpty()) {
                String pnameerror = "Tên sản phẩm không hợp lệ !!!";
                request.setAttribute("pnameerror", pnameerror);
            } else if (pd.checkUserExist(pname)) {
                String nameerror = "Tên sản phẩm đã tồn tại !!!";
                request.setAttribute("nameerror", nameerror);
            } else if (price < 0 || price > 999999999) {
                String priceerror = "Nhập số hợp lệ !!!";
                request.setAttribute("priceerror", priceerror);
            } else if (quantity < 0 || quantity > 999999999) {
                String quantityerror = "Nhập số lượng lớn hơn 0 !!!";
                request.setAttribute("quantityerror", quantityerror);
            } else if (requiredAge < 0 || requiredAge > 110) {
                String ageerror = "Nhập số tuổi hợp lệ !!!";
                request.setAttribute("ageerror", ageerror);
            } else if (numPayer < 0 || numPayer > 100) {
                String numPayererror = "Nhập số người chơi hợp lệ !!!";
                request.setAttribute("numPayererror", numPayererror);
            } else if (description.trim().isEmpty()) {
                String descriptionerr = "Không được để trống mô tả sản phẩm!!!";
                request.setAttribute("descriptionerr", descriptionerr);
            } else if (rules.trim().isEmpty()) {
                String ruleserr = "Không được để trống luật chơi sản phẩm!!!";
                request.setAttribute("ruleserr", ruleserr);
            } else if (timePlay.trim().isEmpty()) {
                String timePlayerr = "Không được để trống thời gian chơi !!!";
                request.setAttribute("timePlayerr", timePlayerr);
            } else if (pubname.trim().isEmpty()) {
                String pubnameerr = "Không được để trống tên Tác giả !!!";
                request.setAttribute("pubnameerr", pubnameerr);
            } else if (country.trim().isEmpty()) {
                String countryerr = "Không được để trống nguồn gốc !!!";
                request.setAttribute("countryerr", countryerr);
            } else if (material.trim().isEmpty()) {
                String materialerr = "Không được để trống nguyên vật liệu !!!";
                request.setAttribute("materialerr", materialerr);
            } else if (size.trim().isEmpty()) {
                String sizeerr = "Không được để trống kích thước sản phẩm !!!";
                request.setAttribute("sizeerr", sizeerr);
            } else if (weight.trim().isEmpty()) {
                String weightnerr = "Không được để trống khối lượng sản phẩm !!!";
                request.setAttribute("weightnerr", weightnerr);
            } else {

                ProductDetailDao pdd = new ProductDetailDao();
                PublisherDao pub = new PublisherDao();
                ProductCategoryDao pcd = new ProductCategoryDao();

                pcd.insertProductCategory(caid);
                pd.insertProduct(pname, price, img, quantity, status);
                pdd.insertProductDetail(numPayer, requiredAge, description, rules, timePlay);
                pub.insertPublisher(pubname, country, material, size, weight);

                String success = "Sản phẩm đã được thêm mới !!!";
                request.setAttribute("success", success);
            }
        }
        request.getRequestDispatcher("AddProduct.jsp").forward(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
