/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.sale;

import Validation.ValidationDate;
import Validation.ValidationString;
import dao.AccountDao;
import dao.OrderDao;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model.Order;

/**
 *
 * @author HP
 */
@WebServlet(name = "OrderListController", urlPatterns = {"/orderList"})
public class OrderListController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet OrderListController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet OrderListController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

         String name = request.getParameter("name");
        String dateFrom = request.getParameter("dateFrom");
        String dateTo = request.getParameter("dateTo");
        String status = request.getParameter("status");
        String page = request.getParameter("page");
        
        ValidationString valtS = new ValidationString();
        ValidationDate valtdate = new ValidationDate();
        OrderDao od = new OrderDao();
        AccountDao ad = new AccountDao();

        //Xử lí trống
        name = valtS.trimAndCheckBlank(name);
        dateTo = valtS.trimAndCheckBlank(dateTo);
        dateFrom = valtS.trimAndCheckBlank(dateFrom);
        status = valtS.trimAndCheckBlank(status);

        // lấy danh sách
        List<Order> orderList = od.getListOrder(name, dateFrom, dateTo, status, page);

        //TÍnh số trang
        int numberPage = od.getNumberPage(od.getTotalOrder(name, dateFrom, dateTo, status), 7);
        
        request.setAttribute("orderList", orderList);
        request.setAttribute("name", name);
        request.setAttribute("dateFrom", dateFrom);
        request.setAttribute("dateTo", dateTo);
        request.setAttribute("status", status);
        request.setAttribute("numberPage", numberPage);
        request.setAttribute("page", page);
        
         request.setAttribute("ad", ad);

        request.getRequestDispatcher("OrderList.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        String name = request.getParameter("name");
//        String dateFrom = request.getParameter("dateFrom");
//        String dateTo = request.getParameter("dateTo");
//        String status = request.getParameter("status");
//        ValidationString valtS = new ValidationString();
//        ValidationDate valtdate = new ValidationDate();
//        OrderDao od = new OrderDao();
//
//        //Xử lí trống
//        name = valtS.trimAndCheckBlank(name);
//        dateTo = valtS.trimAndCheckBlank(dateTo);
//        dateFrom = valtS.trimAndCheckBlank(dateFrom);
//        status = valtS.trimAndCheckBlank(status);
//
//        // lấy danh sách
//        List<Order> orderList = od.getListOrder(name, dateFrom, dateTo, status, null);
//
//        //TÍnh số trang
//        int numberPage = od.getNumberPage(od.getTotalOrder(name, dateFrom, dateTo, status), 7);
//        request.setAttribute("orderList", orderList);
//
//        request.setAttribute("name", name);
//        request.setAttribute("dateFrom", dateFrom);
//        request.setAttribute("dateTo", dateTo);
//        request.setAttribute("status", status);
//        request.setAttribute("numberPage", numberPage);
//
//        request.getRequestDispatcher("OrderList.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
