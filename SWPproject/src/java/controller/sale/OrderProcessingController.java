/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller.sale;

import Validation.ValidationDate;
import dao.OrderDao;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 *
 * @author HP
 */
@WebServlet(name="OrderProcessingController", urlPatterns={"/orderprocess"})
public class OrderProcessingController extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet OrderProcessingController</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet OrderProcessingController at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
          String[] selectedItems = request.getParameterValues("selectedItems");
          String type = request.getParameter("type");
          
           String name = request.getParameter("name");
        String dateFrom = request.getParameter("dateFrom");
        String dateTo = request.getParameter("dateTo");
        String status = request.getParameter("status");
        String page = request.getParameter("page");
             ValidationDate valdate = new ValidationDate();

        String alertMessage = ""; 
        String currenDate = valdate.getCurrentTime();
        OrderDao od = new OrderDao();
        try {
            if (type.equals("cancel")) {
                  
            // Huy don hang
                for (String oid : selectedItems) {
                     od.cancelOrder(oid,currenDate);
                }
               
               
               
            } else if(type.equals("confirm")) {
                  for (String oid : selectedItems) {
                od.confirmOrder(oid);
                }
               
            }else if(type.equals("complete")) {
                for (String oid : selectedItems) {
                     od.completeOrder(oid,currenDate);
                }
                
                
            }

          response.sendRedirect("orderList?page="+page+"&name="+name+"&status="+status+"&dateFrom="+dateFrom+"&dateTo="+dateTo);

        } catch (Exception e) {

        }

    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
       String oid = request.getParameter("oid");
        String type = request.getParameter("type");
          
           String name = request.getParameter("name");
        String dateFrom = request.getParameter("dateFrom");
        String dateTo = request.getParameter("dateTo");
        String status = request.getParameter("status");
        String page = request.getParameter("page");
              
          ValidationDate valdate = new ValidationDate();

        String currenDate = valdate.getCurrentTime();
         OrderDao od = new OrderDao();
        
        
            if (type.equals("cancel")) {
                  
            // Huy don hang
                od.cancelOrder(oid,currenDate);
               
               
            } else if(type.equals("confirm")) {
                od.confirmOrder(oid);
               
            }else {
                 od.completeOrder(oid,currenDate);
                
            }

       response.sendRedirect("orderList?page="+page+"&name="+name+"&status="+status+"&dateFrom="+dateFrom+"&dateTo="+dateTo);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
