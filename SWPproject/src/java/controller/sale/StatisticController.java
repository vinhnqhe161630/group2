/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.sale;

import model.OrderStatistics;
import dao.ProductDao;
import dao.StatisticDao;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import model.Product;

/**
 *
 * @author tuanm
 */
@WebServlet(name = "StatisticController", urlPatterns = {"/saledashboard"})
public class StatisticController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet StatisticController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet StatisticController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        StatisticDao sd = new StatisticDao();
        ProductDao pd = new ProductDao();

        int totalOrders = sd.getTotalOrders();
        double totalEarning = sd.getTotalEarning();
        int  TotalSoldProducts = sd.getTotalSoldProduct();
        int TotalCustomers = sd.getTotalCustomer();
        String indexPage = request.getParameter("index");
        if (indexPage == null) {
            indexPage = "1";
        }
        int index = Integer.parseInt(indexPage);

        List<Product> plist = pd.pagingProduct(index);
        List<OrderStatistics> list = sd.getOrderStatisticsByMonth();
        LinkedHashMap<String, Double> listcount = sd.sos();
         List<Product> listTopSoldOut = sd.ListAllProduct();

        int count = pd.getTotalProduct();
        int endPage = count / 10;
        if (count / 10 != 0) {
            endPage++;
        }

        request.setAttribute("indexPage", indexPage);
        request.setAttribute("endPage", endPage);
        request.setAttribute("count", count);

        request.setAttribute("plist", plist);
        request.setAttribute("listcount", listcount);
        request.setAttribute("list", list);
        request.setAttribute("totalOrders", totalOrders);
        request.setAttribute("totalEarning", totalEarning);
        request.setAttribute("listTopSoldOut", listTopSoldOut);
        request.setAttribute("TotalSoldProducts", TotalSoldProducts);
        request.setAttribute("TotalCustomers", TotalCustomers);

        String sort = request.getParameter("sort");
        String direction = request.getParameter("direction");
        
        if ("quantity".equals(sort)) {
            Comparator<Product> comparator = Comparator.comparing(Product::getQuantity);
            if ("desc".equals(direction)) {
                comparator = comparator.reversed();
            }
             Collections.sort(plist, comparator);
        }
        
         if ("price".equals(sort)) {
            Comparator<Product> comparator = Comparator.comparing(Product::getPrice);
            if ("desc".equals(direction)) {
                comparator = comparator.reversed();
            }
             Collections.sort(plist, comparator);
        }
         
         if ("pname".equals(sort)) {
            Comparator<Product> comparator = Comparator.comparing(Product::getPname);
            if ("desc".equals(direction)) {
                comparator = comparator.reversed();
            }
             Collections.sort(plist, comparator);
        }
         
         if ("status".equals(sort)) {
            Comparator<Product> comparator = Comparator.comparing(Product::getStatus);
            if ("desc".equals(direction)) {
                comparator = comparator.reversed();
            }
             Collections.sort(plist, comparator);
        }
          if ("hotpname".equals(sort)) {
            Comparator<Product> comparator = Comparator.comparing(Product::getPname);
            if ("desc".equals(direction)) {
                comparator = comparator.reversed();
            }
             Collections.sort(listTopSoldOut, comparator);
        }
           if ("hotpquantity".equals(sort)) {
            Comparator<Product> comparator = Comparator.comparing(Product::getQuantity);
            if ("desc".equals(direction)) {
                comparator = comparator.reversed();
            }
             Collections.sort(listTopSoldOut, comparator);
        }
           
         
         
         
          
        

        request.getRequestDispatcher("StatisticSale.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
