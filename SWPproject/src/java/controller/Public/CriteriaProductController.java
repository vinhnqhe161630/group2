/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.Public;

import dao.NewsDao;
import dao.OrderDao;
import dao.ProductDao;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import model.Account;
import model.CategoriesNew;
import model.Category;
import model.Product;

/**
 *
 * @author HP
 */
@WebServlet(name = "CriteriaProductController", urlPatterns = {"/criteria"})
public class CriteriaProductController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet FilterProductController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet FilterProductController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id_raw = request.getParameter("id");
        String sort = request.getParameter("sort");
        String price = request.getParameter("price");
        String page_raw = request.getParameter("page_raw");
        OrderDao od = new OrderDao();
        NewsDao nd = new NewsDao();
        ProductDao pd = new ProductDao();
        HttpSession session = request.getSession();
        try {
            Account ac = (Account) session.getAttribute("account");
            int acid = 0;
            if (ac != null) {
                acid = ac.getAcid();
            }

            List<Category> caList = pd.getListCate();
            List<CategoriesNew> canewList = nd.getListCategoriesNew();

            if (id_raw.equals("1")) {
                //Shock price
                List<Product> shockpriceList = pd.getListProShockprice(sort, price, page_raw);
                request.setAttribute("pList", shockpriceList);
            } else if (id_raw.equals("2")) {
                //Sp hot
                List<Product> photList = pd.getListHotPro(sort, price, page_raw);
                request.setAttribute("pList", photList);
            } else if (id_raw.equals("3")) {
                //Sản phầm mới
                List<Product> pnewList = pd.getListNewProduct(sort, price, page_raw);
                request.setAttribute("pList", pnewList);

            } else if (id_raw.equals("4")) {
                //Recomment
                List<Product> pRecommentList;
                if (od.getListorder(acid,null).size() != 0) {
                    pRecommentList = pd.getListRecomment(acid, sort, price, page_raw);
                } else {
                    pRecommentList = pd.getListpBySaled(sort, price, page_raw);
                }

                request.setAttribute("pList", pRecommentList);
            }

            request.setAttribute("id", id_raw);
            request.setAttribute("sort", sort);
            request.setAttribute("price", price);
            request.setAttribute("canewList", canewList);

            request.setAttribute("caList", caList);
            request.getRequestDispatcher("ProductCriteria.jsp").forward(request, response);

        } catch (Exception e) {

        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
