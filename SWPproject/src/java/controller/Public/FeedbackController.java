/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.Public;

import Validation.ValidationString;
import com.oracle.wls.shaded.org.apache.bcel.generic.AALOAD;
import dao.AccountDao;
import dao.CommentDao;
import dao.FeedbackDao;
import jakarta.servlet.RequestDispatcher;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import model.Account;
import model.Comment;
import model.Feedback;

/**
 *
 * @author hihih
 */
@WebServlet(name = "FeedbackController", urlPatterns = {"/FeedbackController"})
public class FeedbackController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet FeedbackController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet FeedbackController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        CommentDao commentDao = new CommentDao();
        FeedbackDao feedbackDao = new FeedbackDao();
        List<Comment> comments = commentDao.getAllCommentsSortedByFormattedCreateAt();
        List<Feedback> feedbacks = feedbackDao.getAllFeedbacks();
        Map<Integer, List<Feedback>> feedbacksByComment = new HashMap<>();
        for (Feedback feedback : feedbacks) {
            int commentId = feedback.getCommentId();
            feedbacksByComment.computeIfAbsent(commentId, k -> new ArrayList<>()).add(feedback);
        }

        // Lưu thông tin vào request để hiển thị trên trang JSP
        request.setAttribute("feedbacksByComment", feedbacksByComment);

        double averageRate = commentDao.calculateAverageRate();

        // Đưa số rate trung bình vào request attribute
        request.setAttribute("averageRate", averageRate);

        request.setAttribute("feedbacks", feedbacks);
        // Lấy thông tin rate và phần trăm
        Map<Integer, Map<String, Double>> rateStatistics = commentDao.getRateStatistics();

// Đưa thông tin vào request attribute
        request.setAttribute("rateStatistics", rateStatistics);

        // Phân trang:
// Số bình luận trên mỗi trang
        int commentsPerPage = 5;

// Tính tổng số trang
        int totalComments = comments.size();
        int totalPages = (int) Math.ceil((double) totalComments / commentsPerPage);

// Lưu danh sách bình luận và số trang vào request
        request.setAttribute("totalPages", totalPages);
        int currentPage = 1; // Trang mặc định là trang 1
        String pageParam = request.getParameter("page");
        if (pageParam != null && !pageParam.isEmpty()) {
            currentPage = Integer.parseInt(pageParam);
        }

        int startIndex = (currentPage - 1) * commentsPerPage;
        int endIndex = Math.min(startIndex + commentsPerPage, totalComments);
// xử lý next trang
        int previousPage = (currentPage > 1) ? (currentPage - 1) : 1;
        int nextPage = (currentPage < totalPages) ? currentPage + 1 : totalPages;

        request.setAttribute("previousPage", previousPage);
        request.setAttribute("nextPage", nextPage);

        List<Comment> commentsForCurrentPage = comments.subList(startIndex, endIndex);
        request.setAttribute("comments", commentsForCurrentPage);
        request.setAttribute("commentsA", comments);

        // Forward request đến trang JSP để hiển thị
        RequestDispatcher dispatcher = request.getRequestDispatcher("ProductDetail.jsp");
        dispatcher.forward(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Account userAccount = (Account) session.getAttribute("account");
        String Pid = request.getParameter("Pid");
         String action = request.getParameter("action");
        if ("addComment".equals(action)) {
        int rate = Integer.parseInt(request.getParameter("rate"));
        String commentContent = request.getParameter("commentContent");
        String customerName = request.getParameter("customerName");
        String customerEmail = request.getParameter("customerEmail");

        // Tạo cho thằng Guest Acount với role=5
        String user = customerName;
        AccountDao d = new AccountDao();
        ValidationString vas = new ValidationString();
        user = vas.trimAndCheckBlank(user);
        // Kiểm tra và loại bỏ khoảng trống
        if (userAccount == null) {
            // Nếu tên đăng nhập bị khoảng trống, chuyển hướng người dùng về trang đăng nhập với thông báo lỗi
            request.setAttribute("mess", "Tên đăng nhập không được để trống");
            Date currentDate = new Date();
            // Định dạng ngày/tháng/năm (dd/MM/yyyy)
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            // Chuyển đổi thời gian hiện tại sang chuỗi theo định dạng
            String formattedDate = dateFormat.format(currentDate);
            userAccount = Account.builder()
                    .username(customerName)
                    .email(customerEmail)
                    .password("123456789")
                    .role(5)
                    .status(1)
                    .build();
            if (d.checkEmailExist(customerEmail)== false)   {
            d.insertAccount(userAccount);
            userAccount = d.checkAcc(customerEmail);

        }}

        // Lưu bình luận vào cơ sở dữ liệu
        if (commentContent == null || commentContent.isEmpty()) {
            request.setAttribute("error1", "Vui lòng nhập nội dung bình luận.");
        } else {
            Comment comment = Comment.builder()
                    .CommentContent(commentContent)
                    .CustomerEmail(customerEmail)
                    .CustomerName(customerName)
                    .Rate(rate)
                    .AccountId(userAccount.getAcid())
                    .Status(1)
                    .ProductId(Integer.parseInt(Pid))
                    .build();
            CommentDao commentDAO = new CommentDao();
            commentDAO.addComment(comment);
            session.setAttribute("name", customerName);
            session.setAttribute("email", customerEmail);
        }
                    response.sendRedirect("productdetail?Pid=" + Pid);// Redirect lại trang feedback.jsp sau khi lưu

} else
        if ("deleteComment".equals(action)) {
            // Lấy commentId từ tham số POST
            int commentId = Integer.parseInt(request.getParameter("commentId"));
            CommentDao commentDao = new CommentDao();
            commentDao.editComment(commentId, "", 0);
            request.setAttribute("n", commentId);

            // Gọi phương thức để xóa phản hồi dựa trên commentId
            // Chuyển hướng người dùng đến trang sau khi xóa phản hồi (ví dụ: trang danh sách phản hồi)
            response.sendRedirect("productdetail?Pid=" + Pid);// Redirect lại trang feedback.jsp sau khi lưu
        } else if ("editComment".equals(action)) {
            // Lấy commentId từ tham số POST
            int commentId = Integer.parseInt(request.getParameter("commentId"));
            CommentDao commentDao = new CommentDao();
            String newContent = request.getParameter("newContent");

            commentDao.editComment(commentId, newContent, 1);
            request.setAttribute("n", commentId);

            // Gọi phương thức để xóa phản hồi dựa trên commentId
            // Chuyển hướng người dùng đến trang sau khi xóa phản hồi (ví dụ: trang danh sách phản hồi)
            response.sendRedirect("productdetail?Pid=" + Pid);// Redirect lại trang feedback.jsp sau khi lưu
        }

        // Redirect hoặc cập nhật trang JSP
        // Điều này phụ thuộc vào cách bạn muốn xử lý sau khi lưu bình luận
//        response.sendRedirect("productdetail?Pid=" + Pid);// Redirect lại trang feedback.jsp sau khi lưu
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override

    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
