/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.Public;

import dao.AccountDao;
import dao.BannerDao;
import dao.NewsDao;
import dao.OrderDao;
import dao.ProductDao;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import model.Account;
import model.Banner;
import model.CategoriesNew;
import model.Category;
import model.News;
import model.Product;

/**
 *
 * @author HP
 */
@WebServlet(name = "HomeController", urlPatterns = {"/home"})
public class HomeController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet HomeController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet HomeController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();

        Account ac = (Account) session.getAttribute("account");
        int acid = 0;
        if (ac != null) {
            acid = ac.getAcid();
        }
        OrderDao od = new OrderDao();
        ProductDao pd = new ProductDao();
        NewsDao nd = new NewsDao();
        BannerDao bdao = new BannerDao();
        List<Category> caList = pd.getListCate();
        List<CategoriesNew> canewList = nd.getListCategoriesNew();
        List<Banner> banlist = bdao.getListTop3BannerbyStatus();

        //      Banner image address handler
        for (Banner banner : banlist) {
            String images = banner.getImg();
            images = ".\\images\\UploadImgs\\" + images;
            banner.setImg(images);
        }

        //Sản phầm mới
        List<Product> pnewList = pd.getListNewProduct(null, null, null);
        //Shock price
        List<Product> shockpriceList = pd.getListProShockprice(null, null, null);
        //Sp hot
        List<Product> photList = pd.getListHotPro(null, null, null);
        //Recomment
        List<Product> pRecommentList;

        //List new 
        List<News> newsList = nd.getListNew();
        List<News> newsListbyCa = nd.getNewsBycanewId("4");

        //Nếu  có acc hoặc đã mua hàng thì sẽ recomment
        if (od.getListorder(acid, null).size() != 0) {
            pRecommentList = pd.getListRecomment(acid, null, null, null);

        } else {
            //Sản phẩm đã bán được nhiều
            pRecommentList = pd.getListpBySaled(null, null, null);
        }

        request.setAttribute("banlist", banlist);
        request.setAttribute("caList", caList);
        request.setAttribute("canewList", canewList);

        request.setAttribute("pRecommentList", pRecommentList);
        request.setAttribute("photList", photList);
        request.setAttribute("pnewList", pnewList);
        request.setAttribute("shockpriceList", shockpriceList);

        request.setAttribute("newsList", newsList);
        request.setAttribute("newsListbyCa", newsListbyCa);
        request.getRequestDispatcher("Home.jsp").forward(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
