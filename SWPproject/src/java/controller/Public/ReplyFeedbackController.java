/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.Public;

import dao.FeedbackDao;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.Account;
import model.Feedback;

/**
 *
 * @author hihih
 */
@WebServlet(name = "ReplyFeedbackController", urlPatterns = {"/reply"})
public class ReplyFeedbackController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ReplyFeedbackController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ReplyFeedbackController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Account userAccount = (Account) session.getAttribute("account");
        try {
            int commentId = Integer.parseInt(request.getParameter("commentId"));
            String feedbackBySellerName = request.getParameter("feedbackBySellerName");
            String feedbackContent = request.getParameter("feedbackContent");

            if (feedbackContent == null || feedbackContent.isEmpty()) {
                request.setAttribute("error",
                        "Vui lòng nhập nội dung bình luận.");
            } else {
                request.setAttribute("error", "Success");
                Feedback feedback = Feedback.builder()
                        .CommentId(commentId)
                        .FeedbackBySellerName(feedbackBySellerName)
                        .FeedbackContent(feedbackContent)
                        .StatusFeedback(1)
                        .AcountId(1)
                        .build();
                FeedbackDao feedbackDao = new FeedbackDao();
                feedbackDao.addFeedback(feedback);
            }

        } catch (Exception e) {
            request.setAttribute("error", e.toString());
        }
        String Pid = request.getParameter("Pid");

        response.sendRedirect("productdetail?Pid=" + Pid); // Redirect lại trang feedback.jsp sau khi lưu

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
