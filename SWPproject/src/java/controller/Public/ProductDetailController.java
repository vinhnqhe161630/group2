/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.Public;

import dao.CommentDao;
import dao.FeedbackDao;
import dao.ProductDao;
import dao.ProductDetailDao;
import dao.PublisherDao;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import model.Category;
import model.Comment;
import model.Feedback;
import model.Product;
import model.ProductDetail;
import model.Publisher;

/**
 *
 * @author tuanm
 */
@WebServlet(name = "ProductDetail", urlPatterns = {"/productdetail"})
public class ProductDetailController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ProductDetail</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ProductDetail at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        ProductDetailDao pd = new ProductDetailDao();
        ProductDao p = new ProductDao();
        PublisherDao pub = new PublisherDao();

        String Pid = request.getParameter("Pid");

        ProductDetail pdd = pd.getProductDetailByPid(Pid);
        Product pro = p.getProductByID(Pid);
        Publisher pb = pub.getPublisherByPid(Pid);
        List<Category> calist = p.getListCategoryByPid(Pid);

        //hot sale    
        List<Product> photsale = p.getListpBySaled(null, null, null);
        request.setAttribute("photsale", photsale);
        request.setAttribute("pro", pro);
        request.setAttribute("Pid", Pid);
        request.setAttribute("pdd", pdd);
        request.setAttribute("pb", pb);
        request.setAttribute("calist", calist);

        CommentDao commentDao = new CommentDao();
        FeedbackDao feedbackDao = new FeedbackDao();
        List<Comment> comments = commentDao.getAllCommentsSortedByFormattedCreateAt(Pid);
        List<Feedback> feedbacks = feedbackDao.getAllFeedbacks();
        Map<Integer, List<Feedback>> feedbacksByComment = new HashMap<>();
        for (Feedback feedback : feedbacks) {
            int commentId = feedback.getCommentId();
            feedbacksByComment.computeIfAbsent(commentId, k -> new ArrayList<>()).add(feedback);
        }

        // Lưu thông tin vào request để hiển thị trên trang JSP
        request.setAttribute("feedbacksByComment", feedbacksByComment);

        double averageRate = commentDao.calculateAverageRate(Pid);

        // Đưa số rate trung bình vào request attribute
        request.setAttribute("averageRate", averageRate);

        request.setAttribute("feedbacks", feedbacks);
        // Lấy thông tin rate và phần trăm
        Map<Integer, Map<String, Double>> rateStatistics = commentDao.getRateStatistics(Pid);

// Đưa thông tin vào request attribute
        request.setAttribute("rateStatistics", rateStatistics);

        // Phân trang:
// Số bình luận trên mỗi trang
        int commentsPerPage = 5;

// Tính tổng số trang
        int totalComments = comments.size();
        int totalPages = (int) Math.ceil((double) totalComments / commentsPerPage);

// Lưu danh sách bình luận và số trang vào request
        request.setAttribute("totalPages", totalPages);
        int currentPage = 1; // Trang mặc định là trang 1
        String pageParam = request.getParameter("page");
        if (pageParam != null && !pageParam.isEmpty()) {
            currentPage = Integer.parseInt(pageParam);
        }

        int startIndex = (currentPage - 1) * commentsPerPage;
        int endIndex = Math.min(startIndex + commentsPerPage, totalComments);
// xử lý next trang
        int previousPage = (currentPage > 1) ? (currentPage - 1) : 1;
        int nextPage = (currentPage < totalPages) ? currentPage + 1 : totalPages;

        request.setAttribute("previousPage", previousPage);
        request.setAttribute("nextPage", nextPage);

        List<Comment> commentsForCurrentPage = comments.subList(startIndex, endIndex);
        request.setAttribute("comments", commentsForCurrentPage);
        request.setAttribute("commentsA", comments);

        request.getRequestDispatcher("ProductDetail.jsp").forward(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
