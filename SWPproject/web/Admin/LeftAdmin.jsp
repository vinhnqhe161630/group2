<%-- 
    Document   : LeftAdmin
    Created on : 30-9-2023, 05:11:27
    Author     : hihih
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
   <nav id="sidebar">
            <div class="sidebar-header">
                <a href="home"><h3><img src="images/favicon.png" class="img-fluid"/><span>Board Game</span></h3></a>
            </div>
            <ul class="list-unstyled components">
                <li  class="active">
                    <a href="adminDashboard" class="dashboard"><i class="material-icons">dashboard</i>
                        <span>Admin Dashboard</span></a>
                </li>


                <li class="">
                    <a href="userList" 
                       >
                        <i class="material-icons">dashboard</i>User List</a>


                </li>

                <li class="">
                    <a href="setting-list" 
                       >
                        <i class="material-icons">dashboard</i><span>Manage Setting List</span></a>

                </li>
                  <li class="">
                    <a href="saledashboard" 
                       >
                        <i class="material-icons">dashboard</i><span>Sale Dashboard</span></a>

                </li>
                <li class="">
                    <a href="mktdashboard" 
                       >
                        <i class="material-icons">dashboard</i><span>Marketing Dashboard</span></a>

                </li>
                <li class="">
                    <a href="home" 
                       >
                        <i class="material-icons">dashboard</i><span>Back to HomePage</span></a>

                </li>

         

            </ul>


        </nav>
</html>
