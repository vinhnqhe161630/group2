<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="vi" lang="vi"><head>

                                        <link rel="stylesheet" type="text/css" href="css/styles.css">


                                                <link rel="stylesheet" type="text/css" href="https://cdn0.fahasa.com/media/css_secure/180dfde691fce34b2e789c30f74feafe.css?q=102553" media="all">
                                                    <link rel="stylesheet" type="text/css" href="https://cdn0.fahasa.com/media/css_secure/5722a920567ae30e71034727845e9254.css?q=102553" media="print">
         <link href="https://www.fahasa.com/blog/rss/index/store_id/1/" title="Blog" rel="alternate" type="application/rss+xml">

                                                                            <title> Đơn hàng của tôi</title>
                                                                            <link rel="stylesheet" href="css/pager.css" >
                                                                                  <link rel="stylesheet" href="css/styles.css" >
     
        
            <style>
              #footer{background: #132539;
    }
#footer .footer_center .info-foot {
    float: left;
    min-height: 370px;
    padding-top: 35px;
    width: 100%;
    background: #132539;
}

                                </style>
            <%@include file="Header.jsp"  %>
    </head>   <body id="offcanvas-container" class=" customer-account-edit"><div id="moe-osm-pusher" style="display: block !important; height: 0px;"></div>

        <!-- Google Tag Manager (noscript) -->

        <!-- End Google Tag Manager (noscript) -->
        <section id="ves-wrapper">
            <section id="page" class="offcanvas-pusher page" role="main">
                <div class="wrapper" id="wrapper">



                    <div class="page">

                        <div class="main-container col2-left-layout no-margin-top">
                            <div class="main">

                                <!--                                <div></div>-->
                                <div class="container">
                                    <div class="container-inner">
                                        <div class="d-flex justify-content-center">
                                            <div class="col-left sidebar col-lg-3 col-md-3 col-sm-12 col-xs-12" style="padding-left: 0px;">

                                                <div class="block block-account">
                                                    <div class="block-title">
                                                        <strong><span>Tài khoản</span></strong>
                                                    </div>
                                                    <div class="block-content">
                                                        <ul>
                                                           
                                                                                                <li><a href="profile">Thông tin tài khoản</a></li>
                                                                                                <li><a href="shipmentDetails">Sổ địa chỉ</a></li>
                                                                                                <li class="orderHistory"><strong><a href="orderHistory">Đơn hàng của tôi</a></strong></li>   
                                                                                                <li><a href="home">Về trang chủ</a></li>                                                                   
                                                                                              
                                                        </ul>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-main col-lg-9 col-md-9 col-sm-12 col-xs-12 col-fhs-main-body">



                                                <div class="my-account" style="width: 100%;"><style>
                                                        .col-fhs-main-body{
                                                            background: #F0F0F0 !important;
                                                            padding-right: 0 !important;
                                                            padding-left: 0 !important;
                                                        }
                                                        .my-account{
                                                            padding-right: 15px;
                                                            padding-left: 15px;
                                                            margin-top: 0 !important;
                                                        }
                                                        .form-group {
                                                            margin-bottom: 20px; /* Tạo khoảng cách giữa các nhóm */
                                                        }
                                                        .fhs-input-group-horizontal-account > label:first-of-type {
                                                            width: 100%;
                                                        }
                                                        label {
                                                            display: block; /* Để các nhãn hiển thị trên từng dòng riêng biệt */
                                                            font-size: 18px; /* Tăng kích thước của nhãn */
                                                            width: 100%;

                                                        }

                                                        .input-field {
                                                            font-size: 13px; /* Tăng kích thước phông chữ trong trường nhập */
                                                            width: 80%; /* Đặt chiều rộng của ô nhập */
                                                            border: none; /* Xóa đường viền mặc định */
                                                            border-bottom: 2px solid rgba(0, 0, 0, 0.5); /* Đặt đường viền ở phía dưới và làm mờ nó đi */
                                                        }
                                                        .custom-paragraph {
                                                            font-size: 13px;
                                                            width: 100%;
                                                            border: none;
                                                            border-bottom: 2px solid rgba(0, 0, 0, 0.5);
                                                            padding: 10px;
                                                            display: inline-block; /* Để phần tử p hiển thị trên cùng một dòng */
                                                        }
                                                    </style>
                                                    <script>
    document.getElementById('btn-save-account-info').addEventListener('click', function() {
        document.getElementById('myForm').submit(); // Sử dụng phương thức submit để gửi form
    });
</script>
                                                    <div style="background-color: #fff;" class="row">
                                                        <div class="col-md-6">
                                                            <div class="page-title-2">
                                                                <h1>Thông tin tài khoản</h1>
                                                            </div>
                                                            <form action="profile" method="post" id="myForm">
                                                                <div>
                                                                    <input name="form_key" type="hidden" value="GSBpD8uC6OzRNNuZ">


                                                                        <div class="fhs-input-box fhs-input-group-horizontal-account">
                                                                            <label>Tên</label>

                                                                            <input class="input-field" type="text" placeholder="Nhập tên" id="firstname" name="username" value="${sessionScope.account.username}" maxlength="200">
                                                                                <span class="fhs-input-icon fhs-textbox-alert"></span>

                                                                                <div class="fhs-input-alert"></div>
                                                                        </div>

                                                                        <div class="fhs-input-box fhs-input-group-horizontal-account fhs-input-send">
                                                                            <label>Email</label>
                                                                            <label class="fhs-input-description"></label>

                                                                            <input class="input-field" type="text" placeholder="Chưa có email" id="email" name="email" value="${sessionScope.account.email}" maxlength="200" disabled="">


                                                                                <label class="fhs-input-description"></label>
                                                                                <div class="fhs-input-alert"></div>
                                                                        </div>





                                                                        
                                                                </div>
                                                                <div style="align-items: center; text-align: center;padding: 20px 0 10px 0;">
                                                                    <button type="submit" title="Lưu thay đổi" id="btn-save-account-info" class="btn-save-confirm">
            <span><span>Lưu thay đổi</span></span>
        </button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                        <div class="col-md-6">
                                                             <c:choose>
                                                        <c:when test="${ empty requestScope.listsh}">
                                                     
                                                           <div class=" page-title-2 " >
                                                                 <h1>Thông tin giao hàng</h1>
                        
                                                            </div>
                                                            
                                                               <div class="fhs-input-box fhs-input-group-horizontal-account fhs-input-send">
                                                                   <label>Chưa có địa chỉ giao hàng.</label>   <br>
                                                                     <a href="shipmentDetails" style="padding: 10px;margin: 10px; font-size: 15px;">Thêm địa chỉ tại đây</a>
                                                                 </div>
                                                           
                                                                            
                                                            
                                                        </c:when> 
                                                        <c:otherwise>
                                                            <div class="row">
                                                                <div class=" col-md-8 page-title-2 " >
                                                                 <h1>Thông tin giao hàng</h1>
                        
                                                            </div>
                                                                <div class="col-md-4" style="color: red;align-items: center; padding: 10px; font-size: 15px">
                                                                    <a href="shipmentDetails" >Đổi địa chỉ</a>
                                                                </div>
                                                            </div>
                                                             
                                                           

                                                            <div class="fhs-input-box fhs-input-group-horizontal-account">
                                                                <label>Người nhận</label>


                                                                <p class="custom-paragraph">${listsh.get(0).receiver}</p>			    

                                                                <div class="fhs-input-alert"></div>
                                                            </div>
                                                            <div class="fhs-input-box fhs-input-group-horizontal-account fhs-input-send">
                                                                <label>Địa chỉ</label>

                                                                <p class="custom-paragraph">${listsh.get(0).address}</p>


                                                                <div class="fhs-input-alert"></div>
                                                            </div>
                                                            <div class="fhs-input-box fhs-input-group-horizontal-account fhs-input-send">
                                                                <label>Số điện thoại</label>

                                                                <p class="custom-paragraph">${listsh.get(0).phonenumber}</p>


                                                                <div class="fhs-input-alert"></div>
                                                            </div>
                                                                </c:otherwise>
                                                                </c:choose>
                                                            

                                                        </div>
                                                    </div>
          <script>
              function showChangePasswordForm() {
    var form = document.getElementById('changepassword');
    form.style.display = 'block';
}
     var displayFormValue = '${displayForm}';

        if (displayFormValue === 'block') {
    var form = document.getElementById('changepassword');
    form.style.display = 'block';
}

            </script> 
     <!-- Your HTML code -->

<script>
    var displayFormValue = '${displayForm}'; // Make sure the value is correctly set to 'block' if the condition is met

    document.addEventListener('DOMContentLoaded', function() {
        if (displayFormValue === 'block') {
            var form = document.getElementById('changepassword');
            if (form) {
                form.style.display = 'block';
            } else {
                console.error("Element with ID 'changepassword' not found.");
            }
        }
    });
</script>

     
                                                                 
                                                    <div class="account-row" style="margin-top: 10px;">
                                                                            <label class="account-title"></label>
                                                                            <div class="account-input">
                                                                                <label onclick="showChangePasswordForm()">Đổi mật khẩu	
                                                                                   
                                                                        
                                                                                </label> <p style="color:red">${err}  </p>
                                                                            </div>
                                                                            <div style="clear: both;"></div>
                                                                        </div>
                                                                            <div class="fhs-edit-account-password-form" id="changepassword" style="display: none;">
                                                                                <form action="changepass" method="post">
                                                                                    
                                                                                 <div class="fhs-input-box fhs-input-group-horizontal-account">
                                                                                <label>Mật khẩu hiện tại*</label>
                                                                                <div class="fhs-input-item">
                                                                                    <div class="fhs-input-group">
                                                                                        <input class="fhs-textbox" type="password" placeholder="Mật khẩu hiện tại" name="cupass" id="current_password" value="${cupass}" maxlength="16">
                                                                                            <span class="fhs-input-icon fhs-textbox-alert"></span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="fhs-input-alert"></div>
                                                                            </div>
                                                                            <div class="fhs-input-box fhs-input-group-horizontal-account">
                                                                                <label>Mật khẩu mới*</label>
                                                                                <div class="fhs-input-item">
                                                                                    <div class="fhs-input-group">
                                                                                        <input class="fhs-textbox" type="password" placeholder="Mật khẩu mới" name="newpass" id="password" value="${newpass}" maxlength="16">
                                                                                            <span class="fhs-input-icon fhs-textbox-alert"></span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="fhs-input-alert"></div>
                                                                            </div>
                                                                            <div class="fhs-input-box fhs-input-group-horizontal-account">
                                                                                <label>Nhập lại mật khẩu mới*</label>
                                                                                <div class="fhs-input-item">
                                                                                    <div class="fhs-input-group">
                                                                                        <input class="fhs-textbox" type="password" placeholder="Nhập lại mật khẩu mới" name="copass" id="confirmation" value="${copass}" maxlength="16">
                                                                                            <span class="fhs-input-icon fhs-textbox-alert"></span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="fhs-input-alert"></div>
                                                                            </div>
                                                                                    <input  style="align-items: center; width: 10%; background: grey;color: white;" type="submit" value="Đổi">
                                                                                    </form>  
                                                                        </div>
                                                                           
                                                </div>

                                            </div>
                                            






                                        </div>                                            </div>
                                </div>
                            </div>
                        </div>
                    </div>	
                </div>







                <%@include file="Footer.jsp"  %>

                </body></html>