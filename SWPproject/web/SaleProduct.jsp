<%-- 
    Document   : ProductList
    Created on : 17-10-2023, 14:23:17
    Author     : tuanm
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
        <title>Quản Lý Sản Phẩm </title>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <!----css3---->
        <link rel="stylesheet" href="css/custom.css">


        <!--google fonts -->

        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600&display=swap" rel="stylesheet">


        <!--google material icon-->
        <link href="https://fonts.googleapis.com/css2?family=Material+Icons"rel="stylesheet">

    </head>
    <body>


        <div class="wrapper">


            <div class="body-overlay"></div>

            <!-------------------------sidebar------------>
            <!-- Sidebar  -->
            <nav id="sidebar">
                <div class="sidebar-header">
                    <h3><img src="images/favicon.png" class="img-fluid"/><span>Board Game</span></h3>
                </div>
                <ul class="list-unstyled components">
                    <li  class="active">
                        <a href="#" class="dashboard"><i class="material-icons">dashboard</i>
                            <span>Quản Lý Sản Phẩm </span></a>
                    </li>

                    <li  class="">
                        <a href="saledashboard"><i class="material-icons">library_books</i><span>Bảng điều khiển bán hàng
                            </span></a>
                    </li>


                </ul>


            </nav>




            <!--------page-content---------------->

            <div id="content">

                <!--top--navbar----design--------->

                <div class="top-navbar">
                    <div class="xp-topbar">

                        <!-- Start XP Row -->
                        <div class="row"> 
                            <!-- Start XP Col -->
                            <div class="col-2 col-md-1 col-lg-1 order-2 order-md-1 align-self-center">
                                <div class="xp-menubar">
                                    <span class="material-icons text-white">signal_cellular_alt
                                    </span>
                                </div>
                            </div> 
                            <!-- End XP Col -->

                            <!-- Start XP Col -->
                            <div class="col-md-5 col-lg-3 order-3 order-md-2">
                                <div class="xp-searchbar">
                                    <form>
                                        <div class="input-group">
                                            <input type="search" class="form-control" 
                                                   placeholder="Search">
                                            <div class="input-group-append">
                                                <button class="btn" type="submit" 
                                                        id="button-addon2">GO</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!-- End XP Col -->

                            <!-- Start XP Col -->
                            <div class="col-10 col-md-6 col-lg-8 order-1 order-md-3">
                                <div class="xp-profilebar text-right">
                                    <nav class="navbar p-0">
                                        <ul class="nav navbar-nav flex-row ml-auto">   
                                            <li class="dropdown nav-item active">
                                                <a href="#" class="nav-link" data-toggle="dropdown">
                                                    <span class="material-icons">notifications</span>
                                                    <span class="notification">4</span>
                                                </a>
                                                <ul class="dropdown-menu">
                                                    <li>
                                                        <a href="#">You have 5 new messages</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">You're now friend with Mike</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Wish Mary on her birthday!</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">5 warnings in Server Console</a>
                                                    </li>

                                                </ul>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#">
                                                    <span class="material-icons">question_answer</span>

                                                </a>
                                            </li>
                                            <li class="nav-item dropdown">
                                                <a class="nav-link" href="#" data-toggle="dropdown">
                                                    <img src="img/user.jpg" style="width:40px; border-radius:50%;"/>
                                                    <span class="xp-user-live"></span>
                                                </a>
                                                <ul class="dropdown-menu small-menu">
                                                    <li>
                                                        <a href="#">
                                                            <span class="material-icons">
                                                                person_outline
                                                            </span>Profile

                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#"><span class="material-icons">
                                                                settings
                                                            </span>Settings</a>
                                                    </li>
                                                    <li>
                                                        <a href="#"><span class="material-icons">
                                                                logout</span>Logout</a>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>


                                    </nav>

                                </div>
                            </div>
                            <!-- End XP Col -->

                        </div> 
                        <!-- End XP Row -->

                    </div>
                    <div class="xp-breadcrumbbar text-center">
                        <h4 class="page-title">Dashboard</h4>  
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active" aria-current="page"><a href="manageproduct">Quản Lý Sản Phẩm </a></li>
                        </ol>                
                    </div>

                </div>



                <!--------main-content------------->
                <form action="manageproduct" method="post">

                    <div class="main-content">
                        <div class="row">
                            <div class="me-5 ms-n2 pe-5">
                                <div>
                                    <label>
                                        <input type="text" name="pname" class="form-control" placeholder="Tìm kiếm sản phẩm">

                                    </label>
                                    <c:if test="${notfound != null}">
                                        <div class="alert alert-warning" role="alert">
                                            <a style="color: red">${notfound}</a> 
                                        </div>
                                    </c:if>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="table-wrapper">
                                    <div class="table-title">
                                        <div class="row">
                                            <div class="col-sm-6 p-0 d-flex justify-content-lg-start justify-content-center">
                                                <h2 class="ml-lg-2">Quản Lý Sản Phẩm </h2>
                                            </div>
                                            <div class="col-sm-6 p-0 d-flex justify-content-lg-end justify-content-center">
                                                <a href="addproduct" class="btn btn-success" >
                                                    <i class="material-icons">&#xE147;</i> <span>Thêm sản phẩm mới</span></a>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <table class="table table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <span class="custom-checkbox">

                                                    </span>
                                                </th>
                                                <th style="font-weight: bold"> Tên sản phẩm
                                                    <c:choose>
                                                        <c:when test="${param.direction == 'asc'}">
                                                            <a href="manageproduct?sort=pname&direction=asc" style="display: none;">&nbsp⬇</a>
                                                            <a href="manageproduct?sort=pname&direction=desc">&nbsp⬆</a>
                                                        </c:when>
                                                        <c:when test="${param.direction == 'desc'}">
                                                            <a href="manageproduct?sort=pname&direction=asc">&nbsp⬇</a>
                                                            <a href="manageproduct?sort=pname&direction=desc" style="display: none;">&nbsp⬆</a>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <a href="manageproduct?sort=pname&direction=asc">&nbsp⬆</a>
                                                        </c:otherwise>
                                                    </c:choose></th>
                                                <th style="font-weight: bold">Gía
                                                    <c:choose>
                                                        <c:when test="${param.direction == 'asc'}">
                                                            <a href="manageproduct?sort=price&direction=asc&index=${indexPage}" style="display: none;">&nbsp⬇</a>
                                                            <a href="manageproduct?sort=price&direction=desc&index=${indexPage}">&nbsp⬆</a>
                                                        </c:when>
                                                        <c:when test="${param.direction == 'desc'}">
                                                            <a href="manageproduct?sort=price&direction=asc&index=${indexPage}">&nbsp⬇</a>
                                                            <a href="manageproduct?sort=price&direction=desc&index=${indexPage}" style="display: none;">&nbsp⬆</a>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <a href="manageproduct?sort=price&direction=asc&index=${indexPage}">&nbsp⬆</a>
                                                        </c:otherwise>
                                                    </c:choose></th>
                                                <th style="font-weight: bold">Số lượng
                                                    <c:choose>
                                                        <c:when test="${param.direction == 'asc'}">
                                                            <a href="manageproduct?sort=quantity&direction=asc&index=${indexPage}" style="display: none;">&nbsp⬇</a>
                                                            <a href="manageproduct?sort=quantity&direction=desc&index=${indexPage}">&nbsp⬆</a>
                                                        </c:when>
                                                        <c:when test="${param.direction == 'desc'}">
                                                            <a href="manageproduct?sort=quantity&direction=asc&index=${indexPage}">&nbsp⬇</a>
                                                            <a href="manageproduct?sort=quantity&direction=desc&index=${indexPage}" style="display: none;">&nbsp⬆</a>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <a href="manageproduct?sort=quantity&direction=asc&index=${indexPage}">&nbsp⬆</a>
                                                        </c:otherwise>
                                                    </c:choose></th>
                                                <th style="font-weight: bold">Trạng thái
                                                    <c:choose>
                                                        <c:when test="${param.direction == 'asc'}">
                                                            <a href="manageproduct?sort=status&direction=asc&index=${indexPage}" style="display: none;">&nbsp⬇</a>
                                                            <a href="manageproduct?sort=status&direction=desc&index=${indexPage}">&nbsp⬆</a>
                                                        </c:when>
                                                        <c:when test="${param.direction == 'desc'}">
                                                            <a href="manageproduct?sort=status&direction=asc&index=${indexPage}">&nbsp⬇</a>
                                                            <a href="manageproduct?sort=status&direction=desc&index=${indexPage}" style="display: none;">&nbsp⬆</a>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <a href="manageproduct?sort=status&direction=asc&index=${indexPage}">&nbsp⬆</a>
                                                        </c:otherwise>
                                                    </c:choose></th>
                                                <th style="font-weight: bold">Tùy chọn</th>
                                            </tr>
                                        </thead>
                                        <c:forEach items="${pageProduct}" var="pp">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <span class="custom-checkbox">
                                                            <img src="images/${pp.img}" style="width: 165%">
                                                        </span>
                                                    </td>
                                                    <td>${pp.pname}</td>
                                                    <td>${pp.price}</td>
                                                    <td>${pp.quantity}</td>
                                                    <td>
                                                        <c:if test="${pp.status  ==  1}">
                                                            <span class="badge bg-label-success" style="background: green;color: white" text-capitalized="">Hiện</span>
                                                        </c:if>
                                                        <c:if test="${pp.status  ==  0}">
                                                            <span class="badge bg-label-danger"style="background: red;color: white;"  text-capitalized="">Ẩn</span>
                                                        </c:if>
                                                    </td>
                                                    <td>
                                                        <a href="edit?Pid=${pp.pid}" class="edit" >
                                                            <i class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i></a>
                                                        <a href="delete?Pid=${pp.pid}" class="delete" >
                                                            <i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i></a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </c:forEach>
                                    </table>
                                    <div class="clearfix">
                                        <div class="hint-text">Hiển thị <b>10</b> trên <b>${count}</b> sản phẩm</div>
                                        <ul class="pagination">

                                            <c:if test="${indexPage > 1}">
                                                <li class="page-item disabled"><a href="manageproduct?index=${indexPage - 1}">Previous</a></li>
                                                </c:if>

                                            <c:forEach begin="1" end="${endPage}" var="i">
                                                <li class="page-item"><a href="manageproduct?index=${i}" class="page-link">${i}</a></li>
                                                </c:forEach>
                                                <c:if test="${indexPage < endPage}">
                                                <li class="page-item"><a href="manageproduct?index=${indexPage + 1}" class="page-link">Next</a></li>
                                                </c:if>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- Edit Modal HTML -->
                            <!-- Edit Modal HTML -->
                            <!-- Delete Modal HTML -->
                        </div>


                        <!---footer---->


                    </div>
                </form>

                <footer class="footer">
                    <div class="container-fluid">
                        <div class="footer-in">
                            <p class="mb-0">&copy 2020 Vishweb design - All Rights Reserved.</p>
                        </div>
                    </div>
                </footer>
            </div>
        </div>


        <!----------html code compleate----------->









        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="js/jquery-3.3.1.slim.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery-3.3.1.min.js"></script>


        <script type="text/javascript">

            $(document).ready(function () {
                $(".xp-menubar").on('click', function () {
                    $('#sidebar').toggleClass('active');
                    $('#content').toggleClass('active');
                });

                $(".xp-menubar,.body-overlay").on('click', function () {
                    $('#sidebar,.body-overlay').toggleClass('show-nav');
                });

            });

        </script>





    </body>

</html>


