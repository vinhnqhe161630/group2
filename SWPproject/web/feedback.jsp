<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="vi" xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml">
    <head>
            <script type="application/ld+json">
            {
              "@context": "https://schema.org",
              "@type": "LocalBusiness",
              "address": {
                "@type": "PostalAddress",
                "addressLocality": "Ha Noi",
                "addressRegion": "Vietnam",
                "streetAddress": "367 Cầu Giấy, Hà Nội."
              },
                "image": [
                  "/uploads/2023/09/logo-1.png"
                ],
                "priceRange": "$$$",
                "description": "Mô tả trang chủ",
                "name": "Trang chủ",
                "telephone": "1900 63 39 09"
            }
        </script>
       <link rel="stylesheet" href="css/home.min.css">

	<script defer="" src="js/jquery.min.js"></script>

    	<link rel="stylesheet" href="css/products.min.css">
	<link rel="stylesheet" href="css/venobox.min.css">
	
	<script defer="" src="js/venobox.min.js"></script>
	
    <link rel="preload" href="/assets/libs/jquery/jquery.min.js" as="script">
    <style type="text/css">
        .container{display:flex;flex-wrap:wrap;margin:0 auto;padding:0 15px;width:100%}.row{display:flex;flex:0 0 calc(100% + 30px);flex-wrap:wrap;margin:0 -15px;width:calc(100% + 30px)}.col{padding:0 15px;transition:.2s}@media only screen and (max-width:575px){.container{max-width:100%}.col-xs-1{flex:0 0 8.33333%;width:8.33333%}.col-xs-2{flex:0 0 16.66667%;width:16.66667%}.col-xs-3{flex:0 0 25%;width:25%}.col-xs-4{flex:0 0 33.33333%;width:33.33333%}.col-xs-5{flex:0 0 41.66667%;width:41.66667%}.col-xs-6{flex:0 0 50%;width:50%}.col-xs-7{flex:0 0 58.33333%;width:58.33333%}.col-xs-8{flex:0 0 66.66667%;width:66.66667%}.col-xs-9{flex:0 0 75%;width:75%}.col-xs-10{flex:0 0 83.33333%;width:83.33333%}.col-xs-11{flex:0 0 91.66667%;width:91.66667%}.col-xs-12{flex:0 0 100%;width:100%}}@media only screen and (min-width:576px){.container{max-width:540px}.col-sm-1{flex:0 0 8.33333%;width:8.33333%}.col-sm-2{flex:0 0 16.66667%;width:16.66667%}.col-sm-3{flex:0 0 25%;width:25%}.col-sm-4{flex:0 0 33.33333%;width:33.33333%}.col-sm-5{flex:0 0 41.66667%;width:41.66667%}.col-sm-6{flex:0 0 50%;width:50%}.col-sm-7{flex:0 0 58.33333%;width:58.33333%}.col-sm-8{flex:0 0 66.66667%;width:66.66667%}.col-sm-9{flex:0 0 75%;width:75%}.col-sm-10{flex:0 0 83.33333%;width:83.33333%}.col-sm-11{flex:0 0 91.66667%;width:91.66667%}.col-sm-12{flex:0 0 100%;width:100%}}@media only screen and (min-width:768px){.container{max-width:720px}.col-md-1{flex:0 0 8.33333%;width:8.33333%}.col-md-2{flex:0 0 16.66667%;width:16.66667%}.col-md-3{flex:0 0 25%;width:25%}.col-md-4{flex:0 0 33.33333%;width:33.33333%}.col-md-5{flex:0 0 41.66667%;width:41.66667%}.col-md-6{flex:0 0 50%;width:50%}.col-md-7{flex:0 0 58.33333%;width:58.33333%}.col-md-8{flex:0 0 66.66667%;width:66.66667%}.col-md-9{flex:0 0 75%;width:75%}.col-md-10{flex:0 0 83.33333%;width:83.33333%}.col-md-11{flex:0 0 91.66667%;width:91.66667%}.col-md-12{flex:0 0 100%;width:100%}}@media only screen and (min-width:992px){.container{max-width:960px}.col-lg-1{flex:0 0 8.33333%;width:8.33333%}.col-lg-2{flex:0 0 16.66667%;width:16.66667%}.col-lg-3{flex:0 0 25%;width:25%}.col-lg-4{flex:0 0 33.33333%;width:33.33333%}.col-lg-5{flex:0 0 41.66667%;width:41.66667%}.col-lg-6{flex:0 0 50%;width:50%}.col-lg-7{flex:0 0 58.33333%;width:58.33333%}.col-lg-8{flex:0 0 66.66667%;width:66.66667%}.col-lg-9{flex:0 0 75%;width:75%}.col-lg-10{flex:0 0 83.33333%;width:83.33333%}.col-lg-11{flex:0 0 91.66667%;width:91.66667%}.col-lg-12{flex:0 0 100%;width:100%}}@media only screen and (min-width:1200px){.container{max-width:1200px}.col-xl-1{flex:0 0 8.33333%;width:8.33333%}.col-xl-2{flex:0 0 16.66667%;width:16.66667%}.col-xl-3{flex:0 0 25%;width:25%}.col-xl-4{flex:0 0 33.33333%;width:33.33333%}.col-xl-5{flex:0 0 41.66667%;width:41.66667%}.col-xl-6{flex:0 0 50%;width:50%}.col-xl-7{flex:0 0 58.33333%;width:58.33333%}.col-xl-8{flex:0 0 66.66667%;width:66.66667%}.col-xl-9{flex:0 0 75%;width:75%}.col-xl-10{flex:0 0 83.33333%;width:83.33333%}.col-xl-11{flex:0 0 91.66667%;width:91.66667%}.col-xl-12{flex:0 0 100%;width:100%}}.flex{flex-wrap:wrap}.flex-center{justify-content:center}.flex-center,.flex-center-left{align-items:center;display:flex}.flex-center-left{justify-content:flex-start}.flex-center-between{align-items:center;display:flex;justify-content:space-between}.flex-center-around{align-items:center;display:flex;justify-content:space-around}.flex-center-right{align-items:center;display:flex;justify-content:flex-end}.flex-left-center{align-items:flex-start;display:flex;justify-content:center}.flex-right-center{align-items:flex-end;display:flex;justify-content:center}.text-center{text-align:center;width:100%}.f-w-b{font-weight:700}.f-italic{font-style:italic}.color-main{color:#212529}.white{color:#fff}.black{color:#000}.w-100{width:100%}.fw-600{font-weight:600}.fw-500{font-weight:500}.color_head:hover{color:#d70018}.cursor_point{cursor:pointer}.toast-top-right{right:12px;top:12px}#toast-container{display:none;pointer-events:none;position:fixed;z-index:999999999}#toast-container *{box-sizing:border-box}#toast-container>div{background-position:15px;background-repeat:no-repeat;border-radius:3px;box-shadow:0 0 12px #999;color:#fff;margin:0 0 6px;overflow:hidden;padding:10px;pointer-events:auto;position:relative;width:350px}#toast-container>div:hover{box-shadow:0 0 12px #000;cursor:pointer;-ms-filter:progid:DXImageTransform.Microsoft.Alpha(Opacity=100);filter:alpha(opacity=100);opacity:1}#toast-container.toast-bottom-center>div,#toast-container.toast-top-center>div{margin-left:auto;margin-right:auto;width:350px}.toast.toast-error{background-color:#bd362f}.toast.toast-success{background-color:#51a351}.toast{align-items:center;background-color:#fff;display:flex;justify-content:flex-start}.toast-icon{flex:0 0 20px}.toast-icon span{border-radius:50%;display:block;display:none;height:20px;text-align:center;width:20px}.toast-icon span i{color:#fff;font-size:16px}.toast-message{flex:0 0 calc(100% - 20px);padding-left:15px}.toast-message_title{color:#333;font-size:15px;font-weight:600;margin-bottom:5px}.toast-message_content{font-size:1em}.toast-error .toast-icon span.error,.toast-success .toast-icon span.success{display:block}.toast-close{cursor:pointer;height:20px;position:absolute;right:10px;text-align:center;top:10px;width:30px}.toast-close i{color:#333;font-size:16px}.product_search{background:#fff;padding:15px 0}.product_search__main{width:100%}.product_search__category{-ms-overflow-style:-ms-autohiding-scrollbar;margin-bottom:7px;min-height:.01%;overflow-x:auto;overflow-y:hidden;width:100%}.product_search__category::-webkit-scrollbar{background-color:#f4f4f4;width:12px}.product_search__category::-webkit-scrollbar-track{background-color:transparent;-webkit-box-shadow:unset}.product_search__category::-webkit-scrollbar-thumb{background-color:#babac0;border:7px solid #f4f4f4}.product_search__category--list{display:table;display:table!important;height:100%;margin:0!important;max-width:100%;min-height:55px;overflow-x:auto;white-space:nowrap;width:100%}.product_search__category--list .category-item{display:table-cell;float:none!important;padding:0 3px!important;vertical-align:middle;width:auto!important}.product_search__category--list .category-item a{background:transparent;border:1px solid #e5e7eb;border-radius:10px;color:#444;display:block;font-size:14px;font-weight:700;line-height:1.8;padding:3px 6px;text-align:center;text-transform:uppercase;white-space:nowrap}.product_search__list_product .list-products{display:flex;flex:0 0 100%;flex-wrap:wrap;width:100%}.product_search__list_product .list-products__item{background:#fff;border-radius:10px;box-shadow:0 1px 2px 0 rgba(60,64,67,.102),0 2px 6px 2px rgba(60,64,67,.149);margin:0 5px 10px;max-width:calc(20% - 10px);padding:5px 10px;position:relative;width:calc(20% - 10px)}.product_search__list_product .list-products__item .thumbnail{display:flex;height:245px;overflow:hidden;position:relative}.product_search__list_product .list-products__item .thumbnail img{height:150px;margin:0 auto;max-width:100%;-o-object-fit:cover;object-fit:cover;transition:all .3s ease;width:auto}.product_search__list_product .list-products__item .detail{height:150px}.product_search__list_product .list-products__item .title_name{-webkit-line-clamp:3;-webkit-box-orient:vertical;color:#4d4d4d;display:-webkit-box;font-size:13px;font-weight:600;height:auto;line-height:1.4;margin-bottom:0;max-height:55px;overflow:hidden;padding:0 0 5px;text-decoration:none;text-overflow:ellipsis;width:100%}.product_search__list_product .list-products__item .ex_pricesale{background-color:#fff86e;box-shadow:0 1px 0 0 rgba(60,64,67,.102),0 2px 3px 2px rgba(60,64,67,.149);color:#000;font-size:13px;font-weight:600;line-height:22px;padding:1px 10px;position:absolute;right:-6px;top:6px}.product_search__list_product .list-products__item .ex_pricesale:after{border-right:7px solid transparent;border-top:4px solid #d5ca00;bottom:-4px;content:"";display:block;height:0;position:absolute;right:-1px;width:0}.product_search__list_product .list-products__item .price{display:flex;font-size:14px;font-weight:700;width:100%}.product_search__list_product .list-products__item .price .old-price{color:#9e9e9e;font-size:12px;font-weight:400;text-decoration-color:#9e9e9e}.product_search__list_product .list-products__item .price .new-price{color:#e10c00;margin-right:20px;text-decoration:none}.product_search__list_product .list-products__item .ex_rating{font-size:11px;margin:5px 0}.product_search .detail__paginate{background:transparent;padding:10px;text-align:center}.product_search .detail__paginate .viewmore span{background:red;border:0;border-radius:15px;color:#fff;display:inline-block;font-size:16px;font-weight:700;margin:0 auto 5px;padding:10px;text-decoration:none;text-transform:capitalize;width:25%}.product_search .detail__paginate .viewmore span:hover{border-color:transparent;color:#000}.product_search .not_product{word-wrap:break-word;align-items:center;background-color:#fff;border:1px solid #c21d32;border-radius:4px;color:#515151;display:flex;font-size:13px;list-style:none outside;margin:0 0 15px;padding:.5em 15px .5em 3.5em;position:relative;width:auto}.product_search .not_product svg{fill:#1e85be;height:20px;margin-right:20px;width:14px}.form{background:#111;margin:0 auto;padding:18px 40px;width:70%}.form .form-component{border:1px solid #ddd;border-radius:5px;padding:10px 20px}.form .form-title__title{color:#fff;font-size:24px;font-weight:700;text-transform:uppercase}.form .form-content{margin-top:15px}.form .form-group{margin-bottom:15px}.form .form-content button{margin-top:15px}.form .form-control{border:1px solid #ddd;border-radius:5px;font-size:12px;padding:6px;width:100%}.form .btn{background-color:rgba(223,29,29,.961);border:none;border-radius:5px;color:#fff;cursor:pointer;padding:10px 20px;width:100%}.form .btn:hover{background-color:rgba(103,15,15,.961)}#register-message .alert{border-radius:5px;color:#fff;font-size:14px;margin-bottom:15px;padding:10px;text-align:center}#register-message .alert-success{background:green;color:#ebccd1}#register-message .alert-danger{background-color:rgba(223,29,29,.961);color:#ebccd1}.banner_frame{margin:0 auto;width:100%}.banner_frame .gift_content .banner_gift{margin:20px 0;max-width:100%;position:relative}.banner_frame .gift_content .banner_gift img{max-width:100%;-o-object-fit:cover;object-fit:cover;width:auto}.banner_frame .banner_product .product_list{flex-wrap:wrap;justify-content:start;width:100%}.banner_frame .banner_product .product_item{background:#fff;border-radius:10px;box-shadow:0 1px 2px 0 rgba(60,64,67,.102),0 2px 6px 2px rgba(60,64,67,.149);margin:0 5px 10px;max-width:calc(20% - 10px);min-height:310px;padding:5px 10px;position:relative;width:calc(20% - 10px)}.banner_frame .banner_product .product_item .thumb{display:flex;overflow:hidden;position:relative}.banner_frame .banner_product .product_item .thumb img{height:150px;margin:0 auto;max-width:100%;-o-object-fit:cover;object-fit:cover;transition:all .3s ease;width:auto}.banner_frame .banner_product .product_item .title_name{-webkit-line-clamp:3;-webkit-box-orient:vertical;color:#4d4d4d;display:-webkit-box;font-size:13px;font-weight:600;height:auto;line-height:1.4;margin-bottom:0;max-height:55px;overflow:hidden;padding:0 0 5px;text-decoration:none;text-overflow:ellipsis;width:100%}.banner_frame .banner_product .product_item .price{display:flex;font-size:14px;font-weight:700;justify-content:space-between;width:100%}.banner_frame .banner_product .product_item .price .old-price{color:#9e9e9e;font-size:12px;font-weight:400;text-decoration-color:#9e9e9e}.banner_frame .banner_product .product_item .price .new-price{color:#e10c00;text-decoration:none}.banner_frame .banner_product .product_item .btn_order{bottom:10px;left:10px;margin-top:5px;position:absolute}.banner_frame .banner_product .product_item .btn_order span{background:#e10c00;border-radius:5px;color:#fff;display:inline-block;padding:5px 10px}.banner_frame .order_promotion .promtion_tilte{align-items:center;justify-content:center}.banner_frame .order_promotion .promtion_tilte p{color:#fff;font-weight:700;margin-bottom:0;text-transform:uppercase;width:auto}.banner_frame .order_promotion .promtion_tilte .btn_view{background:#ffca3f;border-radius:5px;color:#d00;cursor:pointer;font-size:16px;margin-left:20px;padding:5px 10px;text-transform:uppercase}.banner_frame .order_promotion .promotion_content{background-color:#d00;padding:20px 0;position:relative}.banner_frame .order_promotion .promotion_product{padding:10px 0}.banner_frame .product_old{padding:20px 0}.banner_frame .product_old__tilte{display:flex;justify-content:center;margin-bottom:10px}.banner_frame .product_old__tilte p{color:#f44336;font-size:16px;font-weight:700;text-transform:uppercase}.banner_frame .mini_game{position:relative}.banner_frame .mini_game__banner{border-radius:10px;box-shadow:0 1px 2px 0 rgba(60,64,67,.102),0 2px 6px 2px rgba(60,64,67,.149);overflow:hidden}.banner_frame .mini_game__banner img{max-width:100%;-o-object-fit:cover;object-fit:cover;width:auto}.banner_frame .mini_game__button{bottom:20px;position:absolute;right:20px}.banner_frame .mini_game__button .button_item{background:#e2db02;border-radius:20px;color:#000;cursor:pointer;font-size:16px;padding:10px 20px}.banner_frame .mini_game__button .rule_view{margin-right:10px}.banner_frame .popup_mini,.banner_frame .promotion_detail{background-color:#fff;border-radius:4px;box-shadow:0 0 10px rgba(0,0,0,.5);display:none;left:0;margin-top:5px;padding:10px;position:absolute;top:0;width:100%;z-index:9999}.banner_frame .popup-backdrop{background-color:rgba(0,0,0,.5);bottom:0;display:none;left:0;position:fixed;right:0;top:0;z-index:999}.tab-content{border-top:none;margin-bottom:20px}.tab-container{align-items:center;display:flex;justify-content:center;padding:10px 0}.tab-container .tab-button{background-color:#111;border:none;border-radius:5px;color:#fff;cursor:pointer;font-size:16px;margin-right:20px;padding:10px 20px;text-transform:uppercase}.tab-container .tab-button:last-child{margin-right:0}.tab-container .active{border-bottom:none;color:#f44336}.tab-container .button{background-color:#f44336;border:none;color:#fff;cursor:pointer;font-size:16px;padding:10px 20px}*{box-sizing:border-box;margin:0;outline:none;padding:0}img{height:auto;max-width:100%;-o-object-fit:cover;object-fit:cover}body{background:#fff;font-family:Arial;font-size:16px;overflow:auto}button,input,textarea{font-family:Arial,sans-serif}ul{list-style:none;margin:0}a{color:#212529;text-decoration:none;transition:.3s}a:hover{color:#d70018}b{font-weight:700}h1,h2,h3,h4,p,span{color:#212529}h1,h2,h3,h4{font-weight:600}p{line-height:28px}input{padding:0 10px}.main{background:#fff}.title_box{transition:.3s}.title_box:hover{text-decoration-line:underline}button{transition:.3s}button,button:hover{background:#d00}.loading_overlay{background:rgba(0,0,0,.4);height:100%;opacity:0;position:fixed;top:0;transition:all .3s;visibility:hidden;width:100%;z-index:10}.loading_overlay.active{opacity:1;visibility:visible}#loading_box{background:hsla(0,0%,100%,.5);bottom:0;height:100%;left:0;opacity:0;position:fixed;right:0;top:0;visibility:hidden;width:100%;z-index:100000}#loading_image{background:url(images/loadding.gif) no-repeat 50%;background-size:100px;height:100%;width:100%}.loading{position:relative;transition:.2s}.loading:after,.loading:before{bottom:0;content:"";left:0;position:absolute;right:0;top:0;z-index:100000}.loading:before{background:rgba(0,0,0,.6);height:100%;width:100%}.loading:after{background:url(images/loadding.gif) no-repeat 50%;background-size:100px}.err_show{color:red!important;display:none!important;font-size:12px!important;font-style:italic;padding-top:5px}.err_show.news{clear:both;float:right;margin-bottom:0;text-align:center}.err_show.active{display:block!important}.font-normal{font-weight:400}.fixed{background-color:rgba(0,0,0,.4);bottom:0;left:0;opacity:0;position:fixed;right:0;top:0;transition:all .5s;visibility:hidden;z-index:99998}.fixed.active{opacity:1;visibility:visible}.btn{border-radius:5px;display:inline-block;transition:.5s}.btn,.btn:hover{background:#d00}.flex{align-items:center;display:flex}.mb-10{margin-bottom:10px}.fs-13{font-size:13px}.hidden_more{display:none!important}.text-up{text-transform:uppercase}.justify-end{justify-content:flex-end}.justify-space-between{justify-content:space-between}.header{background-color:#d00;height:65px;position:sticky;top:0;transition:all .3s;width:100%;z-index:12;z-index:9999}.header-top{display:flex;height:65px;padding:0}.header-top .container{align-items:center;justify-content:space-between}.header-top .logo{align-items:center;display:flex;max-width:260px;width:100%}.header-top .logo a{display:block;line-height:0}.header-top .logo a img{display:block;height:auto;height:45px;max-width:245px;-o-object-fit:contain;object-fit:contain}.header-top .search{max-width:calc(100% - 840px);padding:0 10px;position:relative;width:100%}.header-top .search form{background-color:#fff;border-radius:10px;position:relative}.header-top .search form button,.header-top .search form input{background-color:transparent;border:0}.header-top .search form input{border:2px solid transparent;border-radius:10px;font-size:16px;height:35px;padding:5px 47px 5px 12px;transition:border-color .2s ease-in-out;width:100%}.header-top .search form input::-moz-placeholder{color:#212529;opacity:.6}.header-top .search form input::placeholder{color:#212529;opacity:.6}.header-top .search form input:focus{border-color:#212529;margin:1px;outline:0}.header-top .search form button{cursor:pointer;line-height:0;position:absolute;right:10px;top:50%;transform:translateY(-50%)}.header-top .search form button img{height:20px;width:20px}.header-top .location-switch{background:#ff5050;border-radius:7px;color:#fff;cursor:pointer;font-size:13px;margin:0;padding:5px 0;width:170px}.header-top .location-switch .current-location{align-items:center;display:flex;position:relative}.header-top .location-switch .current-location .location-icon{color:#ff5050}.header-top .location-switch .current-location .location-text{color:#ffe45f;display:block;font-size:12px;font-weight:500;text-align:center;width:100%}.header-top .location-switch .current-location svg{fill:#fff;font-size:14px;height:14px;left:7px;position:absolute;top:16px;width:14px}.header-top .location-switch .location-select-wrapper{align-items:center;display:flex;flex-direction:column;position:relative}.header-top .location-switch .location-select-wrapper .my_location{background:transparent;border:none;border-radius:7px;color:#fff;font-size:13px;line-height:14px;margin-left:10px;margin-top:3px;padding:0 15px;text-align:center}.header-top .location-switch .location-select-wrapper .location-select{-webkit-appearance:none;-moz-appearance:none;appearance:none;background:transparent;border:0;color:transparent;cursor:pointer;height:35px;outline:0;position:absolute;text-align:center;width:85%}.header-top .location-switch .location-select-wrapper .location-select option{color:#111;font-size:13px}.header-top .info_website{display:flex;justify-content:space-between;margin-left:10px;max-width:400px;width:100%}.header-top .info_website_icon svg{stroke:#fff;height:16px;width:16px}.header-top .info_website_item{align-items:center;display:inline-flex;flex-flow:column;justify-content:center;margin:0 2.5px;width:calc(33.33333% - 5px)}.header-top .info_website_item a{align-items:center;border-radius:7px;color:#fadede;display:flex;font-size:12px;line-height:1.2;padding:6.5px 5px}.header-top .info_website_item a:hover{background:#ff5050}.header-top .info_website_item a span{color:#fff;font-weight:600}.header-top .info_website_icon{align-items:center;border:1px solid #fff;border-radius:4px;display:flex;height:25px;justify-content:center;margin-right:7px;position:relative;width:28px}.header-top .info_website .cart .cart_counter{color:#fff;font-size:10px;position:absolute;right:10px;top:9px}.header-breadcrumb{background-color:#870000;padding:6px 0}.header-breadcrumb .container{position:relative}.header-breadcrumb__icon{cursor:pointer;display:block;line-height:0;margin-right:15px}.header-breadcrumb__icon svg{height:32px;width:32px}.header-breadcrumb__list{align-items:center;display:flex}.header-breadcrumb__item{height:100%;margin-right:20px;position:relative}.header-breadcrumb__item:after{bottom:7px;color:#fff;content:"»";line-height:normal;position:absolute;right:-14px}.header-breadcrumb__item:last-child{margin-right:0}.header-breadcrumb__item:last-child:after{display:none}.header-breadcrumb__item svg{margin-right:5px;vertical-align:text-top}.header-breadcrumb__item svg path{fill:#fff}.header-breadcrumb__item a,.header-breadcrumb__item h1,.header-breadcrumb__item p{align-items:center;display:flex;font-weight:400;height:100%}.header-breadcrumb__item a,.header-breadcrumb__item p{color:#fff!important;font-size:13px}.header-breadcrumb .menu-category{height:auto;max-height:unset;opacity:0;pointer-events:none;position:absolute;top:100%;transition:all .5s;visibility:hidden}.header-breadcrumb.active .menu-category{opacity:1;pointer-events:unset;visibility:visible}.menu-category{background-color:transparent;border-radius:10px;box-shadow:0 1px 2px 0 rgba(60,64,67,.102),0 2px 6px 2px rgba(60,64,67,.149);height:100%;max-height:100%;max-width:100%;opacity:1;overflow:auto;visibility:visible;width:100%;z-index:99}.menu-category__list{background-color:#fff;border-radius:10px;max-width:220px;width:100%}.menu-category__item{background:transparent;color:#333;display:block;flex-grow:1;font-size:14px;height:40px;line-height:40px;text-align:left}.menu-category__item:last-child{border-radius:10px 10px 0 0;margin-right:0}.menu-category__item:first-child:hover{border-radius:10px 10px 0 0}.menu-category__item:hover{background:#f5f5f5;border-color:#fff;color:#222;text-decoration:none}.menu-category__item:hover .menu-category__child{opacity:1;visibility:visible}.menu-category__link{align-items:center;display:block;display:flex;padding:0 10px;position:relative}.menu-category__link img{margin-right:8px}.menu-category__link.is_has_child:before{background-image:url(images/arrow.svg);background-repeat:no-repeat;content:"";height:10px;position:absolute;right:5px;top:15px;width:8px}.menu-category__child{background-color:#fff;box-shadow:0 2px 5px rgba(0,0,0,.1);display:flex;flex-wrap:wrap;height:auto;left:223px;min-height:100%;opacity:0;position:absolute;top:0;visibility:hidden;width:calc(100% - 300px);z-index:999}.menu-category__child:after{background:transparent;content:"";height:100%;left:-10px;position:absolute;width:10px}.menu-category__child-item{flex:1 1 calc(20% - 20px);margin:10px}.menu-category__child-item a,.menu-category__child-item span{background:#fff;color:#666;display:block;font-size:13px;font-weight:600;line-height:35px;padding:0 10px}.menu-category__child-item .menu-child a{font-weight:400}.menu-category__child-item .menu-child a:hover{background:transparent;color:#c21d32;font-weight:400;text-decoration:none}.suggest{background-color:#fff;border-radius:4px;box-shadow:0 0 8px 0 rgba(0,0,0,.2);max-height:calc(100vh - 140px);opacity:0;overflow:auto;position:absolute;top:100%;transition:all .5s;visibility:hidden;width:100%;z-index:999}.suggest.show{opacity:1;visibility:visible}.suggest-item__title{margin:0 15px;position:relative}.suggest-item__title:after{background-color:#aeaeae;content:"";height:1px;position:absolute;right:0;top:50%;transform:translateY(-50%);width:100%}.suggest-item__title span{background-color:#fff;color:#72777d;padding-right:5px;position:relative;z-index:1}.suggest-item__list ul li{border-bottom:1px dashed #f3f3f3;font-size:14px}.suggest-item__list ul li:last-child{border-bottom:none}.suggest-item__list ul li:hover a{background-color:#eee}.suggest-item__list ul li a{display:block;padding:5px 15px}.suggest-item.suggest-product .suggest-item__list ul li a{display:flex;flex-wrap:nowrap;padding:7px 10px;text-transform:capitalize}.suggest-item.suggest-product .suggest-item__list ul li a .img{flex:0 0 40px;height:40px;max-width:40px;overflow:hidden}.suggest-item.suggest-product .suggest-item__list ul li a .img img{height:100%;width:100%}.suggest-item.suggest-product .suggest-item__list ul li a .desc{flex:0 0 calc(100% - 40px)}.suggest-item.suggest-product .suggest-item__list ul li a .desc .info,.suggest-item.suggest-product .suggest-item__list ul li a .desc .name{color:#505050;display:block;font-size:13px;font-weight:400;line-height:20px;line-height:1.29;padding-left:15px;text-align:left}.suggest-item.suggest-product .suggest-item__list ul li a .desc .name{margin:0}.suggest-item.suggest-product .suggest-item__list ul li .price{color:#d0021b;font-size:14px;margin-bottom:0}.footer{background:#fff;box-shadow:0 1px 2px 0 rgba(60,64,67,.102),0 2px 6px 2px rgba(60,64,67,.149);margin-top:15px;position:relative}.footer-top{align-items:flex-start;display:flex;flex-wrap:wrap;justify-content:space-between;padding:15px 0;width:100%}.footer-top__item{width:24%}.footer-top__item ul li{line-height:1.4;margin-bottom:5px}.footer-top__item ul li>a{font-size:13px}.footer-top__item ul li>a:hover{color:#d0021b}.footer-top__item p{line-height:1.4;margin-bottom:5px}.footer-top__store .iconfooter{background:#ffeb73;border-radius:5px;line-height:1.3;padding:5px 10px;position:relative}.footer-top__store .iconfooter b{color:red;font-size:22px}.footer-top__store .iconfooter a:hover{color:#d0021b}.footer-top__text{display:flex;flex-direction:column}.footer-top__text a:hover{color:#d30029}.footer-top__title{margin-bottom:10px}.footer-top .fanpage-content .social li{margin-right:10px}.footer-top .fanpage-content .social li a{align-items:center;color:#1b74e4;display:flex;flex-wrap:nowrap;font-weight:500}.footer-top .fanpage-content .social li a svg{margin-right:5px}.footer-top .fanpage-content .social li a svg path{fill:#1b74e4}.footer-top .fanpage-content .social li a.youtube{color:#d00}.footer-top .fanpage-content .social li a.youtube svg path{fill:#d00}.footer-top .fanpage-content .social li a:hover{color:#d30029}.footer-top .fanpage-content .social li a:hover svg path{fill:#d30029}.footer-top .fanpage-content .social li:nth-child(3n){margin-right:0}.footer-top .fanpage-content .social img{height:14px;margin-right:8px;-o-object-fit:cover;object-fit:cover;opacity:1;width:14px}.footer-top .fanpage-content .social:nth-child(3n){margin-right:0}.footer-top .fanpage-content .bct_link img{height:auto;margin-left:-7px;max-height:50px;max-width:130px;width:100%}.footer-top .fanpage-content .website_system .title_system{font-size:14px;font-weight:700;line-height:1;margin-bottom:15px}.footer-top .fanpage-content .website_system a{align-items:center;background:#d70018;border-radius:10px;display:flex;justify-content:space-between;margin-bottom:5px;padding:7px 7px 7px 15px}.footer-top .fanpage-content .website_system a span{color:#fff;width:50%}.footer-top .fanpage-content .website_system a img{height:auto;-o-object-fit:cover;object-fit:cover;width:40%}.footer-top .footer_loation #region-menu{display:flex;justify-content:space-between;margin-bottom:5px}.footer-top .footer_loation #region-menu li{background:#fcfcfc;border:1px solid #e5e7eb;border-radius:5px;color:#4a4a4a;cursor:pointer;font-size:12px;font-weight:400;line-height:1.3;padding:1px 10px;text-align:center}.footer-top .footer_loation #region-menu li strong{font-size:12px;text-transform:uppercase}.footer-top .footer_loation #region-menu li.active{background-color:#ccc}.footer-top .footer_loation .tab-pane{display:none}.footer-top .footer_loation .tab-pane.active{display:block}.footer-top .footer_loation .area_selection{background:#fff;border:1px solid #e5e7eb;border-radius:10px;height:210px;overflow:hidden;padding:0}.footer-top .footer_loation .area_selection ul{margin-bottom:0;max-height:210px;overflow-x:hidden;overflow-y:auto}.footer-top .footer_loation .area_selection ul::-webkit-scrollbar{width:6px}.footer-top .footer_loation .area_selection ul::-webkit-scrollbar-track{background-color:#111}.footer-top .footer_loation .area_selection ul::-webkit-scrollbar-thumb{background-color:#888;border-radius:5px}.footer-top .footer_loation .area_selection ul::-webkit-scrollbar-thumb:hover{background-color:#555}.footer-top .footer_loation .area_selection ul li:nth-child(odd){background-color:#f2f2f2}.footer-top .footer_loation .area_selection ul li{margin-bottom:0;padding:5px 5px 5px 25px;position:relative}.footer-top .footer_loation .area_selection ul li svg{fill:#007eaf;height:14px;left:5px;position:absolute;top:8px;width:14px}.footer-bottom{background:#f0f0f0;color:#999;font-size:10px;padding:15px 0;text-align:center}.footer-bottom p{font-size:13px;margin-bottom:0}.scroll{background-color:#f0f0f0;border-bottom-left-radius:5px;border-top-left-radius:5px;bottom:15px;padding:5px 0;position:fixed;right:0}.scroll ul li img{border-radius:50%;display:block;height:36px;margin:0 auto;-o-object-fit:contain;object-fit:contain;width:36px}.scroll ul li a{color:#282828;display:block;font-size:10px;font-weight:500;padding:3px;text-align:center}.list-products{display:flex;flex:0 0 100%;flex-wrap:wrap;width:100%}.list-products__item{background:#fff;border-radius:10px;box-shadow:0 1px 2px 0 rgba(60,64,67,.102),0 2px 6px 2px rgba(60,64,67,.149);margin:0 5px 10px;max-width:calc(20% - 10px);min-height:310px;position:relative;width:calc(20% - 10px)}.list-products__item>a{display:block;height:100%;padding:3px 10px}.list-products__item .thumbnail{align-items:center;display:flex;justify-content:center;margin-bottom:10px;position:relative}.list-products__item .thumbnail img{height:150px;max-width:100%;-o-object-fit:cover;object-fit:cover;width:auto}.list-products__item .title_name{-webkit-line-clamp:3;-webkit-box-orient:vertical;color:#4d4d4d;display:-webkit-box;font-size:13px;font-weight:600;line-height:1.4;max-height:55px;overflow:hidden;padding:0 0 5px;text-overflow:ellipsis;width:100%}.list-products__item .ex_pricesale{background-color:#fff86e;box-shadow:0 1px 0 0 rgba(60,64,67,.102),0 2px 3px 2px rgba(60,64,67,.149);color:#000;font-size:13px;font-weight:600;line-height:22px;padding:1px 10px;position:absolute;right:-6px;top:6px}.list-products__item .ex_pricesale:after{border-right:7px solid transparent;border-top:4px solid #d5ca00;bottom:-4px;content:"";display:block;height:0;position:absolute;right:-1px;width:0}.list-products__item .price{display:flex;font-size:14px;font-weight:700;margin-bottom:5px;width:100%}.list-products__item .price .old-price{color:#9e9e9e;font-size:12px;font-weight:400;text-decoration-color:#9e9e9e}.list-products__item .price .new-price{color:#e10c00;margin-right:20px;text-decoration:none}.list-products__item .ex_rating{font-size:11px;margin:5px 0}.list-products__item .extra_tick{display:flex;flex-flow:column;height:40px;left:0;position:absolute;top:100px}.list-products__item .extra_tick .freeship,.list-products__item .extra_tick .installment{align-items:center;border-bottom-right-radius:5px;border-top-right-radius:5px;color:#fff;display:flex;font-size:10px;font-weight:600;height:20px;margin-bottom:0;padding:0 5px;width:-moz-fit-content;width:fit-content}.list-products__item .extra_tick .installment{background:#de0000}.list-products__item .extra_tick .freeship{background:#0b0a09}.list-products__item .cb_promotion .gift-detail,.list-products__item .cb_promotion_stand .gift-detail{line-height:16px;margin:5px 0;padding-left:30px;position:relative}.list-products__item .cb_promotion .gift-detail:before,.list-products__item .cb_promotion_stand .gift-detail:before{background:#f7941e 0 0 no-repeat padding-box;border-radius:3px;box-shadow:0 4px 6px rgba(0,0,0,.161);color:#fff;content:"KM";font-size:11px;font-weight:600;left:0;line-height:normal;padding:2px 4px;position:absolute;top:2px}.list-products__item .cb_promotion .gift-detail:first-child,.list-products__item .cb_promotion_stand .gift-detail:first-child{margin-top:0}.list-products__item .cb_promotion .gift-detail:last-child,.list-products__item .cb_promotion_stand .gift-detail:last-child{margin-bottom:0}.list-products__item .cb_promotion_stand{background:#fff;border-radius:3px;display:block;line-height:1.2;margin:0 0 10px;max-height:38px;overflow:hidden;padding:5px 0;position:relative;transition:transform .2s}.list-products__item .cb_promotion ul li,.list-products__item .cb_promotion_stand ul li{-webkit-line-clamp:2;-webkit-box-orient:vertical;display:-webkit-box;height:auto;line-height:1.4;margin-bottom:10px;max-height:55px;min-height:24px;overflow:hidden;text-overflow:ellipsis}.list-products__item .cb_promotion *,.list-products__item .cb_promotion_stand *{color:#5d5d5d!important;font-size:11px;font-style:normal!important;line-height:1.4;margin-bottom:0;pointer-events:none;text-decoration:none!important;text-transform:none!important}.list-products__item .cb_promotion{background:#f3f4f6;border:2px solid rgba(221,0,0,.749);border-radius:10px;bottom:5px;box-shadow:0 4px 6px rgba(0,0,0,.161);cursor:pointer;height:auto;left:5px;opacity:0;overflow:hidden;padding:10px 5px;pointer-events:none;position:absolute;right:5px;top:5px;transform:scale(1);visibility:hidden;z-index:2}.list-products__item:hover .cb_promotion{opacity:.9;visibility:visible}.list-products__item:hover .cb_promotion ul li{-webkit-line-clamp:5;max-height:100%;overflow:inherit}.block-rate_product{align-items:center;color:#d3ced2;display:flex;font-size:11px;margin:5px 0}.block-rate_product .block-rate__star{margin-right:10px}.block-rate_product .block-rate__star span.avg{color:#000;font-weight:700;line-height:10px;margin-bottom:5px}.block-rate_product .block-rate__star svg{fill:#d3ced2;height:12px;width:12px}
    </style>
    <script>
        function toggleReplyForm(commentId) {
            var replyForm = document.getElementById("reply-form-" + commentId);
            if (replyForm.style.display === "none" || replyForm.style.display === "") {
                replyForm.style.display = "block";
            } else {
                replyForm.style.display = "none";
            }
        }
        function toggleEditForm1(commentId) {
            var editForm = document.getElementById("edit-form-" + commentId);
            if (editForm.style.display === "none" || editForm.style.display === "") {
                editForm.style.display = "block";
            } else {
                editForm.style.display = "none";
            }
        }
    </script>
</head>
<body>
    
         <main class="main site_main " style="">
	
		<div class="bottom">
		<div class="container">
			<div class="row">
				<div class="col col-xs-8 col-sm-8 col-md-8 col-lg-8">
											
										<div class="product-comment">
						<link rel="stylesheet" href="css/comments.min.css">
<div class="comments" data-type="products" data-type_id="36322">
		<div class="block-rate">
		<div class="block-rate__star">
			<span class="avg">${averageRate} <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" fill="rgb(255, 202, 63)" height="40px" width="40px"><path d="M316.9 18C311.6 7 300.4 0 288.1 0s-23.4 7-28.8 18L195 150.3 51.4 171.5c-12 1.8-22 10.2-25.7 21.7s-.7 24.2 7.9 32.7L137.8 329 113.2 474.7c-2 12 3 24.2 12.9 31.3s23 8 33.8 2.3l128.3-68.5 128.3 68.5c10.8 5.7 23.9 4.9 33.8-2.3s14.9-19.3 12.9-31.3L438.5 329 542.7 225.9c8.6-8.5 11.7-21.2 7.9-32.7s-13.7-19.9-25.7-21.7L381.2 150.3 316.9 18z"></path></svg></span>
			<span>(${commentsA.size()} đánh giá và nhận xét)</span>
                         <div>
    <div>
    
</div>
                             <div>
 
</div>
</div>
		</div>
		<div class="block-rate__chart">
			<ul class="chart">
                               <c:forEach var="entry" items="${rateStatistics}">                             
								<li class="item">
					<div class="left">
						<span class="title">${entry.key} Sao</span>
					</div>
					<div class="right">
						<span class="progress">
							<span class="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="max-width: 100%; width:${entry.value['RatePercentage']}%">
							</span>
						</span>
						<span class="number">${entry.value['RateCount']} đánh giá</span>
							<i class="fas fa-star active" aria-hidden="true" data-point="1" data-id=""></i>
					</div>
					</li>
                                 </c:forEach>                         
                            
							</ul>
		</div>
	</div>
                   
		<div class="comments-content">
		<div class="comments-list list">
<c:forEach var="comment" items="${comments}">
     <c:if test="${comment.status == 1 }">
						
	<div class="item" data-comment_id="471886">
		<div class="item-author">
                    
			<div class="item-author__avatar">${comment.customerName.substring(0, 1)}</div>
			<div class="item-author__name">${comment.customerName}</div>
									<div class="rating">
				<div class="rating-star">
					<input type="hidden" class="rank" name="rank" value="${comment.rate} ">
                                            
<c:forEach var="i" begin="1" end="${comment.rate}">
    <svg class="" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" fill="rgb(255, 202, 63)" height="14px" width="14px">
													<path d="M316.9 18C311.6 7 300.4 0 288.1 0s-23.4 7-28.8 18L195 150.3 51.4 171.5c-12 1.8-22 10.2-25.7 21.7s-.7 24.2 7.9 32.7L137.8 329 113.2 474.7c-2 12 3 24.2 12.9 31.3s23 8 33.8 2.3l128.3-68.5 128.3 68.5c10.8 5.7 23.9 4.9 33.8-2.3s14.9-19.3 12.9-31.3L438.5 329 542.7 225.9c8.6-8.5 11.7-21.2 7.9-32.7s-13.7-19.9-25.7-21.7L381.2 150.3 316.9 18z"></path>
											</svg>
</c:forEach>
<c:forEach var="i" begin="${comment.rate + 1}" end="5">
     <svg class="" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" fill="#d3ced2" height="14px" width="14px">
													<path d="M287.9 0c9.2 0 17.6 5.2 21.6 13.5l68.6 141.3 153.2 22.6c9 1.3 16.5 7.6 19.3 16.3s.5 18.1-5.9 24.5L433.6 328.4l26.2 155.6c1.5 9-2.2 18.1-9.6 23.5s-17.3 6-25.3 1.7l-137-73.2L151 509.1c-8.1 4.3-17.9 3.7-25.3-1.7s-11.2-14.5-9.7-23.5l26.2-155.6L31.1 218.2c-6.5-6.4-8.7-15.9-5.9-24.5s10.3-14.9 19.3-16.3l153.2-22.6L266.3 13.5C270.4 5.2 278.7 0 287.9 0zm0 79L235.4 187.2c-3.5 7.1-10.2 12.1-18.1 13.3L99 217.9 184.9 303c5.5 5.5 8.1 13.3 6.8 21L171.4 443.7l105.2-56.2c7.1-3.8 15.6-3.8 22.6 0l105.2 56.2L384.2 324.1c-1.3-7.7 1.2-15.5 6.8-21l85.9-85.1L358.6 200.5c-7.8-1.2-14.6-6.1-18.1-13.3L287.9 79z"></path>
											</svg>
</c:forEach>

                                                                           
									</div>
			</div>
					</div>
		<div class="item-content">${comment.commentContent}</div>
                
				<div class="item-action">
                                  
			<div class="item-action__reply"  data-reply="${comment.customerName}:"><span>  <a href="javascript:void(0);" onclick="toggleReplyForm(${comment.commentId});">Trả lời</a></span></div>
                        <div class="item-action__time"><span>${comment.create_at} &nbsp;</span></div>
                        <c:choose>
                            
        <c:when test="${sessionScope.account.role == 1}">  
            <!-- Hiển thị nút chỉnh sửa và nút xóa cho admin -->
       	<div class="item-action__reply"  data-reply="${comment.customerName}:"><span>  <a href="javascript:void(0);" onclick="toggleEditForm1(${comment.commentId});">Chỉnh sửa</a></span></div>

        <form action="FeedbackController" method="post">
                            <input type="hidden" value="${Pid}" name="Pid">
                            <input type="hidden" name="action" value="deleteComment">
                            <input type="hidden" name="commentId" value="${comment.commentId}">
                            <input type="submit" value="Xóa">  
                        </form>
        </c:when>
        <c:when test="${sessionScope.account != null && sessionScope.account.username == comment.customerName}">
            <!-- Hiển thị nút chỉnh sửa và nút xóa cho người tạo bình luận -->
                  	<div class="item-action__reply"  data-reply="${comment.customerName}:"><span>  <a href="javascript:void(0);" onclick="toggleEditForm1(${comment.commentId});">Chỉnh sửa</a></span></div>
                        <form action="FeedbackController" method="post">
                            <input type="hidden" value="${Pid}" name="Pid">
                            <input type="hidden" name="action" value="deleteComment">
                            <input type="hidden" name="commentId" value="${comment.commentId}">
                            <input type="submit" value="Xóa">  
                        </form>
        </c:when>
    </c:choose>
                        ${error}
		</div>
    <c:forEach var="feedback" items="${feedbacksByComment[comment.commentId]}">
            
        <div class="item-child">
									<div class="item">
						<div class="item-author">
							<div class="item-author__avatar">${feedback.feedbackBySellerName.substring(0, 1)}</div>
							<div class="item-author__name">${feedback.feedbackBySellerName}</div>
                                                        
                                                            <div class="item-author__role">Quản trị viên</div>
                                                     
													</div>
						<div class="item-content">${feedback.feedbackContent}</div>
												<div class="item-action">
			<div class="item-action__reply"  data-reply="${comment.customerName}:"><span>  <a href="javascript:void(0);" onclick="toggleReplyForm(${comment.commentId});">Trả lời</a></span></div>
                        
                        <div class="item-action__time"><span>${feedback.create_atFeedback}&nbsp;</span></div>
							                  	     <c:choose>
                            
        <c:when test="${sessionScope.account.role == 1}">  
            <!-- Hiển thị nút chỉnh sửa và nút xóa cho admin -->
       	<div class="item-action__reply"  data-reply="${comment.customerName}:"><span>  <a href="javascript:void(0);" onclick="toggleEditForm1(${comment.commentId});">Chỉnh sửa</a></span></div>

        <form action="FeedbackController" method="post">
                            <input type="hidden" value="${Pid}" name="Pid">
                            <input type="hidden" name="action" value="deleteComment">
                            <input type="hidden" name="commentId" value="${comment.commentId}">
                            <input type="submit" value="Xóa">  
                        </form>
        </c:when>
        <c:when test="${sessionScope.account != null && sessionScope.account.username == comment.customerName}">
            <!-- Hiển thị nút chỉnh sửa và nút xóa cho người tạo bình luận -->
                  	<div class="item-action__reply"  data-reply="${comment.customerName}:"><span>  <a href="javascript:void(0);" onclick="toggleEditForm1(${comment.commentId});">Chỉnh sửa</a></span></div>
                        <form action="FeedbackController" method="post">
                            <input type="hidden" value="${Pid}" name="Pid">
                            <input type="hidden" name="action" value="deleteComment">
                            <input type="hidden" name="commentId" value="${comment.commentId}">
                            <input type="submit" value="Xóa">  
                        </form>
        </c:when>
    </c:choose>

						</div>
					</div>
							</div>
    </c:forEach>
                        <div id="reply-form-${comment.commentId}" style="display: none;">				
                            <form  action="reply" method="post">
                                <input type="hidden" value="${Pid}" name="Pid">
                                    <input type="hidden" name="commentId" value="${comment.commentId}">
                                        <div class="item-reply">

                                            <p class="notes">
                                                <span id="email-notes">Email của bạn sẽ không được hiển thị công khai.</span>
                                                <span class="required-field-message" aria-hidden="true">Các trường bắt buộc được đánh dấu
                                                    <span class="required" aria-hidden="true">*</span>
                                                </span>
                                            </p>
                                            <div class="comments-add__form">
                                                <label for="comment" class="comment bold" style="margin-bottom: 0.5rem;">Nhận xét của bạn<span style="color: red;">*</span></label>
                                                <textarea class="comments-add__form-field" name="feedbackContent" placeholder="Nhận xét của bạn..."></textarea>
                                                <p class="comment-form-author textbox">
                                                    <input id="name" name="feedbackBySellerName" type="text" value="${sessionScope.account.username}" placeholder="Tên" size="30" required="">
                                                </p>
                                                <p class="comment-form-email textbox right">
                                                    <input id="email" name="email" type="email" value="${sessionScope.account.email}" placeholder="Email" size="30" required=""></p>
                                                <p class="comment-form-cookies-consent">
                                                    <input id="wp-comment-cookies-consent" name="wp-comment-cookies-consent" type="checkbox" value="true">
                                                        <label for="wp-comment-cookies-consent">Lưu tên của tôi, email, vào trang web trong trình duyệt này cho lần bình luận kế tiếp của tôi.</label>
                                                </p>

                                            </div>
                                            <div class="comments-add__action">
                                                <div class="comments-add__action-left">
                                                    <button type="submit" title="Gửi" >Gửi</button>
                                                </div>

                                            </div>
                                        </div>

                                        </form>
                </div>
            <div id="edit-form-${comment.commentId}" style="display: none;">				
                <form action="FeedbackController" method="post">
                            <input type="hidden" value="${Pid}" name="Pid">
                            <input type="hidden" name="action" value="editComment">
                            <input type="hidden" name="commentId" value="${comment.commentId}">

                                <div class="item-edit">

                                    <p class="notes">
                                        <span id="email-notes">Email của bạn sẽ không được hiển thị công khai.</span>
                                        <span class="required-field-message" aria-hidden="true">Các trường bắt buộc được đánh dấu
                                            <span class="required" aria-hidden="true">*</span>
                                        </span>
                                    </p>
                                    <div class="comments-add__form">
                                        <label for="comment" class="comment bold" style="margin-bottom: 0.5rem;">Nhận xét của bạn<span style="color: red;">*</span></label>
                                        <textarea class="comments-add__form-field" name="newContent" placeholder="${comment.commentContent}" ></textarea>
                                        <p class="comment-form-author textbox">
                                            <input id="name" name="feedbackBySellerName" type="text" value="${sessionScope.account.username}" placeholder="Tên" size="30" required="">
                                        </p>
                                        <p class="comment-form-email textbox right">
                                            <input id="email" name="email" type="email" value="${sessionScope.account.email}" placeholder="Email" size="30" required=""></p>
                                        <p class="comment-form-cookies-consent">
                                            <input id="wp-comment-cookies-consent" name="wp-comment-cookies-consent" type="checkbox" value="true">
                                                <label for="wp-comment-cookies-consent">Lưu tên của tôi, email, vào trang web trong trình duyệt này cho lần bình luận kế tiếp của tôi.</label>
                                        </p>

                                    </div>
                                    <div class="comments-add__action">
                                        <div class="comments-add__action-left">
                                                                      <input type="submit" value="Chỉnh sửa">  

                                        </div>

                                    </div>
                                </div>

                                </form>
                </div>    
	</div>
     </c:if>
        </c:forEach>  
					</div>
				<div class="pagination">
            <nav>
<!-- feedback.jsp -->
<!-- Các phần hiển thị danh sách bình luận -->
<!-- ... -->

<ul class="pagination">
    <li class="page-item ${currentPage == 1 ? 'disabled' : ''}">
        <a class="page-link" href="?Pid=${Pid}&page=${previousPage}">Previous</a>
    </li>
    <c:forEach var="page" begin="1" end="${totalPages}">
        <li class="page-item ${page == currentPage ? 'active' : ''}">
            <a class="page-link" href="?Pid=${Pid}&page=${page}">${page}</a>
        </li>
    </c:forEach>
    <li class="page-item ${currentPage == totalPages ? 'disabled' : ''}">
        <a class="page-link" href="?page=${nextPage}">Next</a>
    </li>
</ul>


    </nav>

        </div>
			</div>
    
  
</div>
    
<form action="FeedbackController" method="post">   
    <input type="hidden" value="${Pid}" name="Pid">
        <input type="hidden" value="${Pid}" name="Pid">
                            <input type="hidden" name="action" value="addComment">
	<div class="comments-add" id="comments-actions">
	<p class="notes">
		<span id="email-notes">Email của bạn sẽ không được hiển thị công khai.</span>
		<span class="required-field-message" aria-hidden="true">Các trường bắt buộc được đánh dấu
			<span class="required" aria-hidden="true">*</span>
		</span>
	</p>
            
			<div class="comments-add__rate">
			<p class="bold">Đánh giá của bạn<span style="color: red;">*</span></p>
			<select name="rate">
            <option value="5">5</option>
            <option value="4">4</option>
            <option value="3">3</option>
            <option value="2">2</option>
            <option value="1">1</option>
        </select>
		</div>
		<div class="comments-add__form">
		<label for="comment" class="comment bold" style="margin-bottom: 0.5rem;">Nhận xét của bạn<span style="color: red;">*</span></label>
		<textarea class="comments-add__form-field" name="commentContent" placeholder="Nhận xét của bạn..." required=""></textarea>
		<p class="comment-form-author textbox">
			<input id="customerName" name="customerName" type="text" value="${sessionScope.account.username}" placeholder="Tên" size="30" required="">
		</p>
		<p class="comment-form-email textbox right">
			<input id="customerEmail" name="customerEmail" type="email" value="${sessionScope.account.email}" placeholder="Email" size="30" required=""></p>
		<p class="comment-form-cookies-consent">
			<input id="wp-comment-cookies-consent" name="saveInfo" type="checkbox" value="true">
			<label for="wp-comment-cookies-consent">Lưu tên của tôi, email, vào trang web trong trình duyệt này cho lần bình luận kế tiếp của tôi.</label>
		</p>
                	<div class="g-recaptcha" data-sitekey="6LdYbIcjAAAAAOOKYQDqGbJFQhESwBNgIqnNBfQx"></div>
        
	</div>
	<div class="comments-add__action">
            <p style="color: red">${mess}</p>
		<div class="comments-add__action-left">
                  
			<input type="submit" value="Gửi">
		</div>
		<div class="comments-add__action-right">
			<ul>
							</ul>
		</div>
	</div>
	</div>		
                 </form>
                
                
                <div class="lang_comments" data-value="eyJwYWdlX251bWJlciI6NSwidXBsb2FkX2ltYWdlIjpmYWxzZSwiYWxsb3dlZF9zaXplIjoyMDk3MTUyLCJ2YWxpZF9zaXplIjoiQ1x1MDBlMWMgRmlsZSBwaFx1MWVhM2kgY1x1MDBmMyBrXHUwMGVkY2ggdGhcdTAxYjBcdTFlZGJjIG5oXHUxZWNmIGhcdTAxYTFuIiwidmFsaWRfZXh0ZW50aW9uIjoiXHUwMTEwXHUxZWNibmggZFx1MWVhMW5nIGNobyBwaFx1MDBlOXAgbFx1MDBlMCIsInZhbGlkX2VtcHR5IjoiQlx1MWVhMW4gY1x1MWVhN24gbmhcdTFlYWRwIFx1MDExMVx1MWVhNXkgXHUwMTExXHUxZWU3IHRoXHUwMGY0bmcgdGluLiIsInZhbGlkX2Zvcm1hdF9waG9uZSI6Ilx1MDExMFx1MWVjYm5oIGRcdTFlYTFuZyBzXHUxZWQxIFx1MDExMWlcdTFlYzduIHRob1x1MWVhMWkga2hcdTAwZjRuZyBjaFx1MDBlZG5oIHhcdTAwZTFjLiIsInZhbGlkX2Zvcm1hdF9lbWFpbCI6Ilx1MDExMFx1MWVjYm5oIGRcdTFlYTFuZyBFbWFpbCBraFx1MDBmNG5nIGNoXHUwMGVkbmggeFx1MDBlMWMuIiwiYWpheF9sb2FkX2Vycm9yX3RleHQiOiJDXHUwMGYzIGxcdTFlZDdpIHhcdTFlYTN5IHJhLiBWdWkgbFx1MDBmMm5nIHRoXHUxZWVkIGxcdTFlYTFpISIsImFqYXhfbG9hZF91cmwiOiJodHRwczpcL1wvY2xpY2tidXkuY29tLnZuXC9hamF4XC9jb21tZW50cyIsImFqYXhfYWRkX3VybCI6Imh0dHBzOlwvXC9jbGlja2J1eS5jb20udm5cL2FqYXhcL2NvbW1lbnRzXC9hZGQiLCJhamF4X3NlYXJjaF91cmwiOiJodHRwczpcL1wvY2xpY2tidXkuY29tLnZuXC9hamF4XC9jb21tZW50c1wvc2VhcmNoIiwidm90ZSI6dHJ1ZX0="></div>
	<div class="comments-loading"><div class="comments-loading__box"></div></div>

	<section class="comments-popup previews">
		<div class="comments-popup__close" data-comments_close=""><i class="fa fa-remove"></i></div>
		<div class="comments-popup__dialog">
			<div class="comments-popup__body">
				<img src="images/default_image.png" alt="" width="200" height="200">
			</div>
		</div>
	</section>
	<div class="comments-popup moreinfo">
		<div class="comments-popup__close" data-comments_close=""></div>
		<div class="comments-popup__dialog">
			<div class="comments-popup__header">
				<div class="comments-popup__header__content">
					<p>Thông tin bình luận</p>
				</div>
				<div class="comments-popup__header__close" data-comments_close=""><i class="fa fa-close"></i></div>
			</div>
			<div class="comments-popup__body">
				<div class="moreinfo-form">
					<div class="form-group">
						<input type="text" class="form-control" name="name" placeholder="Họ v�&nbsp; tên">
					</div>
					<div class="form-group">
						<input type="text" class="form-control" name="phone" placeholder="Điện thoại">
					</div>
					<div class="form-group">
						<input type="text" class="form-control" name="email" placeholder="Email">
					</div>
					<div class="form-group">
						<button aria-label="Gửi bình luận" title="Gửi bình luận" type="button" data-comments_submit=""><i class="fa fa-paper-plane"></i>Gửi bình luận</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="js/jquery.resizeImg.js"></script>
<script type="text/javascript" src="js/mobileBUGFix.mini.js"></script>

					</div>
				</div>
				
			</div>
		</div>
	</div>
     
         
    </main>
  
    
   

</body></html>