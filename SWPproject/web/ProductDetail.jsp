<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<!DOCTYPE html><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="vi" lang="vi">
    <head>     
        <%@include file="Header.jsp"  %>
    </head>
    <body>

        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WV23PD"
                          height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>



        <div id="main" class="container-content">

            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div id="breadcrumbs" itemprop="breadcrumb">
                            <div class="br-container">
                                <ul id="br" itemscope="" itemtype="http://schema.org/BreadcrumbList">
                                    <li class="home" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                                        <a itemprop="item" href="https://boardgame.vn" title="Trang ch?">
                                            <span itemprop="name">Trang chủ</span>
                                            <meta itemprop="position" content="1">
                                        </a>
                                    </li>
                                    <li class="home" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                                        <a itemprop="item" href="/san-pham" title="S?n ph?m">
                                            <span itemprop="name">Sảnn phẩm</span>
                                            <meta itemprop="position" content="2">
                                        </a>
                                    </li>
                                    <li class="home" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                                        <a itemprop="item" href="/category/all-games" title="T?t C? Board Game">
                                            <span itemprop="name">Tất Cả Board Game</span>
                                            <meta itemprop="position" content="3">
                                        </a>
                                    </li>

                                    <li class="home" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                                        <a itemprop="item" href="javascript:void(0)" title="">
                                            <span itemprop="name">${pro.pname}</span>
                                            <meta itemprop="position" content="4">
                                        </a>
                                    </li>

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">

                    <div class="col-lg-4">
                        <div class="product-img-box">
                            <div class="product-image" style="margin-bottom: 10px;">
                                <div id="wrap" style="top:0px;z-index:200;position:relative;">
                                    <div class="box-img" style="position: relative; display: table-cell;vertical-align: middle; text-align:center;width:400px;height:400px; ">
                                        <a title="" href="https://boardgame.vn/uploads/u/boardgame.vn/product/2021/05/05/19/10/btp1620195046.png" rel="productlist" id="zoom1" name="gal1" style="position: relative; display: block;margin:auto;outline-style:none;">
                                            <img src="images/${pro.img}" alt="" title="" width="400" height="400">
                                        </a>
                                    </div>
                                    <a class="pre-zoom" href="javascript:void(0);" rel="nofollow"></a>
                                    <a class="next-zoom" href="javascript:void(0);" rel="nofollow"></a>
                                </div>

                            </div>



                        </div>
                    </div>

                    <div class="col-lg-5">





                        <div class="product-shop">
                            <div class="product-name">
                                <h1>
                                    ${pro.pname} </h1>
                            </div>
                            <div class="price-box-detail">
                              
                                    <c:if test="${pro.isIsDiscount()}">
                                          Giá:<span class="value">${pro.priceSale}${formattedPrice} VNĐ</span>
                                Giá cũ:<span class="old_price">${pro.price}VNĐ</span>
                                                                        <div class="discount-label">

                                        <fmt:formatNumber value="${pro.priceSale}" pattern="#,##0" var="formattedPriceSale" />
                                        <fmt:formatNumber value="${pro.price}" pattern="#,##0" var="formattedPrice" />
                                        <c:set var="price" value="${pro.price}" />
                                        <c:set var="priceSale" value="${pro.priceSale}" />

                                        <%-- Kiểm tra nếu có giảm giá và giá gốc lớn hơn 0 --%>

                                        <%-- Tính phần trăm giảm giá --%>
                                        <c:set var="discountPercentage" value="${((price - priceSale) / price) * 100}" />

                                        <%-- Hiển thị phần trăm giảm giá --%>
                                        <p>${discountPercentage}%</p>
                                                                           </div> 

                                    </c:if> 
                                <c:if test="${!pro.isIsDiscount()}">Giá: <span >${pro.price}${formattedPrice}VNĐ</span></c:if>
                                       
                                       
                                        
                            </div>



                            <div class="attribute-box">
                                <div class="attribute-item">
                                    <div class="attribute-title">Số người chơi: </div>
                                    <div class="attribute-content noboder"> <div class="att_item " style="" title="">${pdd.numPlayer}</div>
                                    </div>
                                </div>
                                <div class="attribute-item">
                                    <div class="attribute-title">Thời gian chơi:</div>
                                    <div class="attribute-content noboder"> <div class="att_item " style="" title="">${pdd.timeplay} phút</div>
                                    </div>
                                </div>
                                <div class="attribute-item">
                                    <div class="attribute-title">Tuổi:</div>
                                    <div class="attribute-content noboder"> <div class="att_item " style="" title="">${pdd.requiredAge}+</div>
                                    </div>
                                </div>
                                <div class="attribute-item">
                                    <div class="attribute-title">Thể loại:</div>


                                    <div class="attribute-content "> <div class="att_item " style="" title="">Board Game US </div>
                                    </div>



                                </div>
                            </div>
                            <div class="link-add-cart">
                                <form name="addtocart" id="productAddToCartForm" method="post" action="">


                                    <div class="box_qty" style="position: relative;">
                                        Số lượng:
                                        <select id="qty_input" name="qty_input">
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                            <option value="10">10</option>
                                        </select>

                                    </div>


                                    <input type="hidden" id="price_lang" name="price_lang" value="vn">
                                        <input type="hidden" id="link_add_to_cart" value="/checkout/cart?product_id=1091">
                                            <a rel="nofollow" class="btn_datmua button-tooltip-buying" href="checkout?pid=${pro.pid}">Mua ngay</a>
                                            <a class="btn_addcart"  href="addcart?pid=${pro.pid}&url=productdetail?Pid=${pro.pid}" title="">Thêm vào giỏ hàng </a>
                                            <input type="hidden" name="return_url" value="">
                                                <input type="hidden" name="qty-item" value="">
                                                    </form>
                                                    </div>

                                                    </div> 




                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="box_policy">
                                                            <div class="ship_policy">
                                                                Chính sách vận chuyển 
                                                                <a href="https://boardgame.vn/ns79/chinh-sach-van-chuyen" title="Chính sách v?n chuy?n">Xem thêm </a>
                                                            </div>
                                                            <div class="payment_policy">
                                                                Thanh toán dễ dàng
                                                            </div>
                                                            <div class="group_policy">
                                                                Nhà bán hàng uy tín
                                                            </div>
                                                            <div class="box_contactp">
                                                                <div class="title">
                                                                    Liên hệ Board Game VN
                                                                </div>
                                                                <div class="content">
                                                                   
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                  
                                                    </div>

                                                    </div>

                                                    <div class="product-detail">
                                                        <div class="container">
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <div class="box-product-new">
                                                                        <h3 class="box-title">Sản phẩm liên quan</h3>
                                                                        <ul id="slide_product_related">

                                                                            <li class=""> 
                                                                                <c:forEach items="${photsale}" var="p" begin="0" end="4">
                                                                                    <div class="product ">
                                                                                        <div class="thumb-img">
                                                                                            <a href="" title="">
                                                                                                <img src="images/${p.img}" alt="${p.pname}" width="260" height="260">
                                                                                            </a>
                                                                                            <div class="icon-sale"></div>
                                                                                        </div>
                                                                                        <div class="product-info">
                                                                                            <div class="info">
                                                                                                <a class="name-p" href="productDetails?Pid=${p.pid}" title="${p.pname}">${p.pname}</a>
                                                                                            </div>

                                                                                            <div class="price">
                                                                                                <c:choose>
                                                                                                    <c:when test="${p.isIsDiscount() && p.priceSale != 0}">
                                                                                                        <fmt:formatNumber value="${p.priceSale}" pattern="#,##0" var="formattedPriceSale" />
                                                                                                        <fmt:formatNumber value="${p.price}" pattern="#,##0" var="formattedPrice" />
                                                                                                        <font>${formattedPriceSale} VNĐ</font>
                                                                                                        <span class="old_price">${formattedPrice} VNĐ</span>
                                                                                                        <c:if test="${p.getQuantity() == 0}">
                                                                                                            <span class="outofstock">Cháy hàng</span>
                                                                                                        </c:if>
                                                                                                    </c:when>
                                                                                                    <c:otherwise>
                                                                                                        <fmt:formatNumber value="${p.price}" pattern="#,##0" var="formattedPrice" />
                                                                                                        <font>${formattedPrice} VNĐ</font>
                                                                                                        </c:otherwise>
                                                                                                    </c:choose>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="border"></div>
                                                                                    </div>
                                                                                </c:forEach>


                                                                            </li>

                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <div class="pro_ttchitiet">
                                                                        <div class="title_ttchititet">
                                                                            Thông tin chi tiết
                                                                        </div>

                                                                        <div class="ttchititet_content">
                                                                            <div class="chitet_content">

                                                                                <table border="0" cellpadding="0" cellspacing="0" style="box-sizing: border-box; border-spacing: 0px; border-collapse: collapse; background-color: rgb(255, 255, 255); border-style: solid; border-color: rgb(226, 226, 226); float: left; width: 595px; color: rgb(51, 51, 51); font-family: " open="" sans";="" font-size:="" 14px;"="">

                                                                                    <tbody style="box-sizing: border-box;">
                                                                                        <tr style="box-sizing: border-box; border-bottom: 1px solid rgb(226, 226, 226);">
                                                                                            <td style="box-sizing: border-box; padding: 10px 0px 10px 15px; width: 297px; background: rgb(247, 247, 247); border-right-style: solid; border-right-color: rgb(226, 226, 226);">
                                                                                                <span style="font-family:arial,helvetica,sans-serif;">Tác giả</span></td>
                                                                                            <td style="box-sizing: border-box; padding: 10px 0px 10px 20px; width: 297px;">
                                                                                                <span style="font-family:arial,helvetica,sans-serif;">${pb.pubname}<font color="#007ff0" style="box-sizing: border-box;"><span style="box-sizing: border-box; font-size: 13px;"></span></font></span></td>
                                                                                        </tr>
                                                                                        <tr style="box-sizing: border-box; border-bottom: 1px solid rgb(226, 226, 226);">
                                                                                            <td style="box-sizing: border-box; padding: 10px 0px 10px 15px; width: 297px; background: rgb(247, 247, 247); border-right-style: solid; border-right-color: rgb(226, 226, 226);">
                                                                                                <span style="font-family:arial,helvetica,sans-serif;">Sản xuất tại</span></td>
                                                                                            <td style="box-sizing: border-box; padding: 10px 0px 10px 20px; width: 297px;">
                                                                                                <span style="font-family:arial,helvetica,sans-serif;">${pb.country}</span></td>
                                                                                        </tr>
                                                                                        <tr style="box-sizing: border-box; border-bottom: 1px solid rgb(226, 226, 226);">
                                                                                            <td style="box-sizing: border-box; padding: 10px 0px 10px 15px; width: 297px; background: rgb(247, 247, 247); border-right-style: solid; border-right-color: rgb(226, 226, 226);">
                                                                                                <span style="font-family:arial,helvetica,sans-serif;">Khối lượng </span></td>
                                                                                            <td style="box-sizing: border-box; padding: 10px 0px 10px 20px; width: 297px;">
                                                                                                <span style="font-family:arial,helvetica,sans-serif;">${pb.weight}</span></td>
                                                                                        </tr>
                                                                                        <tr style="box-sizing: border-box; border-bottom: 1px solid rgb(226, 226, 226);">
                                                                                            <td style="box-sizing: border-box; padding: 10px 0px 10px 15px; width: 297px; background: rgb(247, 247, 247); border-right-style: solid; border-right-color: rgb(226, 226, 226);">
                                                                                                <span style="font-family:arial,helvetica,sans-serif;">Kích thước</span></td>
                                                                                            <td style="box-sizing: border-box; padding: 10px 0px 10px 20px; width: 297px;">
                                                                                                <span style="font-family:arial,helvetica,sans-serif;">${pb.size}</span></td>
                                                                                        </tr>
                                                                                    </tbody>

                                                                                </table>

                                                                            </div>



                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <ul class="nav nav-tabs" role="tablist" id="detail_product">
                                                                        <li role="presentation" class="item-tab-v active"><a href="#home" role="tab" data-toggle="tab">Mô tả sản phẩm <span class="icon_down"></span></a></li>
                                                                        <li role="presentation" class="item-tab-v"><a href="#profile" role="tab" data-toggle="tab">Luật chơi<span class="icon_down"></span></a></li>
                                                                    </ul>

                                                                    <div class="tab-content" id="tab-info-p">
                                                                        <div role="tabpanel" class="tab-pane active p-des" id="home">
                                                                            <!-- N?i dung Mô t? s?n ph?m ? ?ây -->

                                                                            ${pdd.description}



                                                                        </div>
                                                                        <div role="tabpanel" class="tab-pane p-profile" id="profile">
                                                                            <!-- N?i dung Lu?t ch?i ? ?ây -->

                                                                            ${pdd.rules}

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <script>
                                                                (function () {
                                                                    // S? d?ng jQuery ?? x? lý s? ki?n khi tab ???c nh?p
                                                                    $(document).ready(function () {
                                                                        $('#detail_product a[data-toggle="tab"]').on('click', function (e) {
                                                                            e.preventDefault();
                                                                            var tabId = $(this).attr('href'); // L?y ID c?a tab ???c nh?p

                                                                            // ?n t?t c? các tab-content tr??c
                                                                            $('.tab-pane').removeClass('active'); // G? b? l?p active kh?i t?t c? tab-content
                                                                            $(tabId).addClass('active'); // Thêm l?p active vào tab-content t??ng ?ng

                                                                            // C?p nh?t giao di?n tab ?? ch? tr?ng thái active cho tab ???c nh?p
                                                                            $('#detail_product li').removeClass('active');
                                                                            $(this).parent('li').addClass('active');
                                                                        });
                                                                    });
                                                                })();
                                                            </script>


                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <div class="pro_ttchitiet" style="margin-bottom: 40px;margin-top: 0;">
                                                                        <div class="title_ttchititet">
                                                                            Hỏi 
                                                                        </div>
                                                                        <div class="ttchititet_content">
                                                                            <div class="comment_face">
                                                                                <div class="fb-comments fb_iframe_widget fb_iframe_widget_fluid_desktop" data-href="https://boardgame.vn/all-games/khu-rung-kim-cuong-1694" data-colorscheme="light" data-numposts="10" data-width="860" fb-xfbml-state="rendered" fb-iframe-plugin-query="app_id=&amp;color_scheme=light&amp;container_width=1230&amp;height=100&amp;href=https%3A%2F%2Fboardgame.vn%2Fall-games%2Fkhu-rung-kim-cuong-1694&amp;locale=vi_VN&amp;numposts=10&amp;sdk=joey&amp;version=v2.12&amp;width=860"><span style="vertical-align: top; width: 0px; height: 0px; overflow: hidden;"><iframe name="f36ddbe1ca0aad" width="860px" height="100px" data-testid="fb:comments Facebook Social Plugin" title="fb:comments Facebook Social Plugin" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" allow="encrypted-media" src="https://web.facebook.com/v2.12/plugins/comments.php?app_id=&amp;channel=https%3A%2F%2Fstaticxx.facebook.com%2Fx%2Fconnect%2Fxd_arbiter%2F%3Fversion%3D46%23cb%3Df8a843adabbaf%26domain%3Dlocalhost%26is_canvas%3Dfalse%26origin%3Dhttp%253A%252F%252Flocalhost%253A9999%252Ff9323db6dae4f%26relation%3Dparent.parent&amp;color_scheme=light&amp;container_width=1230&amp;height=100&amp;href=https%3A%2F%2Fboardgame.vn%2Fall-games%2Fkhu-rung-kim-cuong-1694&amp;locale=vi_VN&amp;numposts=10&amp;sdk=joey&amp;version=v2.12&amp;width=860" style="border: none; visibility: visible; width: 0px; height: 0px;"></iframe></span></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>



                                                    </div>





                                                    <div class="clear"></div>


                                                 


                                                    <%@include file="feedback.jsp"  %>


                                                    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                                                        <title>JSP Page</title>


                                                        <div id="footer">


                                                            <div class="footer_center">
                                                                <div class="container">
                                                                    <div class="info-foot">
                                                                        <div class="title_c">
                                                                            <h4>C?A HÀNG BOARD GAME VN</h4>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="bottom_bottom">
                                                                <div class="container">
                                                                    <div class="box-email">
                                                                        <div class="form_mail">
                                                                            <label class="title_notice">??NG KÍ NH?N THÔNG TIN KHUY?N MÃI</label>
                                                                            <form id="emailNewsletter-form" action="/" method="post"> <input size="50" maxlength="60" placeholder="Nh?p email c?a b?n" name="EmailNewsletter[email]" id="EmailNewsletter_email" type="text"> <a href="javascript:void(0)" onclick="submit_emailNewsletter()" class="submit_emailNewsletter">??ng ký</a>
                                                                            </form> </div>
                                                                    </div>
                                                                    <div class="box_method">
                                                                        <div class="s_t">
                                                                            PH??NG TH?C THANH TOÁN
                                                                        </div>
                                                                        <div class="s_c">
                                                                            <a href=""><img width="54" height="33" src="images/i_visa.png" alt=""></a>
                                                                            <a href=""><img width="54" height="33" src="images/i_master.png" alt=""></a>
                                                                            <a href=""><img width="54" height="33" src="images/i_onepay.png" alt=""></a>
                                                                            <a href=""><img width="54" height="33" src="images/i_usd.png" alt=""></a>
                                                                            <a href=""><img width="54" height="33" src="images/i_atm.png" alt=""></a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="box_social">
                                                                        <div class="s_t">
                                                                            K?T N?I V?I CHÚNG TÔI
                                                                        </div>
                                                                        <div class="s_c">
                                                                            <a href="https://www.facebook.com/boardgamevn" target="_blank"><img width="34" height="34" src="images/i_face_2.png" alt=""></a>
                                                                            <a href="https://www.instagram.com/boardgamevn" target="_blank"><img width="34" height="34" src="images/i_insta.png" alt=""></a>
                                                                            <a href="https://www.youtube.com/c/boardgamevn2012" target="_blank"><img width="34" height="34" src="images/i_youtube.png" alt=""></a>
                                                                            <a href="/cdn-cgi/l/email-protection#0f6d606e7d6b686e626a79613d3f3e3d4f68626e6663216c6062" target="_blank"><img width="34" height="34" src="images/i_mail.png" alt=""></a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="copyright">
                                                                    © Board Game VN
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div id="fb-root" class=" fb_reset"><div style="position: absolute; top: -10000px; width: 0px; height: 0px;"><div></div></div></div>

                                                        <div id="fb-root"></div>










                                                        <script type="text/javascript" id="">mePuzz("track", "Cuon_chuot_50%");</script></body></html>