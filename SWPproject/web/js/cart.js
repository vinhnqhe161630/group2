
// Hàm xử lý khi người dùng xác nhận xóa sản phẩm
function submitDeleteForm(productId) {
    var confirmDelete = confirm("Bạn muốn xóa sản phẩm này khỏi giỏ hàng?");
    if (confirmDelete) {
        document.getElementById("delete-pid").value = productId;
        document.getElementById("delete-form").submit();
    }
}

// Hàm giảm số lượng sản phẩm
function subtractQuantity(index) {
    var inputElement = document.getElementById("quantityInput_" + index);
    var currentValue = parseInt(inputElement.value, 10);

    if (currentValue > 1) {
        inputElement.value = currentValue - 1;
    }
}

// Hàm tăng số lượng sản phẩm
function addQuantity(index) {
    var inputElement = document.getElementById("quantityInput_" + index);
    var currentValue = parseInt(inputElement.value, 10);

    // Tăng giá trị lên 1 khi người dùng nhấp vào nút tăng số lượng
    inputElement.value = currentValue + 1;
}

// Hàm chọn tất cả checkbox
function selectAllCheckboxes() {
    var selectAllCheckbox = document.getElementById('selectAll');
    var checkboxes = document.querySelectorAll('input[name="options[]"]');

    for (var i = 0; i < checkboxes.length; i++) {
        checkboxes[i].checked = selectAllCheckbox.checked;
    }
}
