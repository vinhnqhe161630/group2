<%-- 
    Document   : PostList
    Created on : Oct 8, 2023, 4:45:51 AM
    Author     : Thanh
--%>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
        <title>Board Game</title>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" />
        <link rel="stylesheet"
              href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,400,0,0" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
        <link rel="stylesheet" href="./css/post/style.css">
        <link rel="stylesheet" href="./css/post/loginStyle.css">
        <link rel="stylesheet" href="./css/post/addProductStyle.css">
        <link rel="stylesheet" href="./css/post/responsive.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/custom.css">
        

        <!--google fonts -->

        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600&display=swap" rel="stylesheet">


        <!--google material icon-->
        <link href="https://fonts.googleapis.com/css2?family=Material+Icons"rel="stylesheet">

    </head>
    <body>


        <div class="wrapper">


            <div class="body-overlay"></div>

            <!-------------------------sidebar------------>
            <!-- Sidebar  -->
            <nav id="sidebar">
                <div class="sidebar-header">
                    <a href="home"><h3><img src="images/favicon.png" class="img-fluid"/><span>Board Game</span></h3></a>
                </div>
                <ul class="list-unstyled components">
                    <li  class="">
                    <a href="mktdashboard" class="dashboard"><i class="material-icons">dashboard</i>
                        <span>MKT Dashboard</span></a>
                </li>


                <li class="">
                    <a href="customerList" >
                        <i class="fas fa-user"></i>Quản Lý Khách Hàng</a>
                </li>

                <li class="active">
                    <a href="postlist" >
                        <i class="fas fa-newspaper"></i><span>Quản Lý Tin Tức</span></a>
                </li>

                <li class="">
                    <a href="codeSaleList">
                        <i class="fas fa-list"></i><span>Quản Lý Mã Giảm giá</span></a>
                </li>
                
                <li class="">
                    <a href="bannerlist">
                        <i class="fas fa-list"></i><span>Quản Lý Banner</span></a>
                </li>
                 

                <li  class="">
                    <a href="home"><i class="fas fa-home"></i><span>Trở lại trang chính
                        </span></a>
                </li>

                </ul>


            </nav>




            <!--------page-content---------------->

            <div id="content">

                <!--top--navbar----design--------->

                <div class="top-navbar">
                    <div class="xp-topbar">

                        <!-- Start XP Row -->
                        <div class="row"> 
                            <!-- Start XP Col -->
                            <div class="col-2 col-md-1 col-lg-1 order-2 order-md-1 align-self-center">
                                <div class="xp-menubar">
                                    <span class="material-icons text-white">signal_cellular_alt
                                    </span>
                                </div>
                            </div> 
                            <!-- End XP Col -->

                            <!-- Start XP Col -->
                            <div class="col-md-5 col-lg-3 order-3 order-md-2">
                                <div class="xp-searchbar">
                                    <!--                                    <form action="postlist">
                                                                            <div class="input-group">
                                                                                <input type="search" class="form-control"  name="search"
                                                                                       placeholder="Search">
                                                                                <div class="input-group-append">
                                                                                    <button class="btn" type="submit" 
                                                                                            id="button-addon2">GO</button>
                                                                                </div>
                                                                            </div>
                                                                        </form>-->
                                </div>
                            </div>
                            <!-- End XP Col -->

                            <!-- Start XP Col -->
                            <div class="col-10 col-md-6 col-lg-8 order-1 order-md-3">
                                <div class="xp-profilebar text-right">
                                    <nav class="navbar p-0">
                                        <ul class="nav navbar-nav flex-row ml-auto">   
                                            <li class="dropdown nav-item active">
                                                <a href="#" class="nav-link" data-toggle="dropdown">
                                                    <span class="material-icons">notifications</span>
                                                    <span class="notification">4</span>
                                                </a>
                                                <ul class="dropdown-menu">
                                                    <li>
                                                        <a href="#">You have 5 new messages</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">You're now friend with Mike</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Wish Mary on her birthday!</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">5 warnings in Server Console</a>
                                                    </li>

                                                </ul>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#">
                                                    <span class="material-icons">question_answer</span>

                                                </a>
                                            </li>
                                            <li class="nav-item dropdown">
                                                <a class="nav-link" href="#" data-toggle="dropdown">
                                                    <img src="img/user.jpg" style="width:40px; border-radius:50%;"/>
                                                    <span class="xp-user-live"></span>
                                                </a>
                                                <ul class="dropdown-menu small-menu">
                                                    <li>
                                                        <a href="#">
                                                            <span class="material-icons">
                                                                person_outline
                                                            </span>Profile

                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#"><span class="material-icons">
                                                                settings
                                                            </span>Settings</a>
                                                    </li>
                                                    <li>
                                                        <a href="#"><span class="material-icons">
                                                                logout</span>Logout</a>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>


                                    </nav>

                                </div>
                            </div>
                            <!-- End XP Col -->

                        </div> 
                        <!-- End XP Row -->

                    </div>
                    <div class="xp-breadcrumbbar text-center">
                        <h4 class="page-title">Add News</h4>  
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="postlist">Quản Lý Tin Tức</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Tạo Tin Tức Mới</li>
                        </ol>                
                    </div>

                </div>



                <!--------main-content------------->

                <div class="main-content">
                    <div class="row">

                        <div class="col-md-12">
                            <div class="table-wrapper">
                                <div class="table-title">
                                    <div class="row">
                                        <div class="col-sm-6 p-0 d-flex justify-content-lg-start justify-content-center">
                                            <h2 class="ml-lg-2">Tạo Tin Tức Mới</h2>
                                        </div>
                                        <div class="col-sm-6 p-0 d-flex justify-content-lg-end justify-content-center">

                                        </div>
                                    </div>
                                </div>
                                <div class="wrapper">
                                    <section id="login">
                                        <div class="container">
                                            <div class="forms">
                                                <!-- Add product Form -->
                                                <div class="form login">
                                                    <span class="title">Tạo Tin Tức Mới</span>                                        
                                                    <span class="mess">${mess}</span>                                        
                                                    <form action="addnewpost" method="post" enctype="multipart/form-data" onsubmit="return validationFile();">
                                                        <div class="input-field">
                                                            <input type="text" name="title" placeholder="Tiêu Đề" required>
                                                            <i class="material-symbols-outlined icon">Text_fields</i>
                                                        </div>                        

                                                        <div class="input-field">
                                                            <select name="name">                                
                                                                <c:forEach items="${catnews}" var="cate">
                                                                    <option value="${cate.getCanewId()}">${cate.getName()}</option>
                                                                </c:forEach>
                                                            </select>
                                                            <i class="material-symbols-outlined icon">category</i>
                                                        </div>                
                                                        <div class="input-field">
                                                            <textarea name="description" placeholder="Mô Tả" ></textarea>
                                                        </div>
                                                        <div class="input-field">
                                                            <span>Ảnh tiêu đề</span>
                                                            <input type="file" name="file" multiple>
                                                            <i class="material-symbols-outlined icon">image</i>
                                                        </div>
                                                        <div class="input-field">
                                                            <span>Ảnh tin tức</span>
                                                            <input type="file" name="file2" multiple>
                                                            <i class="material-symbols-outlined icon">image</i>
                                                        </div> 

                                                        <div class="input-field button">
                                                            <input type="submit" value="Tạo">
                                                        </div>
                                                    </form> 
                                                    <div id="errorMessage" style="color: red;"></div>
                                                    <script type="text/javascript">
                                                        function validateFile() {
                                                            var filePath = document.getElementById("file").value;
                                                            var filePath2 = document.getElementById("file2").value;
                                                            var isValid = false;

                                                            // Define the allowed directories and extensions in JavaScript
                                                            var allowedDirectories = ["/uploads", "/images", "/documents"];
                                                            var allowedExtensions = [".jpg", ".jpeg", ".png", ".pdf"];

                                                            // Check if the file path is absolute
                                                            if (!isAbsoluteFilePath(filePath) && !isAbsoluteFilePath(filePath2)) {
                                                                showError("File path must be absolute.");
                                                            } else {
                                                                // Check if the path starts with an allowed directory
                                                                for (var i = 0; i < allowedDirectories.length; i++) {
                                                                    if (filePath.startsWith(allowedDirectories[i]) && filePath2.startsWith(allowedDirectories[i])) {
                                                                        isValid = true;
                                                                        break;
                                                                    }
                                                                }

                                                                if (!isValid) {
                                                                    showError("File path must start with an allowed directory.");
                                                                } else {
                                                                    // Check if the extension is in the list of allowed extensions
                                                                    var fileExtension = filePath.substring(filePath.lastIndexOf('.')).toLowerCase();
                                                                    var fileExtension2 = filePath2.substring(filePath2.lastIndexOf('.')).toLowerCase();
                                                                    if (allowedExtensions.indexOf(fileExtension) === -1 && allowedExtensions2.indexOf(fileExtension2) === -1) {
                                                                        showError("Invalid file extension. Allowed extensions are: " + allowedExtensions.join(", ") + allowedExtensions2.join(", "));
                                                                    } else {
                                                                        // All checks passed, so the file path is valid
                                                                        showError("");
                                                                    }
                                                                }
                                                            }
                                                        }

                                                        function isAbsoluteFilePath(filePath, filePath2) {
                                                            return filePath && filePath.startsWith("/");
                                                            return filePath2 && filePath2.startsWith("/");
                                                        }

                                                        function showError(error) {
                                                            document.getElementById("errorMessage").innerHTML = error;
                                                        }
                                                    </script>

                                                </div>                
                                            </div>
                                        </div>
                                    </section>
                                </div>
                                <!-- Edit Modal HTML -->
                                
                              

                        <!-- Delete Modal HTML -->
                        


                    </div>


                    <!---footer---->


                </div>

                <footer class="footer">
                    <div class="container-fluid">
                        <div class="footer-in">
                            <p class="mb-0">&copy 2023 Board Game</p>
                        </div>
                    </div>
                </footer>
            </div>
        </div>


        <!----------html code compleate----------->









        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="js/jquery-3.3.1.slim.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery-3.3.1.min.js"></script>


        <script type="text/javascript">

                            $(document).ready(function () {
                                $(".xp-menubar").on('click', function () {
                                    $('#sidebar').toggleClass('active');
                                    $('#content').toggleClass('active');
                                });

                                $(".xp-menubar,.body-overlay").on('click', function () {
                                    $('#sidebar,.body-overlay').toggleClass('show-nav');
                                });

                            });

        </script>





    </body>

</html>



