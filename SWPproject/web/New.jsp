<%-- 
    Document   : New
    Created on : 14-09-2023, 18:10:45
    Author     : hihih
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="vi" lang="vi"><head></head>
    <body>
        <%@include file="Header.jsp"  %>
        <div id="main" class="container-content">
            <div class="box_slide_news">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="title">
                                Tin nổi bật
                            </div>

                            <ul id="slide_news" class="slide_news">



                             <c:forEach items="${newsList}" var="listAllNews" begin="0" end="1">
                               
                                <li>
                                    <div class="list_item">
                                        <div class="item_image">
                                            <a href="newsdetails?nid=${listAllNews.nid}" title="${listAllNews.title}">
                                                <img src="images/${listAllNews.img}" width="570" height="325">
                                            </a>
                                        </div>
                                        <div class="bg_info"></div>
                                        <div class="info_news">
                                            <div class="item_name">
                                                <a href="newsdetails?nid=${listAllNews.nid}" title="">${listAllNews.title} <span class="bg_catenews" style="background-color:#000">Tin tức</span> </a>
                                            </div>
                                            <div class="item_summary">
                                                </div>
                                        </div>
                                    </div>
                                </li>
                                         
                                </c:forEach>
                            </ul>


                        </div>
                    </div>
                </div>
            </div>
            <div class="news_content">
<div class="container">
                    <div class="row">
                        <div class="col-lg-9">
                            <div class="title_page">
                                <h1 class="title_cate">Tin mới</h1>


                                <!--  list ca new-->
                                <ul class="box_child" itemscope itemtype="http://www.schema.org/SiteNavigationElement">
                                    <c:forEach items="${canewList}" var="canew">
                                        
                                    
                                    <li class itemprop="name"><a href="" itemprop="url" >${canew.name} </a></li>
</c:forEach>
                                </ul>
                                <!--  list ca new-->
                            </div>


    <div class="content">
          


       
            <c:forEach items="${requestScope.newsListbyCa}" var="news">
                <div id="list-contentNews" class="list-view">
                    <div class="summary"></div>
                    <div class="list-news">
                        <div class="list_item">
                            <div class="item_news">
                                <div class="item_image">
                                    <a href="newsdetails?nid=${news.nid}" title="${news.title.substring(0,50)}">
                                        <img src="images/${news.img}">
                                    </a>
                                </div>
                                <div class="info_news">
                                    <div class="time">
                                        ${news.createat} 
                                        <span class="bg_catenews" style="background-color:#000">Tin tức</span> 
                                    </div>
                                    <div class="time">
                                        <div class="item_name">
                                            <li ><a href="newsdetails?nid=${news.nid}">${news.title.substring(0,40)}....</a></li>
                                        </div>
                                        <div class="item_summary">
                                        </div>
                                        <div class="item_more">
                                            <a href="">Đọc tiếp ></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                                </c:forEach>
                    
                                        
                <div class="pager">Trang:<ul id="yw0" class="yiiPager"><li class="first hidden"><a href="/cn1/tin-tuc?q=%2Fcn1%2Ftin-tuc"></a></li>
                        <li class="previous hidden"><a href="/cn1/tin-tuc?q=%2Fcn1%2Ftin-tuc"><span class="glyphicon glyphicon-triangle-left"></span></a></li>
                        <li class="page selected"><a href="/cn1/tin-tuc?q=%2Fcn1%2Ftin-tuc">1</a></li>
                        <li class="page"><a href="/cn1/tin-tuc?q=%2Fcn1%2Ftin-tuc&NewsContents_page=2">2</a></li>
                        <li class="page"><a href="/cn1/tin-tuc?q=%2Fcn1%2Ftin-tuc&NewsContents_page=3">3</a></li>
                        <li class="page"><a href="/cn1/tin-tuc?q=%2Fcn1%2Ftin-tuc&NewsContents_page=4">4</a></li>
                        <li class="page"><a href="/cn1/tin-tuc?q=%2Fcn1%2Ftin-tuc&NewsContents_page=5">5</a></li>
                        <li class="page"><a href="/cn1/tin-tuc?q=%2Fcn1%2Ftin-tuc&NewsContents_page=6">6</a></li>
                        <li class="next"><a href="/cn1/tin-tuc?q=%2Fcn1%2Ftin-tuc&NewsContents_page=2"><span class="glyphicon glyphicon-triangle-right"></span></a></li>
                        <li class="last"><a href="/cn1/tin-tuc?q=%2Fcn1%2Ftin-tuc&NewsContents_page=79"></a></li></ul></div><div class="keys" style="display:none" title="/cn1/tin-tuc"><span>907</span><span>906</span><span>905</span><span>904</span><span>903</span><span>900</span><span>899</span><span>896</span></div>
                </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="box_tag">
                                <div class="content">
                                    <a target="_blank" class="tag-link" href="/tag/56/bai-uno" title="bài uno">bài uno</a>
                                    <a target="_blank" class="tag-link" href="/tag/20/ma-soi" title="ma sói">ma sói</a>
                                    <a target="_blank" class="tag-link" href="/tag/14/chien-thuat" title="chiến thuật">chiến thuật</a>
                                    <a target="_blank" class="tag-link" href="/tag/13/vui-nhon" title="vui nhộn">vui nhộn</a>
                                    <a target="_blank" class="tag-link" href="/tag/12/game-ban" title="game bàn">game bàn</a>
<a target="_blank" class="tag-link" href="/tag/10/ma-soi" title="Ma sói">Ma sói</a>
                                    <a target="_blank" class="tag-link" href="/tag/9/tri-tue" title="trí tuệ">trí tuệ</a>
                                    <a target="_blank" class="tag-link" href="/tag/1/game-15" title="game 15+">game 15+</a>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="box_face">
                                <div class="fb-page" data-href="https://www.facebook.com/boardgamevn" data-width="270" data-hide-cover="false" data-show-facepile="true" data-show-posts="false">
                                    <div class="fb-xfbml-parse-ignore">
                                        <blockquote cite="https://www.facebook.com/boardgamevn">
                                            <a href="https://www.facebook.com/boardgamevn">Hội những người khoái chơi BoardGame</a>
                                        </blockquote>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <%@include file="Footer.jsp"  %>

    </body></html>