<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="vi" lang="vi"><head>
        <title>Board Game VN - Bring People Closer</title>
        <head>

            <%@include file="Header.jsp"  %>
        </head>

        <body>
            <div id="main" class="container-content">
                <div class="news_content">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-9">
                                <div class="title_page">
                                    <h1 class="title_cate">Tin mới</h1>

                                    <ul class="box_child" itemscope="" itemtype="http://www.schema.org/SiteNavigationElement">
                                        <c:forEach items="${canewList}" var="canew">
                                            <li class="" itemprop="name"><a href="categoriesnews?canewId=${canew.canewId}" itemprop="url" title="">${canew.name} </a></li>
                                     
                                        </c:forEach>
                                    </ul>

                                </div>
                                <h1 class="title_news">
                                    <span items="${requestScope.news}" var="news">
                                        ${news.title}
                                    </span>
                                </h1>
                                <div class="page_info">
                                    <div class="date">

                                        Đăng ngày ${news.createat} 

                                    </div>
                                         <div class="page_info">
                                        View : ${count}
                                    </div>
                                    <div class="product-share">
                                    </div>
                                </div>
                                <div class="content_description">
                                    <p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;">
                                        &nbsp;</p>
                                    <p>
                                        <span style="font-size: 12pt; font-family: Arial; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">${requestScope.NewsDetails.description}</span></p>
                                    <p>
                                        &nbsp;</p>
                                    <!--                                    
                                    -->                                    <p>
                                        <span id="docs-internal-guid-089b7580-7fff-4d41-4278-52bb8e35e8e0"><span style="font-size: 12pt; font-family: Arial; font-weight: 700; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;"><span style="border:none;display:inline-block;overflow:hidden;width:602px;height:339px;"><img height="339" src="${requestScope.NewsDetails.img}" style="margin-left:0px;margin-top:0px;" width="602"></span></span></span></p>
                                    <p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;">
                                        &nbsp;</p>
                                </div>
                                <div class="comment_face">
                                    <div class="fb-comments fb_iframe_widget fb_iframe_widget_fluid_desktop" data-href="https://boardgame.vn/nv903/tham-tu-lung-danh-conan-nang-dau-halloween-thinh" data-colorscheme="light" data-numposts="10" data-width="915" fb-xfbml-state="rendered" fb-iframe-plugin-query="app_id=&amp;color_scheme=light&amp;container_width=915&amp;height=100&amp;href=https%3A%2F%2Fboardgame.vn%2Fnv903%2Ftham-tu-lung-danh-conan-nang-dau-halloween-thinh&amp;locale=vi_VN&amp;numposts=10&amp;sdk=joey&amp;version=v2.12&amp;width=915"><span style="vertical-align: top; width: 0px; height: 0px; overflow: hidden;"><iframe name="f1af1762471121c" width="915px" height="100px" data-testid="fb:comments Facebook Social Plugin" title="fb:comments Facebook Social Plugin" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" allow="encrypted-media" src="https://www.facebook.com/v2.12/plugins/comments.php?app_id=&amp;channel=https%3A%2F%2Fstaticxx.facebook.com%2Fx%2Fconnect%2Fxd_arbiter%2F%3Fversion%3D46%23cb%3Dfa0d049bcecf34%26domain%3Dboardgame.vn%26is_canvas%3Dfalse%26origin%3Dhttps%253A%252F%252Fboardgame.vn%252Ff3f1e73c53c0a7c%26relation%3Dparent.parent&amp;color_scheme=light&amp;container_width=915&amp;height=100&amp;href=https%3A%2F%2Fboardgame.vn%2Fnv903%2Ftham-tu-lung-danh-conan-nang-dau-halloween-thinh&amp;locale=vi_VN&amp;numposts=10&amp;sdk=joey&amp;version=v2.12&amp;width=915" style="border: none; visibility: visible; width: 0px; height: 0px;"></iframe></span></div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="box_relatednews">
                                    <div class="box_relatednews_title">
                                        Tin liên quan </div>
                                    <div class="box_relatednews_content">
                                        <div class="relatednews_item">
                                            <c:forEach items="${requestScope.nlist}" var="nlist" varStatus="loop">
                                                <c:if test="${nlist.nid != news.nid}">
                                                    <c:if test="${loop.index <= 3}">
                                                        <div class="item_image">
                                                            <a href="newsdetails?nid=${nlist.nid}" title="${nlist.title}">
                                                                <img src="${nlist.img}">
                                                            </a>
                                                        </div>
                                                        <div class="info_news">
                                                            <div class="item_name">
                                                                <a href="newsdetails?nid=${nlist.nid}" title="${nlist.title}">${nlist.title}</a>
                                                            </div>
                                                            <div class="item_date">
                                                                ${nlist.createat} </div>
                                                        </div>
                                                    </c:if>
                                                </c:if>
                                            </c:forEach>                                    
                                            <div class="box_tag">
                                                <div class="content">
                                                    <a target="_blank" class="tag-link" href="/tag/56/bai-uno" title="bài uno">bài uno</a>
                                                    <a target="_blank" class="tag-link" href="/tag/20/ma-soi" title="ma sói">ma sói</a>
                                                    <a target="_blank" class="tag-link" href="/tag/14/chien-thuat" title="chiến thuật">chiến thuật</a>
                                                    <a target="_blank" class="tag-link" href="/tag/13/vui-nhon" title="vui nhộn">vui nhộn</a>
                                                    <a target="_blank" class="tag-link" href="/tag/12/game-ban" title="game bàn">game bàn</a>
                                                    <a target="_blank" class="tag-link" href="/tag/10/ma-soi" title="Ma sói">Ma sói</a>
                                                    <a target="_blank" class="tag-link" href="/tag/9/tri-tue" title="trí tuệ">trí tuệ</a>
                                                    <a target="_blank" class="tag-link" href="/tag/1/game-15" title="game 15+">game 15+</a>
                                                </div>
                                            </div>
                                            <div class="clear"></div>
                                            <div class="box_face">
                                                <div class="fb-page fb_iframe_widget" data-href="https://www.facebook.com/boardgamevn" data-width="270" data-hide-cover="false" data-show-facepile="true" data-show-posts="false" fb-xfbml-state="rendered" fb-iframe-plugin-query="app_id=&amp;container_width=285&amp;hide_cover=false&amp;href=https%3A%2F%2Fwww.facebook.com%2Fboardgamevn&amp;locale=vi_VN&amp;sdk=joey&amp;show_facepile=true&amp;show_posts=false&amp;width=270"><span style="vertical-align: bottom; width: 0px; height: 0px;"><iframe name="f19b266fb2080f4" width="270px" height="1000px" data-testid="fb:page Facebook Social Plugin" title="fb:page Facebook Social Plugin" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" allow="encrypted-media" src="https://www.facebook.com/v2.12/plugins/page.php?app_id=&amp;channel=https%3A%2F%2Fstaticxx.facebook.com%2Fx%2Fconnect%2Fxd_arbiter%2F%3Fversion%3D46%23cb%3Df3c830b58fee33c%26domain%3Dboardgame.vn%26is_canvas%3Dfalse%26origin%3Dhttps%253A%252F%252Fboardgame.vn%252Ff3f1e73c53c0a7c%26relation%3Dparent.parent&amp;container_width=285&amp;hide_cover=false&amp;href=https%3A%2F%2Fwww.facebook.com%2Fboardgamevn&amp;locale=vi_VN&amp;sdk=joey&amp;show_facepile=true&amp;show_posts=false&amp;width=270" style="border: none; visibility: visible; width: 0px; height: 0px;" class=""></iframe></span></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clear"></div>


            <%@include file="Footer.jsp"  %>

        </body></html>