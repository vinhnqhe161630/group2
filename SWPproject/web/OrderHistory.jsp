<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="vi" lang="vi"><head>

                                        <link rel="stylesheet" type="text/css" href="css/styles.css">


                                                <link rel="stylesheet" type="text/css" href="https://cdn0.fahasa.com/media/css_secure/180dfde691fce34b2e789c30f74feafe.css?q=102553" media="all">
                                                    <link rel="stylesheet" type="text/css" href="https://cdn0.fahasa.com/media/css_secure/5722a920567ae30e71034727845e9254.css?q=102553" media="print">
         <link href="https://www.fahasa.com/blog/rss/index/store_id/1/" title="Blog" rel="alternate" type="application/rss+xml">

                                                                            <title> Đơn hàng của tôi</title>
                                                                            <link rel="stylesheet" href="css/pager.css" >
                                                                                  <link rel="stylesheet" href="css/styles.css" >
     
        
            <style>
              #footer{background: #132539;
    }
#footer .footer_center .info-foot {
    float: left;
    min-height: 370px;
    padding-top: 35px;
    width: 100%;
    background: #132539;
}

                                </style>
            <%@include file="Header.jsp"  %>
    </head>
    <body id="offcanvas-container" class=" sales-order-history">


        <div class="main-container col2-left-layout no-margin-top">
            <div class="main">
                <div>
                    <div class="container" style="background-color : transparent!important;">
                    </div>
                </div>
                <!--                                <div></div>-->
                <div class="container">
                    <div class="container-inner">
                        <div class="d-flex justify-content-center">
                            <div class="col-left sidebar col-lg-3 col-md-3 col-sm-12 col-xs-12" style="padding-left: 0px;">

                                <div class="block block-account">
                                    <div class="block-title">
                                        <strong><span>Tài khoản</span></strong>
                                    </div>
                                    <div class="block-content">
                                        <ul>
                                           
                                                                                                <li><a href="profile">Thông tin tài khoản</a></li>
                                                                                                <li><a href="shipmentDetails">Sổ địa chỉ</a></li>
                                                                                                <li class="orderList"><strong><a href="orderHistory">Đơn hàng của tôi</a></strong></li>   
                                                                                                 <li><a href="home">Về trang chủ</a></li>
                                                                                               
                                        </ul>
                                    </div>
                                </div>
                                <div class="block block-reorder">
                                    <div class="block-title">
                                        <strong><span>Đơn hàng của tôi</span></strong>
                                    </div>

                                </div>

                            </div>
                            <div class="col-main col-lg-9 col-md-9 col-sm-12 col-xs-12 col-fhs-main-body">







                            </div>  


                        </div>
                        <div class="col-main col-lg-9 col-md-9 col-sm-12 col-xs-12 col-fhs-main-body" style="margin-top: 8px;">
                            <div class="my-tabslider-order">
                                <div class="page-title">
                                    <h1>Đơn hàng của tôi</h1>
                                </div>
                                <div >
                                    <div >
                                        <div class="swiper-wrapper" > 
                                        
                                            <div class="tab-history-item swiper-slide ${status == null ? 'tab-history-item-active' : ''} swiper-slide-active" onclick="location.href = 'orderList';" style="width: 150px;">
                                                <div class="tab-history-item-border-left"></div>                <div class="tab-history-item-number">${all}</div>
                                                <div class="tab-history-item-text">Tất Cả</div>
                                                <div class="tab-history-item-border"></div>
                                            </div>
                                            
                                           <div class="tab-history-item swiper-slide ${status == '0' ? 'tab-history-item-active' : ''}" onclick="location.href = 'orderList?status=0';" style="width: 150px;">
   
                                                <div class="tab-history-item-number">${wait}</div>
                                                <div class="tab-history-item-text">Chờ xác nhận</div>
                                                <div class="tab-history-item-border"></div>
                                            </div>
                                            <div class="tab-history-item swiper-slide ${status == '1' ? 'tab-history-item-active' : ''}" onclick="location.href = 'orderList?status=1';" style="width: 150px;">
                                                <div class="tab-history-item-number">${shipping}</div>
                                                <div class="tab-history-item-text">Đang xử lí</div>
                                                <div class="tab-history-item-border"></div>
                                            </div>
                                            <div class="tab-history-item swiper-slide ${status == '2' ? 'tab-history-item-active' : ''}" onclick="location.href = 'orderList?status=2';" style="width: 150px;">
                                                <div class="tab-history-item-number">${receive}</div>
                                                <div class="tab-history-item-text">Hoàn tất</div>
                                                <div class="tab-history-item-border"></div>
                                            </div>
                                            <div class="tab-history-item swiper-slide ${status == '3' ? 'tab-history-item-active' : ''}" onclick="location.href = 'orderList?status=3';" style="width: 150px;">
                                                <div class="tab-history-item-number">${cancel}</div>
                                                <div class="tab-history-item-text">Bị hủy</div>
                                                <div class="tab-history-item-border"></div>
                                            </div>
                                               
                                        </div>
                                        <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span></div>
                                   
                                </div>

                            </div>
                            <div class="my-account">
                                
                                <div class="table-order-container">
                                    <div class="table-order-row table-order-header">
                                        <div class="table-order-cell">Mã đơn hàng</div>
                                        <div class="table-order-cell">Ngày mua</div>
                                        <div class="table-order-cell">Người nhận</div>
                                        <div class="table-order-cell">Tổng Tiền</div>
                                        <div class="table-order-cell">Trạng thái</div>
                                        <div class="table-order-cell"></div>
                                    </div>
                                   <c:forEach items="${orderList}" var="o">
                                    <div class="table-order-row" data-hrefs="https://www.fahasa.com/sales/order/view/order_id/7256000/">
                                        <div class="table-order-cell">
                                            <div class="table-order-cell-content table-order-cell-content-mobile">
                                                <div class="order-history-id">${o.oid}</div>
                                               
                                            </div>
                                        </div>
                                        <div class="table-order-cell">
                                            <div class="table-order-cell-content">
                                                <div class="order-history-date-mobile">Ngày mua:</div>
                                                  
                                                <div>${o.ordered_at}</div>
                                            </div>
                                        </div>
                                        <div class="table-order-cell" style="max-width:250px;">
                                            <div class="table-order-cell-content">
                                                <div class="order-history-person-mobile">Người nhận:</div>
                                                <div>${o.receiver}</div>
                                            </div>

                                        </div>
                                        <div class="table-order-cell">
                                            <div class="table-order-cell-content">
                                                <div class="order-history-total-mobile">Tổng Tiền:</div>
                                                  <fmt:formatNumber value="${o.totalAmount-o.discount}" pattern="#,##0" var="totalAmount" />
                                                <div><span class="price">${totalAmount}</span>&nbsp;đ</div>
                                            </div>
                                        </div>
                                            <div class="table-order-cell hidden-max-width-992 " >
                                            <c:if test="${o.status==0}"> Chờ xác nhận</c:if> 
                                            <c:if test="${o.status==1}"> Đang xử lí</c:if> 
                                            <c:if test="${o.status==2}"> Hoàn tất</c:if> 
                                            <c:if test="${o.status==3}">Đơn hàng bị hủy</c:if> 
                                        </div>
                             <div class="table-order-cell table-order-link-more hidden-max-width-992" onclick="postData('orderInfo', {oid: '${o.oid}'});">Xem chi tiết</div>

<script>
    function postData(url, data) {
        // Create a form
        const form = document.createElement('form');
        form.method = 'post';
        form.action = url;

        // Add the data as hidden inputs to the form
        for (let key in data) {
            if (data.hasOwnProperty(key)) {
                const input = document.createElement('input');
                input.type = 'hidden';
                input.name = key;
                input.value = data[key];
                form.appendChild(input);
            }
        }

        // Append the form to the body and submit
        document.body.appendChild(form);
        form.submit();
    }
</script>

                                    </div>
                                    </c:forEach>
                                </div>
                                
                               

                            </div>                                            </div>
                    </div>
                </div>
            </div>
        </div>	
        </div>

      <%@include file="Footer.jsp"  %>


        </body></html>