<%-- 
    Document   : ResetPassword
    Created on : 24-09-2023, 15:35:28
    Author     : hihih
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="vi" lang="vi"><head>
        <%@include file="Header.jsp" %>

<div id="main" class="container-content">
<div class="profile">
<div class="title_profile">
<i></i>
Khôi phục mật khẩu
</div>
<div class="content_profile">
<div class="container">
<div class="row">
<div class="col-lg-12">
<div class="sub-content">
<form id="forgotpass-form" action="forgotpassword" method="post"> <div class="row">
<label for="Customer_email" class="required">Email xác thực <span class="required">*</span></label> <input size="40" maxlength="40" name="email" id="Customer_email" type="text"> </div>

<div class="row btn-forgot-pass">

<input class="btn_forgot" type="submit" name="yt1" value="Tạo mới mật khẩu"> </div>

</form> 
    <p>${message}</p> 
</div>
</div>
</div>
</div>
</div>
</div>
</div>
        <%@include file="Footer.jsp" %>
</html>