<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<!DOCTYPE html><html lang="en"><head>
        <title>Manage Product</title>
        <link href="css/main.d810cf0ae7f39f28f336.css" rel="stylesheet"></head>
    <body>
        <div class="app-container app-theme-white body-tabs-shadow fixed-header fixed-sidebar">
            <div class="app-header header-shadow">


                <div class="app-header__content">
                    <div class="app-header-left">
                        <div class="input-holder">
                            <input type="text" class="search-input" placeholder="Type to search">

                        </div>

                        <ul class="header-megamenu nav">
                            <li class="nav-item">


                            </li>
                            <li class="btn-group nav-item">
                                <a class="nav-link" data-toggle="dropdown" aria-expanded="false">
                                    <span class="badge badge-pill badge-danger ml-0 mr-2">4</span> Settings
                                    <i class="fa fa-angle-down ml-2 opacity-5"></i>
                                </a>

                            </li>

                        </ul> </div>
                    <div class="app-header-right">
                        <div class="header-dots">

                            <div class="dropdown">
                                <button type="button" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" class="p-0 mr-2 btn btn-link">
                                    <span class="icon-wrapper icon-wrapper-alt rounded-circle">
                                        <span class="icon-wrapper-bg bg-danger"></span>
                                        <i class="icon text-danger icon-anim-pulse ion-android-notifications"></i>
                                        <span class="badge badge-dot badge-dot-sm badge-danger">Notifications</span>
                                    </span>
                                </button>

                            </div>

                            <div class="dropdown">
                                <button type="button" aria-haspopup="true" data-toggle="dropdown" aria-expanded="false" class="p-0 btn btn-link dd-chart-btn">
                                    <span class="icon-wrapper icon-wrapper-alt rounded-circle">
                                        <span class="icon-wrapper-bg bg-success"></span>
                                        <i class="icon text-success ion-ios-analytics"></i>
                                    </span>
                                </button>

                            </div>
                        </div>

                        <div class="header-btn-lg">
                            <button type="button" class="hamburger hamburger--elastic open-right-drawer">
                                <span class="hamburger-box">
                                    <span class="hamburger-inner"></span>
                                </span>
                            </button>
                        </div> </div>
                </div>
            </div> <div class="ui-theme-settings">


            </div> <div class="app-main">
                <div class="app-sidebar sidebar-shadow">
                    <div class="app-header__logo">

                        <div class="header__pane ml-auto">
                            <div>
                                <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                                    <span class="hamburger-box">
                                        <span class="hamburger-inner"></span>
                                    </span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="app-header__mobile-menu">
                        <div>
                            <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                                <span class="hamburger-box">
                                    <span class="hamburger-inner"></span>
                                </span>
                            </button>
                        </div>
                    </div>
                    <div class="app-header__menu">
                        <span>
                            <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                                <span class="btn-icon-wrapper">
                                    <i class="fa fa-ellipsis-v fa-w-6"></i>
                                </span>
                            </button>
                        </span>
                    </div> <div class="scrollbar-sidebar">
                        <div class="app-sidebar__inner">
                            <ul class="vertical-nav-menu">
                                <a href="home">
                                    <li class="app-sidebar__heading">Trang chủ</li>
                                </a>
                                <li class="mm-active">
                                    <a href="manageproduct">
                                        <i class="metismenu-icon pe-7s-rocket"></i>Quản lý sản phẩm 
                                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                                    </a>
                                    <ul>
                                        <li>
                                            <a href="addproduct" class="mm-active">
                                                <i class="metismenu-icon"></i>Thêm sản phẩm 
                                            </a>
                                        </li>
                                        <li>
                                            <a href="dashboards-commerce.html">
                                                <i class="metismenu-icon"></i>Commerce
                                            </a>
                                        </li>
                                        <li>
                                            <a href="dashboards-sales.html">
                                                <i class="metismenu-icon">
                                                </i>Sales
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="metismenu-icon"></i> Minimal
                                                <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                                            </a>
                                            <ul>
                                                <li>

                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a href="dashboards-crm.html">
                                                <i class="metismenu-icon"></i> CRM
                                            </a>
                                        </li>
                                    </ul>
                                </li>








                            </ul>
                        </div>
                    </div>
                </div><div class="app-main__outer">
                    <div class="app-main__inner">
                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">

                                    <div>Quản lý sản phẩm 
                                    </div>
                                </div>
                            </div>
                        </div> 


                        <%-- Bảng thống kê 1  --%>

                        <div class="tabs-animation">
                            <div class="mb-3 card">
                                <div class="card-header-tab card-header">
                                    <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                                        <i class="header-icon lnr-charts icon-gradient bg-happy-green"> </i>
                                       
                                    </div>
                                    <div class="btn-actions-pane-right text-capitalize">
                                    </div>
                                </div>
                                <div class="no-gutters row">
                                    <div class="col-sm-6 col-md-4 col-xl-4">
                                        <div class="card no-shadow rm-border bg-transparent widget-chart text-left">
                                            <div class="icon-wrapper rounded-circle">
                                                <div class="icon-wrapper-bg opacity-10 bg-warning"></div>
                                                <i class="lnr-laptop-phone text-dark opacity-8"></i>
                                            </div>
                                            <div class="widget-chart-content">
                                                <div class="widget-subheading">Tổng số sản phẩm </div>
                                                <div class="widget-numbers">${count}</div>
                                                <div class="widget-description opacity-8 text-focus">
                                                    <div class="d-inline text-danger pr-1">
                                                      
                                                        
                                                    </div>
                                                  
                                                </div>
                                            </div>
                                        </div>
                                        <div class="divider m-0 d-md-none d-sm-block"></div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-xl-4">
                                        <div class="card no-shadow rm-border bg-transparent widget-chart text-left">
                                            <div class="icon-wrapper rounded-circle">
                                                <div class="icon-wrapper-bg opacity-9 bg-danger"></div>
                                                <i class="lnr-graduation-hat text-white"></i>
                                            </div>
                                            <div class="widget-chart-content">
                                                <div class="widget-subheading">Tổng số khách hàng</div>
                                                <div class="widget-numbers"><span></span></div>
                                                <div class="widget-description opacity-8 text-focus">
                                                   
                                                   
                                                </div>
                                            </div>
                                        </div>
                                        <div class="divider m-0 d-md-none d-sm-block"></div>
                                    </div>
                                    <div class="col-sm-12 col-md-4 col-xl-4">
                                        <div class="card no-shadow rm-border bg-transparent widget-chart text-left">
                                            <div class="icon-wrapper rounded-circle">
                                                <div class="icon-wrapper-bg opacity-9 bg-success"></div>
                                                <i class="lnr-apartment text-white"></i>
                                            </div>
                                            <div class="widget-chart-content">
                                                <div class="widget-subheading">Tổng số đơn hàng </div>
                                                <div class="widget-numbers text-success"><span></span></div>
                                              
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>


                            <%--Bảng thống kê 2  --%>

                            <div class="row">
                                <div class="col-md-6 col-xl-3">
                                    <div class="card mb-3 widget-chart widget-chart2 text-left card-btm-border card-shadow-success border-success">
                                        <div class="widget-chat-wrapper-outer">
                                            <div class="widget-chart-content pt-3 pl-3 pb-1">
                                                <div class="widget-chart-flex">
                                                    <div class="widget-numbers">
                                                        <div class="widget-chart-flex">
                                                            <div class="fsize-4">
                                                                <small class="opacity-5">$</small>
                                                                <span>874</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <h6 class="widget-subheading mb-0 opacity-5">sales last month</h6>
                                            </div>
                                            <div class="no-gutters widget-chart-wrapper mt-3 mb-3 pl-2 he-auto row">
                                                <div class="col-md-9">
                                                    <div id="dashboard-sparklines-1"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xl-3">
                                    <div class="card mb-3 widget-chart widget-chart2 text-left card-btm-border card-shadow-primary border-primary">
                                        <div class="widget-chat-wrapper-outer">
                                            <div class="widget-chart-content pt-3 pl-3 pb-1">
                                                <div class="widget-chart-flex">
                                                    <div class="widget-numbers">
                                                        <div class="widget-chart-flex">
                                                            <div class="fsize-4">
                                                                <small class="opacity-5">$</small>
                                                                <span>1283</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <h6 class="widget-subheading mb-0 opacity-5">sales Income</h6>
                                            </div>
                                            <div class="no-gutters widget-chart-wrapper mt-3 mb-3 pl-2 he-auto row">
                                                <div class="col-md-9">
                                                    <div id="dashboard-sparklines-2"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xl-3">
                                    <div class="card mb-3 widget-chart widget-chart2 text-left card-btm-border card-shadow-warning border-warning">
                                        <div class="widget-chat-wrapper-outer">
                                            <div class="widget-chart-content pt-3 pl-3 pb-1">
                                                <div class="widget-chart-flex">
                                                    <div class="widget-numbers">
                                                        <div class="widget-chart-flex">
                                                            <div class="fsize-4">
                                                                <small class="opacity-5">$</small>
                                                                <span>1286</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <h6 class="widget-subheading mb-0 opacity-5">last month sales</h6>
                                            </div>
                                            <div class="no-gutters widget-chart-wrapper mt-3 mb-3 pl-2 he-auto row">
                                                <div class="col-md-9">
                                                    <div id="dashboard-sparklines-3"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xl-3">
                                    <div class="card mb-3 widget-chart widget-chart2 text-left card-btm-border card-shadow-danger border-danger">
                                        <div class="widget-chat-wrapper-outer">
                                            <div class="widget-chart-content pt-3 pl-3 pb-1">
                                                <div class="widget-chart-flex">
                                                    <div class="widget-numbers">
                                                        <div class="widget-chart-flex">
                                                            <div class="fsize-4">
                                                                <small class="opacity-5">$</small>
                                                                <span>564</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <h6 class="widget-subheading mb-0 opacity-5">total revenue</h6>
                                            </div>
                                            <div class="no-gutters widget-chart-wrapper mt-3 mb-3 pl-2 he-auto row">
                                                <div class="col-md-9">
                                                    <div id="dashboard-sparklines-4"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <%-- Danh sách sản phẩm  --%>
                            <div class="card mb-3">
                                <div class="card-header-tab card-header">
                                    <div class="card-header-title font-size-lg text-capitalize font-weight-normal"><i class="header-icon lnr-laptop-phone mr-3 text-muted opacity-6"> </i>Danh sách sản phẩm 
                                    </div>
                                </div>
                                <form action="manageproduct" method="post"> 
                                    <div class="card-body">
                                        <input type="text" class="search-input" name="pname" placeholder="Type to search">
                                        <button type="submit">Search</button>
                                        <table id="productTable" style="width: 100%;" id="example" class="table table-hover table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th onclick="sortTable(0)">Tên sản phẩm  </th>
                                                    <th onclick="sortTable(1)">Giá sản phẩm </th>
                                                    <th onclick="sortTable(2)">Số lượng </th>
                                                    <th>Ngày tạo </th>
                                                    <th>Chi tiết sản phẩm </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <c:forEach items="${pageProduct}" var="pp">



                                                    <tr>
                                                        <td>${pp.pname}</td>
                                                        <td>${pp.price}</td>
                                                        <td>${pp.quantity}</td>
                                                        <td>${pp.created_at}</td>
                                                        <td>
                                                            <a href="productdetail?Pid=${pp.pid}">Detail</a>    
                                                            <a href="edit?Pid=${pp.pid}">Edit</a>
                                                            <a href="delete?Pid=${pp.pid}">Delete</a>
                                                        </td>
                                                    </tr>
                                                <style>
                                                    td a {
                                                        margin-right: 10px;
                                                    }

                                                </style>
                                            </c:forEach>

                                            </tbody>

                                        </table>
                                        <style>
                                            .pagination {
                                                display: inline-block;
                                                margin: 10px;
                                                text-align: left;
                                            }

                                            .pagination a {
                                                color: #007BFF;
                                                padding: 8px 16px;
                                                text-decoration: none;
                                            }

                                            .pagination a:hover {
                                                background-color: #007BFF;
                                                color: #fff;
                                            }

                                            .pagination .current {
                                                background-color: #007BFF;
                                                color: #fff;
                                                padding: 8px 16px;
                                            }
                                        </style>

                                        <c:forEach begin="1" end="${endPage}" var="i" varStatus="loop">
                                            <c:set var="isActive" value="${i == currentPage ? 'current' : ''}" />
                                            <a class="pagination ${isActive}" href="manageproduct?index=${i}">${i}</a>
                                        </c:forEach>
                                    </div>
                                </form>  


                                <script>

                                    let sortPriceAsc = true;
                                    let sortQtyAsc = true;

                                    function sortTable(col) {

                                        if (col == 1) {
                                            sortPriceAsc = !sortPriceAsc;
                                        }
                                        if (col == 2) {
                                            // Đảo chiều sắp xếp 
                                            sortQtyAsc = !sortQtyAsc;
                                        }

                                        var table, rows, switching, i, x, y, shouldSwitch;

                                        table = document.getElementById("productTable");

                                        switching = true;

                                        while (switching) {

                                            switching = false;

                                            rows = table.rows;

                                            for (i = 1; i < (rows.length - 1); i++) {

                                                shouldSwitch = false;

                                                x = rows[i].getElementsByTagName("TD")[col];
                                                y = rows[i + 1].getElementsByTagName("TD")[col];

                                                // Kiểm tra nếu đang sắp xếp theo giá
                                                if (col == 1) {

                                                    // Chuyển sang number để so sánh
                                                    xPrice = Number(x.innerHTML);
                                                    yPrice = Number(y.innerHTML);

                                                    if (sortPriceAsc) {
                                                        if (xPrice > yPrice) {
                                                            shouldSwitch = true;
                                                            break;
                                                        }
                                                    } else {
                                                        if (xPrice < yPrice) {
                                                            shouldSwitch = true;
                                                            break;
                                                        }
                                                    }

                                                }
                                                if (col == 0) {

                                                    // Sắp xếp theo tên
                                                    if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                                                        shouldSwitch = true;
                                                        break;
                                                    }

                                                }
                                                if (col == 2) {
                                                    // Chuyển sang number
                                                    xQty = Number(x.innerHTML);
                                                    yQty = Number(y.innerHTML);

                                                    // So sánh theo chiều sắp xếp
                                                    if (sortQtyAsc) {
                                                        if (xQty > yQty) {
                                                            shouldSwitch = true;
                                                            break;
                                                        }
                                                    } else {
                                                        if (xQty < yQty) {
                                                            shouldSwitch = true;
                                                            break;
                                                        }
                                                    }
                                                }


                                            }

                                            if (shouldSwitch) {
                                                rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                                                switching = true;
                                            }

                                        }

                                    }

                                </script>

                            </div>
                        </div>

                        <%--Thêm sản phẩm mới  --%>


                    </div>
                    <%-- Bảng thống kê 3  --%>

                    <div class="card no-shadow bg-transparent no-border rm-borders mb-3">
                        <div class="card">
                            <div class="no-gutters row">
                                <div class="col-md-12 col-lg-4">
                                    <ul class="list-group list-group-flush">
                                        <li class="bg-transparent list-group-item">
                                            <div class="widget-content p-0">
                                                <div class="widget-content-outer">
                                                    <div class="widget-content-wrapper">
                                                        <div class="widget-content-left">
                                                            <div class="widget-heading">Total Orders</div>
                                                            <div class="widget-subheading">Last year expenses</div>
                                                        </div>
                                                        <div class="widget-content-right">
                                                            <div class="widget-numbers text-success">1896</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="bg-transparent list-group-item">
                                            <div class="widget-content p-0">
                                                <div class="widget-content-outer">
                                                    <div class="widget-content-wrapper">
                                                        <div class="widget-content-left">
                                                            <div class="widget-heading">Clients</div>
                                                            <div class="widget-subheading">Total Clients Profit</div>
                                                        </div>
                                                        <div class="widget-content-right">
                                                            <div class="widget-numbers text-primary">$12.6k</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-12 col-lg-4">
                                    <ul class="list-group list-group-flush">
                                        <li class="bg-transparent list-group-item">
                                            <div class="widget-content p-0">
                                                <div class="widget-content-outer">
                                                    <div class="widget-content-wrapper">
                                                        <div class="widget-content-left">
                                                            <div class="widget-heading">Followers</div>
                                                            <div class="widget-subheading">People Interested</div>
                                                        </div>
                                                        <div class="widget-content-right">
                                                            <div class="widget-numbers text-danger">45,9%</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="bg-transparent list-group-item">
                                            <div class="widget-content p-0">
                                                <div class="widget-content-outer">
                                                    <div class="widget-content-wrapper">
                                                        <div class="widget-content-left">
                                                            <div class="widget-heading">Products Sold</div>
                                                            <div class="widget-subheading">Total revenue streams</div>
                                                        </div>
                                                        <div class="widget-content-right">
                                                            <div class="widget-numbers text-warning">$3M</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-12 col-lg-4">
                                    <ul class="list-group list-group-flush">
                                        <li class="bg-transparent list-group-item">
                                            <div class="widget-content p-0">
                                                <div class="widget-content-outer">
                                                    <div class="widget-content-wrapper">
                                                        <div class="widget-content-left">
                                                            <div class="widget-heading">Total Orders</div>
                                                            <div class="widget-subheading">Last year expenses</div>
                                                        </div>
                                                        <div class="widget-content-right">
                                                            <div class="widget-numbers text-success">1896</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="bg-transparent list-group-item">
                                            <div class="widget-content p-0">
                                                <div class="widget-content-outer">
                                                    <div class="widget-content-wrapper">
                                                        <div class="widget-content-left">
                                                            <div class="widget-heading">Clients</div>
                                                            <div class="widget-subheading">Total Clients Profit</div>
                                                        </div>
                                                        <div class="widget-content-right">
                                                            <div class="widget-numbers text-primary">$12.6k</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="app-drawer-wrapper">
    <div class="drawer-nav-btn">
        <button type="button" class="hamburger hamburger--elastic is-active">
            <span class="hamburger-box"><span class="hamburger-inner"></span></span>
        </button>
    </div>
    <div class="drawer-content-wrapper">
        <div class="scrollbar-container">


            <h3 class="drawer-heading">Tasks in Progress</h3>
            <div class="drawer-section p-0">
                <div class="todo-box">
                    <ul class="todo-list-wrapper list-group list-group-flush">
                        <li class="list-group-item">
                            <div class="todo-indicator bg-warning"></div>
                            <div class="widget-content p-0">
                                <div class="widget-content-wrapper">
                                    <div class="widget-content-left mr-2">
                                        <div class="custom-checkbox custom-control">
                                            <input type="checkbox" id="exampleCustomCheckbox1266" class="custom-control-input">
                                            <label class="custom-control-label" for="exampleCustomCheckbox1266">&nbsp;</label>
                                        </div>
                                    </div>
                                    <div class="widget-content-left">
                                        <div class="widget-heading">Wash the car
                                            <div class="badge badge-danger ml-2">Rejected</div>
                                        </div>
                                        <div class="widget-subheading"><i>Written by Bob</i></div>
                                    </div>
                                    <div class="widget-content-right widget-content-actions">
                                        <button class="border-0 btn-transition btn btn-outline-success">
                                            <i class="fa fa-check"></i>
                                        </button>
                                        <button class="border-0 btn-transition btn btn-outline-danger">
                                            <i class="fa fa-trash-alt"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="todo-indicator bg-focus"></div>
                            <div class="widget-content p-0">
                                <div class="widget-content-wrapper">
                                    <div class="widget-content-left mr-2">
                                        <div class="custom-checkbox custom-control">
                                            <input type="checkbox" id="exampleCustomCheckbox1666" class="custom-control-input">
                                            <label class="custom-control-label" for="exampleCustomCheckbox1666">&nbsp;</label>
                                        </div>
                                    </div>
                                    <div class="widget-content-left">
                                        <div class="widget-heading">Task with hover dropdown menu</div>
                                        <div class="widget-subheading">
                                            <div>By Johnny
                                                <div class="badge badge-pill badge-info ml-2">NEW</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="widget-content-right widget-content-actions">
                                        <div class="d-inline-block dropdown">
                                            <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="border-0 btn-transition btn btn-link">
                                                <i class="fa fa-ellipsis-h"></i>
                                            </button>
                                            <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
                                                <h6 tabindex="-1" class="dropdown-header">Header</h6>
                                                <button type="button" disabled="" tabindex="-1" class="disabled dropdown-item">Action</button>
                                                <button type="button" tabindex="0" class="dropdown-item">Another Action</button>
                                                <div tabindex="-1" class="dropdown-divider"></div>
                                                <button type="button" tabindex="0" class="dropdown-item">Another Action</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="todo-indicator bg-primary"></div>
                            <div class="widget-content p-0">
                                <div class="widget-content-wrapper">
                                    <div class="widget-content-left mr-2">
                                        <div class="custom-checkbox custom-control">
                                            <input type="checkbox" id="exampleCustomCheckbox4777" class="custom-control-input">
                                            <label class="custom-control-label" for="exampleCustomCheckbox4777">&nbsp;</label>
                                        </div>
                                    </div>
                                    <div class="widget-content-left flex2">
                                        <div class="widget-heading">Badge on the right task</div>
                                        <div class="widget-subheading">This task has show on hover actions!</div>
                                    </div>
                                    <div class="widget-content-right widget-content-actions">
                                        <button class="border-0 btn-transition btn btn-outline-success">
                                            <i class="fa fa-check"></i>
                                        </button>
                                    </div>
                                    <div class="widget-content-right ml-3">
                                        <div class="badge badge-pill badge-success">Latest Task</div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="todo-indicator bg-info"></div>
                            <div class="widget-content p-0">
                                <div class="widget-content-wrapper">
                                    <div class="widget-content-left mr-2">
                                        <div class="custom-checkbox custom-control">
                                            <input type="checkbox" id="exampleCustomCheckbox2444" class="custom-control-input">
                                            <label class="custom-control-label" for="exampleCustomCheckbox2444">&nbsp;</label>
                                        </div>
                                    </div>
                                    <div class="widget-content-left mr-3">
                                        <div class="widget-content-left">
                                            <img width="42" class="rounded" src="images/1.jpg" alt="">
                                        </div>
                                    </div>
                                    <div class="widget-content-left">
                                        <div class="widget-heading">Go grocery shopping</div>
                                        <div class="widget-subheading">A short description ...</div>
                                    </div>
                                    <div class="widget-content-right widget-content-actions">
                                        <button class="border-0 btn-transition btn btn-sm btn-outline-success">
                                            <i class="fa fa-check"></i>
                                        </button>
                                        <button class="border-0 btn-transition btn btn-sm btn-outline-danger">
                                            <i class="fa fa-trash-alt"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="todo-indicator bg-success"></div>
                            <div class="widget-content p-0">
                                <div class="widget-content-wrapper">
                                    <div class="widget-content-left mr-2">
                                        <div class="custom-checkbox custom-control">
                                            <input type="checkbox" id="exampleCustomCheckbox3222" class="custom-control-input">
                                            <label class="custom-control-label" for="exampleCustomCheckbox3222">&nbsp;</label>
                                        </div>
                                    </div>
                                    <div class="widget-content-left flex2">
                                        <div class="widget-heading">Development Task</div>
                                        <div class="widget-subheading">Finish React ToDo List App</div>
                                    </div>
                                    <div class="widget-content-right">
                                        <div class="badge badge-warning mr-2">69</div>
                                    </div>
                                    <div class="widget-content-right">
                                        <button class="border-0 btn-transition btn btn-outline-success">
                                            <i class="fa fa-check"></i>
                                        </button>
                                        <button class="border-0 btn-transition btn btn-outline-danger">
                                            <i class="fa fa-trash-alt"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
</div>
</body></html>