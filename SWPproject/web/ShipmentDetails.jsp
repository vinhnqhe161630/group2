<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="vi" lang="vi"><head>


<link rel="stylesheet" type="text/css" href="https://cdn0.fahasa.com/media/css_secure/180dfde691fce34b2e789c30f74feafe.css?q=102591" media="all">
<link rel="stylesheet" type="text/css" href="https://cdn0.fahasa.com/media/css_secure/5722a920567ae30e71034727845e9254.css?q=102591" media="print">
<script type="text/javascript" async="" src="https://analytics.tiktok.com/i18n/pixel/static/main.MTEyYzFhMzhjNQ.js" data-id="CCUF92BC77U9S7CCBOSG"></script><script type="text/javascript" async="" src="https://www.googletagmanager.com/gtag/js?id=G-7L2XB3TJSV&amp;l=dataLayer&amp;cx=c"></script><script async="" src="https://www.clarity.ms/tag/hm5gwpvvf8?ref=gtm2"></script><script type="text/javascript" async="" src="https://analytics.tiktok.com/i18n/pixel/events.js?sdkid=CCUF92BC77U9S7CCBOSG&amp;lib=ttq"></script><script type="text/javascript" async="" src="https://connect.facebook.net/en_US/fbevents.js"></script><script src="https://connect.facebook.net/en_US/sdk.js?hash=2334e6e8e1f714a2444ee57f1dc10a1b" async="" crossorigin="anonymous"></script><script id="facebook-jssdk" src="https://connect.facebook.net/en_US/sdk.js"></script><script async="" src="https://webchat.caresoft.vn:8091/widget/widget.min.js?v=1.0"></script><script async="" src="https://www.googletagmanager.com/gtm.js?id=GTM-K6Z5N97"></script><script async="" src="https://cdn.moengage.com/webpush/moe_webSdk.min.latest.js"></script><script async="" src="//connect.facebook.net/en_US/fbevents.js"></script><script type="text/javascript" src="https://cdn0.fahasa.com/media/js/4d6839960373e1ce1ebc32503daa0a70.js?q=102591" async=""></script>
<script type="text/javascript" src="https://cdn0.fahasa.com/media/js/32a44da5d2065b5c746a7fcfe7e6904b.js?q=102591" defer=""></script>
<script type="text/javascript" src="https://cdn0.fahasa.com/media/js/f6f2e84d77312cef62e67b96df8d2ec0.js?q=102591"></script>
<link href="https://www.fahasa.com/blog/rss/index/store_id/1/" title="Blog" rel="alternate" type="application/rss+xml">
         <link rel="stylesheet" href="css/pager.css" >
                                                                                  <link rel="stylesheet" href="css/styles.css" >
         <%@include file="Header.jsp"  %>
          <style>
        #inFor {
            display: block;
        }
        #addNew {
            display: none;
        }
    </style>
<style>/* Chỉnh kiểu chữ và khoảng cách giữa dòng cho toàn bộ trang web */
    body {
        font-family: Arial, sans-serif; /* Thay đổi sang font chữ phù hợp */
        line-height: 1.6; /* Điều chỉnh khoảng cách giữa các dòng cho dễ đọc hơn */
       
    }

  
    .account_confirm_notification,
    .my-account,
    .col2-set {
        font-family: Arial, sans-serif; /* Thay đổi sang font chữ phù hợp */
        line-height: 1.6; /* Điều chỉnh khoảng cách giữa các dòng cho dễ đọc hơn */
    }

   
    h1, h2, h3 {
        font-family: Arial, sans-serif; /* Thay đổi sang font chữ phù hợp */
        line-height: 2.5; 
        font-size: 24px; 
        padding: 10px;
    }

    /* Chỉnh kiểu chữ và khoảng cách giữa dòng cho nội dung địa chỉ */
    .addresses-list, address {
        font-family: Arial, sans-serif; /* Thay đổi sang font chữ phù hợp */
        line-height: 2.4; /* Điều chỉnh khoảng cách giữa các dòng cho dễ đọc hơn */
        font-size: 16px; /* Tăng kích thước phông chữ cho nội dung địa chỉ */
       
      
       
    }

    /* Chỉnh kiểu chữ và khoảng cách giữa dòng cho nút */
    .button {
        font-family: Arial, sans-serif; /* Thay đổi sang font chữ phù hợp */
        line-height: 1.6; /* Điều chỉnh khoảng cách giữa các dòng cho dễ đọc hơn */
        font-size: 16px; /* Tăng kích thước phông chữ cho nút */
    }
    a {
        font-weight: bold; /* Làm đậm chữ cho liên kết */
        font-size: 13px; /* Tăng kích thước phông chữ cho liên kết */
    }






</style>
</head>

<div class="page">

    <div class="main-container col2-left-layout no-margin-top">
        <div class="main">
            <div>
                <div class="container" style="background-color : transparent!important;">
                </div>
            </div>
            <!--                                <div></div>-->
            <div class="container">
                <div class="container-inner">
                    <div class="d-flex justify-content-center">
                        <div class="col-left sidebar col-lg-3 col-md-3 col-sm-12 col-xs-12" style="padding-left: 0px;">

                            <div class="block block-account">
                                <div class="block-title">
                                    <strong><span>Tài khoản</span></strong>
                                </div>
                                <div class="block-content">
                                    <ul>
                                         <li><a href="profile">Thông tin tài khoản</a></li>
                                                                                                <li><a href="shipmentDetails">Sổ địa chỉ</a></li>
                                                                                                <li class="orderHistory"><strong><a href="orderHistory">Đơn hàng của tôi</a></strong></li>    
                                                                                                 <li><a href="home">Về trang chủ</a></li>
                                    </ul>
                                </div>
                            </div>


                        </div>
             

                        <div id="inFor" class="col-main col-lg-9 col-md-9 col-sm-12 col-xs-12 col-fhs-main-body" style="margin-top: 8px;">
                            <div class="my-account"><div class="page-title title-buttons">
                                    <h1 style="padding-top: 7px;">Sổ địa chỉ</h1>
                                    <button style="padding-bottom: 15px;" type="button" title="Thêm địa chỉ mới" class="button" onclick="toggleCode()"><span>Thêm địa chỉ mới</span></button>
                                </div>
                                <div class="col2-set addresses-list">
                                    <div class="col-1 addresses-primary">
                                        <h2>Địa chỉ mặc định</h2>
                                        <ol>
                                            <li class="item">

                                                <address>
                                                 Người nhận : ${sh.receiver}  <br>
                                                 Địa chỉ: ${sh.address}<br>
                                                 Điện Thoại ${sh.phonenumber}

                                                </address>
                                            </li>

                                            
                                        </ol>
                                    </div>
                                    <div class="col-2 addresses-additional">
                                        <h2>Địa chỉ khác</h2>
                                        <ol>
                                            <c:forEach items="${listsh}" var="sh">
                                            <li class="item">
                                                  <address>
                                                   Người nhận : ${sh.receiver}  <br>
                                                 Địa chỉ: ${sh.address}<br>
                                                 Điện Thoại ${sh.phonenumber}

                                                </address>
                                                <form id="removeForm" action="shipmentDetails" method="post" style="display: none;">
    <input type="hidden" name="type" value="removeInfo">
    <input type="hidden" name="removeInfo" id="removeInfoValue">
</form>

<form id="changeForm" action="shipmentDetails" method="post" style="display: none;">
    <input type="hidden" name="type" value="changeInfo">
    <input type="hidden" name="changeMarkShip" id="changeMarkShipValue">
</form>

<script>
    function submitForm(type, value) {
        if (type === 'removeInfo') {
            document.getElementById('removeInfoValue').value = value;
            document.getElementById('removeForm').submit();
        } else if (type === 'changeInfo') {
            document.getElementById('changeMarkShipValue').value = value;
            document.getElementById('changeForm').submit();
        }
    }
</script>

<p>
    <a href="#" onclick="submitForm('removeInfo', '${sh.aaid}')">Xóa địa chỉ</a>
    <span class="separator">|</span>
    <a href="#" onclick="submitForm('changeInfo', '${sh.aaid}')" style="color: black">Chọn làm địa chỉ mặc định</a>
</p>

                                            </li>
                                            </c:forEach>
                                        </ol>
                                    </div>
                                </div>
                               
                            </div>                                            </div>
                        
                        
                                
   <div id="addNew" class="col-main col-lg-9 col-md-9 col-sm-12 col-xs-12 col-fhs-main-body" style="margin-top: 8px; display: none">
<div class="my-account"><div class="page-title">
    <h1>Thêm địa chỉ mới</h1>
</div>
<form class="form-edit-center form-edit-address-account" action="shipmentDetails?type=addNew" method="post" id="form-validate">
    <input name="form_key" type="hidden" value="GSBpD8uC6OzRNNuZ">
    <input type="hidden" name="success_url" value="">
    <input type="hidden" name="error_url" value="">
    
    <div class="col-lg-6 col-md-6 col-sm-6 form-list">
        <div class="edit-address-block">        
            <h2 class="legend">Thông tin liên hệ</h2>
            <ul class="form-list">
                <li class="fields">
                    </li>

<li class="name-edit">
    <div class="input-box">
        <input placeholder="Người nhận" value="${receiver}"  name="newReceiver" title="Người nhận" maxlength="255" class="input-text required-entry">
    </div>
</li>
                
                <li class="fields">
                    <div class="field" style="padding-top: 0px;">
                        <div class="input-box">
                            <input type="text" maxlength="10" minlength="10"   name="newPhonenumber" placeholder="Ex: 0972xxxx" value="${phonenumber}" title="Điện thoại" class="input-text validate-phone-numberonly-atleast7  required-entry" id="telephone">
                        </div>
                    </div>                
                </li>
            </ul>
        </div>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 form-list">
        <div class="edit-address-block">
            <h2 class="legend">Địa chỉ</h2>
            <ul class="form-list">
                            <li class="wide" style="padding-top: 14px;">
                    <div class="input-box">
                        <input type="text" maxlength="200" placeholder="Địa chỉ" name="newAddress" value="${address}" title="Địa chỉ" id="street_1" class="input-text  required-entry">
                    </div>
                </li>
                                                        <li class="fields">
                                 
                    <div class="field" style="padding-bottom: 5px;">
                        <label for="region_id" class="required"><em>*</em>Tỉnh/Thành phố</label>
                        <div class="input-box address-edit-dd">
                       
                            <select   class="validate-select required-entry"   name="city"  id="city" aria-label=".form-select-sm">
                                                                            <option value=""  selected>Chọn tỉnh thành</option>           
                                                                        </select>
                        </div>
                    </div>
                </li>
                <li class="fields">
                    <div class="field">
                        <label for="city" class="required"><em>*</em>Quận/Huyện</label>
                        <div class="input-box address-edit-dd">
                             <select name="district" style="margin-bottom: 15px;" class="validate-select"  id="district" aria-label=".form-select-sm">
                                                                            <option value="" selected>Chọn quận huyện</option>
                                     </select> 
                        </div>
                    </div>
                    <div id="ward-field" class="field">
                        <label for="ward" class="required"><em>*</em>Xã/Phường</label>
                        <div class="input-box address-edit-dd">
                                                      <select name="ward" style="margin-bottom: 15px;" class="validate-select" id="ward" aria-label=".form-select-sm">
                                                                            <option value="" selected>Chọn phường xã</option>
                                                                        </select>
                           
                        </div>
                    </div>
                                
                </li>
               
            </ul>
        </div>
    </div>
    
    <div class="buttons-set">
        <p class="required">${err}</p>
        <p class="back-link"><a href="shipmentDetails" ><small>« </small>Quay lại</a></p>
        <button data-action="save-customer-address" type="submit" title="Lưu địa chỉ" class="button"><span>Lưu địa chỉ</span></button>
    </div>
</form>
</div>                                            </div>
                        
      
    <script> 
var displayFormValue = '${displayForm}'; // Giả sử displayForm có giá trị 'block' hoặc 'none' như bạn đã đề cập

document.addEventListener('DOMContentLoaded', function() {
      var inFor = document.getElementById("inFor");
            var addNew = document.getElementById("addNew");

    if (displayFormValue === 'none') {
        if (inFor) {
            inFor.style.display = 'block';
        }
        if (addNew) {
            addNew.style.display = 'none';
        }
    } else if (displayFormValue === 'block') {
        if (inFor) {
            inFor.style.display = 'none';
        }
        if (addNew) {
            addNew.style.display = 'block';
        }
    } else {
        console.error("Invalid value for displayFormValue. Please use 'block' or 'none'.");
    }
});
    </script>
 <script>
       
        function toggleCode() {
            var inFor = document.getElementById("inFor");
            var addNew = document.getElementById("addNew");

            inFor.style.display = "none";
            addNew.style.display = "block";
        }
          function toggleCode2() {
            var inFor = document.getElementById("inFor");
            var addNew = document.getElementById("addNew");

            inFor.style.display = "block";
            addNew.style.display = "none";
        }
    </script>
             <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.1/axios.min.js"></script>
                                                                                                                <script>
                                                                                                                                    var cities = document.getElementById("city");
                                                                                                                                    var districts = document.getElementById("district");
                                                                                                                                    var wards = document.getElementById("ward");
                                                                                                                                    var selectedCityValue = '${city}'; // Replace with the actual value of the selected city

                                                                                                                                    var Parameter = {
                                                                                                                                        url: "https://raw.githubusercontent.com/kenzouno1/DiaGioiHanhChinhVN/master/data.json",
                                                                                                                                        method: "GET",
                                                                                                                                        responseType: "application/json",
                                                                                                                                    };

                                                                                                                                    var promise = axios(Parameter);

                                                                                                                                    promise.then(function (result) {
                                                                                                                                        renderCity(result.data);
                                                                                                                                        selectCityOption(result.data);
                                                                                                                                        selectDistrictOption(result.data);
                                                                                                                                        selectWardOption(result.data);
                                                                                                                                    });

                                                                                                                                    function selectCityOption(data) {
                                                                                                                                        for (let i = 0; i < cities.options.length; i++) {
                                                                                                                                            if (cities.options[i].value === selectedCityValue) {
                                                                                                                                                cities.options[i].selected = true;
                                                                                                                                                // Trigger change event to populate districts and wards
                                                                                                                                                simulateEvent(cities, 'change');
                                                                                                                                                break;
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                    function selectDistrictOption(data) {
                                                                                                                                        const selectedCity = data.find((city) => city.Name === selectedCityValue);
                                                                                                                                        for (let i = 0; i < selectedCity.Districts.length; i++) {
                                                                                                                                            if (selectedCity.Districts[i].Name === '${district}') {
                                                                                                                                                districts.options[i + 1].selected = true; // Plus 1 to account for the initial "Chọn quận huyện" option
                                                                                                                                                simulateEvent(districts, 'change');
                                                                                                                                                break;
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                    }

                                                                                                                                    function selectWardOption(data) {
                                                                                                                                        const selectedCity = data.find((city) => city.Name === selectedCityValue);
                                                                                                                                        const selectedDistrict = selectedCity.Districts.find((district) => district.Name === '${district}');
                                                                                                                                        for (let i = 0; i < selectedDistrict.Wards.length; i++) {
                                                                                                                                            if (selectedDistrict.Wards[i].Name === '${ward}') {
                                                                                                                                                wards.options[i + 1].selected = true; // Plus 1 to account for the initial "Chọn phường xã" option
                                                                                                                                                simulateEvent(wards, 'change');
                                                                                                                                                break;
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                    function renderCity(data) {
                                                                                                                                        for (const city of data) {
                                                                                                                                            cities.options[cities.options.length] = new Option(city.Name, city.Name); // Use "Name" as both value and text.
                                                                                                                                        }

                                                                                                                                        cities.onchange = function () {
                                                                                                                                            districts.length = 1;
                                                                                                                                            wards.length = 1;

                                                                                                                                            if (this.value !== "") {
                                                                                                                                                const selectedCity = data.find((city) => city.Name === this.value);

                                                                                                                                                for (const district of selectedCity.Districts) {
                                                                                                                                                    districts.options[districts.options.length] = new Option(district.Name, district.Name); // Use "Name" as both value and text.
                                                                                                                                                }
                                                                                                                                            }
                                                                                                                                        };

                                                                                                                                        districts.onchange = function () {
                                                                                                                                            wards.length = 1;
                                                                                                                                            const selectedCity = data.find((city) => city.Name === cities.value);
                                                                                                                                            const selectedDistrict = selectedCity.Districts.find((district) => district.Name === this.value);

                                                                                                                                            if (this.value !== "") {
                                                                                                                                                for (const ward of selectedDistrict.Wards) {
                                                                                                                                                    wards.options[wards.options.length] = new Option(ward.Name, ward.Name); // Use "Name" as both value and text.
                                                                                                                                                }
                                                                                                                                            }
                                                                                                                                        };
                                                                                                                                    }

                                                                                                                                    // Function to simulate change event
                                                                                                                                    function simulateEvent(element, eventName) {
                                                                                                                                        var event = new Event(eventName);
                                                                                                                                        element.dispatchEvent(event);
                                                                                                                                    }

                                                                                                                </script>  
                        
                        
                    </div>
                </div>
            </div>
        </div>	
    </div>













</div>
</div>
</section>
</section>

<style>
    #footer{
        background: #132539;
    }
    #footer .footer_center .info-foot {
        float: left;
        min-height: 370px;
        padding-top: 35px;
        width: 100%;
        background: #132539;
    }

</style>

<%@include file="Footer.jsp"  %>


<div id="fb-root" class=" fb_reset"><div style="position: absolute; top: -10000px; width: 0px; height: 0px;"><div></div></div></div><iframe id="moengage-web-helper-frame" src="https://cdn.moengage.com/webpush/beta/webpushhelper.html" style="display: none;"></iframe></body><div style="position: absolute; top: 0px; z-index: 2147483647; display: block !important;"></div><iframe id="__JSBridgeIframe_1.0__" title="jsbridge___JSBridgeIframe_1.0__" style="display: none;"></iframe><iframe id="__JSBridgeIframe_SetResult_1.0__" title="jsbridge___JSBridgeIframe_SetResult_1.0__" style="display: none;"></iframe><iframe id="__JSBridgeIframe__" title="jsbridge___JSBridgeIframe__" style="display: none;"></iframe><iframe id="__JSBridgeIframe_SetResult__" title="jsbridge___JSBridgeIframe_SetResult__" style="display: none;"></iframe></html>