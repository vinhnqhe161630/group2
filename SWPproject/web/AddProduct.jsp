<%-- 
    Document   : AddProduct
    Created on : 13-10-2023, 15:16:50
    Author     : tuanm
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en" class="light-style layout-navbar-fixed layout-menu-fixed layout-compact " dir="ltr" data-theme="theme-default" data-assets-path="../../assets/" data-template="vertical-menu-template">
    <head>
        <title>Web Board Game</title>
        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin="">
        <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700&amp;ampdisplay=swap" rel="stylesheet">

        <!-- Icons -->
        <link rel="stylesheet" href="css/materialdesignicons.css">
        <link rel="stylesheet" href="css/flag-icons.css">

        <!-- Menu waves for no-customizer fix -->
        <link rel="stylesheet" href="css/node-waves.css">

        <!-- Core CSS -->
        <link rel="stylesheet" href="css/core.css" class="template-customizer-core-css">
        <link rel="stylesheet" href="css/theme-default.css" class="template-customizer-theme-css">
        <link rel="stylesheet" href="css/demo.css">

        <!-- Vendors CSS -->
        <link rel="stylesheet" href="css/perfect-scrollbar.css">
        <link rel="stylesheet" href="css/typeahead.css"> 
        <link rel="stylesheet" href="css/typography.css">
        <link rel="stylesheet" href="css/katex.css">
        <link rel="stylesheet" href="css/editor.css">
        <link rel="stylesheet" href="css/select2.css">
        <link rel="stylesheet" href="css/dropzone.css">
        <link rel="stylesheet" href="css/flatpickr.css">
        <link rel="stylesheet" href="css/tagify.css">



    </head>
    <body>
        <div class="layout-container">

            <!-- Menu -->

            <aside class="layout-menu menu-vertical menu bg-menu-theme">
                <div class="app-brand demo ">
                    <a href="index.html" class="app-brand-link">
                        <span class="app-brand-logo demo me-1">
                        </span>
                        <span class="app-brand-text demo menu-text fw-semibold ms-2">Add product</span>
                    </a>
                </div>

                <ul class="menu-inner py-1">
                    <!-- Dashboards -->
                    <li class="menu-item">
                        <a href="manageproduct" class="menu-link menu-toggle">
                            <i class="menu-icon tf-icons mdi mdi-home-outline"></i>
                            <div>ManageProduct</div>
                            <div class="badge bg-danger rounded-pill ms-auto"></div>
                        </a>

                    </li>
                </ul>
            </aside>
            <!-- / Menu -->
            <!-- Layout container -->

            <div class="layout-page">
                <div class="content-wrapper">

                    <!-- Content -->

                    <div class="container-xxl flex-grow-1 container-p-y">
                        <form action="addproduct" method="get">


                            <div class="app-ecommerce">

                                <!-- Add Product -->
                                <div class="d-flex flex-column flex-md-row justify-content-between align-items-start align-items-md-center mb-3">

                                    <div class="d-flex flex-column justify-content-center">
                                        <h4 class="mb-1 mt-3">Add a new Product</h4>
                                    </div>
                                    <a style="color: green">${success}</a>


                                    <div class="d-flex align-content-center flex-wrap gap-3">

                                        <button type="submit" class="btn btn-primary">Publish product</button>
                                    </div>
                                </div>

                                <div class="row">

                                    <!-- First column-->
                                    <div class="col-12 col-lg-8">
                                        <!-- Product Information -->
                                        <div class="card mb-4">
                                            <div class="card-header">
                                                <h5 class="card-tile mb-0">Product information</h5>
                                            </div>
                                            <div class="card-body">
                                                <div class="form-floating form-floating-outline mb-4">
                                                    <input type="text" name="pname" class="form-control" required placeholder="Product title" aria-label="Product title" value="${param.pname}">
                                                    <label for="ecommerce-product-name">Product Name </label>
                                                    <a style="color: red">${nameerror}</a>
                                                    <a style="color: red">${pnameerror}</a>
                                                </div>


                                                <div class="row mb-3">

                                                    <div class="col">
                                                        <div class="form-floating form-floating-outline">
                                                            <input type="number" name="quantity" class="form-control" required value="${param.quantity}" >
                                                            <label for="ecommerce-product-name">Quantity</label>
                                                            <a style="color: red">${quantityerror}</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Comment -->
                                                <div>
                                                    <label class="form-label" >Description </label>
                                                    <textarea  name="description" style="width: 100%" rows="5" cols="106"  required >${param.description}</textarea><br>
                                                    <a style="color: red">${descriptionerr}</a>
                                                </div>

                                                <div>
                                                    <label class="form-label" >Rules </label>
                                                    <textarea name="rules" style="width: 100%" rows="5" cols="106" required>${param.rules}</textarea><br>
                                                    <a style="color: red">${ruleserr}</a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /Product Information -->
                                        <!-- Media -->
                                        <div class="card mb-4">
                                            <div class="card-header d-flex justify-content-between align-items-center">
                                                <h5 class="mb-0 card-title">Ảnh</h5>

                                            </div>
                                            <input type="file" id="avatar" name="picture" value="${param.picture}" required  accept="image/*" ><br>
                                        </div>

                                        <!-- Variants -->
                                        <div class="card mb-4">
                                            <div class="card-header">
                                                <h5 class="card-title mb-0">Product Category</h5>
                                            </div>
                                            <div class="card-body">

                                                <div data-repeater-list="group-a">
                                                    <div data-repeater-item="">
                                                        <div class="row mb-3 mb-sm-0">
                                                            <div class="mb-3 col-sm-12">
                                                                <div class="form-floating form-floating-outline">
                                                                    <select name="category" class="form-select">
                                                                        <c:forEach items="${listca}" var="c">
                                                                            <option value="${c.caid}">${c.caname}</option>
                                                                        </c:forEach>
                                                                    </select>
                                                                    <label for="select2Basic">Option</label>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                    <div class="col-12 col-lg-4">
                                        <!-- Pricing Card -->
                                        <div class="card mb-4">
                                            <div class="card-header">
                                                <h5 class="card-title mb-0">Pricing</h5>
                                            </div>
                                            <div class="card-body">
                                                <!-- Base Price -->
                                                <div class="form-floating form-floating-outline mb-4">
                                                    <input type="number" class="form-control" id="ecommerce-product-price" placeholder="Price" name="productPrice" required aria-label="Product price"value="${param.productPrice}">
                                                    <label for="ecommerce-product-price"> Price</label>
                                                    <a style="color: red">${priceerror}</a>
                                                </div>

                                                <!-- Discounted Price -->
                                                <div class="form-floating form-floating-outline mb-4">
                                                    <input type="number" class="form-control" id="ecommerce-product-discount-price" placeholder="Discounted Price" name="productDiscountedPrice" aria-label="Product discounted price" value="${param.discount}">
                                                    <label for="ecommerce-product-discount-price">Discounted Price</label>
                                                </div>

                                                <!-- Instock switch -->

                                            </div>
                                        </div>
                                        <!-- /Pricing Card -->
                                        <!-- Organize Card -->
                                        <div class="card mb-4">
                                            <div class="card-header">
                                                <h5 class="card-title mb-0">Other</h5>
                                            </div>
                                            <div class="card-body">
                                                <!-- Vendor -->
                                                <div class="mb-4 col ecommerce-select2-dropdown">
                                                    <div class="form-floating form-floating-outline">
                                                        <input type="number" name="requiredage" class="form-control" required placeholder="Required Age"  aria-label="Product price" value="${param.requiredage}">
                                                        <label for="Required Age">Required Age</label>
                                                        <a style="color: red">${ageerror}</a>
                                                    </div>
                                                </div>
                                                <div class="mb-4 col ecommerce-select2-dropdown">
                                                    <div class="form-floating form-floating-outline">
                                                        <input type="number" name="numofplayer" class="form-control" required placeholder="Number of player" value="${param.numofplayer}">
                                                        <label for="Number of player">Number of player</label>
                                                        <a style="color: red">${numPayererror}</a>
                                                    </div>
                                                </div>
                                                <div class="mb-4 col ecommerce-select2-dropdown">
                                                    <div class="form-floating form-floating-outline">
                                                        <input type="text" name="timeplay" class="form-control" required placeholder="Time play" aria-label="Product price" value="${param.timeplay}">
                                                        <label for="Time play">Time play</label>
                                                        <a style="color: red">${timePlayerr}</a>
                                                    </div>
                                                </div>
                                                <div class="mb-4 col ecommerce-select2-dropdown">
                                                    <div class="form-floating form-floating-outline">
                                                        <input type="text" name="author" class="form-control" required placeholder="Author" aria-label="Product price" value="${param.author}">
                                                        <label for="Author">Author</label>
                                                        <a style="color: red">${pubnameerr}</a>
                                                    </div>
                                                </div>
                                                <div class="mb-4 col ecommerce-select2-dropdown">
                                                    <div class="form-floating form-floating-outline">
                                                        <input type="text" name="country" class="form-control" required placeholder="Country" aria-label="Product price" value="${param.country}">
                                                        <label for="Country">Country</label>
                                                        <a style="color: red">${countryerr}</a>
                                                    </div>
                                                </div>
                                                <div class="mb-4 col ecommerce-select2-dropdown">
                                                    <div class="form-floating form-floating-outline">
                                                        <input type="text" name="material" class="form-control" required placeholder="Material" aria-label="Product price" value="${param.material}">
                                                        <label for="Material">Material</label>
                                                        <a style="color: red">${materialerr}</a>
                                                    </div>
                                                </div>
                                                <div class="mb-4 col ecommerce-select2-dropdown">
                                                    <div class="form-floating form-floating-outline">
                                                        <input type="text" name="size" class="form-control" required placeholder="Size" aria-label="Product price" value="${param.size}">
                                                        <label for="Size">Size</label>
                                                        <a style="color: red">${sizeerr}</a>
                                                    </div>
                                                </div>
                                                <div class="mb-4 col ecommerce-select2-dropdown">
                                                    <div class="form-floating form-floating-outline">
                                                        <input type="text" name="weight" class="form-control" required placeholder="Weight" aria-label="Product price" value="${param.weight}">
                                                        <label for="Weight">Weight</label>
                                                        <a style="color: red">${weightnerr}</a>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <!-- /Organize Card -->
                                    </div>
                                    <!-- /Second column -->
                                </div>

                            </div>
                        </form>



                    </div>
                    <!-- / Content -->






                </div>
            </div>
        </div>
    </body>
</html>
