<%-- 
    Document   : EditCodeSale
    Created on : Oct 25, 2023, 4:38:33 AM
    Author     : FPT
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions"%>
<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
        <title>Board Game</title>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <!----css3---->
        <link rel="stylesheet" href="css/custom.css">
        <!--google fonts -->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600&display=swap" rel="stylesheet">
        <!--google material icon-->
        <link href="https://fonts.googleapis.com/css2?family=Material+Icons"rel="stylesheet">
    </head>
    <body>
        <div class="wrapper">
            <div class="body-overlay"></div>
            <div id="content">
                <div class="main-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-wrapper">
                                <form action="updateCodeSale" method="post" id="codeSaleForm">

                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">Edit Code Sale</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            </div>
                                            <div>
                                                <h6 id="errorMessage" style="color: red; font-size: 15px; font-weight: bold; text-align: center; padding: 10px;"></h6>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label>ID</label>
                                                    <input value="${st.csid}" type="text" name="acid" class="form-control" readonly id="csid">
                                                </div>
                                                <div class="form-group">
                                                    <label>Mã Sale</label>    
                                                    <input value="${st.codeSale}" id="codeSale" type="text" name="codeSale" class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label>Giảm giá	</label>    
                                                    <input value="${st.discount}" id="discount" type="text" name="discount" class="form-control">
                                                </div>
                                                <div class="form-group" style="font-size: 15px; font-weight: bold;">
                                                    <label>Trạng thái</label><br>
                                                    <input value="1" type="radio" name="csStatus" ${st.csStatus == 1 ? "checked" : ""}> Active
                                                    &nbsp;&nbsp;&nbsp;<input value="0" type="radio" name="csStatus" ${st.csStatus == 0 ? "checked" : ""}> Suspended
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label>Ngày bắt đầu</label>    
                                                    <input value="${st.dateStart}" id="dateStart" type="date" name="dateStart" class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label>Ngày kết thúc</label>    
                                                    <input value="${st.dateEnd}" id="dateEnd" type="date" name="dateEnd" class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label>Số lượng</label>    
                                                    <input value="${st.limitedQuantity}" id="limitedQuantity" type="text" name="limitedQuantity" class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label>Điều kiện</label>    
                                                    <input value="${st.discountConditions}" id="discountConditions" type="text" name="discountConditions" class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label>Tiêu đề</label>    
                                                    <input value="${st.titile}" id="titile" type="text" name="titile" class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label>Số lượng đã sử dụng</label>    
                                                    <input value="${st.quantityUsed}" id="quantityUsed" type="text" name="quantityUsed" class="form-control">
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <a href="codeSaleList" class="btn btn-default" data-dismiss="modal">Cancel</a>

                                                <input type="submit" class="btn btn-info" value="Save">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="js/jquery-3.3.1.slim.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery-3.3.1.min.js"></script>
        <script type="text/javascript">
            document.addEventListener("DOMContentLoaded", function () {
                document.getElementById("codeSaleForm").addEventListener("submit", function (event) {
                    var codeSale = document.getElementById("codeSale").value;
                    var discount = document.getElementById("discount").value;
                    var dateStart = document.getElementById("dateStart").value;
                    var dateEnd = document.getElementById("dateEnd").value;
                    var limitedQuantity = document.getElementById("limitedQuantity").value;
                    var discountConditions = document.getElementById("discountConditions").value;
                    var titile = document.getElementById("titile").value;
                    var quantityUsed = document.getElementById("quantityUsed").value;

                    var errorMessage = document.getElementById("errorMessage");

                    // Kiểm tra điều kiện validation ở đây
                    if (codeSale.trim() === "" || discount.trim() === "" || dateStart.trim() === "" || dateEnd.trim() === "" || limitedQuantity.trim() === "" || discountConditions.trim() === "" || titile.trim() === "" || quantityUsed.trim() === "") {
                        errorMessage.textContent = "Vui lòng điền đầy đủ thông tin.";
                        event.preventDefault();
                        return;
                    }

                    if (isNaN(discount) || discount <= 0) {
                        errorMessage.textContent = "Discount phải là một số dương.";
                        event.preventDefault();
                        return;
                    }

                    if (!isValidDate(dateStart) || !isValidDate(dateEnd)) {
                        errorMessage.textContent = "Ngày phải có định dạng ngày tháng hợp lệ (VD: YYYY-MM-DD).";
                        event.preventDefault();
                        return;
                    }

                    if (!isPositiveInteger(limitedQuantity) || !isPositiveInteger(discountConditions) || !isPositiveInteger(quantityUsed)) {
                        errorMessage.textContent = "Các trường phải là số nguyên dương.";
                        event.preventDefault();
                        return;
                    }
                    if (new Date(dateStart) > new Date(dateEnd)) {
                        alert("Ngày bắt đầu không được lớn hơn ngày kết thúc.");
                        event.preventDefault();
                        return;
                    }
                    if (codeSale.length < 6 || codeSale.length > 12) {
                        alert("Mã giảm giá phải có từ 6 đến 12 ký tự.");
                        event.preventDefault();
                        return;
                    }


                    // Nếu không có lỗi, xóa thông báo lỗi
                    errorMessage.textContent = "";
                });
            });


        </script>


        <script type="text/javascript">

            $(document).ready(function () {
                $(".xp-menubar").on('click', function () {
                    $('#sidebar').toggleClass('active');
                    $('#content').toggleClass('active');
                });

                $(".xp-menubar,.body-overlay").on('click', function () {
                    $('#sidebar,.body-overlay').toggleClass('show-nav');
                });
            });
        </script>
    </body>
</html>



