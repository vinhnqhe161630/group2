<%-- 
    Document   : CustomerDetail
    Created on : Oct 15, 2023, 4:11:36 PM
    Author     : FPT
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
        <title>Board Game</title>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <!----css3---->
        <link rel="stylesheet" href="css/custom.css">
        <link rel="stylesheet" type="text/css" href="css/stylecuadat.css">


        <!--google fonts -->

        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600&display=swap" rel="stylesheet">

        <link rel="stylesheet" type="text/css" href="https://cdn0.fahasa.com/media/css_secure/180dfde691fce34b2e789c30f74feafe.css?q=102553" media="all">
        <link rel="stylesheet" type="text/css" href="https://cdn0.fahasa.com/media/css_secure/5722a920567ae30e71034727845e9254.css?q=102553" media="print">



        <!--google material icon-->
        <link href="https://fonts.googleapis.com/css2?family=Material+Icons"rel="stylesheet">

    </head>
    <body>


        <div class="wrapper">


            <div class="body-overlay"></div>

            <!-------------------------sidebar------------>
            <!-- Sidebar  -->
            <nav id="sidebar">
                <div class="sidebar-header">
                    <h3><img src="images/favicon.png" class="img-fluid"/><span>Board Game</span></h3>
                </div>
                <ul class="list-unstyled components">
                    <li  >
                    <a href="saledashboard" class="dashboard"><i class="material-icons">dashboard</i>
                        <span>Thống kê</span></a>
                </li>
                 <li class="active">
                    <a href="orderList" class="dashboard"><i class="material-icons">dashboard</i>
                        <span>Xử lí đơn hàng</span></a>
                </li>
                <li>
                    <a href="manageproduct" class="dashboard"><i class="material-icons">dashboard</i>
                        <span>Quản lí sản phẩm</span></a>
                </li>

                <li>
                    <a href="addproduct" class="dashboard"><i class="material-icons">dashboard</i>
                        <span>Thêm sản phẩm mới</span></a>
                </li>
                  <li>
                    <a href="home" class="dashboard"><i class="material-icons">dashboard</i>
                        <span>Về trang chủ</span></a>
                </li>


                </ul>


            </nav>




            <!--------page-content---------------->

            <div id="content">

                <!--top--navbar----design--------->

                <div class="top-navbar">
                    <div class="xp-topbar">

                        <!-- Start XP Row -->
                        <div class="row"> 
                            <!-- Start XP Col -->
                            <div class="col-2 col-md-1 col-lg-1 order-2 order-md-1 align-self-center">
                                <div class="xp-menubar">
                                    <span class="material-icons text-white">signal_cellular_alt
                                    </span>
                                </div>
                            </div> 
                            <!-- End XP Col -->

                            <!-- Start XP Col -->
                            <div class="col-md-5 col-lg-3 order-3 order-md-2">
                                <div class="xp-searchbar">
                                    
                                </div>
                            </div>
                            <!-- End XP Col -->

                            <!-- Start XP Col -->
                            <div class="col-10 col-md-6 col-lg-8 order-1 order-md-3">
                                <div class="xp-profilebar text-right">
                                    

                                </div>
                            </div>
                            <!-- End XP Col -->

                        </div> 
                        <!-- End XP Row -->

                    </div>
                    <div class="xp-breadcrumbbar text-center">
                        <h4 class="page-title">Customer Detail</h4>  
                                       
                    </div>

                </div>



                <!--------main-content------------->

                <div class="main-content">
                    <div class="row">

                        <div class="col-md-12">
                            <div class="table-wrapper">
                                
                                
                                <table class="table table-striped table-hover">
                                    
                                    <tr>

                                        <th style="width: 20%">Tên người dùng</th>
                                        <th>Email</th>
                                        <th>Địa chỉ giao hàng</th>
                                        <th>Thời gian đăng kí </th>
                                        <th>Trạng thái</th>


                                    </tr>


                                    <tr>
                                        <td>${a.username}</td>
                                        <td>${a.email}</td>
                                        <td>${sh.address}<br>${sh.phonenumber}</td>
                                        <td>${a.created_at}</td>

                                        <td style="font-size: 14px"><c:if test="${a.status == 1}">
                                                Hoạt động
                                            </c:if>
                                            <c:if test="${a.status == 0}">
                                                Bị khóa
                                            </c:if></td>


                                    </tr>




                                </table>

                                        <div style="margin: auto; padding: auto; background: #FFC107; text-align: center">
                                    <h2>Đơn Hàng</h2>
                                </div>
                                <div >
                                    <div >
                                        <div class="swiper-wrapper" > 
                                        
     

    <div style="width: 16%" class="tab-history-item swiper-slide ${status == null ? 'tab-history-item-active' : ''} swiper-slide-active" onclick="postToCustomerDetail(null, null, '${a.acid}');" style="width: 150px;">
        <div class="tab-history-item-border-left"></div>
        <div class="tab-history-item-number">${all}</div>
        <div class="tab-history-item-text">Tất Cả</div>
        <div class="tab-history-item-border"></div>
    </div>

    <div style="width: 16%" class="tab-history-item swiper-slide ${status == '0' ? 'tab-history-item-active' : ''}" onclick="postToCustomerDetail(null, '0', '${a.acid}');" style="width: 150px;">
        <div class="tab-history-item-number">${wait}</div>
        <div class="tab-history-item-text">Chờ xác nhận</div>
        <div class="tab-history-item-border"></div>
    </div>

    <div style="width: 16%" class="tab-history-item swiper-slide ${status == '1' ? 'tab-history-item-active' : ''}" onclick="postToCustomerDetail(null, '1', '${a.acid}');" style="width: 150px;">
        <div class="tab-history-item-number">${shipping}</div>
        <div class="tab-history-item-text">Đang chuyển hàng</div>
        <div class="tab-history-item-border"></div>
    </div>

    <div style="width: 16%" class="tab-history-item swiper-slide ${status == '2' ? 'tab-history-item-active' : ''}" onclick="postToCustomerDetail(null, '2', '${a.acid}');" style="width: 150px;">
        <div class="tab-history-item-number">${receive}</div>
        <div class="tab-history-item-text">Đã nhận hàng</div>
        <div class="tab-history-item-border"></div>
    </div>

    <div style="width: 16%" class="tab-history-item swiper-slide ${status == '3' ? 'tab-history-item-active' : ''}" onclick="postToCustomerDetail(null, '3', '${a.acid}');" style="width: 150px;">
        <div class="tab-history-item-number">${cancel}</div>
        <div class="tab-history-item-text">Bị hủy</div>
        <div class="tab-history-item-border"></div>
    </div>

                                        </div>
                                        <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span></div>
                                   
                                </div>        
                                <table class="table table-striped table-hover">

                                    <tr>

                                        <th style="width: 15%">Mã đơn hàng</th>
                                        <th>Ngày mua</th>
                                        
                                        <th>tổng tiền</th>
                                        <th>Trạng thái</th>
                                        <th></th>
                                        


                                    </tr>

                                    <c:forEach items="${orderList}" var="o">
                                    <tr>
                                        <td>${o.oid}</td>
                                        <td>${o.ordered_at}</td>
                                        
                                        <td><fmt:formatNumber value="${o.totalAmount-o.discount}" pattern="#,##0" var="totalAmount"  />
                                                <div><span class="price">${totalAmount}</span>&nbsp;đ</div></td>

                                        <td style="font-size: 14px"><c:if test="${o.status==0}"> Chờ xác nhận</c:if> 
                                            <c:if test="${o.status==1}"> Đang chuyển hàng</c:if> 
                                            <c:if test="${o.status==2}"> Đã nhận hàng</c:if> 
                                            <c:if test="${o.status==3}">Đơn hàng bị hủy</c:if> 
                                        </td>
                                        <td style="font-size : 14px; color: orange"><div  onclick="postData('orderInfo', {oid: '${o.oid}'});">Xem chi tiết</div></td>
                                        <script>
    function postData(url, data) {
        // Create a form
        const form = document.createElement('form');
        form.method = 'post';
        form.action = url;

        // Add the data as hidden inputs to the form
        for (let key in data) {
            if (data.hasOwnProperty(key)) {
                const input = document.createElement('input');
                input.type = 'hidden';
                input.name = key;
                input.value = data[key];
                form.appendChild(input);
            }
        }

        // Append the form to the body and submit
        document.body.appendChild(form);
        form.submit();
    }
</script>
                                        
                                  


                                    </tr>
                                    </c:forEach>




                                </table>
        <div class="clearfix">

                                    <ul class="pagination">
                                        <c:if test="${page != null && page ne '1'}">
                                            <li class="page-item disabled">
                                                <a onclick="postToCustomerDetail(${page-1}, '${status}', '${a.acid}');">Previous</a>
                                            </li>
                                        </c:if>

                                        <c:forEach begin="1" end="${numberPage}" var="i">
                                            <c:choose>
                                                <c:when test="${page eq i}">
                                                    <li class="page-item active">
                                                        <a href="#" onclick="postToCustomerDetail(${i}, '${status}', '${a.acid}');" class="page-link">${i}</a>
                                                    </li>
                                                </c:when>
                                                <c:otherwise>
                                                    <li class="page-item">
                                                           <a href="#" onclick="postToCustomerDetail(${i}, '${status}', '${a.acid}');" class="page-link">${i}</a>
                                                    </li>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:forEach>




                                    </ul>
                                </div>

                                







                            </div>


                            <!---footer---->


                        </div>

                        <footer class="footer">
                            <div class="container-fluid">
                                <div class="footer-in">
                                    <p class="mb-0">&copy 2020 Vishweb design - All Rights Reserved.</p>
                                </div>
                            </div>
                        </footer>
                    </div>
                </div>


                <!----------html code compleate----------->









                <!-- Optional JavaScript -->
                <!-- jQuery first, then Popper.js, then Bootstrap JS -->
                <script src="js/jquery-3.3.1.slim.min.js"></script>
                <script src="js/popper.min.js"></script>
                <script src="js/bootstrap.min.js"></script>
                <script src="js/jquery-3.3.1.min.js"></script>


                <script type="text/javascript">

                    $(document).ready(function () {
                        $(".xp-menubar").on('click', function () {
                            $('#sidebar').toggleClass('active');
                            $('#content').toggleClass('active');
                        });

                        $(".xp-menubar,.body-overlay").on('click', function () {
                            $('#sidebar,.body-overlay').toggleClass('show-nav');
                        });

                    });

                </script>
                                                      <script>
        function postToCustomerDetail(page,status, id) {
            var form = document.createElement('form');
            form.method = 'post';
            form.action = 'customerDetail_2';

        var inputPage = document.createElement('input');
        inputPage.type = 'hidden';
        inputPage.name = 'page';
        inputPage.value = page;
        form.appendChild(inputPage);
            var inputStatus = document.createElement('input');
            inputStatus.type = 'hidden';
            inputStatus.name = 'status';
            inputStatus.value = status;
            form.appendChild(inputStatus);

            var inputId = document.createElement('input');
            inputId.type = 'hidden';
            inputId.name = 'id';
            inputId.value = id;
            form.appendChild(inputId);
            
           

            document.body.appendChild(form);

            form.submit();
        }
    </script>





                </body>

                </html>



