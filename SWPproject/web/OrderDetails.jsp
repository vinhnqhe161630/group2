<!doctype html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
        <title>Order List</title>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <!----css3---->
        <link rel="stylesheet" href="css/custom.css">


        <!--google fonts -->

        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600&display=swap" rel="stylesheet">


        <!--google material icon-->
        <link href="https://fonts.googleapis.com/css2?family=Material+Icons"rel="stylesheet">
          <link rel="stylesheet" type="text/css" href="https://cdn0.fahasa.com/media/css_secure/5cc046b695588987268a3c8c9699f3ac.css?q=102551" media="all">
                <link rel="stylesheet" type="text/css" href="https://cdn0.fahasa.com/media/css_secure/5722a920567ae30e71034727845e9254.css?q=102551" media="print">
                                       <link href="https://www.fahasa.com/blog/rss/index/store_id/1/" title="Blog" rel="alternate" type="application/rss+xml">
                        <link rel="stylesheet" type="text/css" href="https://cdn0.fahasa.com/skin/frontend/ma_vanese/fahasa/css/default.css?q=102551" media="all">

                            <link rel="stylesheet" type="text/css" href="https://cdn0.fahasa.com/skin/frontend/ma_vanese/fahasa/css/header.css?q=102551" media="all">
                                <link rel="stylesheet" type="text/css" href="https://cdn0.fahasa.com/skin/frontend/ma_vanese/fahasa/css/select2.min.css?q=102551" media="all">
    </head>
    <body>


        <div class="wrapper">


            <div class="body-overlay"></div>

            <!-------------------------sidebar------------>
            <!-- Sidebar  -->
            <nav id="sidebar">
                  <div class="sidebar-header">
                <a href="home"><h3><img src="images/favicon.png" class="img-fluid"/><span>Board Game</span></h3></a>
            </div>
            <ul class="list-unstyled components">

                <li >
                    <a href="saledashboard" class="dashboard"><i class="material-icons">dashboard</i>
                        <span>Thống kê</span></a>
                </li>
                 <li class="active">
                    <a href="orderList" class="dashboard"><i class="material-icons">dashboard</i>
                        <span>Xử lí đơn hàng</span></a>
                </li>
                <li>
                    <a href="manageproduct" class="dashboard"><i class="material-icons">dashboard</i>
                        <span>Quản lí sản phẩm</span></a>
                </li>

                <li>
                    <a href="addproduct" class="dashboard"><i class="material-icons">dashboard</i>
                        <span>Thêm sản phẩm mới</span></a>
                </li>
                  <li>
                    <a href="home" class="dashboard"><i class="material-icons">dashboard</i>
                        <span>Về trang chủ</span></a>
                </li>


               




            </ul>



            </nav>




            <!--------page-content---------------->

            <div id="content">

                <!--top--navbar----design--------->

                <div class="top-navbar">
                    <div class="xp-topbar">

                        <!-- Start XP Row -->
                        <div class="row"> 
                            <!-- Start XP Col -->
                            <div class="col-2 col-md-1 col-lg-1 order-2 order-md-1 align-self-center">
                                <div class="xp-menubar">
                                    <span class="material-icons text-white">signal_cellular_alt
                                    </span>
                                </div>
                            </div> 
                            <!-- End XP Col -->

                            <!-- Start XP Col -->
                            <div class="col-md-5 col-lg-3 order-3 order-md-2">
                                <div class="xp-searchbar">
                                    
                                </div>
                            </div>
                            <!-- End XP Col -->

                            <!-- Start XP Col -->
                            <div class="col-10 col-md-6 col-lg-8 order-1 order-md-3">
                                <div class="xp-profilebar text-right">
                                    <nav class="navbar p-0">
                                        <ul class="nav navbar-nav flex-row ml-auto">   
                                            <li class="dropdown nav-item active">
                                                <a href="#" class="nav-link" data-toggle="dropdown">
                                                    <span class="material-icons">notifications</span>
                                                    <span class="notification">4</span>
                                                </a>
                                                <ul class="dropdown-menu">


                                                </ul>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#">
                                                    <span class="material-icons">question_answer</span>

                                                </a>
                                            </li>
                                            <li class="nav-item dropdown">
                                                <a class="nav-link" href="#" data-toggle="dropdown">
                                                    <img src="img/user.jpg" style="width:40px; border-radius:50%;"/>
                                                    <span class="xp-user-live"></span>
                                                </a>
                                                <ul class="dropdown-menu small-menu">
                                                    <li>
                                                        <a href="#">
                                                            <span class="material-icons">
                                                                person_outline
                                                            </span>Profile

                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#"><span class="material-icons">
                                                                settings
                                                            </span>Settings</a>
                                                    </li>
                                                    <li>
                                                        <a href="#"><span class="material-icons">
                                                                logout</span>Logout</a>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>


                                    </nav>

                                </div>
                            </div>
                            <!-- End XP Col -->

                        </div> 
                        <!-- End XP Row -->

                    </div>
                    <div class="xp-breadcrumbbar text-center">
                        <h4 class="page-title">Chi tiết đơn hàng</h4>  
                                   
                    </div>

                </div>



                <!--------main-content------------->

                <div class="main-content">
                    <div class="row">

                        <div class="col-md-12">
                            <div class="table-wrapper">
                                <div>
                        


                                </div>

                                <div class="table-title">
                                    <div class="row">
                                        <div class="col-sm-6 p-0 d-flex justify-content-lg-start justify-content-center">
                                           
                                        </div>
                                      
                                    </div>
                                </div>
                                    <div class="my-account"><!--<meta name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover">-->

                                                                                    <div class="order-view-content-info">
                                                                                        <div class="order-view-title">Chi tiết đơn hàng</div><div class="order-view-id-mobile"><span>Mã đơn hàng: </span><span>${order.oid}</span></div>
                                                                                        <div>
                                                                                            <c:if test="${order.status==0}">
                                                                                            <div class="order-view-status" style="background:#FCDAB0;color:#F7941E;border-color:#FCDAB0;">Đơn hàng Chờ xác nhận</div></c:if>
                                                                                               <c:if test="${order.status==1}">
                                                                                            <div class="order-view-status" style="background:#FCDAB0;color:#F7941E;border-color:#FCDAB0;">Đang xử lí</div></c:if>
                                                                        <c:if test="${order.status==2}">
                                                                                            <div class="order-view-status" style="background:#FCDAB0;color:#F7941E;border-color:#FCDAB0;">Hoàn tất</div></c:if>
                                                                       
                                                                                            
                                                                                            <c:if test="${order.status==3}">
                                                                                          <div class="subOder-progress-bar" style="background:#F3B4AF;color:#A90000;border-color:#F3B4AF;">Đơn hàng Bị hủy</div></c:if>
                                                                                            
                                                                                            <div class="order-view-id"><span>Mã đơn hàng: </span><span>#${order.oid}</span></div>
                                                                                            <div class="order-view-date"><span>Ngày mua: </span><span>${order.ordered_at}</span></div>
                                                                                            <fmt:formatNumber value="${order.totalAmount}" pattern="#,##0" var="totalAmount" />
                                                                                            <fmt:formatNumber value="${order.discount}" pattern="#,##0" var="discount" />
                                                                                            <fmt:formatNumber value="${order.totalAmount-order.discount}" pattern="#,##0" var="total" />
                                                                                            <div class="order-view-total"><span>Tổng Tiền: </span><span><span class="price">${total}</span><span class="sym-totals">đ</span></span></div>

                                                                                            <div class="order-view-note"><span style="flex:1">Ghi chú: </span>
                                                                                                <c:if test="${order.note==null}">
                                                                                                     <span class="dont-have-info" style="flex:16">(Không có)</span>
                                                                                                </c:if>
                                                                                                      <c:if test="${order.note!=null}">
                                                                                                     <span class="dont-have-info" style="flex:16">${order.note}</span>
                                                                                                </c:if>
                                                                                                     
                                                                                               
                                                                                            </div>
                                                                                        </div>


                                                                                        <div style="text-align:center;" class="order-view-buttons-color">

                                                                                            <div class="order-view-buttons-color-child">
                                                                                               

                                                                                                <c:if test="${order.status==0}">
                                                                                           <a href="#" onclick="confirmOrder('${order.oid}')" class="link-reorder order-view-buy-again-btn">Xác nhận đơn hàng</a>

                                                                                              
                                                                                                    <a href="#" id="cancel-order" class="order-view-review-btn">Hủy đơn hàng</a>
                                                                                                    <form id="cancel-order-form" action="orderDetails" method="post">
                                                                                                        <input type="hidden" name="oid" value="${order.oid}">\
                                                                                                         <input type="hidden" name="type" value="cancel">
                                                                                                    </form>
                                                                                                    </c:if>
                                                                                                    <c:if test="${order.status==1}">
                                                                                                         <a href="#" onclick="completeOrder('${order.oid}')" class="link-reorder order-view-buy-again-btn">Hoàn tất đơn hàng</a>

                                                                                                    </c:if>
                                                                                                      <script>
 document.getElementById("cancel-order").addEventListener("click", function (event) {
    event.preventDefault(); // Ngăn chặn chuyển đến liên kết mặc định

     // Hiển thị hộp thoại cảnh báo
     var confirmation = confirm("Bạn có chắc chắn muốn hủy đơn hàng không?");

     // Nếu người dùng xác nhận thông báo, gửi biểu mẫu
      if (confirmation) {
          document.getElementById("cancel-order-form").submit();
            }
        });
        function confirmOrder(oid) {
        var form = document.createElement('form');
        form.method = 'post';
        form.action = 'orderDetails';

        var inputOid = document.createElement('input');
        inputOid.type = 'hidden';
        inputOid.name = 'oid';
        inputOid.value = oid;

        var inputType = document.createElement('input');
        inputType.type = 'hidden';
        inputType.name = 'type';
        inputType.value = 'confirm';

        form.appendChild(inputOid);
        form.appendChild(inputType);
        document.body.appendChild(form);
        form.submit();
    }
     function completeOrder(oid) {
        var form = document.createElement('form');
        form.method = 'post';
        form.action = 'orderDetails';

        var inputOid = document.createElement('input');
        inputOid.type = 'hidden';
        inputOid.name = 'oid';
        inputOid.value = oid;

        var inputType = document.createElement('input');
        inputType.type = 'hidden';
        inputType.name = 'type';
        inputType.value = 'complete';

        form.appendChild(inputOid);
        form.appendChild(inputType);
        document.body.appendChild(form);
        form.submit();
    }
                                                                                                    
                                                                                                </script>
                                                                                              
                                                                                                   
                                                                                              

                                                                                            </div>
                                                                                        </div>   


                                                                                    </div>


                                                                                    <div class="order-info-border-block"></div>
                                                                                    <div class="order-view-content-details">
                                                                                        <div class="order-view-content-box1">
                                                                                            <div class="order-view-box">
                                                                                                <div class="order-box-title">
                                                                                                    <div class="order-view-title">Thông tin người nhận</div>
                                                                                                </div>
                                                                                                <div class="order-box-info">
                                                                                                    <address>
                                                                                                 Người nhận:${order.receiver}<br>
                                                                                                  Email :${accCustomer.email}
                                                                                                   
                                                                                                        <br>

                                                                                                              Địa chỉ:  ${order.address}<br>
                                                                                                                Điện thoại: ${order.phonenumber}

                                                                                                                    </address>
                                                                                                                    </div>
                                                                                                                    </div>
                                                                                                                    <div class="order-view-box">
                                                                                                                        <div class="order-info-shipping-description">
                                                                                                                            <div class="order-box-title">
                                                                                                                                <div class="order-view-title">Phương thức vận chuyển</div>
                                                                                                                            </div>
                                                                                                                            <div class="order-box-info">
                                                                                                                                Giao hàng tiêu chuẩn                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="order-view-box">
                                                                                                                        <div class="order-box-title">
                                                                                                                            <div class="order-view-title">Phương thức thanh toán</div>
                                                                                                                        </div>

                                                                                                                        <div class="order-box-info">
                                                                                                                            <div class="order-box-info-historypayment">
                                                                                                                                <div><p><strong>Thanh toán bằng tiền mặt khi nhận hàng</strong></p>


                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                
                                                                                                                    </div>
                                                                                                                  
          <div class="order-view-status-container">
         <c:if test="${order.status==0}" >                  
         <div class="order-view-status-new-order">
            <div class="order-view-icon-container">
                <div style="height: 60px;width: 60px;align-self: center;"><div class="order-view-icon-img" style="background: url(https://cdn0.fahasa.com/skin/frontend/ma_vanese/fahasa/images/order/ico_donhangmoi_orange.svg) no-repeat center;border-color:#f7941f;"></div></div>
                <div class="order-view-icon-content"><p>Đơn hàng mới</p><p>${order.ordered_at}</p></div>
            </div>
           
        </div>
         </c:if>    
                                                                                                                    
              <c:if test="${order.status==1}" >   
       <div class="order-view-status-new-order">
            <div class="order-view-icon-container">
                <div style="height: 60px;width: 60px;align-self: center;"><div class="order-view-icon-img" style="background: url(https://cdn0.fahasa.com/skin/frontend/ma_vanese/fahasa/images/order/ico_donhangmoi_blue.svg) no-repeat center;border-color:#005FCC;"></div></div>
                <div class="order-view-icon-content"><p>Đơn hàng mới</p><p>${order.ordered_at}</p></div>
            </div>
             <div class="order-view-progress-bar" style="background:#005FCC;"></div>
        </div>
        <div class="order-view-status-new-order">
            <div class="order-view-icon-container">
                <div style="height: 60px;width: 60px;align-self: center;"><div class="order-view-icon-img" style="background: url(https://cdn0.fahasa.com/skin/frontend/ma_vanese/fahasa/images/order/ico_dangxuly_blue.svg) no-repeat center;border-color:#005FCC;"></div></div>
                <div class="order-view-icon-content"><p>Đang xử lý</p><p></p></div>
            </div>
         
        </div> 
       </c:if>    
        <c:if test="${order.status==2}" > <div class="order-view-status-new-order">
            <div class="order-view-icon-container">
                <div style="height: 60px;width: 60px;align-self: center;"><div class="order-view-icon-img" style="background: url(https://cdn0.fahasa.com/skin/frontend/ma_vanese/fahasa/images/order/ico_donhangmoi_green.svg) no-repeat center;border-color:#228B22;;"></div></div>
                <div class="order-view-icon-content"><p>Đơn hàng mới</p><p>${order.ordered_at}</p></div>
            </div>
            <div class="order-view-progress-bar" style="border-color:#228B22;"></div>
        </div>
        <div class="order-view-status-new-order">
            <div class="order-view-icon-container">
                <div style="height: 60px;width: 60px;align-self: center;"><div class="order-view-icon-img" style="background: url(https://cdn0.fahasa.com/skin/frontend/ma_vanese/fahasa/images/order/ico_dangxuly_green.svg) no-repeat center;border-color:#228B22;"></div></div>
                <div class="order-view-icon-content"><p>Đang xử lý</p><p></p></div>
            </div>
            <div class="order-view-progress-bar" style="background:#228B22;"></div>
        </div>
        <div class="order-view-status-new-order">
             <div class="order-view-icon-container">
                <div style="height: 60px;width: 60px;align-self: center;"><div class="order-view-icon-img" style="background: url(https://cdn0.fahasa.com/skin/frontend/ma_vanese/fahasa/images/order/ico_hoantat_green.svg) no-repeat center;border-color:#228B22;"></div></div>
                <div class="order-view-icon-content"><p>Hoàn tất</p><p>${order.enddate}</p></div>
            </div>
            
        </div>  </c:if>       
        <c:if test="${order.status==3}" >     <div class="order-view-status-new-order">
            <div class="order-view-icon-container">
                <div style="height: 60px;width: 60px;align-self: center;"><div class="order-view-icon-img" style="background: url(https://cdn0.fahasa.com/skin/frontend/ma_vanese/fahasa/images/order/ico_donhangmoi_red.svg) no-repeat center;border-color:#fa0001;"></div></div>
                <div class="order-view-icon-content"><p>Đơn hàng mới</p><p>${order.ordered_at}</p></div>
            </div>
             <div class="order-view-progress-bar" style="background:#fa0001;"></div>
        </div>
        <div class="order-view-status-new-order">
            <div class="order-view-icon-container">
                <div style="height: 60px;width: 60px;align-self: center;"><div class="order-view-icon-img" style="background: url(https://cdn0.fahasa.com/skin/frontend/ma_vanese/fahasa/images/order/ico_dangxuly_red.svg) no-repeat center;border-color:#fa0001;"></div></div>
                <div class="order-view-icon-content"><p>Đang xử lý</p><p></p></div>
            </div>
            <div class="order-view-progress-bar" style="background:#fa0001;"></div>
        </div>
        <div class="order-view-status-new-order">
            <div class="order-view-icon-container">
                <div style="height: 60px;width: 60px;align-self: center;"><div class="order-view-icon-img" style="background: url(https://cdn0.fahasa.com/skin/frontend/ma_vanese/fahasa/images/order/ico_huy_red.svg) no-repeat center;border-color:#fa0001;"></div></div>
                <div class="order-view-icon-content"><p>Bị hủy</p><p>${order.enddate}</p></div>
            </div>
            
        </div>   </c:if>      
 

                                                                                                                    </div>
                                                                                                                    <div class="order-details-items">

                                                                                                                        <div class="border-block-mobile-desktop"></div>
                                                                                                                        <div class="order-subOrder-container">
                                                                                                                            <div class="order-subOrder-items">
                                                                                                                             
                                                                                                                            </div>
                                                                                                                            <div class="order-subOrder-products id-list-products-103313930">
                                                                                                                                <div class="table-subOrder-container">
                                                                                                                                    <div class="table-subOrder-header-and-img">
                                                                                                                                        <div class="table-subOrder-title-product order-view-title">Sản phẩm</div>
                                                                                                                                        <div class="table-subOrder-row table-subOrder-header">
                                                                                                                                            <div class="table-subOrder-cell" style="width:100px">Hình ảnh</div>
                                                                                                                                            <div class="table-subOrder-cell">Tên sản phẩm</div>

                                                                                                                                            <div class="table-subOrder-cell">Giá bán</div>
                                                                                                                                            <div class="table-subOrder-cell">SL</div>
                                                                                                                                            <div class="table-subOrder-cell">Thành tiền</div>
                                                                                                                                        </div>
                                                                                                                                    </div>
                                                                                                                                    <c:forEach items="${list}" var="entry">


                                                                                                                                        <div class="table-subOrder-parent-img-and-cell">

                                                                                                                                            <div class="table-subOrder-row">
                                                                                                                                                <div class="table-subOrder-cell table-subOrder-img-web"><img src="images/${entry.getValue().img}"></div>
                                                                                                                                                <div class="table-subOrder-cell table-subOrder-name-product">
                                                                                                                                                    <div class="table-subOrder-name-tag-a">
                                                                                                                                                        <a href="productdetail?Pid=${entry.value.pid}" style="height: auto;">
                                                                                                                                                            ${entry.value.pname}    </a>
                                                                                                                                                    </div>
                                                                                                                                                </div>

                                                                                                                                                <div class="table-subOrder-cell">
                                                                                                                                                    <span class="table-subOrder-hidden-desktop">Giá bán:&nbsp;</span>
                                                                                                                                                    <span>
                                                                                                                                                        <!--<span class="price-incl-tax">-->
                                                                                                                                                        <span class="cart-price">

                                                                                                                                                            <div class="cart-orderHs-price">
                                                                                                                                                                
                                                                                                                                                                <fmt:formatNumber value="${entry.key.price}" pattern="#,##0" var="Price" />
                                                                                                                                                                
                                                                                                                                                                <div><span class="price">${Price}</span> đ</div>
                                                                                                                                                               
                                                                                                                                                            </div>
                                                                                                                                                            <!--</span>-->
                                                                                                                                                        </span>
                                                                                                                                                    </span> 
                                                                                                                                                </div>
                                                                                                                                                <div class="table-subOrder-cell">
                                                                                                                                                    <span class="table-subOrder-hidden-desktop">Số lượng:&nbsp;</span>
                                                                                                                                                    <span>
                                                                                                                                                        <strong>${entry.key.quantity}</strong><br>
                                                                                                                                                    </span> 
                                                                                                                                                </div>
                                                                                                                                                <div class="table-subOrder-cell">
                                                                                                                                                    <span class="table-subOrder-hidden-desktop">Thành tiền:&nbsp;</span>
                                                                                                                                                    <span>
                                                                                                                                                        <!--<span class="price-incl-tax">-->
                                                                                                                                                        <span class="cart-price">
                                                                                                                                                            <fmt:formatNumber value="${entry.key.price*entry.key.quantity}" pattern="#,##0" var="totalPrice" />
                                                                                                                                                            <span class="price">${totalPrice}</span> đ					    <!--</span>-->
                                                                                                                                                        </span>

                                                                                                                                                    </span> 
                                                                                                                                                </div>
                                                                                                                                            </div>
                                                                                                                                        </div>
                                                                                                                                    </c:forEach>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                            <div class="order-subOrder-total id-total-103313930">
                                                                                                                             
                                                                                                                                <div class="order-subOrder-total-desktop">
                                                                                                                                    <div>

                                                                                                                                        <p><span>Thành tiền: </span></p>
                                                                                                                                        <p><span>Giảm giá: </span></p>



<!--                                                                                                                                        <p><span>Phí vận chuyển: </span></p>-->

                                                                                                                                        <p><span>Tổng Số Tiền: </span></p>

                                                                                                                                    </div>
                                                                                                                                    <div>

                                                                                                                                        <p class="order-totals-price"><span class="price">${totalAmount}</span>&nbsp;<span class="sym-totals">đ</span></p>
                                                                                                                                        
                                                                                                                                        
                                                                                                                                        <p class="order-totals-price"><span class="price">${discount}</span>&nbsp;<span class="sym-totals">đ</span></p>






                                                                                                                                        <p class="order-totals-price"><span class="price">${total}</span>&nbsp;<span class="sym-totals">đ</span></p>

                                                                                                                                    </div>
                                                                                                                                </div> </div>

                                                                                                                        </div>


                                                                                                                    </div></div>       
                            
                               
                            </div>
                        </div>
                        <!-- Edit Modal HTML -->
                     
                        <!-- Edit Modal HTML -->
                     



              


                    </div>


                    <!---footer---->


                </div>

                <footer class="footer">
                 
                </footer>
            </div>
        </div>


        <!----------html code compleate----------->









        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="js/jquery-3.3.1.slim.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery-3.3.1.min.js"></script>


        <script type="text/javascript">

            $(document).ready(function () {
                $(".xp-menubar").on('click', function () {
                    $('#sidebar').toggleClass('active');
                    $('#content').toggleClass('active');
                });

                $(".xp-menubar,.body-overlay").on('click', function () {
                    $('#sidebar,.body-overlay').toggleClass('show-nav');
                });

            });
            

        </script>





    </body>

</html>


