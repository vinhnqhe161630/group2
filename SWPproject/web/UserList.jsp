F<%-- 
    Document   : UserList
    Created on : Sep 27, 2023, 2:17:35 AM
    Author     : FPT
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions"%>
<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
        <title>Board Game</title>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <!----css3---->
        <link rel="stylesheet" href="css/custom.css">


        <!--google fonts -->

        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600&display=swap" rel="stylesheet">


        <!--google material icon-->
        <link href="https://fonts.googleapis.com/css2?family=Material+Icons"rel="stylesheet">

    </head>
    <body>


        <div class="wrapper">


            <div class="body-overlay"></div>

            <!-------------------------sidebar------------>
            <!-- Sidebar  -->
            <%@include  file="Admin/LeftAdmin.jsp"%>




            <!--------page-content---------------->

            <div id="content">

                <!--top--navbar----design--------->

                <div class="top-navbar">
                    <div class="xp-topbar">

                        <!-- Start XP Row -->
                        <div class="row"> 
                            <!-- Start XP Col -->
                            <div class="col-2 col-md-1 col-lg-1 order-2 order-md-1 align-self-center">
                                <div class="xp-menubar">
                                    <span class="material-icons text-white">signal_cellular_alt
                                    </span>
                                </div>
                            </div> 
                            <!-- End XP Col -->

                            <!-- Start XP Col -->
                            <div class="col-md-5 col-lg-3 order-3 order-md-2">
                                <div class="xp-searchbar">
                                    <form action="userList" id="myForm">
                                        <div class="input-group">
                                            <input type="text" name="search" class="form-control" 
                                                   placeholder="Search" id="pw">
                                            <div class="input-group-append">
                                                <button class="btn" type="submit" id="button-addon2">GO</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!-- End XP Col -->

                            <!-- Start XP Col -->
                            <div class="col-10 col-md-6 col-lg-8 order-1 order-md-3">
                                <div class="xp-profilebar text-right">
                                    <nav class="navbar p-0">
                                        <ul class="nav navbar-nav flex-row ml-auto">   
                                            <li class="dropdown nav-item active">
                                                <a href="#" class="nav-link" data-toggle="dropdown">
                                                    <span class="material-icons">notifications</span>
                                                    <span class="notification">4</span>
                                                </a>
                                                <ul class="dropdown-menu">
                                                    <li>
                                                        <a href="#">You have 5 new messages</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">You're now friend with Mike</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Wish Mary on her birthday!</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">5 warnings in Server Console</a>
                                                    </li>

                                                </ul>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#">
                                                    <span class="material-icons">question_answer</span>

                                                </a>
                                            </li>
                                            <li class="nav-item dropdown">
                                                <a class="nav-link" href="#" data-toggle="dropdown">
                                                    <img src="img/user.jpg" style="width:40px; border-radius:50%;"/>
                                                    <span class="xp-user-live"></span>
                                                </a>
                                                <ul class="dropdown-menu small-menu">
                                                    <li>
                                                        <a href="#">
                                                            <span class="material-icons">
                                                                person_outline
                                                            </span>Profile

                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#"><span class="material-icons">
                                                                settings
                                                            </span>Settings</a>
                                                    </li>
                                                    <li>
                                                        <a href="#"><span class="material-icons">
                                                                logout</span>Logout</a>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>


                                    </nav>

                                </div>
                            </div>
                            <!-- End XP Col -->

                        </div> 
                        <!-- End XP Row -->

                    </div>
                    <div class="xp-breadcrumbbar text-center">
                        <h4 class="page-title">Dashboard</h4>  
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Booster</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
                        </ol>                
                    </div>

                </div>

                <style>
                    .sortable {
                        cursor: pointer; /* Add a pointer cursor to indicate it's clickable */
                    }

                    .fa-sort, .fa-sort-asc, .fa-sort-desc {
                        /* Define your styling for the sorting icons here */
                        font-size: 16px; /* Adjust the icon size as needed */
                        color: #777; /* Default color for unsorted column */
                    }

                    .fa-sort-asc {
                        /* Styling for ascending sort icon */
                        color: #00C853; /* Change the color to represent ascending sort */
                    }

                    .fa-sort-desc {
                        /* Styling for descending sort icon */
                        color: #FF6F61; /* Change the color to represent descending sort */
                    }
                </style>

                <!--------main-content------------->

                <div class="main-content">
                    <div class="row">

                        <div class="col-md-12">
                            <div class="table-wrapper">
                                <div class="table-title">
                                    <div class="row">
                                        <div class="col-sm-6 p-0 d-flex justify-content-lg-start justify-content-center">
                                            <h2 class="ml-lg-2">Danh Sách Người Dùng</h2>


                                        </div>

                                        <div class="col-sm-6 p-0 d-flex justify-content-lg-end justify-content-center">
                                            <a href="#addEmployeeModal" class="btn btn-success" data-toggle="modal">
                                                <i class="material-icons">&#xE147;</i> <span>Thêm Tài Khoản Mới</span></a>
                                        </div>
                                    </div>
                                </div>
                                <table class="table table-striped table-hover">

                                    <tr style="color: #777; font-size: 35px; font-weight: bolder;  padding: 20px; ">
                                        <!--          <th>
                                                    <span class="custom-checkbox">
                                                                <input type="checkbox" id="selectAll">
                                                                <label for="selectAll"></label>
                                                    </span>
                                                </th>-->
                                        <th style="font-size:15px;font-weight:bolder;padding:10px;">ID</th>
                                        <th style="font-size:15px;font-weight:bolder;padding:10px;">Tên tài khoản</th>
                                        <th style="font-size:15px;font-weight:bolder;padding:10px;">Email</th>
                                        <th style="font-size:15px;font-weight:bolder;padding:10px;">Role<a href="userList?sort=role">&nbsp⬆⬇</a></th>
                                        <th class="sortable" style="font-size:15px;font-weight:bolder;padding:10px;">
                                            Ngày đăng ký
                                            <c:choose>

                                                <c:when test="${param.sort eq 'created_at' && param.order eq 'oldest'}"> <a href="userList?page=${param.page}&sort=newest">&nbsp⬆⬇</a>
                                                </c:when>

                                                <c:when test="${param.sort eq 'created_at' && param.order eq 'newest'}"> <a href="userList?page=${param.page}&sort=oldest">▲</a>
                                                </c:when>

                                                <c:otherwise> <a href="userList?page=${param.page}&sort=created_at&order=oldest">&nbsp⬆⬇</a>
                                                </c:otherwise>

                                            </c:choose>
                                        </th>


                                        <th style="font-size:15px;font-weight:bolder;padding:10px;">Trạng thái</th>
                                        <th style="font-size:15px;font-weight:bolder;padding:10px;"></th>
                                    </tr>
                                        <div style="text-align: center">${mess}</div>
                                    <c:if test="${acc.size()!=0}"> 
                                        
                                        <c:forEach items="${requestScope.acc}" var="c" begin="${param.page*8-8}" end="${param.page*8-1}"> 
                                            
                                            <tr>
                                                <td>${c.acid}</td>
                                                <td>
                                                    ${c.username}
                                                </td>
                                                <td>${c.email}</td>
                                                

                                                <td>
                                                    <c:if test="${c.role == 1}">
                                                        Admin
                                                    </c:if>
                                                    <c:if test="${c.role == 2}">
                                                        Sale
                                                    </c:if>
                                                    <c:if test="${c.role == 3}">
                                                        Maketing
                                                    </c:if>
                                                    <c:if test="${c.role == 4}">
                                                        User
                                                    </c:if>
                                                </td>

                                                <td>${c.created_at}</td>
                                                <td>
                                                    <c:if test="${c.status == 1}">
                                                       <span style="color: blue;">Hoạt động</span>
                                                    </c:if>
                                                    <c:if test="${c.status == 0}">
                                                        <span style="color: red;">Bị khóa</span>
                                                    </c:if>
                                                </td>
                                                <td>
                                                    <a href="update?sid=${c.acid}">
                                                        <i class="material-icons" data-toggle="tooltip" title="Edit"></i></a>

                                                </td>

                                            </tr>
                                        </c:forEach  ></c:if>
                                    </table>
                                    <div class="clearfix">
                                        <div class="hint-text">Showing ${fn:length(requestScope.acc)} Accounts</div>
                                    <% int i=1; %>

                                    <ul class="pagination">
                                        <c:if test="${param.page>1}">
                                            <li class="page-item "><a href="userList?page=${param.page-1}&sort=${param.sort}">Previous</a></li>
                                            </c:if>

                                        <c:forEach items="${requestScope.acc}" var="o" step="8" >
                                            <c:set var="i" value="<%=i%>"/>
                                            <li<c:if test="${param.page eq i}"> class="page-item active"</c:if>>
                                                <a href="userList?page=<%=i%>&sort=${param.sort}" class="page-link"><%=i%></a></li> 
                                                <% i++;%>
                                            </c:forEach>
                                            <c:if test="${fn:length(requestScope.acc)>=(param.page*8+1)}">
                                            <li class="page-item"><a href="userList?page=${param.page+1}&sort=${param.sort}" class="page-link">Next</a></li>
                                            </c:if>

                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- Edit Modal HTML -->

                        <div id="addEmployeeModal" class="modal fade">

                            <div class="modal-dialog">

                                <div class="modal-content">

                                    <form action="add" method="post" onsubmit="return validationForm();">

                                        <div class="modal-header">

                                            <h4 class="modal-title">Thêm Tài Khoản Mới</h4>



                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                                        </div>
                                        <div>
                                            <h6 id="errorMessage" style="color: red; font-size: 15px; font-weight: bold; text-align: center;  padding: 10px; ">
                                            </h6>
                                        </div>

                                        <div class="modal-body">

                                            <div class="form-group">
                                                <label>Email</label>    
                                                <input id="email" type="text" name="email" class="form-control">
                                            </div>

                                            <div class="form-group">
                                                <label>Mật khẩu</label>
                                                <input id="password" type="text" name="password" class="form-control">
                                            </div>

                                            <div class="form-group">
                                                <label>Tên tài khoản</label>
                                                <input id="username" type="text" name="username" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label>Role</label><br>
                                                <input type="radio" name="role" value="1"> Admin 
                                                &nbsp&nbsp <input type="radio" name="role" value="2"> Sale 
                                                &nbsp&nbsp <input type="radio" name="role" value="3"> Maketing
                                                &nbsp&nbsp <input type="radio" name="role" value="4"> User
                                            </div>
                                            <!-- code cho radio button role -->

                                        </div>

                                        <div class="modal-footer">
                                            <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                                            <input type="submit" class="btn btn-success" value="Add">
                                        </div>

                                    </form>

                                </div>

                            </div>

                        </div>
                        <script>
                            function validationForm() {

                                var email = document.getElementById("email").value;
                                var password = document.getElementById("password").value;
                                var username = document.getElementById("username").value;

                                var emailRegex = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;
                                var specialChars = /[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/;

                                if (email === "") {
                                    showError("Email không được để trống");
                                    return false;
                                }
                                if (!emailRegex.test(email)) {
                                    showError("Email không đúng định dạng\n\
                                                vd:user123@gmail.com");
                                    return false;
                                }
                                if (password.length < 6) {
                                    showError("Mật khẩu phải chứa ít nhất 6 ký tự");
                                    return false;
                                }
                                if (specialChars.test(password)) {
                                    showError("Mật khẩu không được chứa ký tự đặc biệt");
                                    return false;
                                }
                                if (username === "") {
                                    showError("Username không được để trống");
                                    return false;
                                }
                                if (specialChars.test(username)) {
                                    showError("Username không được chứa ký tự đặc biệt");
                                    return false;
                                }
                                if (username.length < 6) {
                                    showError("Username phải chứa ít nhất 6 ký tự");
                                    return false;
                                }
                                var roleRadio = document.querySelector('input[name="role"]:checked');
                                if (!roleRadio) {
                                    showError("Vui lòng chọn role");
                                    return false;
                                }
                                return true;
                            }
                            function showError(error) {
                                document.getElementById("errorMessage").innerHTML = error;
                            }
                        </script>


                    </div>

                </div>

                <footer class="footer">
                    <div class="container-fluid">
                        <div class="footer-in">
                            <p class="mb-0">&copy 2020 Vishweb design - All Rights Reserved.</p>
                        </div>
                    </div>
                </footer>
            </div>
        </div>


        <!----------html code compleate----------->





        <style>
            .material-icons {
                font-family: 'Material Icons';
                font-weight: normal;
                font-style: normal;
                font-size: 24px;
                line-height: 1;
                letter-spacing: normal;
                text-transform: none;
                display: inline-block;
                white-space: nowrap;
                word-wrap: normal;
                direction: ltr;
                -webkit-font-feature-settings: 'liga';
                -webkit-font-smoothing: antialiased;
            }
        </style>



        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="js/jquery-3.3.1.slim.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery-3.3.1.min.js"></script>


        <script type="text/javascript">

                            $(document).ready(function () {
                                $(".xp-menubar").on('click', function () {
                                    $('#sidebar').toggleClass('active');
                                    $('#content').toggleClass('active');
                                });

                                $(".xp-menubar,.body-overlay").on('click', function () {
                                    $('#sidebar,.body-overlay').toggleClass('show-nav');
                                });

                            });

        </script>







    </body>

</html>


