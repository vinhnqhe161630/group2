F<%-- 
    Document   : UserList
    Created on : Sep 27, 2023, 2:17:35 AM
    Author     : FPT
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions"%>
<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
        <title>Board Game</title>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <!----css3---->
        <link rel="stylesheet" href="css/custom.css">
        <!--google fonts -->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600&display=swap" rel="stylesheet">
        <!--google material icon-->
        <link href="https://fonts.googleapis.com/css2?family=Material+Icons"rel="stylesheet">
    </head>
    <body>
        <div class="wrapper">
            <div class="body-overlay"></div>
            <div id="content">
                <div class="main-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-wrapper">
                                <form action="update" method="post" onsubmit="return validationForm();">

                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">Sửa tài khoản</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            </div>
                                            <div>
                                                <h6 id="errorMessage" style="color: red; font-size: 15px; font-weight: bold; text-align: center; padding: 10px;"></h6>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label>ID</label>
                                                    <input value="${st.acid}" type="text" name="acid" class="form-control" readonly id="acid">
                                                </div>
                                                <div class="form-group">
                                                    <label>Email</label>
                                                    <input value="${st.email}" type="text" name="email" class="form-control" id="email">
                                                </div>
<!--                                                <div class="form-group">
                                                    <label>Password</label>
                                                    <input value="" type="text" name="password" class="form-control" readonly id="password">
                                                </div>-->
                                                <div class="form-group">
                                                    <label>Tên tài khoản</label>
                                                    <input value="${st.username}" type="text" name="username" class="form-control" id="username">
                                                </div>
                                                <div class="form-group" style="font-size: 15px; font-weight: bold;">
                                                    <label>Role</label><br>
                                                    <input type="radio" name="role" value="1" ${st.role == 1 ? "checked" : ""}> Admin
                                                    &nbsp;&nbsp;&nbsp;<input type="radio" name="role" value="2" ${st.role == 2 ? "checked" : ""}> Sale
                                                    &nbsp;&nbsp;&nbsp;<input type="radio" name="role" value="3" ${st.role == 3 ? "checked" : ""}> Maketing
                                                    &nbsp;&nbsp;&nbsp;<input type="radio" name="role" value="4" ${st.role == 4 ? "checked" : ""}> User
                                                </div>
                                                <div class="form-group" style="font-size: 15px; font-weight: bold;">
                                                    <label>Trạng thái</label><br>
                                                    <input value="1" type="radio" name="status" ${st.status == 1 ? "checked" : ""}> Hoạt động
                                                    &nbsp;&nbsp;&nbsp;<input value="0" type="radio" name="status" ${st.status == 0 ? "checked" : ""}> Bị khóa
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <a href="userList" class="btn btn-default" data-dismiss="modal">Cancel</a>

                                                <input type="submit" class="btn btn-info" value="Save">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            function validationForm() {
                var email = document.getElementById("email").value;
                var password = document.getElementById("password").value;
                var username = document.getElementById("username").value;
                var emailRegex = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;
                var specialChars = /[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/;
                if (email === "") {
                    showError("Email không được để trống");
                    return false;
                }
                if (!emailRegex.test(email)) {
                    showError("Email không đúng định dạng\nvd: user123@gmail.com");
                    return false;
                }
                if (password.length < 6) {
                    showError("Mật khẩu phải chứa ít nhất 6 ký tự");
                    return false;
                }

                if (specialChars.test(password)) {
                    showError("Mật khẩu không được chứa ký tự đặc biệt");
                    return false;
                }
                if (username === "") {
                    showError("Username không được để trống");
                    return false;
                }
                if (specialChars.test(username)) {
                    showError("Username không được chứa ký tự đặc biệt");
                    return false;
                }
                if (username.length < 6) {
                    showError("Username phải chứa ít nhất 6 ký tự");
                    return false;
                }
            }

            function showError(error) {
                document.getElementById("errorMessage").innerHTML = error;
            }
        </script>
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="js/jquery-3.3.1.slim.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery-3.3.1.min.js"></script>
        <script type="text/javascript">

            $(document).ready(function () {
                $(".xp-menubar").on('click', function () {
                    $('#sidebar').toggleClass('active');
                    $('#content').toggleClass('active');
                });

                $(".xp-menubar,.body-overlay").on('click', function () {
                    $('#sidebar,.body-overlay').toggleClass('show-nav');
                });
            });
        </script>
    </body>
</html>


